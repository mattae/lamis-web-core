import { Router } from '@angular/router';
import { JhiEventManager } from 'ng-jhipster';
import { LocalStorageService, SessionStorageService } from 'ngx-store';
import { Observable } from 'rxjs';
import { StateStorageService } from '../core/auth/state-storage.service';
import { LoginService } from './login.service';
export declare class LoginAuthenticationService {
    private router;
    private loginService;
    private $localStorage;
    private $sessionStorage;
    private eventManager;
    private stateStorageService;
    constructor(router: Router, loginService: LoginService, $localStorage: LocalStorageService, $sessionStorage: SessionStorageService, eventManager: JhiEventManager, stateStorageService: StateStorageService);
    setRedirect(value: any): void;
    isEcmLoggedIn(): boolean;
    isBpmLoggedIn(): boolean;
    isOauth(): boolean;
    getRedirect(): any;
    login(username: string, password: string, rememberMe?: boolean): Observable<any>;
}
