import { Observable } from 'rxjs';
import { AccountService } from '../core/auth/account.service';
import { AuthServerProvider } from '../core/auth/auth-jwt.service';
import { Router } from '@angular/router';
export declare class LoginService {
    private accountService;
    private authServerProvider;
    private router;
    constructor(accountService: AccountService, authServerProvider: AuthServerProvider, router: Router);
    login(credentials: any): Observable<Account | null>;
    logout(): void;
}
