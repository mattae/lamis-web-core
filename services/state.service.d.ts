import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IState } from '../shared/model/state.model';
import { ServerApiUrlConfig } from '../app.constants';
export declare class StateService {
    private http;
    private serverUrl;
    resourceUrl: string;
    constructor(http: HttpClient, serverUrl: ServerApiUrlConfig);
    find(id: number): Observable<HttpResponse<IState>>;
    getStates(): Observable<HttpResponse<IState[]>>;
}
