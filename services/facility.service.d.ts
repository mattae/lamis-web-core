import { HttpClient, HttpResponse } from '@angular/common/http';
import { ServerApiUrlConfig } from '../app.constants';
import { Observable } from 'rxjs';
import { Facility } from '../shared/model/facility.model';
declare type EntityResponseType = HttpResponse<Facility>;
declare type EntityArrayResponseType = HttpResponse<Facility[]>;
export declare class FacilityService {
    private http;
    private serverUrl;
    resourceUrl: string;
    constructor(http: HttpClient, serverUrl: ServerApiUrlConfig);
    create(facility: Facility): Observable<EntityResponseType>;
    update(facility: Facility): Observable<EntityResponseType>;
    delete(id: number): Observable<HttpResponse<any>>;
    find(id: number): Observable<HttpResponse<Facility>>;
    findByLga(id: number): Observable<EntityArrayResponseType>;
}
export {};
