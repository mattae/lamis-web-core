import { HttpClient, HttpResponse } from '@angular/common/http';
import { ServerApiUrlConfig } from '../app.constants';
import { Observable } from 'rxjs';
import { ILGA } from '../shared/model/lga.model';
export declare class LgaService {
    private http;
    private serverUrl;
    resourceUrl: string;
    constructor(http: HttpClient, serverUrl: ServerApiUrlConfig);
    find(id: number): Observable<HttpResponse<ILGA>>;
    findByState(id: number): Observable<HttpResponse<ILGA[]>>;
}
