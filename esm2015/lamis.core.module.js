var LamisCoreModule_1;
import * as tslib_1 from "tslib";
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { InjectableRxStompConfig, RxStompService, rxStompServiceFactory } from '@stomp/ng2-stompjs';
import { SERVER_API_URL_CONFIG } from './app.constants';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { AuthInterceptor } from './blocks/interceptor/auth.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { RxStompConfig } from './services/rx-stomp.config';
import { LamisSharedModule } from './shared/lamis-shared.module';
let LamisCoreModule = LamisCoreModule_1 = class LamisCoreModule {
    static forRoot(serverApiUrlConfig, dateTimeConfig) {
        // MomentDateFormat.DATE_FORMAT = dateTimeConfig.DATE_FORMAT;
        return {
            ngModule: LamisCoreModule_1,
            providers: [
                AuthExpiredInterceptor,
                AuthInterceptor,
                ErrorHandlerInterceptor,
                NotificationInterceptor,
                {
                    provide: SERVER_API_URL_CONFIG,
                    useValue: serverApiUrlConfig
                },
                {
                    provide: InjectableRxStompConfig,
                    useValue: RxStompConfig
                },
                {
                    provide: RxStompService,
                    useFactory: rxStompServiceFactory,
                    deps: [InjectableRxStompConfig]
                }
            ]
        };
    }
};
LamisCoreModule = LamisCoreModule_1 = tslib_1.__decorate([
    NgModule({
        declarations: [],
        imports: [
            CommonModule,
            LamisSharedModule
        ],
        exports: [
            LamisSharedModule
        ],
        providers: []
    })
], LamisCoreModule);
export { LamisCoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGFtaXMuY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJsYW1pcy5jb3JlLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFBLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQXVCLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5RCxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsY0FBYyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDcEcsT0FBTyxFQUFrQixxQkFBcUIsRUFBc0IsTUFBTSxpQkFBaUIsQ0FBQztBQUM1RixPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUN2RixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDeEUsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDeEYsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDeEYsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQzNELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBY2pFLElBQWEsZUFBZSx1QkFBNUIsTUFBYSxlQUFlO0lBQ3hCLE1BQU0sQ0FBQyxPQUFPLENBQUMsa0JBQXNDLEVBQUUsY0FBK0I7UUFDbEYsNkRBQTZEO1FBQzdELE9BQU87WUFDSCxRQUFRLEVBQUUsaUJBQWU7WUFDekIsU0FBUyxFQUFFO2dCQUNQLHNCQUFzQjtnQkFDdEIsZUFBZTtnQkFDZix1QkFBdUI7Z0JBQ3ZCLHVCQUF1QjtnQkFDdkI7b0JBQ0ksT0FBTyxFQUFFLHFCQUFxQjtvQkFDOUIsUUFBUSxFQUFFLGtCQUFrQjtpQkFDL0I7Z0JBQ0Q7b0JBQ0ksT0FBTyxFQUFFLHVCQUF1QjtvQkFDaEMsUUFBUSxFQUFFLGFBQWE7aUJBQzFCO2dCQUNEO29CQUNJLE9BQU8sRUFBRSxjQUFjO29CQUN2QixVQUFVLEVBQUUscUJBQXFCO29CQUNqQyxJQUFJLEVBQUUsQ0FBQyx1QkFBdUIsQ0FBQztpQkFDbEM7YUFDSjtTQUNKLENBQUM7SUFDTixDQUFDO0NBQ0osQ0FBQTtBQTFCWSxlQUFlO0lBWDNCLFFBQVEsQ0FBQztRQUNOLFlBQVksRUFBRSxFQUFFO1FBQ2hCLE9BQU8sRUFBRTtZQUNMLFlBQVk7WUFDWixpQkFBaUI7U0FDcEI7UUFDRCxPQUFPLEVBQUU7WUFDTCxpQkFBaUI7U0FDcEI7UUFDRCxTQUFTLEVBQUUsRUFBRTtLQUNoQixDQUFDO0dBQ1csZUFBZSxDQTBCM0I7U0ExQlksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IE1vZHVsZVdpdGhQcm92aWRlcnMsIE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEluamVjdGFibGVSeFN0b21wQ29uZmlnLCBSeFN0b21wU2VydmljZSwgcnhTdG9tcFNlcnZpY2VGYWN0b3J5IH0gZnJvbSAnQHN0b21wL25nMi1zdG9tcGpzJztcclxuaW1wb3J0IHsgRGF0ZVRpbWVDb25maWcsIFNFUlZFUl9BUElfVVJMX0NPTkZJRywgU2VydmVyQXBpVXJsQ29uZmlnIH0gZnJvbSAnLi9hcHAuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgQXV0aEV4cGlyZWRJbnRlcmNlcHRvciB9IGZyb20gJy4vYmxvY2tzL2ludGVyY2VwdG9yL2F1dGgtZXhwaXJlZC5pbnRlcmNlcHRvcic7XHJcbmltcG9ydCB7IEF1dGhJbnRlcmNlcHRvciB9IGZyb20gJy4vYmxvY2tzL2ludGVyY2VwdG9yL2F1dGguaW50ZXJjZXB0b3InO1xyXG5pbXBvcnQgeyBFcnJvckhhbmRsZXJJbnRlcmNlcHRvciB9IGZyb20gJy4vYmxvY2tzL2ludGVyY2VwdG9yL2Vycm9yaGFuZGxlci5pbnRlcmNlcHRvcic7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvbkludGVyY2VwdG9yIH0gZnJvbSAnLi9ibG9ja3MvaW50ZXJjZXB0b3Ivbm90aWZpY2F0aW9uLmludGVyY2VwdG9yJztcclxuaW1wb3J0IHsgUnhTdG9tcENvbmZpZyB9IGZyb20gJy4vc2VydmljZXMvcngtc3RvbXAuY29uZmlnJztcclxuaW1wb3J0IHsgTGFtaXNTaGFyZWRNb2R1bGUgfSBmcm9tICcuL3NoYXJlZC9sYW1pcy1zaGFyZWQubW9kdWxlJztcclxuXHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgZGVjbGFyYXRpb25zOiBbXSxcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgTGFtaXNTaGFyZWRNb2R1bGVcclxuICAgIF0sXHJcbiAgICBleHBvcnRzOiBbXHJcbiAgICAgICAgTGFtaXNTaGFyZWRNb2R1bGVcclxuICAgIF0sXHJcbiAgICBwcm92aWRlcnM6IFtdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMYW1pc0NvcmVNb2R1bGUge1xyXG4gICAgc3RhdGljIGZvclJvb3Qoc2VydmVyQXBpVXJsQ29uZmlnOiBTZXJ2ZXJBcGlVcmxDb25maWcsIGRhdGVUaW1lQ29uZmlnPzogRGF0ZVRpbWVDb25maWcpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgICAgICAvLyBNb21lbnREYXRlRm9ybWF0LkRBVEVfRk9STUFUID0gZGF0ZVRpbWVDb25maWcuREFURV9GT1JNQVQ7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgbmdNb2R1bGU6IExhbWlzQ29yZU1vZHVsZSxcclxuICAgICAgICAgICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAgICAgICAgICBBdXRoRXhwaXJlZEludGVyY2VwdG9yLFxyXG4gICAgICAgICAgICAgICAgQXV0aEludGVyY2VwdG9yLFxyXG4gICAgICAgICAgICAgICAgRXJyb3JIYW5kbGVySW50ZXJjZXB0b3IsXHJcbiAgICAgICAgICAgICAgICBOb3RpZmljYXRpb25JbnRlcmNlcHRvcixcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBwcm92aWRlOiBTRVJWRVJfQVBJX1VSTF9DT05GSUcsXHJcbiAgICAgICAgICAgICAgICAgICAgdXNlVmFsdWU6IHNlcnZlckFwaVVybENvbmZpZ1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBwcm92aWRlOiBJbmplY3RhYmxlUnhTdG9tcENvbmZpZyxcclxuICAgICAgICAgICAgICAgICAgICB1c2VWYWx1ZTogUnhTdG9tcENvbmZpZ1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBwcm92aWRlOiBSeFN0b21wU2VydmljZSxcclxuICAgICAgICAgICAgICAgICAgICB1c2VGYWN0b3J5OiByeFN0b21wU2VydmljZUZhY3RvcnksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVwczogW0luamVjdGFibGVSeFN0b21wQ29uZmlnXVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBdXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxufVxyXG4iXX0=