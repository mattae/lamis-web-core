import * as tslib_1 from "tslib";
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { SERVER_API_URL_CONFIG } from '../../app.constants';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../../app.constants";
let MenuService = class MenuService {
    constructor(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
    }
    getMenus() {
        return this.http.get(this.serverUrl.SERVER_API_URL + 'api/modules/menus', {});
    }
};
MenuService.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
MenuService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function MenuService_Factory() { return new MenuService(i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i2.SERVER_API_URL_CONFIG)); }, token: MenuService, providedIn: "root" });
MenuService = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    }),
    tslib_1.__param(1, Inject(SERVER_API_URL_CONFIG))
], MenuService);
export { MenuService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVudS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsiY29yZS9tZW51L21lbnUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxxQkFBcUIsRUFBc0IsTUFBTSxxQkFBcUIsQ0FBQzs7OztBQU1oRixJQUFhLFdBQVcsR0FBeEIsTUFBYSxXQUFXO0lBRXBCLFlBQW9CLElBQWdCLEVBQXlDLFNBQTZCO1FBQXRGLFNBQUksR0FBSixJQUFJLENBQVk7UUFBeUMsY0FBUyxHQUFULFNBQVMsQ0FBb0I7SUFDMUcsQ0FBQztJQUVNLFFBQVE7UUFDWCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFjLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxHQUFHLG1CQUFtQixFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQy9GLENBQUM7Q0FDSixDQUFBOztZQU42QixVQUFVOzRDQUFHLE1BQU0sU0FBQyxxQkFBcUI7OztBQUYxRCxXQUFXO0lBSHZCLFVBQVUsQ0FBQztRQUNSLFVBQVUsRUFBRSxNQUFNO0tBQ3JCLENBQUM7SUFHeUMsbUJBQUEsTUFBTSxDQUFDLHFCQUFxQixDQUFDLENBQUE7R0FGM0QsV0FBVyxDQVF2QjtTQVJZLFdBQVciLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU0VSVkVSX0FQSV9VUkxfQ09ORklHLCBTZXJ2ZXJBcGlVcmxDb25maWcgfSBmcm9tICcuLi8uLi9hcHAuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgSU1lbnVJdGVtIH0gZnJvbSAnLi4vLi4vc2hhcmVkL21vZGVsJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTWVudVNlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCwgQEluamVjdChTRVJWRVJfQVBJX1VSTF9DT05GSUcpIHByaXZhdGUgc2VydmVyVXJsOiBTZXJ2ZXJBcGlVcmxDb25maWcpIHtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgZ2V0TWVudXMoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQ8SU1lbnVJdGVtW10+KHRoaXMuc2VydmVyVXJsLlNFUlZFUl9BUElfVVJMICsgJ2FwaS9tb2R1bGVzL21lbnVzJywge30pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==