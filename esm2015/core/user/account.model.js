export class Account {
    constructor(activated, authorities, email, firstName, langKey, lastName, login, imageUrl) {
        this.activated = activated;
        this.authorities = authorities;
        this.email = email;
        this.firstName = firstName;
        this.langKey = langKey;
        this.lastName = lastName;
        this.login = login;
        this.imageUrl = imageUrl;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWNjb3VudC5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbImNvcmUvdXNlci9hY2NvdW50Lm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE1BQU0sT0FBTyxPQUFPO0lBQ2hCLFlBQ1csU0FBa0IsRUFDbEIsV0FBcUIsRUFDckIsS0FBYSxFQUNiLFNBQWlCLEVBQ2pCLE9BQWUsRUFDZixRQUFnQixFQUNoQixLQUFhLEVBQ2IsUUFBZ0I7UUFQaEIsY0FBUyxHQUFULFNBQVMsQ0FBUztRQUNsQixnQkFBVyxHQUFYLFdBQVcsQ0FBVTtRQUNyQixVQUFLLEdBQUwsS0FBSyxDQUFRO1FBQ2IsY0FBUyxHQUFULFNBQVMsQ0FBUTtRQUNqQixZQUFPLEdBQVAsT0FBTyxDQUFRO1FBQ2YsYUFBUSxHQUFSLFFBQVEsQ0FBUTtRQUNoQixVQUFLLEdBQUwsS0FBSyxDQUFRO1FBQ2IsYUFBUSxHQUFSLFFBQVEsQ0FBUTtJQUN4QixDQUFDO0NBQ1AiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgQWNjb3VudCB7XHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgYWN0aXZhdGVkOiBib29sZWFuLFxyXG4gICAgICAgIHB1YmxpYyBhdXRob3JpdGllczogc3RyaW5nW10sXHJcbiAgICAgICAgcHVibGljIGVtYWlsOiBzdHJpbmcsXHJcbiAgICAgICAgcHVibGljIGZpcnN0TmFtZTogc3RyaW5nLFxyXG4gICAgICAgIHB1YmxpYyBsYW5nS2V5OiBzdHJpbmcsXHJcbiAgICAgICAgcHVibGljIGxhc3ROYW1lOiBzdHJpbmcsXHJcbiAgICAgICAgcHVibGljIGxvZ2luOiBzdHJpbmcsXHJcbiAgICAgICAgcHVibGljIGltYWdlVXJsOiBzdHJpbmdcclxuICAgICkge31cclxufVxyXG4iXX0=