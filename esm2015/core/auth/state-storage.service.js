import * as tslib_1 from "tslib";
import { Injectable, Injector } from '@angular/core';
import { SessionStorageService } from 'ngx-store';
import * as i0 from "@angular/core";
let StateStorageService = class StateStorageService {
    constructor(injector) {
        this.injector = injector;
        this.$sessionStorage = injector.get(SessionStorageService);
    }
    getPreviousState() {
        return this.$sessionStorage.get('previousState');
    }
    resetPreviousState() {
        this.$sessionStorage.remove('previousState');
    }
    storePreviousState(previousStateName, previousStateParams) {
        const previousState = { name: previousStateName, params: previousStateParams };
        this.$sessionStorage.set('previousState', previousState);
    }
    getDestinationState() {
        return this.$sessionStorage.get('destinationState');
    }
    storeUrl(url) {
        this.$sessionStorage.set('previousUrl', url);
    }
    getUrl() {
        return this.$sessionStorage.get('previousUrl');
    }
    storeDestinationState(destinationState, destinationStateParams, fromState) {
        const destinationInfo = {
            destination: {
                name: destinationState.name,
                data: destinationState.data
            },
            params: destinationStateParams,
            from: {
                name: fromState.name
            }
        };
        this.$sessionStorage.set('destinationState', destinationInfo);
    }
};
StateStorageService.ctorParameters = () => [
    { type: Injector }
];
StateStorageService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function StateStorageService_Factory() { return new StateStorageService(i0.ɵɵinject(i0.INJECTOR)); }, token: StateStorageService, providedIn: "root" });
StateStorageService = tslib_1.__decorate([
    Injectable({ providedIn: 'root' })
], StateStorageService);
export { StateStorageService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGUtc3RvcmFnZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsiY29yZS9hdXRoL3N0YXRlLXN0b3JhZ2Uuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDckQsT0FBTyxFQUF1QixxQkFBcUIsRUFBRSxNQUFNLFdBQVcsQ0FBQzs7QUFHdkUsSUFBYSxtQkFBbUIsR0FBaEMsTUFBYSxtQkFBbUI7SUFHNUIsWUFBb0IsUUFBa0I7UUFBbEIsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQUNsQyxJQUFJLENBQUMsZUFBZSxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBRUQsZ0JBQWdCO1FBQ1osT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUNyRCxDQUFDO0lBRUQsa0JBQWtCO1FBQ2QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDakQsQ0FBQztJQUVELGtCQUFrQixDQUFDLGlCQUFzQixFQUFFLG1CQUF3QjtRQUMvRCxNQUFNLGFBQWEsR0FBRyxFQUFDLElBQUksRUFBRSxpQkFBaUIsRUFBRSxNQUFNLEVBQUUsbUJBQW1CLEVBQUMsQ0FBQztRQUM3RSxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsYUFBYSxDQUFDLENBQUM7SUFDN0QsQ0FBQztJQUVELG1CQUFtQjtRQUNmLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQztJQUN4RCxDQUFDO0lBRUQsUUFBUSxDQUFDLEdBQVc7UUFDaEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMsYUFBYSxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFFRCxNQUFNO1FBQ0YsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUNuRCxDQUFDO0lBRUQscUJBQXFCLENBQUMsZ0JBQXFCLEVBQUUsc0JBQTJCLEVBQUUsU0FBYztRQUNwRixNQUFNLGVBQWUsR0FBRztZQUNwQixXQUFXLEVBQUU7Z0JBQ1QsSUFBSSxFQUFFLGdCQUFnQixDQUFDLElBQUk7Z0JBQzNCLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxJQUFJO2FBQzlCO1lBQ0QsTUFBTSxFQUFFLHNCQUFzQjtZQUM5QixJQUFJLEVBQUU7Z0JBQ0YsSUFBSSxFQUFFLFNBQVMsQ0FBQyxJQUFJO2FBQ3ZCO1NBQ0osQ0FBQztRQUNGLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLGtCQUFrQixFQUFFLGVBQWUsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7Q0FDSixDQUFBOztZQTFDaUMsUUFBUTs7O0FBSDdCLG1CQUFtQjtJQUQvQixVQUFVLENBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDLENBQUM7R0FDcEIsbUJBQW1CLENBNkMvQjtTQTdDWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3RvciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBMb2NhbFN0b3JhZ2VTZXJ2aWNlLCBTZXNzaW9uU3RvcmFnZVNlcnZpY2UgfSBmcm9tICduZ3gtc3RvcmUnO1xyXG5cclxuQEluamVjdGFibGUoe3Byb3ZpZGVkSW46ICdyb290J30pXHJcbmV4cG9ydCBjbGFzcyBTdGF0ZVN0b3JhZ2VTZXJ2aWNlIHtcclxuICAgIHByaXZhdGUgJHNlc3Npb25TdG9yYWdlOiBTZXNzaW9uU3RvcmFnZVNlcnZpY2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBpbmplY3RvcjogSW5qZWN0b3IpIHtcclxuICAgICAgICB0aGlzLiRzZXNzaW9uU3RvcmFnZSA9IGluamVjdG9yLmdldChTZXNzaW9uU3RvcmFnZVNlcnZpY2UpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFByZXZpb3VzU3RhdGUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuJHNlc3Npb25TdG9yYWdlLmdldCgncHJldmlvdXNTdGF0ZScpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlc2V0UHJldmlvdXNTdGF0ZSgpIHtcclxuICAgICAgICB0aGlzLiRzZXNzaW9uU3RvcmFnZS5yZW1vdmUoJ3ByZXZpb3VzU3RhdGUnKTtcclxuICAgIH1cclxuXHJcbiAgICBzdG9yZVByZXZpb3VzU3RhdGUocHJldmlvdXNTdGF0ZU5hbWU6IGFueSwgcHJldmlvdXNTdGF0ZVBhcmFtczogYW55KSB7XHJcbiAgICAgICAgY29uc3QgcHJldmlvdXNTdGF0ZSA9IHtuYW1lOiBwcmV2aW91c1N0YXRlTmFtZSwgcGFyYW1zOiBwcmV2aW91c1N0YXRlUGFyYW1zfTtcclxuICAgICAgICB0aGlzLiRzZXNzaW9uU3RvcmFnZS5zZXQoJ3ByZXZpb3VzU3RhdGUnLCBwcmV2aW91c1N0YXRlKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXREZXN0aW5hdGlvblN0YXRlKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLiRzZXNzaW9uU3RvcmFnZS5nZXQoJ2Rlc3RpbmF0aW9uU3RhdGUnKTtcclxuICAgIH1cclxuXHJcbiAgICBzdG9yZVVybCh1cmw6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuJHNlc3Npb25TdG9yYWdlLnNldCgncHJldmlvdXNVcmwnLCB1cmwpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFVybCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy4kc2Vzc2lvblN0b3JhZ2UuZ2V0KCdwcmV2aW91c1VybCcpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0b3JlRGVzdGluYXRpb25TdGF0ZShkZXN0aW5hdGlvblN0YXRlOiBhbnksIGRlc3RpbmF0aW9uU3RhdGVQYXJhbXM6IGFueSwgZnJvbVN0YXRlOiBhbnkpIHtcclxuICAgICAgICBjb25zdCBkZXN0aW5hdGlvbkluZm8gPSB7XHJcbiAgICAgICAgICAgIGRlc3RpbmF0aW9uOiB7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiBkZXN0aW5hdGlvblN0YXRlLm5hbWUsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiBkZXN0aW5hdGlvblN0YXRlLmRhdGFcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgcGFyYW1zOiBkZXN0aW5hdGlvblN0YXRlUGFyYW1zLFxyXG4gICAgICAgICAgICBmcm9tOiB7XHJcbiAgICAgICAgICAgICAgICBuYW1lOiBmcm9tU3RhdGUubmFtZVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLiRzZXNzaW9uU3RvcmFnZS5zZXQoJ2Rlc3RpbmF0aW9uU3RhdGUnLCBkZXN0aW5hdGlvbkluZm8pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==