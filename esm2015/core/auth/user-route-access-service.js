import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AccountService } from './account.service';
import { StateStorageService } from './state-storage.service';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "./account.service";
import * as i3 from "./state-storage.service";
let UserRouteAccessService = class UserRouteAccessService {
    constructor(router, accountService, stateStorageService) {
        this.router = router;
        this.accountService = accountService;
        this.stateStorageService = stateStorageService;
    }
    canActivate(route, state) {
        const authorities = route.data['authorities'];
        // We need to call the checkLogin / and so the accountService.identity() function, to ensure,
        // that the client has a principal too, if they already logged in by the server.
        // This could happen on a page refresh.
        return this.checkLogin(authorities, state.url);
    }
    checkLogin(authorities, url) {
        return this.accountService.identity().then(account => {
            if (!authorities || authorities.length === 0) {
                return true;
            }
            if (account && account.login !== 'anonymoususer') {
                const hasAnyAuthority = this.accountService.hasAnyAuthority(authorities);
                if (hasAnyAuthority) {
                    return true;
                }
                this.router.navigate(['sessions/accessdenied']);
                return false;
            }
            this.stateStorageService.storeUrl(url);
            this.router.navigate(['sessions/login']);
            return false;
        });
    }
};
UserRouteAccessService.ctorParameters = () => [
    { type: Router },
    { type: AccountService },
    { type: StateStorageService }
];
UserRouteAccessService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function UserRouteAccessService_Factory() { return new UserRouteAccessService(i0.ɵɵinject(i1.Router), i0.ɵɵinject(i2.AccountService), i0.ɵɵinject(i3.StateStorageService)); }, token: UserRouteAccessService, providedIn: "root" });
UserRouteAccessService = tslib_1.__decorate([
    Injectable({ providedIn: 'root' })
], UserRouteAccessService);
export { UserRouteAccessService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci1yb3V0ZS1hY2Nlc3Mtc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbImNvcmUvYXV0aC91c2VyLXJvdXRlLWFjY2Vzcy1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxXQUFXLEVBQUUsTUFBTSxFQUFFLG1CQUFtQixFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFbkcsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlCQUF5QixDQUFDOzs7OztBQUc5RCxJQUFhLHNCQUFzQixHQUFuQyxNQUFhLHNCQUFzQjtJQUMvQixZQUNZLE1BQWMsRUFDZCxjQUE4QixFQUM5QixtQkFBd0M7UUFGeEMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO0lBQ2pELENBQUM7SUFFSixXQUFXLENBQUMsS0FBNkIsRUFBRSxLQUEwQjtRQUNqRSxNQUFNLFdBQVcsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzlDLDZGQUE2RjtRQUM3RixnRkFBZ0Y7UUFDaEYsdUNBQXVDO1FBQ3ZDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFRCxVQUFVLENBQUMsV0FBcUIsRUFBRSxHQUFXO1FBQ3pDLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUU7WUFDakQsSUFBSSxDQUFDLFdBQVcsSUFBSSxXQUFXLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtnQkFDMUMsT0FBTyxJQUFJLENBQUM7YUFDZjtZQUVELElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxLQUFLLEtBQUssZUFBZSxFQUFFO2dCQUM5QyxNQUFNLGVBQWUsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDekUsSUFBSSxlQUFlLEVBQUU7b0JBQ2pCLE9BQU8sSUFBSSxDQUFDO2lCQUNmO2dCQUNELElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsdUJBQXVCLENBQUMsQ0FBQyxDQUFDO2dCQUNoRCxPQUFPLEtBQUssQ0FBQzthQUNoQjtZQUVELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDdkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7WUFDekMsT0FBTyxLQUFLLENBQUM7UUFDakIsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0NBQ0osQ0FBQTs7WUFqQ3VCLE1BQU07WUFDRSxjQUFjO1lBQ1QsbUJBQW1COzs7QUFKM0Msc0JBQXNCO0lBRGxDLFVBQVUsQ0FBQyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsQ0FBQztHQUN0QixzQkFBc0IsQ0FtQ2xDO1NBbkNZLHNCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgQ2FuQWN0aXZhdGUsIFJvdXRlciwgUm91dGVyU3RhdGVTbmFwc2hvdCB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcblxyXG5pbXBvcnQgeyBBY2NvdW50U2VydmljZSB9IGZyb20gJy4vYWNjb3VudC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU3RhdGVTdG9yYWdlU2VydmljZSB9IGZyb20gJy4vc3RhdGUtc3RvcmFnZS5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKHsgcHJvdmlkZWRJbjogJ3Jvb3QnIH0pXHJcbmV4cG9ydCBjbGFzcyBVc2VyUm91dGVBY2Nlc3NTZXJ2aWNlIGltcGxlbWVudHMgQ2FuQWN0aXZhdGUge1xyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICBwcml2YXRlIGFjY291bnRTZXJ2aWNlOiBBY2NvdW50U2VydmljZSxcclxuICAgICAgICBwcml2YXRlIHN0YXRlU3RvcmFnZVNlcnZpY2U6IFN0YXRlU3RvcmFnZVNlcnZpY2VcclxuICAgICkge31cclxuXHJcbiAgICBjYW5BY3RpdmF0ZShyb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3QpOiBib29sZWFuIHwgUHJvbWlzZTxib29sZWFuPiB7XHJcbiAgICAgICAgY29uc3QgYXV0aG9yaXRpZXMgPSByb3V0ZS5kYXRhWydhdXRob3JpdGllcyddO1xyXG4gICAgICAgIC8vIFdlIG5lZWQgdG8gY2FsbCB0aGUgY2hlY2tMb2dpbiAvIGFuZCBzbyB0aGUgYWNjb3VudFNlcnZpY2UuaWRlbnRpdHkoKSBmdW5jdGlvbiwgdG8gZW5zdXJlLFxyXG4gICAgICAgIC8vIHRoYXQgdGhlIGNsaWVudCBoYXMgYSBwcmluY2lwYWwgdG9vLCBpZiB0aGV5IGFscmVhZHkgbG9nZ2VkIGluIGJ5IHRoZSBzZXJ2ZXIuXHJcbiAgICAgICAgLy8gVGhpcyBjb3VsZCBoYXBwZW4gb24gYSBwYWdlIHJlZnJlc2guXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY2hlY2tMb2dpbihhdXRob3JpdGllcywgc3RhdGUudXJsKTtcclxuICAgIH1cclxuXHJcbiAgICBjaGVja0xvZ2luKGF1dGhvcml0aWVzOiBzdHJpbmdbXSwgdXJsOiBzdHJpbmcpOiBQcm9taXNlPGJvb2xlYW4+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hY2NvdW50U2VydmljZS5pZGVudGl0eSgpLnRoZW4oYWNjb3VudCA9PiB7XHJcbiAgICAgICAgICAgIGlmICghYXV0aG9yaXRpZXMgfHwgYXV0aG9yaXRpZXMubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGFjY291bnQgJiYgYWNjb3VudC5sb2dpbiAhPT0gJ2Fub255bW91c3VzZXInKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBoYXNBbnlBdXRob3JpdHkgPSB0aGlzLmFjY291bnRTZXJ2aWNlLmhhc0FueUF1dGhvcml0eShhdXRob3JpdGllcyk7XHJcbiAgICAgICAgICAgICAgICBpZiAoaGFzQW55QXV0aG9yaXR5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJ3Nlc3Npb25zL2FjY2Vzc2RlbmllZCddKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5zdGF0ZVN0b3JhZ2VTZXJ2aWNlLnN0b3JlVXJsKHVybCk7XHJcbiAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnc2Vzc2lvbnMvbG9naW4nXSk7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=