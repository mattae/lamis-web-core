import * as tslib_1 from "tslib";
import { Inject, Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LocalStorageService, SessionStorageService } from 'ngx-store';
import { SERVER_API_URL_CONFIG } from '../../app.constants';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../../app.constants";
let AuthServerProvider = class AuthServerProvider {
    constructor(http, injector, serverUrl) {
        this.http = http;
        this.injector = injector;
        this.serverUrl = serverUrl;
        this.$localStorage = this.injector.get(LocalStorageService);
        this.$sessionStorage = this.injector.get(SessionStorageService);
    }
    getToken() {
        return this.$localStorage.get('authenticationToken') || this.$sessionStorage.get('authenticationToken');
    }
    getAuthorizationToken() {
        return 'Bearer ' + this.getToken();
    }
    login(credentials) {
        const data = {
            username: credentials.username,
            password: credentials.password,
            rememberMe: credentials.rememberMe
        };
        return this.http
            .post(this.serverUrl.SERVER_API_URL + 'api/authenticate', data)
            .pipe(map(response => this.authenticateSuccess(response, credentials.rememberMe)));
    }
    logout() {
        return new Observable(observer => {
            this.$localStorage.remove('authenticationToken');
            this.$sessionStorage.remove('authenticationToken');
            observer.complete();
        });
    }
    authenticateSuccess(response, rememberMe) {
        const jwt = response.id_token;
        if (rememberMe) {
            this.$localStorage.set('authenticationToken', jwt);
        }
        else {
            this.$sessionStorage.set('authenticationToken', jwt);
        }
    }
};
AuthServerProvider.ctorParameters = () => [
    { type: HttpClient },
    { type: Injector },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
AuthServerProvider.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AuthServerProvider_Factory() { return new AuthServerProvider(i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i0.INJECTOR), i0.ɵɵinject(i2.SERVER_API_URL_CONFIG)); }, token: AuthServerProvider, providedIn: "root" });
AuthServerProvider = tslib_1.__decorate([
    Injectable({ providedIn: 'root' }),
    tslib_1.__param(2, Inject(SERVER_API_URL_CONFIG))
], AuthServerProvider);
export { AuthServerProvider };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1qd3Quc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbImNvcmUvYXV0aC9hdXRoLWp3dC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDN0QsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDbEMsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3JDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxxQkFBcUIsRUFBRSxNQUFNLFdBQVcsQ0FBQztBQUN2RSxPQUFPLEVBQUUscUJBQXFCLEVBQXNCLE1BQU0scUJBQXFCLENBQUM7Ozs7QUFHaEYsSUFBYSxrQkFBa0IsR0FBL0IsTUFBYSxrQkFBa0I7SUFJM0IsWUFBb0IsSUFBZ0IsRUFDaEIsUUFBa0IsRUFDYSxTQUE2QjtRQUY1RCxTQUFJLEdBQUosSUFBSSxDQUFZO1FBQ2hCLGFBQVEsR0FBUixRQUFRLENBQVU7UUFDYSxjQUFTLEdBQVQsU0FBUyxDQUFvQjtRQUM1RSxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO0lBQ3BFLENBQUM7SUFFRCxRQUFRO1FBQ0osT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUM7SUFDNUcsQ0FBQztJQUVELHFCQUFxQjtRQUNqQixPQUFPLFNBQVMsR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDdkMsQ0FBQztJQUVELEtBQUssQ0FBQyxXQUFnQjtRQUNsQixNQUFNLElBQUksR0FBRztZQUNULFFBQVEsRUFBRSxXQUFXLENBQUMsUUFBUTtZQUM5QixRQUFRLEVBQUUsV0FBVyxDQUFDLFFBQVE7WUFDOUIsVUFBVSxFQUFFLFdBQVcsQ0FBQyxVQUFVO1NBQ3JDLENBQUM7UUFDRixPQUFPLElBQUksQ0FBQyxJQUFJO2FBQ1gsSUFBSSxDQUFNLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxHQUFHLGtCQUFrQixFQUFFLElBQUksQ0FBQzthQUNuRSxJQUFJLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsRUFBRSxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzNGLENBQUM7SUFFRCxNQUFNO1FBQ0YsT0FBTyxJQUFJLFVBQVUsQ0FBQyxRQUFRLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQ2pELElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDbkQsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3hCLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVPLG1CQUFtQixDQUFDLFFBQWEsRUFBRSxVQUFtQjtRQUMxRCxNQUFNLEdBQUcsR0FBRyxRQUFRLENBQUMsUUFBUSxDQUFDO1FBQzlCLElBQUksVUFBVSxFQUFFO1lBQ1osSUFBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUUsR0FBRyxDQUFDLENBQUM7U0FDdEQ7YUFBTTtZQUNILElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLHFCQUFxQixFQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQ3hEO0lBQ0wsQ0FBQztDQUNKLENBQUE7O1lBMUM2QixVQUFVO1lBQ04sUUFBUTs0Q0FDekIsTUFBTSxTQUFDLHFCQUFxQjs7O0FBTmhDLGtCQUFrQjtJQUQ5QixVQUFVLENBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDLENBQUM7SUFPaEIsbUJBQUEsTUFBTSxDQUFDLHFCQUFxQixDQUFDLENBQUE7R0FOakMsa0JBQWtCLENBOEM5QjtTQTlDWSxrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUsIEluamVjdG9yIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBMb2NhbFN0b3JhZ2VTZXJ2aWNlLCBTZXNzaW9uU3RvcmFnZVNlcnZpY2UgfSBmcm9tICduZ3gtc3RvcmUnO1xyXG5pbXBvcnQgeyBTRVJWRVJfQVBJX1VSTF9DT05GSUcsIFNlcnZlckFwaVVybENvbmZpZyB9IGZyb20gJy4uLy4uL2FwcC5jb25zdGFudHMnO1xyXG5cclxuQEluamVjdGFibGUoe3Byb3ZpZGVkSW46ICdyb290J30pXHJcbmV4cG9ydCBjbGFzcyBBdXRoU2VydmVyUHJvdmlkZXIge1xyXG4gICAgcHJpdmF0ZSAkbG9jYWxTdG9yYWdlOiBMb2NhbFN0b3JhZ2VTZXJ2aWNlO1xyXG4gICAgcHJpdmF0ZSAkc2Vzc2lvblN0b3JhZ2U6IFNlc3Npb25TdG9yYWdlU2VydmljZTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGluamVjdG9yOiBJbmplY3RvcixcclxuICAgICAgICAgICAgICAgIEBJbmplY3QoU0VSVkVSX0FQSV9VUkxfQ09ORklHKSBwcml2YXRlIHNlcnZlclVybDogU2VydmVyQXBpVXJsQ29uZmlnKSB7XHJcbiAgICAgICAgdGhpcy4kbG9jYWxTdG9yYWdlID0gdGhpcy5pbmplY3Rvci5nZXQoTG9jYWxTdG9yYWdlU2VydmljZSk7XHJcbiAgICAgICAgdGhpcy4kc2Vzc2lvblN0b3JhZ2UgPSB0aGlzLmluamVjdG9yLmdldChTZXNzaW9uU3RvcmFnZVNlcnZpY2UpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFRva2VuKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLiRsb2NhbFN0b3JhZ2UuZ2V0KCdhdXRoZW50aWNhdGlvblRva2VuJykgfHwgdGhpcy4kc2Vzc2lvblN0b3JhZ2UuZ2V0KCdhdXRoZW50aWNhdGlvblRva2VuJyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QXV0aG9yaXphdGlvblRva2VuKCkge1xyXG4gICAgICAgIHJldHVybiAnQmVhcmVyICcgKyB0aGlzLmdldFRva2VuKCk7XHJcbiAgICB9XHJcblxyXG4gICAgbG9naW4oY3JlZGVudGlhbHM6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgZGF0YSA9IHtcclxuICAgICAgICAgICAgdXNlcm5hbWU6IGNyZWRlbnRpYWxzLnVzZXJuYW1lLFxyXG4gICAgICAgICAgICBwYXNzd29yZDogY3JlZGVudGlhbHMucGFzc3dvcmQsXHJcbiAgICAgICAgICAgIHJlbWVtYmVyTWU6IGNyZWRlbnRpYWxzLnJlbWVtYmVyTWVcclxuICAgICAgICB9O1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHBcclxuICAgICAgICAgICAgLnBvc3Q8YW55Pih0aGlzLnNlcnZlclVybC5TRVJWRVJfQVBJX1VSTCArICdhcGkvYXV0aGVudGljYXRlJywgZGF0YSlcclxuICAgICAgICAgICAgLnBpcGUobWFwKHJlc3BvbnNlID0+IHRoaXMuYXV0aGVudGljYXRlU3VjY2VzcyhyZXNwb25zZSwgY3JlZGVudGlhbHMucmVtZW1iZXJNZSkpKTtcclxuICAgIH1cclxuXHJcbiAgICBsb2dvdXQoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUob2JzZXJ2ZXIgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLiRsb2NhbFN0b3JhZ2UucmVtb3ZlKCdhdXRoZW50aWNhdGlvblRva2VuJyk7XHJcbiAgICAgICAgICAgIHRoaXMuJHNlc3Npb25TdG9yYWdlLnJlbW92ZSgnYXV0aGVudGljYXRpb25Ub2tlbicpO1xyXG4gICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgYXV0aGVudGljYXRlU3VjY2VzcyhyZXNwb25zZTogYW55LCByZW1lbWJlck1lOiBib29sZWFuKTogdm9pZCB7XHJcbiAgICAgICAgY29uc3Qgand0ID0gcmVzcG9uc2UuaWRfdG9rZW47XHJcbiAgICAgICAgaWYgKHJlbWVtYmVyTWUpIHtcclxuICAgICAgICAgICAgdGhpcy4kbG9jYWxTdG9yYWdlLnNldCgnYXV0aGVudGljYXRpb25Ub2tlbicsIGp3dCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy4kc2Vzc2lvblN0b3JhZ2Uuc2V0KCdhdXRoZW50aWNhdGlvblRva2VuJywgand0KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19