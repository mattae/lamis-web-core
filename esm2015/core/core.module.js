import * as tslib_1 from "tslib";
import { LOCALE_ID, NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Title } from '@angular/platform-browser';
let CoreModule = class CoreModule {
    constructor() {
        //registerLocaleData(locale);
    }
};
CoreModule = tslib_1.__decorate([
    NgModule({
        imports: [HttpClientModule],
        exports: [],
        declarations: [],
        providers: [
            Title,
            {
                provide: LOCALE_ID,
                useValue: 'en'
            },
            DatePipe
        ]
    })
], CoreModule);
export { CoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJjb3JlL2NvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNwRCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDM0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBZWxELElBQWEsVUFBVSxHQUF2QixNQUFhLFVBQVU7SUFDbkI7UUFDSSw2QkFBNkI7SUFDakMsQ0FBQztDQUNKLENBQUE7QUFKWSxVQUFVO0lBYnRCLFFBQVEsQ0FBQztRQUNOLE9BQU8sRUFBRSxDQUFDLGdCQUFnQixDQUFDO1FBQzNCLE9BQU8sRUFBRSxFQUFFO1FBQ1gsWUFBWSxFQUFFLEVBQUU7UUFDaEIsU0FBUyxFQUFFO1lBQ1AsS0FBSztZQUNMO2dCQUNJLE9BQU8sRUFBRSxTQUFTO2dCQUNsQixRQUFRLEVBQUUsSUFBSTthQUNqQjtZQUNELFFBQVE7U0FDWDtLQUNKLENBQUM7R0FDVyxVQUFVLENBSXRCO1NBSlksVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IExPQ0FMRV9JRCwgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRGF0ZVBpcGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBUaXRsZSB9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtIdHRwQ2xpZW50TW9kdWxlXSxcclxuICAgIGV4cG9ydHM6IFtdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbXSxcclxuICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgIFRpdGxlLFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgcHJvdmlkZTogTE9DQUxFX0lELFxyXG4gICAgICAgICAgICB1c2VWYWx1ZTogJ2VuJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgRGF0ZVBpcGVcclxuICAgIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIENvcmVNb2R1bGUge1xyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgLy9yZWdpc3RlckxvY2FsZURhdGEobG9jYWxlKTtcclxuICAgIH1cclxufVxyXG4iXX0=