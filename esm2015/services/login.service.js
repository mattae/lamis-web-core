import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { flatMap } from 'rxjs/operators';
import { AccountService } from '../core/auth/account.service';
import { AuthServerProvider } from '../core/auth/auth-jwt.service';
import { Router } from '@angular/router';
import * as i0 from "@angular/core";
import * as i1 from "../core/auth/account.service";
import * as i2 from "../core/auth/auth-jwt.service";
import * as i3 from "@angular/router";
let LoginService = class LoginService {
    constructor(accountService, authServerProvider, router) {
        this.accountService = accountService;
        this.authServerProvider = authServerProvider;
        this.router = router;
    }
    login(credentials) {
        return this.authServerProvider.login(credentials).pipe(flatMap(() => this.accountService.identity(true)));
    }
    logout() {
        this.authServerProvider.logout().subscribe();
        this.accountService.authenticate(null);
        this.router.navigateByUrl('/sessions/login');
    }
};
LoginService.ctorParameters = () => [
    { type: AccountService },
    { type: AuthServerProvider },
    { type: Router }
];
LoginService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function LoginService_Factory() { return new LoginService(i0.ɵɵinject(i1.AccountService), i0.ɵɵinject(i2.AuthServerProvider), i0.ɵɵinject(i3.Router)); }, token: LoginService, providedIn: "root" });
LoginService = tslib_1.__decorate([
    Injectable({ providedIn: 'root' })
], LoginService);
export { LoginService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL2xvZ2luLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3pDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUNuRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7Ozs7O0FBSXpDLElBQWEsWUFBWSxHQUF6QixNQUFhLFlBQVk7SUFDeEIsWUFBb0IsY0FBOEIsRUFBVSxrQkFBc0MsRUFDdkYsTUFBYztRQURMLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUFVLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdkYsV0FBTSxHQUFOLE1BQU0sQ0FBUTtJQUN6QixDQUFDO0lBRUQsS0FBSyxDQUFDLFdBQVc7UUFDaEIsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzNHLENBQUM7SUFFRCxNQUFNO1FBQ0wsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sRUFBRSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQzdDLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLENBQUE7SUFDN0MsQ0FBQztDQUNELENBQUE7O1lBYm9DLGNBQWM7WUFBOEIsa0JBQWtCO1lBQy9FLE1BQU07OztBQUZiLFlBQVk7SUFEeEIsVUFBVSxDQUFDLEVBQUMsVUFBVSxFQUFFLE1BQU0sRUFBQyxDQUFDO0dBQ3BCLFlBQVksQ0FjeEI7U0FkWSxZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IGZsYXRNYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IEFjY291bnRTZXJ2aWNlIH0gZnJvbSAnLi4vY29yZS9hdXRoL2FjY291bnQuc2VydmljZSc7XHJcbmltcG9ydCB7IEF1dGhTZXJ2ZXJQcm92aWRlciB9IGZyb20gJy4uL2NvcmUvYXV0aC9hdXRoLWp3dC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuXHJcblxyXG5ASW5qZWN0YWJsZSh7cHJvdmlkZWRJbjogJ3Jvb3QnfSlcclxuZXhwb3J0IGNsYXNzIExvZ2luU2VydmljZSB7XHJcblx0Y29uc3RydWN0b3IocHJpdmF0ZSBhY2NvdW50U2VydmljZTogQWNjb3VudFNlcnZpY2UsIHByaXZhdGUgYXV0aFNlcnZlclByb3ZpZGVyOiBBdXRoU2VydmVyUHJvdmlkZXIsXHJcblx0XHRcdFx0cHJpdmF0ZSByb3V0ZXI6IFJvdXRlcikge1xyXG5cdH1cclxuXHJcblx0bG9naW4oY3JlZGVudGlhbHMpOiBPYnNlcnZhYmxlPEFjY291bnQgfCBudWxsPiB7XHJcblx0XHRyZXR1cm4gdGhpcy5hdXRoU2VydmVyUHJvdmlkZXIubG9naW4oY3JlZGVudGlhbHMpLnBpcGUoZmxhdE1hcCgoKSA9PiB0aGlzLmFjY291bnRTZXJ2aWNlLmlkZW50aXR5KHRydWUpKSk7XHJcblx0fVxyXG5cclxuXHRsb2dvdXQoKSB7XHJcblx0XHR0aGlzLmF1dGhTZXJ2ZXJQcm92aWRlci5sb2dvdXQoKS5zdWJzY3JpYmUoKTtcclxuXHRcdHRoaXMuYWNjb3VudFNlcnZpY2UuYXV0aGVudGljYXRlKG51bGwpO1xyXG5cdFx0dGhpcy5yb3V0ZXIubmF2aWdhdGVCeVVybCgnL3Nlc3Npb25zL2xvZ2luJylcclxuXHR9XHJcbn1cclxuIl19