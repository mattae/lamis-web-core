import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RelativeTimePipe } from './relative-time.pipe';
import { ExcerptPipe } from './excerpt.pipe';
import { KeysPipe } from './keys.pipe';
import { MapValuesPipe } from './map-value.pipe';
import { NairaPipe } from './naira.pipe';
let CommonPipesModule = class CommonPipesModule {
};
CommonPipesModule = tslib_1.__decorate([
    NgModule({
        declarations: [
            RelativeTimePipe,
            ExcerptPipe,
            KeysPipe,
            MapValuesPipe,
            NairaPipe
        ],
        exports: [
            RelativeTimePipe,
            ExcerptPipe,
            KeysPipe,
            MapValuesPipe,
            NairaPipe
        ]
    })
], CommonPipesModule);
export { CommonPipesModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLXBpcGVzLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC9waXBlcy9jb21tb24vY29tbW9uLXBpcGVzLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUN2QyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDakQsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQWtCekMsSUFBYSxpQkFBaUIsR0FBOUIsTUFBYSxpQkFBaUI7Q0FDN0IsQ0FBQTtBQURZLGlCQUFpQjtJQWhCN0IsUUFBUSxDQUFDO1FBQ04sWUFBWSxFQUFFO1lBQ1YsZ0JBQWdCO1lBQ2hCLFdBQVc7WUFDWCxRQUFRO1lBQ1IsYUFBYTtZQUNiLFNBQVM7U0FDWjtRQUNELE9BQU8sRUFBRTtZQUNMLGdCQUFnQjtZQUNoQixXQUFXO1lBQ1gsUUFBUTtZQUNSLGFBQWE7WUFDYixTQUFTO1NBQ1o7S0FDSixDQUFDO0dBQ1csaUJBQWlCLENBQzdCO1NBRFksaUJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUmVsYXRpdmVUaW1lUGlwZSB9IGZyb20gJy4vcmVsYXRpdmUtdGltZS5waXBlJztcclxuaW1wb3J0IHsgRXhjZXJwdFBpcGUgfSBmcm9tICcuL2V4Y2VycHQucGlwZSc7XHJcbmltcG9ydCB7IEtleXNQaXBlIH0gZnJvbSAnLi9rZXlzLnBpcGUnO1xyXG5pbXBvcnQgeyBNYXBWYWx1ZXNQaXBlIH0gZnJvbSAnLi9tYXAtdmFsdWUucGlwZSc7XHJcbmltcG9ydCB7IE5haXJhUGlwZSB9IGZyb20gJy4vbmFpcmEucGlwZSc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgUmVsYXRpdmVUaW1lUGlwZSxcclxuICAgICAgICBFeGNlcnB0UGlwZSxcclxuICAgICAgICBLZXlzUGlwZSxcclxuICAgICAgICBNYXBWYWx1ZXNQaXBlLFxyXG4gICAgICAgIE5haXJhUGlwZVxyXG4gICAgXSxcclxuICAgIGV4cG9ydHM6IFtcclxuICAgICAgICBSZWxhdGl2ZVRpbWVQaXBlLFxyXG4gICAgICAgIEV4Y2VycHRQaXBlLFxyXG4gICAgICAgIEtleXNQaXBlLFxyXG4gICAgICAgIE1hcFZhbHVlc1BpcGUsXHJcbiAgICAgICAgTmFpcmFQaXBlXHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb21tb25QaXBlc01vZHVsZSB7XHJcbn1cclxuIl19