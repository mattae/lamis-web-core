import * as tslib_1 from "tslib";
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgJhipsterModule } from 'ng-jhipster';
import { AlertErrorComponent } from './alert/alert-error.component';
import { AlertComponent } from './alert/alert.component';
import { PhoneNumberValidator } from './phone.number.validator';
import { CommonPipesModule } from './pipes/common/common-pipes.module';
let SharedCommonModule = class SharedCommonModule {
};
SharedCommonModule = tslib_1.__decorate([
    NgModule({
        imports: [
            CommonModule,
            NgJhipsterModule,
            CommonPipesModule,
        ],
        declarations: [
            AlertComponent,
            AlertErrorComponent,
            PhoneNumberValidator
        ],
        exports: [
            AlertComponent,
            AlertErrorComponent,
            CommonPipesModule,
            PhoneNumberValidator
        ]
    })
], SharedCommonModule);
export { SharedCommonModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmVkLWNvbW1vbi5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvc2hhcmVkLWNvbW1vbi5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUMvQyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUNwRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDekQsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDaEUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFvQnZFLElBQWEsa0JBQWtCLEdBQS9CLE1BQWEsa0JBQWtCO0NBQzlCLENBQUE7QUFEWSxrQkFBa0I7SUFsQjlCLFFBQVEsQ0FBQztRQUNOLE9BQU8sRUFBRTtZQUNMLFlBQVk7WUFDWixnQkFBZ0I7WUFDaEIsaUJBQWlCO1NBQ3BCO1FBQ0QsWUFBWSxFQUFFO1lBQ1YsY0FBYztZQUNkLG1CQUFtQjtZQUNuQixvQkFBb0I7U0FDdkI7UUFDRCxPQUFPLEVBQUU7WUFDTCxjQUFjO1lBQ2QsbUJBQW1CO1lBQ25CLGlCQUFpQjtZQUNqQixvQkFBb0I7U0FDdkI7S0FDSixDQUFDO0dBQ1csa0JBQWtCLENBQzlCO1NBRFksa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTmdKaGlwc3Rlck1vZHVsZSB9IGZyb20gJ25nLWpoaXBzdGVyJztcclxuaW1wb3J0IHsgQWxlcnRFcnJvckNvbXBvbmVudCB9IGZyb20gJy4vYWxlcnQvYWxlcnQtZXJyb3IuY29tcG9uZW50JztcclxuaW1wb3J0IHsgQWxlcnRDb21wb25lbnQgfSBmcm9tICcuL2FsZXJ0L2FsZXJ0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFBob25lTnVtYmVyVmFsaWRhdG9yIH0gZnJvbSAnLi9waG9uZS5udW1iZXIudmFsaWRhdG9yJztcclxuaW1wb3J0IHsgQ29tbW9uUGlwZXNNb2R1bGUgfSBmcm9tICcuL3BpcGVzL2NvbW1vbi9jb21tb24tcGlwZXMubW9kdWxlJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgICAgIE5nSmhpcHN0ZXJNb2R1bGUsXHJcbiAgICAgICAgQ29tbW9uUGlwZXNNb2R1bGUsXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgQWxlcnRDb21wb25lbnQsXHJcbiAgICAgICAgQWxlcnRFcnJvckNvbXBvbmVudCxcclxuICAgICAgICBQaG9uZU51bWJlclZhbGlkYXRvclxyXG4gICAgXSxcclxuICAgIGV4cG9ydHM6IFtcclxuICAgICAgICBBbGVydENvbXBvbmVudCxcclxuICAgICAgICBBbGVydEVycm9yQ29tcG9uZW50LFxyXG4gICAgICAgIENvbW1vblBpcGVzTW9kdWxlLFxyXG4gICAgICAgIFBob25lTnVtYmVyVmFsaWRhdG9yXHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTaGFyZWRDb21tb25Nb2R1bGUge1xyXG59XHJcbiJdfQ==