import * as tslib_1 from "tslib";
import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { FormioComponent } from 'angular-material-formio';
import { LocalStorageService, SessionStorageService } from 'ngx-store';
import { LayoutTemplateService } from '../../services/layout.template.service';
let JsonFormComponent = class JsonFormComponent {
    constructor(templateService, localStorage, sessionStorage) {
        this.templateService = templateService;
        this.localStorage = localStorage;
        this.sessionStorage = sessionStorage;
        this.dataEvent = new EventEmitter(true);
        this.customEvent = new EventEmitter(true);
        this.form = {};
        this.isValid = false;
    }
    ngOnInit() {
        if (this.templateId) {
            this.templateService.getTemplate(this.templateId).subscribe((json) => {
                this.form = json.form;
            });
        }
        else {
            this.form = this.template;
        }
    }
    reset() {
        this.formio.onRefresh({
            submission: this.model
        });
    }
    onCustomEvent(event) {
        this.customEvent.emit(event);
    }
    change(event) {
        if (event.hasOwnProperty('isValid')) {
            this.isValid = event.isValid;
            if (this.isValid) {
                this.dataEvent.emit(event.data);
            }
        }
    }
    ngOnChanges(changes) {
        if (changes['model']) {
            const token = this.localStorage.get('authenticationToken') || this.sessionStorage.get('authenticationToken');
            this.model = {
                data: Object.assign({}, changes['model'].currentValue, { authorization: token })
            };
        }
    }
};
JsonFormComponent.ctorParameters = () => [
    { type: LayoutTemplateService },
    { type: LocalStorageService },
    { type: SessionStorageService }
];
tslib_1.__decorate([
    ViewChild(FormioComponent, { static: true })
], JsonFormComponent.prototype, "formio", void 0);
tslib_1.__decorate([
    Input()
], JsonFormComponent.prototype, "template", void 0);
tslib_1.__decorate([
    Input()
], JsonFormComponent.prototype, "templateId", void 0);
tslib_1.__decorate([
    Input()
], JsonFormComponent.prototype, "model", void 0);
tslib_1.__decorate([
    Output()
], JsonFormComponent.prototype, "dataEvent", void 0);
JsonFormComponent = tslib_1.__decorate([
    Component({
        selector: 'json-form',
        template: `
        <mat-formio [form]="form"
                    (ready)="reset()"
                    (customEvent)="onCustomEvent($event)"
                    (change)="change($event)">
        </mat-formio>
    `
    })
], JsonFormComponent);
export { JsonFormComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoianNvbi1mb3JtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC9qc29uLWZvcm0vanNvbi1mb3JtLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFxQixNQUFNLEVBQWlCLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNwSCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFFMUQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLHFCQUFxQixFQUFFLE1BQU0sV0FBVyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBWS9FLElBQWEsaUJBQWlCLEdBQTlCLE1BQWEsaUJBQWlCO0lBZ0IxQixZQUFvQixlQUFzQyxFQUFVLFlBQWlDLEVBQ2pGLGNBQXFDO1FBRHJDLG9CQUFlLEdBQWYsZUFBZSxDQUF1QjtRQUFVLGlCQUFZLEdBQVosWUFBWSxDQUFxQjtRQUNqRixtQkFBYyxHQUFkLGNBQWMsQ0FBdUI7UUFOekQsY0FBUyxHQUFzQixJQUFJLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0RCxnQkFBVyxHQUFzQixJQUFJLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4RCxTQUFJLEdBQVEsRUFBRSxDQUFDO1FBQ2YsWUFBTyxHQUFHLEtBQUssQ0FBQztJQUloQixDQUFDO0lBRUQsUUFBUTtRQUNKLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNqQixJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBUyxFQUFFLEVBQUU7Z0JBQ3RFLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztZQUMxQixDQUFDLENBQUMsQ0FBQztTQUNOO2FBQU07WUFDSCxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7U0FDN0I7SUFDTCxDQUFDO0lBRUQsS0FBSztRQUNELElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDO1lBQ2xCLFVBQVUsRUFBRSxJQUFJLENBQUMsS0FBSztTQUN6QixDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsYUFBYSxDQUFDLEtBQVU7UUFDcEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDakMsQ0FBQztJQUVELE1BQU0sQ0FBQyxLQUFVO1FBQ2IsSUFBSSxLQUFLLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxFQUFFO1lBQ2pDLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQztZQUM3QixJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ25DO1NBQ0o7SUFDTCxDQUFDO0lBRUQsV0FBVyxDQUFDLE9BQXNCO1FBQzlCLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ2xCLE1BQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUM3RyxJQUFJLENBQUMsS0FBSyxHQUFHO2dCQUNULElBQUksRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUMsWUFBWSxFQUFFLEVBQUMsYUFBYSxFQUFFLEtBQUssRUFBQyxDQUFDO2FBQ2pGLENBQUM7U0FDTDtJQUNMLENBQUM7Q0FDSixDQUFBOztZQXpDd0MscUJBQXFCO1lBQXdCLG1CQUFtQjtZQUNqRSxxQkFBcUI7O0FBZnpEO0lBREMsU0FBUyxDQUFDLGVBQWUsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUMsQ0FBQztpREFDbkI7QUFHeEI7SUFEQyxLQUFLLEVBQUU7bURBQ1M7QUFFakI7SUFEQyxLQUFLLEVBQUU7cURBQ1c7QUFFbkI7SUFEQyxLQUFLLEVBQUU7Z0RBQ0c7QUFFWDtJQURDLE1BQU0sRUFBRTtvREFDNkM7QUFYN0MsaUJBQWlCO0lBVjdCLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxXQUFXO1FBQ3JCLFFBQVEsRUFBRTs7Ozs7O0tBTVQ7S0FDSixDQUFDO0dBQ1csaUJBQWlCLENBeUQ3QjtTQXpEWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uQ2hhbmdlcywgT25Jbml0LCBPdXRwdXQsIFNpbXBsZUNoYW5nZXMsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtaW9Db21wb25lbnQgfSBmcm9tICdhbmd1bGFyLW1hdGVyaWFsLWZvcm1pbyc7XHJcbmltcG9ydCB7IEZvcm1pbyB9IGZyb20gJ2Zvcm1pb2pzJztcclxuaW1wb3J0IHsgTG9jYWxTdG9yYWdlU2VydmljZSwgU2Vzc2lvblN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnbmd4LXN0b3JlJztcclxuaW1wb3J0IHsgTGF5b3V0VGVtcGxhdGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvbGF5b3V0LnRlbXBsYXRlLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2pzb24tZm9ybScsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxtYXQtZm9ybWlvIFtmb3JtXT1cImZvcm1cIlxyXG4gICAgICAgICAgICAgICAgICAgIChyZWFkeSk9XCJyZXNldCgpXCJcclxuICAgICAgICAgICAgICAgICAgICAoY3VzdG9tRXZlbnQpPVwib25DdXN0b21FdmVudCgkZXZlbnQpXCJcclxuICAgICAgICAgICAgICAgICAgICAoY2hhbmdlKT1cImNoYW5nZSgkZXZlbnQpXCI+XHJcbiAgICAgICAgPC9tYXQtZm9ybWlvPlxyXG4gICAgYFxyXG59KVxyXG5leHBvcnQgY2xhc3MgSnNvbkZvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XHJcbiAgICBAVmlld0NoaWxkKEZvcm1pb0NvbXBvbmVudCwge3N0YXRpYzogdHJ1ZX0pXHJcbiAgICBmb3JtaW86IEZvcm1pb0NvbXBvbmVudDtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgdGVtcGxhdGU6IHN0cmluZztcclxuICAgIEBJbnB1dCgpXHJcbiAgICB0ZW1wbGF0ZUlkOiBzdHJpbmc7XHJcbiAgICBASW5wdXQoKVxyXG4gICAgbW9kZWw6IGFueTtcclxuICAgIEBPdXRwdXQoKVxyXG4gICAgZGF0YUV2ZW50OiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXIodHJ1ZSk7XHJcbiAgICBjdXN0b21FdmVudDogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyKHRydWUpO1xyXG4gICAgZm9ybTogYW55ID0ge307XHJcbiAgICBpc1ZhbGlkID0gZmFsc2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSB0ZW1wbGF0ZVNlcnZpY2U6IExheW91dFRlbXBsYXRlU2VydmljZSwgcHJpdmF0ZSBsb2NhbFN0b3JhZ2U6IExvY2FsU3RvcmFnZVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHNlc3Npb25TdG9yYWdlOiBTZXNzaW9uU3RvcmFnZVNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgICAgICBpZiAodGhpcy50ZW1wbGF0ZUlkKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGVtcGxhdGVTZXJ2aWNlLmdldFRlbXBsYXRlKHRoaXMudGVtcGxhdGVJZCkuc3Vic2NyaWJlKChqc29uOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZm9ybSA9IGpzb24uZm9ybTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5mb3JtID0gdGhpcy50ZW1wbGF0ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmVzZXQoKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtaW8ub25SZWZyZXNoKHtcclxuICAgICAgICAgICAgc3VibWlzc2lvbjogdGhpcy5tb2RlbFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG9uQ3VzdG9tRXZlbnQoZXZlbnQ6IGFueSkge1xyXG4gICAgICAgIHRoaXMuY3VzdG9tRXZlbnQuZW1pdChldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgY2hhbmdlKGV2ZW50OiBhbnkpIHtcclxuICAgICAgICBpZiAoZXZlbnQuaGFzT3duUHJvcGVydHkoJ2lzVmFsaWQnKSkge1xyXG4gICAgICAgICAgICB0aGlzLmlzVmFsaWQgPSBldmVudC5pc1ZhbGlkO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5pc1ZhbGlkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRhdGFFdmVudC5lbWl0KGV2ZW50LmRhdGEpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkIHtcclxuICAgICAgICBpZiAoY2hhbmdlc1snbW9kZWwnXSkge1xyXG4gICAgICAgICAgICBjb25zdCB0b2tlbiA9IHRoaXMubG9jYWxTdG9yYWdlLmdldCgnYXV0aGVudGljYXRpb25Ub2tlbicpIHx8IHRoaXMuc2Vzc2lvblN0b3JhZ2UuZ2V0KCdhdXRoZW50aWNhdGlvblRva2VuJyk7XHJcbiAgICAgICAgICAgIHRoaXMubW9kZWwgPSB7XHJcbiAgICAgICAgICAgICAgICBkYXRhOiBPYmplY3QuYXNzaWduKHt9LCBjaGFuZ2VzWydtb2RlbCddLmN1cnJlbnRWYWx1ZSwge2F1dGhvcml6YXRpb246IHRva2VufSlcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19