import * as tslib_1 from "tslib";
import { Component, Input, ViewEncapsulation } from '@angular/core';
import get from 'lodash.get';
import { LayoutTemplateService } from '../../../services/layout.template.service';
import { CardViewBoolItemModel, CardViewDateItemModel, CardViewDatetimeItemModel, CardViewFloatItemModel, CardViewIntItemModel, CardViewTextItemModel } from '@alfresco/adf-core';
export var FieldType;
(function (FieldType) {
    FieldType["date"] = "date";
    FieldType["datetime"] = "datetime";
    FieldType["text"] = "text";
    FieldType["boolean"] = "boolean";
    FieldType["int"] = "int";
    FieldType["float"] = "float";
})(FieldType || (FieldType = {}));
let DetailsComponent = class DetailsComponent {
    constructor(templateService) {
        this.templateService = templateService;
    }
    ngOnInit() {
        this.templateService.getTemplate(this.template).subscribe((json) => {
            this.details = json.template;
        });
    }
    propertiesForDetail(detail) {
        const properties = [];
        for (const field of detail.fields) {
            const dataType = field.type;
            let item;
            switch (dataType) {
                case FieldType.boolean:
                    item = new CardViewBoolItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label
                    });
                    break;
                case FieldType.int:
                    item = new CardViewIntItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label,
                    });
                    break;
                case FieldType.float:
                    item = new CardViewFloatItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label,
                    });
                    break;
                case FieldType.date:
                    item = new CardViewDateItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label,
                        format: 'dd MMM, yyyy'
                    });
                    break;
                case FieldType.datetime:
                    item = new CardViewDatetimeItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label,
                        format: 'dd MMM, yyyy HH:mm'
                    });
                    break;
                default:
                    item = new CardViewTextItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label,
                    });
            }
            properties.push(item);
        }
        return properties;
    }
    getValueForKey(key) {
        return get(this.model, key);
    }
};
DetailsComponent.ctorParameters = () => [
    { type: LayoutTemplateService }
];
tslib_1.__decorate([
    Input()
], DetailsComponent.prototype, "template", void 0);
tslib_1.__decorate([
    Input()
], DetailsComponent.prototype, "model", void 0);
DetailsComponent = tslib_1.__decorate([
    Component({
        selector: 'details-component',
        template: "<ng-container *ngIf=\"model && details\">\r\n    <mat-card *ngFor=\"let detail of details\" class=\"default mb-1 pb-0\">\r\n        <ng-container *ngIf=\"!!detail.header\">\r\n            <mat-card-title>{{detail.header}}</mat-card-title>\r\n            <mat-divider></mat-divider>\r\n        </ng-container>\r\n        <mat-card-content>\r\n            <adf-card-view [properties]=\"propertiesForDetail(detail)\"></adf-card-view>\r\n        </mat-card-content>\r\n    </mat-card>\r\n</ng-container>\r\n",
        encapsulation: ViewEncapsulation.None,
        styles: [""]
    })
], DetailsComponent);
export { DetailsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGV0YWlscy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvanNvbi1mb3JtL2NvbXBvbmVudC9kZXRhaWxzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQVUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDNUUsT0FBTyxHQUFHLE1BQU0sWUFBWSxDQUFDO0FBQzdCLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQ2xGLE9BQU8sRUFDSCxxQkFBcUIsRUFDckIscUJBQXFCLEVBQ3JCLHlCQUF5QixFQUN6QixzQkFBc0IsRUFDdEIsb0JBQW9CLEVBRXBCLHFCQUFxQixFQUN4QixNQUFNLG9CQUFvQixDQUFDO0FBYzVCLE1BQU0sQ0FBTixJQUFZLFNBT1g7QUFQRCxXQUFZLFNBQVM7SUFDakIsMEJBQWEsQ0FBQTtJQUNiLGtDQUFxQixDQUFBO0lBQ3JCLDBCQUFhLENBQUE7SUFDYixnQ0FBbUIsQ0FBQTtJQUNuQix3QkFBVyxDQUFBO0lBQ1gsNEJBQWUsQ0FBQTtBQUNuQixDQUFDLEVBUFcsU0FBUyxLQUFULFNBQVMsUUFPcEI7QUFRRCxJQUFhLGdCQUFnQixHQUE3QixNQUFhLGdCQUFnQjtJQVN6QixZQUFvQixlQUFzQztRQUF0QyxvQkFBZSxHQUFmLGVBQWUsQ0FBdUI7SUFDMUQsQ0FBQztJQUVELFFBQVE7UUFDSixJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsSUFBUyxFQUFFLEVBQUU7WUFDcEUsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ2pDLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVNLG1CQUFtQixDQUFDLE1BQWM7UUFDckMsTUFBTSxVQUFVLEdBQUcsRUFBRSxDQUFDO1FBQ3RCLEtBQUssTUFBTSxLQUFLLElBQUksTUFBTSxDQUFDLE1BQU0sRUFBRTtZQUMvQixNQUFNLFFBQVEsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO1lBQzVCLElBQUksSUFBa0IsQ0FBQztZQUN2QixRQUFRLFFBQVEsRUFBRTtnQkFDZCxLQUFLLFNBQVMsQ0FBQyxPQUFPO29CQUNsQixJQUFJLEdBQUcsSUFBSSxxQkFBcUIsQ0FBQzt3QkFDN0IsS0FBSyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQzt3QkFDckMsR0FBRyxFQUFFLEVBQUU7d0JBQ1AsS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLO3FCQUNyQixDQUFDLENBQUM7b0JBQ0gsTUFBTTtnQkFDVixLQUFLLFNBQVMsQ0FBQyxHQUFHO29CQUNkLElBQUksR0FBRyxJQUFJLG9CQUFvQixDQUFDO3dCQUM1QixLQUFLLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO3dCQUNyQyxHQUFHLEVBQUUsRUFBRTt3QkFDUCxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUs7cUJBQ3JCLENBQUMsQ0FBQztvQkFDSCxNQUFNO2dCQUNWLEtBQUssU0FBUyxDQUFDLEtBQUs7b0JBQ2hCLElBQUksR0FBRyxJQUFJLHNCQUFzQixDQUFDO3dCQUM5QixLQUFLLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO3dCQUNyQyxHQUFHLEVBQUUsRUFBRTt3QkFDUCxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUs7cUJBQ3JCLENBQUMsQ0FBQztvQkFDSCxNQUFNO2dCQUNWLEtBQUssU0FBUyxDQUFDLElBQUk7b0JBQ2YsSUFBSSxHQUFHLElBQUkscUJBQXFCLENBQUM7d0JBQzdCLEtBQUssRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7d0JBQ3JDLEdBQUcsRUFBRSxFQUFFO3dCQUNQLEtBQUssRUFBRSxLQUFLLENBQUMsS0FBSzt3QkFDbEIsTUFBTSxFQUFFLGNBQWM7cUJBQ3pCLENBQUMsQ0FBQztvQkFDSCxNQUFNO2dCQUNWLEtBQUssU0FBUyxDQUFDLFFBQVE7b0JBQ25CLElBQUksR0FBRyxJQUFJLHlCQUF5QixDQUFDO3dCQUNqQyxLQUFLLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO3dCQUNyQyxHQUFHLEVBQUUsRUFBRTt3QkFDUCxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUs7d0JBQ2xCLE1BQU0sRUFBRSxvQkFBb0I7cUJBQy9CLENBQUMsQ0FBQztvQkFDSCxNQUFNO2dCQUNWO29CQUNJLElBQUksR0FBRyxJQUFJLHFCQUFxQixDQUFDO3dCQUM3QixLQUFLLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO3dCQUNyQyxHQUFHLEVBQUUsRUFBRTt3QkFDUCxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUs7cUJBQ3JCLENBQUMsQ0FBQzthQUNWO1lBQ0QsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUN6QjtRQUNELE9BQU8sVUFBVSxDQUFDO0lBQ3RCLENBQUM7SUFFTyxjQUFjLENBQUMsR0FBVztRQUM5QixPQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ2hDLENBQUM7Q0FDSixDQUFBOztZQW5Fd0MscUJBQXFCOztBQVAxRDtJQURDLEtBQUssRUFBRTtrREFDUztBQUdqQjtJQURDLEtBQUssRUFBRTsrQ0FDRztBQUxGLGdCQUFnQjtJQU41QixTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsbUJBQW1CO1FBQzdCLG1nQkFBdUM7UUFFdkMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O0tBQ3hDLENBQUM7R0FDVyxnQkFBZ0IsQ0E0RTVCO1NBNUVZLGdCQUFnQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCwgVmlld0VuY2Fwc3VsYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IGdldCBmcm9tICdsb2Rhc2guZ2V0JztcclxuaW1wb3J0IHsgTGF5b3V0VGVtcGxhdGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2VydmljZXMvbGF5b3V0LnRlbXBsYXRlLnNlcnZpY2UnO1xyXG5pbXBvcnQge1xyXG4gICAgQ2FyZFZpZXdCb29sSXRlbU1vZGVsLFxyXG4gICAgQ2FyZFZpZXdEYXRlSXRlbU1vZGVsLFxyXG4gICAgQ2FyZFZpZXdEYXRldGltZUl0ZW1Nb2RlbCxcclxuICAgIENhcmRWaWV3RmxvYXRJdGVtTW9kZWwsXHJcbiAgICBDYXJkVmlld0ludEl0ZW1Nb2RlbCxcclxuICAgIENhcmRWaWV3SXRlbSxcclxuICAgIENhcmRWaWV3VGV4dEl0ZW1Nb2RlbFxyXG59IGZyb20gJ0BhbGZyZXNjby9hZGYtY29yZSc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIERldGFpbCB7XHJcbiAgICBoZWFkZXI/OiBzdHJpbmc7XHJcbiAgICBoZWFkZXJDbGFzcz86IHN0cmluZztcclxuICAgIGZpZWxkczogRmllbGRbXTtcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBGaWVsZCB7XHJcbiAgICB0eXBlOiBGaWVsZFR5cGU7XHJcbiAgICBrZXk6IHN0cmluZztcclxuICAgIGxhYmVsOiBzdHJpbmc7XHJcbn1cclxuXHJcbmV4cG9ydCBlbnVtIEZpZWxkVHlwZSB7XHJcbiAgICBkYXRlID0gJ2RhdGUnLFxyXG4gICAgZGF0ZXRpbWUgPSAnZGF0ZXRpbWUnLFxyXG4gICAgdGV4dCA9ICd0ZXh0JyxcclxuICAgIGJvb2xlYW4gPSAnYm9vbGVhbicsXHJcbiAgICBpbnQgPSAnaW50JyxcclxuICAgIGZsb2F0ID0gJ2Zsb2F0J1xyXG59XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnZGV0YWlscy1jb21wb25lbnQnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2RldGFpbHMuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vZGV0YWlscy5jb21wb25lbnQuc2NzcyddLFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRGV0YWlsc0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcbiAgICBASW5wdXQoKVxyXG4gICAgdGVtcGxhdGU6IHN0cmluZztcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgbW9kZWw6IGFueTtcclxuXHJcbiAgICBkZXRhaWxzOiBEZXRhaWxbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHRlbXBsYXRlU2VydmljZTogTGF5b3V0VGVtcGxhdGVTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy50ZW1wbGF0ZVNlcnZpY2UuZ2V0VGVtcGxhdGUodGhpcy50ZW1wbGF0ZSkuc3Vic2NyaWJlKChqc29uOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5kZXRhaWxzID0ganNvbi50ZW1wbGF0ZTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgcHJvcGVydGllc0ZvckRldGFpbChkZXRhaWw6IERldGFpbCk6IEFycmF5PENhcmRWaWV3SXRlbT4ge1xyXG4gICAgICAgIGNvbnN0IHByb3BlcnRpZXMgPSBbXTtcclxuICAgICAgICBmb3IgKGNvbnN0IGZpZWxkIG9mIGRldGFpbC5maWVsZHMpIHtcclxuICAgICAgICAgICAgY29uc3QgZGF0YVR5cGUgPSBmaWVsZC50eXBlO1xyXG4gICAgICAgICAgICBsZXQgaXRlbTogQ2FyZFZpZXdJdGVtO1xyXG4gICAgICAgICAgICBzd2l0Y2ggKGRhdGFUeXBlKSB7XHJcbiAgICAgICAgICAgICAgICBjYXNlIEZpZWxkVHlwZS5ib29sZWFuOlxyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW0gPSBuZXcgQ2FyZFZpZXdCb29sSXRlbU1vZGVsKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZ2V0VmFsdWVGb3JLZXkoZmllbGQua2V5KSxcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5OiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw6IGZpZWxkLmxhYmVsXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBjYXNlIEZpZWxkVHlwZS5pbnQ6XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbSA9IG5ldyBDYXJkVmlld0ludEl0ZW1Nb2RlbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmdldFZhbHVlRm9yS2V5KGZpZWxkLmtleSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleTogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsOiBmaWVsZC5sYWJlbCxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgRmllbGRUeXBlLmZsb2F0OlxyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW0gPSBuZXcgQ2FyZFZpZXdGbG9hdEl0ZW1Nb2RlbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmdldFZhbHVlRm9yS2V5KGZpZWxkLmtleSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleTogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsOiBmaWVsZC5sYWJlbCxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgRmllbGRUeXBlLmRhdGU6XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbSA9IG5ldyBDYXJkVmlld0RhdGVJdGVtTW9kZWwoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5nZXRWYWx1ZUZvcktleShmaWVsZC5rZXkpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBrZXk6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYWJlbDogZmllbGQubGFiZWwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1hdDogJ2RkIE1NTSwgeXl5eSdcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgRmllbGRUeXBlLmRhdGV0aW1lOlxyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW0gPSBuZXcgQ2FyZFZpZXdEYXRldGltZUl0ZW1Nb2RlbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmdldFZhbHVlRm9yS2V5KGZpZWxkLmtleSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleTogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsOiBmaWVsZC5sYWJlbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9ybWF0OiAnZGQgTU1NLCB5eXl5IEhIOm1tJ1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgICAgICBpdGVtID0gbmV3IENhcmRWaWV3VGV4dEl0ZW1Nb2RlbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmdldFZhbHVlRm9yS2V5KGZpZWxkLmtleSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleTogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsOiBmaWVsZC5sYWJlbCxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBwcm9wZXJ0aWVzLnB1c2goaXRlbSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBwcm9wZXJ0aWVzO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0VmFsdWVGb3JLZXkoa2V5OiBzdHJpbmcpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiBnZXQodGhpcy5tb2RlbCwga2V5KTtcclxuICAgIH1cclxufVxyXG4iXX0=