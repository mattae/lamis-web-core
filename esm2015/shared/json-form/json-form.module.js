import * as tslib_1 from "tslib";
import { CardViewModule } from '@alfresco/adf-core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatFormioModule } from 'angular-material-formio';
import { Formio } from 'formiojs';
import { LocalStorageService, SessionStorageService } from 'ngx-store';
import { isObject } from 'util';
import { DetailsComponent } from './component/details.component';
import { JsonFormComponent } from './json-form.component';
let JsonFormModule = class JsonFormModule {
    constructor(localStorage, sessionStorage) {
        this.localStorage = localStorage;
        this.sessionStorage = sessionStorage;
        const token = this.localStorage.get('authenticationToken') || this.sessionStorage.get('authenticationToken');
        Formio.getRequestArgs = (formio, type, url, method, data, opts) => {
            method = (method || 'GET').toUpperCase();
            if (!opts || !isObject(opts)) {
                opts = {};
            }
            opts['Authorization'] = token;
            const requestArgs = {
                url,
                method,
                data: data || null,
                opts
            };
            if (type) {
                requestArgs['type'] = type;
            }
            if (formio) {
                requestArgs['formio'] = formio;
            }
            return requestArgs;
        };
    }
};
JsonFormModule.ctorParameters = () => [
    { type: LocalStorageService },
    { type: SessionStorageService }
];
JsonFormModule = tslib_1.__decorate([
    NgModule({
        imports: [MatFormioModule, CommonModule, CardViewModule],
        declarations: [JsonFormComponent, DetailsComponent],
        exports: [JsonFormComponent, DetailsComponent, MatFormioModule],
        entryComponents: [JsonFormComponent]
    })
], JsonFormModule);
export { JsonFormModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoianNvbi1mb3JtLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC9qc29uLWZvcm0vanNvbi1mb3JtLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0sVUFBVSxDQUFDO0FBQ2xDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxxQkFBcUIsRUFBRSxNQUFNLFdBQVcsQ0FBQztBQUN2RSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2hDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQ2pFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBUTFELElBQWEsY0FBYyxHQUEzQixNQUFhLGNBQWM7SUFDdkIsWUFBb0IsWUFBaUMsRUFBVSxjQUFxQztRQUFoRixpQkFBWSxHQUFaLFlBQVksQ0FBcUI7UUFBVSxtQkFBYyxHQUFkLGNBQWMsQ0FBdUI7UUFDaEcsTUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBQzdHLE1BQU0sQ0FBQyxjQUFjLEdBQUcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxFQUFFO1lBQzlELE1BQU0sR0FBRyxDQUFDLE1BQU0sSUFBSSxLQUFLLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUN6QyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUMxQixJQUFJLEdBQUcsRUFBRSxDQUFDO2FBQ2I7WUFDRCxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBQzlCLE1BQU0sV0FBVyxHQUFHO2dCQUNoQixHQUFHO2dCQUNILE1BQU07Z0JBQ04sSUFBSSxFQUFFLElBQUksSUFBSSxJQUFJO2dCQUNsQixJQUFJO2FBQ1AsQ0FBQztZQUVGLElBQUksSUFBSSxFQUFFO2dCQUNOLFdBQVcsQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUM7YUFDOUI7WUFFRCxJQUFJLE1BQU0sRUFBRTtnQkFDUixXQUFXLENBQUMsUUFBUSxDQUFDLEdBQUcsTUFBTSxDQUFDO2FBQ2xDO1lBQ0QsT0FBTyxXQUFXLENBQUM7UUFDdkIsQ0FBQyxDQUFDO0lBQ04sQ0FBQztDQUNKLENBQUE7O1lBekJxQyxtQkFBbUI7WUFBMEIscUJBQXFCOztBQUQzRixjQUFjO0lBTjFCLFFBQVEsQ0FBQztRQUNOLE9BQU8sRUFBRSxDQUFDLGVBQWUsRUFBRSxZQUFZLEVBQUUsY0FBYyxDQUFDO1FBQ3hELFlBQVksRUFBRSxDQUFDLGlCQUFpQixFQUFFLGdCQUFnQixDQUFDO1FBQ25ELE9BQU8sRUFBRSxDQUFDLGlCQUFpQixFQUFFLGdCQUFnQixFQUFFLGVBQWUsQ0FBQztRQUMvRCxlQUFlLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQztLQUN2QyxDQUFDO0dBQ1csY0FBYyxDQTBCMUI7U0ExQlksY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENhcmRWaWV3TW9kdWxlIH0gZnJvbSAnQGFsZnJlc2NvL2FkZi1jb3JlJztcclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0Rm9ybWlvTW9kdWxlIH0gZnJvbSAnYW5ndWxhci1tYXRlcmlhbC1mb3JtaW8nO1xyXG5pbXBvcnQgeyBGb3JtaW8gfSBmcm9tICdmb3JtaW9qcyc7XHJcbmltcG9ydCB7IExvY2FsU3RvcmFnZVNlcnZpY2UsIFNlc3Npb25TdG9yYWdlU2VydmljZSB9IGZyb20gJ25neC1zdG9yZSc7XHJcbmltcG9ydCB7IGlzT2JqZWN0IH0gZnJvbSAndXRpbCc7XHJcbmltcG9ydCB7IERldGFpbHNDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudC9kZXRhaWxzLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEpzb25Gb3JtQ29tcG9uZW50IH0gZnJvbSAnLi9qc29uLWZvcm0uY29tcG9uZW50JztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbTWF0Rm9ybWlvTW9kdWxlLCBDb21tb25Nb2R1bGUsIENhcmRWaWV3TW9kdWxlXSxcclxuICAgIGRlY2xhcmF0aW9uczogW0pzb25Gb3JtQ29tcG9uZW50LCBEZXRhaWxzQ29tcG9uZW50XSxcclxuICAgIGV4cG9ydHM6IFtKc29uRm9ybUNvbXBvbmVudCwgRGV0YWlsc0NvbXBvbmVudCwgTWF0Rm9ybWlvTW9kdWxlXSxcclxuICAgIGVudHJ5Q29tcG9uZW50czogW0pzb25Gb3JtQ29tcG9uZW50XVxyXG59KVxyXG5leHBvcnQgY2xhc3MgSnNvbkZvcm1Nb2R1bGUge1xyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBsb2NhbFN0b3JhZ2U6IExvY2FsU3RvcmFnZVNlcnZpY2UsIHByaXZhdGUgc2Vzc2lvblN0b3JhZ2U6IFNlc3Npb25TdG9yYWdlU2VydmljZSkge1xyXG4gICAgICAgIGNvbnN0IHRva2VuID0gdGhpcy5sb2NhbFN0b3JhZ2UuZ2V0KCdhdXRoZW50aWNhdGlvblRva2VuJykgfHwgdGhpcy5zZXNzaW9uU3RvcmFnZS5nZXQoJ2F1dGhlbnRpY2F0aW9uVG9rZW4nKTtcclxuICAgICAgICBGb3JtaW8uZ2V0UmVxdWVzdEFyZ3MgPSAoZm9ybWlvLCB0eXBlLCB1cmwsIG1ldGhvZCwgZGF0YSwgb3B0cykgPT4ge1xyXG4gICAgICAgICAgICBtZXRob2QgPSAobWV0aG9kIHx8ICdHRVQnKS50b1VwcGVyQ2FzZSgpO1xyXG4gICAgICAgICAgICBpZiAoIW9wdHMgfHwgIWlzT2JqZWN0KG9wdHMpKSB7XHJcbiAgICAgICAgICAgICAgICBvcHRzID0ge307XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgb3B0c1snQXV0aG9yaXphdGlvbiddID0gdG9rZW47XHJcbiAgICAgICAgICAgIGNvbnN0IHJlcXVlc3RBcmdzID0ge1xyXG4gICAgICAgICAgICAgICAgdXJsLFxyXG4gICAgICAgICAgICAgICAgbWV0aG9kLFxyXG4gICAgICAgICAgICAgICAgZGF0YTogZGF0YSB8fCBudWxsLFxyXG4gICAgICAgICAgICAgICAgb3B0c1xyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgaWYgKHR5cGUpIHtcclxuICAgICAgICAgICAgICAgIHJlcXVlc3RBcmdzWyd0eXBlJ10gPSB0eXBlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoZm9ybWlvKSB7XHJcbiAgICAgICAgICAgICAgICByZXF1ZXN0QXJnc1snZm9ybWlvJ10gPSBmb3JtaW87XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHJlcXVlc3RBcmdzO1xyXG4gICAgICAgIH07XHJcbiAgICB9XHJcbn1cclxuIl19