import * as tslib_1 from "tslib";
import { Inject, Injectable } from '@angular/core';
import { DefaultLocaleConfig, LOCALE_CONFIG } from './date-range-picker.config';
let LocaleService = class LocaleService {
    constructor(_config) {
        this._config = _config;
    }
    get config() {
        if (!this._config) {
            return DefaultLocaleConfig;
        }
        return Object.assign({}, DefaultLocaleConfig, this._config);
    }
};
LocaleService.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [LOCALE_CONFIG,] }] }
];
LocaleService = tslib_1.__decorate([
    Injectable(),
    tslib_1.__param(0, Inject(LOCALE_CONFIG))
], LocaleService);
export { LocaleService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYWxlcy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL3V0aWwvZGF0ZS1yYW5nZS1waWNrZXIvbG9jYWxlcy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsYUFBYSxFQUFnQixNQUFNLDRCQUE0QixDQUFDO0FBRzlGLElBQWEsYUFBYSxHQUExQixNQUFhLGFBQWE7SUFDekIsWUFBMkMsT0FBcUI7UUFBckIsWUFBTyxHQUFQLE9BQU8sQ0FBYztJQUNoRSxDQUFDO0lBRUQsSUFBSSxNQUFNO1FBQ1QsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDbEIsT0FBTyxtQkFBbUIsQ0FBQztTQUMzQjtRQUVELHlCQUFXLG1CQUFtQixFQUFLLElBQUksQ0FBQyxPQUFPLEVBQUM7SUFDakQsQ0FBQztDQUNELENBQUE7OzRDQVZhLE1BQU0sU0FBQyxhQUFhOztBQURyQixhQUFhO0lBRHpCLFVBQVUsRUFBRTtJQUVDLG1CQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQTtHQUR0QixhQUFhLENBV3pCO1NBWFksYUFBYSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEZWZhdWx0TG9jYWxlQ29uZmlnLCBMT0NBTEVfQ09ORklHLCBMb2NhbGVDb25maWcgfSBmcm9tICcuL2RhdGUtcmFuZ2UtcGlja2VyLmNvbmZpZyc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBMb2NhbGVTZXJ2aWNlIHtcclxuXHRjb25zdHJ1Y3RvcihASW5qZWN0KExPQ0FMRV9DT05GSUcpIHByaXZhdGUgX2NvbmZpZzogTG9jYWxlQ29uZmlnKSB7XHJcblx0fVxyXG5cclxuXHRnZXQgY29uZmlnKCkge1xyXG5cdFx0aWYgKCF0aGlzLl9jb25maWcpIHtcclxuXHRcdFx0cmV0dXJuIERlZmF1bHRMb2NhbGVDb25maWc7XHJcblx0XHR9XHJcblxyXG5cdFx0cmV0dXJuIHsuLi5EZWZhdWx0TG9jYWxlQ29uZmlnLCAuLi50aGlzLl9jb25maWd9XHJcblx0fVxyXG59XHJcbiJdfQ==