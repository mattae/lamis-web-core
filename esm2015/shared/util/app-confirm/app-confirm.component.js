import * as tslib_1 from "tslib";
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
let AppConfirmComponent = class AppConfirmComponent {
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
};
AppConfirmComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
];
AppConfirmComponent = tslib_1.__decorate([
    Component({
        selector: 'app-confirm',
        template: `<h1 matDialogTitle class="mb-05">{{ data.title }}</h1>
    <div mat-dialog-content class="mb-1">{{ data.message }}</div>
    <div mat-dialog-actions>
        <button
                type="button"
                mat-raised-button
                color="primary"
                (click)="dialogRef.close(true)">OK
        </button>
        &nbsp;
        <span fxFlex></span>
        <button
                type="button"
                color="accent"
                mat-raised-button
                (click)="dialogRef.close(false)">Cancel
        </button>
    </div>`
    }),
    tslib_1.__param(1, Inject(MAT_DIALOG_DATA))
], AppConfirmComponent);
export { AppConfirmComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLWNvbmZpcm0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL3V0aWwvYXBwLWNvbmZpcm0vYXBwLWNvbmZpcm0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNsRCxPQUFPLEVBQUUsZUFBZSxFQUFFLFlBQVksRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBdUJsRSxJQUFhLG1CQUFtQixHQUFoQyxNQUFhLG1CQUFtQjtJQUM1QixZQUNXLFNBQTRDLEVBQ25CLElBQVM7UUFEbEMsY0FBUyxHQUFULFNBQVMsQ0FBbUM7UUFDbkIsU0FBSSxHQUFKLElBQUksQ0FBSztJQUU3QyxDQUFDO0NBQ0osQ0FBQTs7WUFKeUIsWUFBWTs0Q0FDN0IsTUFBTSxTQUFDLGVBQWU7O0FBSGxCLG1CQUFtQjtJQXJCL0IsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLGFBQWE7UUFDdkIsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7OztXQWlCSDtLQUNWLENBQUM7SUFJTyxtQkFBQSxNQUFNLENBQUMsZUFBZSxDQUFDLENBQUE7R0FIbkIsbUJBQW1CLENBTS9CO1NBTlksbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbmplY3QgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTUFUX0RJQUxPR19EQVRBLCBNYXREaWFsb2dSZWYgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYXBwLWNvbmZpcm0nLFxyXG4gICAgdGVtcGxhdGU6IGA8aDEgbWF0RGlhbG9nVGl0bGUgY2xhc3M9XCJtYi0wNVwiPnt7IGRhdGEudGl0bGUgfX08L2gxPlxyXG4gICAgPGRpdiBtYXQtZGlhbG9nLWNvbnRlbnQgY2xhc3M9XCJtYi0xXCI+e3sgZGF0YS5tZXNzYWdlIH19PC9kaXY+XHJcbiAgICA8ZGl2IG1hdC1kaWFsb2ctYWN0aW9ucz5cclxuICAgICAgICA8YnV0dG9uXHJcbiAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcclxuICAgICAgICAgICAgICAgIG1hdC1yYWlzZWQtYnV0dG9uXHJcbiAgICAgICAgICAgICAgICBjb2xvcj1cInByaW1hcnlcIlxyXG4gICAgICAgICAgICAgICAgKGNsaWNrKT1cImRpYWxvZ1JlZi5jbG9zZSh0cnVlKVwiPk9LXHJcbiAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgJm5ic3A7XHJcbiAgICAgICAgPHNwYW4gZnhGbGV4Pjwvc3Bhbj5cclxuICAgICAgICA8YnV0dG9uXHJcbiAgICAgICAgICAgICAgICB0eXBlPVwiYnV0dG9uXCJcclxuICAgICAgICAgICAgICAgIGNvbG9yPVwiYWNjZW50XCJcclxuICAgICAgICAgICAgICAgIG1hdC1yYWlzZWQtYnV0dG9uXHJcbiAgICAgICAgICAgICAgICAoY2xpY2spPVwiZGlhbG9nUmVmLmNsb3NlKGZhbHNlKVwiPkNhbmNlbFxyXG4gICAgICAgIDwvYnV0dG9uPlxyXG4gICAgPC9kaXY+YCxcclxufSlcclxuZXhwb3J0IGNsYXNzIEFwcENvbmZpcm1Db21wb25lbnQge1xyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHVibGljIGRpYWxvZ1JlZjogTWF0RGlhbG9nUmVmPEFwcENvbmZpcm1Db21wb25lbnQ+LFxyXG4gICAgICAgIEBJbmplY3QoTUFUX0RJQUxPR19EQVRBKSBwdWJsaWMgZGF0YTogYW55XHJcbiAgICApIHtcclxuICAgIH1cclxufVxyXG4iXX0=