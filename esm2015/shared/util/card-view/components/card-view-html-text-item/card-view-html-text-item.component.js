import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
let CardViewHtmlTextItemComponent = class CardViewHtmlTextItemComponent {
    constructor() {
    }
    ngOnChanges() {
    }
};
tslib_1.__decorate([
    Input()
], CardViewHtmlTextItemComponent.prototype, "property", void 0);
CardViewHtmlTextItemComponent = tslib_1.__decorate([
    Component({
        selector: 'tradcard-view-html-text',
        template: "<div class=\"adf-property-label\">{{ property.label | translate }}</div>\r\n<div class=\"adf-property-value\">\r\n    <span>\r\n        <span [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n            <span [innerHtml]=\"property.displayValue\"></span>\r\n        </span>\r\n    </span>\r\n</div>\r\n",
        styles: [""]
    })
], CardViewHtmlTextItemComponent);
export { CardViewHtmlTextItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWh0bWwtdGV4dC1pdGVtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC91dGlsL2NhcmQtdmlldy9jb21wb25lbnRzL2NhcmQtdmlldy1odG1sLXRleHQtaXRlbS9jYXJkLXZpZXctaHRtbC10ZXh0LWl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBYSxNQUFNLGVBQWUsQ0FBQztBQVE1RCxJQUFhLDZCQUE2QixHQUExQyxNQUFhLDZCQUE2QjtJQUl0QztJQUNBLENBQUM7SUFFRCxXQUFXO0lBQ1gsQ0FBQztDQUVKLENBQUE7QUFSRztJQURDLEtBQUssRUFBRTsrREFDNEI7QUFGM0IsNkJBQTZCO0lBTHpDLFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSx5QkFBeUI7UUFDbkMsaVZBQXdEOztLQUUzRCxDQUFDO0dBQ1csNkJBQTZCLENBVXpDO1NBVlksNkJBQTZCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25DaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENhcmRWaWV3SHRtbFRleHRJdGVtTW9kZWwgfSBmcm9tICcuLi8uLi9tb2RlbHMvY2FyZC12aWV3LWh0bWwtdGV4dC1pdGVtLm1vZGVsJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICd0cmFkY2FyZC12aWV3LWh0bWwtdGV4dCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vY2FyZC12aWV3LWh0bWwtdGV4dC1pdGVtLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2NhcmQtdmlldy1odG1sLXRleHQtaXRlbS5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDYXJkVmlld0h0bWxUZXh0SXRlbUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XHJcbiAgICBASW5wdXQoKVxyXG4gICAgcHJvcGVydHk6IENhcmRWaWV3SHRtbFRleHRJdGVtTW9kZWw7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkNoYW5nZXMoKTogdm9pZCB7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==