/*!
 * @license
 * Copyright 2016 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import * as tslib_1 from "tslib";
import { MatTableDataSource } from '@angular/material';
import { Component, Input } from '@angular/core';
import { CardViewKeyValuePairsItemType, CardViewUpdateService } from '@alfresco/adf-core';
let CardViewFixedKeyvaluepairsitemComponent = class CardViewFixedKeyvaluepairsitemComponent {
    constructor(cardViewUpdateService) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.editable = false;
    }
    ngOnChanges() {
        this.values = this.property.value || [];
        this.matTableValues = new MatTableDataSource(this.values);
    }
    isEditable() {
        return this.editable && this.property.editable;
    }
    add() {
        this.values.push({ name: '', value: '' });
    }
    remove(index) {
        this.values.splice(index, 1);
        this.save(true);
    }
    onBlur(value) {
        if (value.length) {
            this.save();
        }
    }
    save(remove) {
        const validValues = this.values.filter(i => i.name.length && i.value.length);
        if (remove || validValues.length) {
            this.cardViewUpdateService.update(this.property, validValues);
            this.property.value = validValues;
        }
    }
};
CardViewFixedKeyvaluepairsitemComponent.ctorParameters = () => [
    { type: CardViewUpdateService }
];
tslib_1.__decorate([
    Input()
], CardViewFixedKeyvaluepairsitemComponent.prototype, "property", void 0);
tslib_1.__decorate([
    Input()
], CardViewFixedKeyvaluepairsitemComponent.prototype, "editable", void 0);
CardViewFixedKeyvaluepairsitemComponent = tslib_1.__decorate([
    Component({
        selector: 'card-view-keyvaluepair',
        template: "<div [attr.data-automation-id]=\"'card-key-value-pairs-label-' + property.key\" class=\"adf-property-label\">{{ property.label | translate }}</div>\r\n<div class=\"adf-property-value\">\r\n\r\n    <div *ngIf=\"!isEditable()\" class=\"card-view__key-value-pairs__read-only\">\r\n        <mat-table #table [dataSource]=\"matTableValues\" class=\"mat-elevation-z8\">\r\n            <ng-container matColumnDef=\"name\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.NAME' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let item\">{{item.name}}</mat-cell>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"value\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.VALUE' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let item\">{{item.value}}</mat-cell>\r\n            </ng-container>\r\n\r\n            <mat-header-row *matHeaderRowDef=\"['name', 'value']\"></mat-header-row>\r\n            <mat-row *matRowDef=\"let row; columns: ['name', 'value'];\"></mat-row>\r\n        </mat-table>\r\n    </div>\r\n\r\n\r\n    <div class=\"card-view__key-value-pairs\" *ngIf=\"isEditable() && values && values.length\">\r\n        <div class=\"card-view__key-value-pairs__row\">\r\n            <div class=\"card-view__key-value-pairs__col\">{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.NAME' | translate }}</div>\r\n            <div class=\"card-view__key-value-pairs__col\">{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.VALUE' | translate }}</div>\r\n        </div>\r\n\r\n        <div class=\"card-view__key-value-pairs__row\" *ngFor=\"let item of values; let i = index\">\r\n            <div class=\"card-view__key-value-pairs__col\">\r\n                <mat-form-field class=\"example-full-width\">\r\n                    <input matInput\r\n                           [disabled]=\"true\"\r\n                           [attr.data-automation-id]=\"'card-'+ property.key +'-name-input-' + i\"\r\n                           [(ngModel)]=\"values[i].name\">\r\n                </mat-form-field>\r\n            </div>\r\n            <div class=\"card-view__key-value-pairs__col\">\r\n                <mat-form-field class=\"example-full-width\">\r\n                    <input matInput\r\n                           (blur)=\"onBlur(item.value)\"\r\n                           [attr.data-automation-id]=\"'card-'+ property.key +'-value-input-' + i\"\r\n                           [(ngModel)]=\"values[i].value\">\r\n                </mat-form-field>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n",
        styles: [".card-view__key-value-pairs__col{display:inline-block;width:39%}.card-view__key-value-pairs__col .mat-form-field{width:100%}.card-view__key-value-pairs__read-only .mat-table{box-shadow:none}.card-view__key-value-pairs__read-only .mat-header-row,.card-view__key-value-pairs__read-only .mat-row{padding:0}"]
    })
], CardViewFixedKeyvaluepairsitemComponent);
export { CardViewFixedKeyvaluepairsitemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWZpeGVkLWtleXZhbHVlcGFpcnNpdGVtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC91dGlsL2NhcmQtdmlldy9jb21wb25lbnRzL2NhcmQtdmlldy1maXhlZC1rZXl2YWx1ZXBhaXJzaXRlbS9jYXJkLXZpZXctZml4ZWQta2V5dmFsdWVwYWlyc2l0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7Ozs7R0FlRzs7QUFFSCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUN2RCxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUM1RCxPQUFPLEVBQUUsNkJBQTZCLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQVMxRixJQUFhLHVDQUF1QyxHQUFwRCxNQUFhLHVDQUF1QztJQVdoRCxZQUFvQixxQkFBNEM7UUFBNUMsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUF1QjtRQUxoRSxhQUFRLEdBQVksS0FBSyxDQUFDO0lBS3lDLENBQUM7SUFFcEUsV0FBVztRQUNQLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVELFVBQVU7UUFDTixPQUFPLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7SUFDbkQsQ0FBQztJQUVELEdBQUc7UUFDQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxFQUFFLEVBQUUsS0FBSyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDOUMsQ0FBQztJQUVELE1BQU0sQ0FBQyxLQUFhO1FBQ2hCLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQztRQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3BCLENBQUM7SUFFRCxNQUFNLENBQUMsS0FBSztRQUNSLElBQUksS0FBSyxDQUFDLE1BQU0sRUFBRTtZQUNkLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUNmO0lBQ0wsQ0FBQztJQUVELElBQUksQ0FBQyxNQUFnQjtRQUNqQixNQUFNLFdBQVcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFN0UsSUFBSSxNQUFNLElBQUksV0FBVyxDQUFDLE1BQU0sRUFBRTtZQUM5QixJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsV0FBVyxDQUFDLENBQUM7WUFDOUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsV0FBVyxDQUFDO1NBQ3JDO0lBQ0wsQ0FBQztDQUNKLENBQUE7O1lBbEM4QyxxQkFBcUI7O0FBUmhFO0lBREMsS0FBSyxFQUFFO3lFQUNzQztBQUc5QztJQURDLEtBQUssRUFBRTt5RUFDa0I7QUFOakIsdUNBQXVDO0lBTm5ELFNBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSx3QkFBd0I7UUFDbEMsbWxGQUEyRDs7S0FFOUQsQ0FBQztHQUVXLHVDQUF1QyxDQTZDbkQ7U0E3Q1ksdUNBQXVDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE2IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IE1hdFRhYmxlRGF0YVNvdXJjZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25DaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENhcmRWaWV3S2V5VmFsdWVQYWlyc0l0ZW1UeXBlLCBDYXJkVmlld1VwZGF0ZVNlcnZpY2UgfSBmcm9tICdAYWxmcmVzY28vYWRmLWNvcmUnO1xyXG5pbXBvcnQgeyBDYXJkVmlld0ZpeGVkS2V5VmFsdWVQYWlyc0l0ZW1Nb2RlbCB9IGZyb20gJy4uLy4uL21vZGVscy9jYXJkLXZpZXctZml4ZWQta2V5dmFsdWVwYWlycy5tb2RlbCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnY2FyZC12aWV3LWtleXZhbHVlcGFpcicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vY2FyZC12aWV3LWtleXZhbHVlcGFpcnNpdGVtLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2NhcmQtdmlldy1rZXl2YWx1ZXBhaXJzaXRlbS5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQ2FyZFZpZXdGaXhlZEtleXZhbHVlcGFpcnNpdGVtQ29tcG9uZW50IGltcGxlbWVudHMgT25DaGFuZ2VzIHtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgcHJvcGVydHk6IENhcmRWaWV3Rml4ZWRLZXlWYWx1ZVBhaXJzSXRlbU1vZGVsO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBlZGl0YWJsZTogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIHZhbHVlczogQ2FyZFZpZXdLZXlWYWx1ZVBhaXJzSXRlbVR5cGVbXTtcclxuICAgIG1hdFRhYmxlVmFsdWVzOiBNYXRUYWJsZURhdGFTb3VyY2U8Q2FyZFZpZXdLZXlWYWx1ZVBhaXJzSXRlbVR5cGU+O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgY2FyZFZpZXdVcGRhdGVTZXJ2aWNlOiBDYXJkVmlld1VwZGF0ZVNlcnZpY2UpIHt9XHJcblxyXG4gICAgbmdPbkNoYW5nZXMoKSB7XHJcbiAgICAgICAgdGhpcy52YWx1ZXMgPSB0aGlzLnByb3BlcnR5LnZhbHVlIHx8IFtdO1xyXG4gICAgICAgIHRoaXMubWF0VGFibGVWYWx1ZXMgPSBuZXcgTWF0VGFibGVEYXRhU291cmNlKHRoaXMudmFsdWVzKTtcclxuICAgIH1cclxuXHJcbiAgICBpc0VkaXRhYmxlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmVkaXRhYmxlICYmIHRoaXMucHJvcGVydHkuZWRpdGFibGU7XHJcbiAgICB9XHJcblxyXG4gICAgYWRkKCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMudmFsdWVzLnB1c2goeyBuYW1lOiAnJywgdmFsdWU6ICcnIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbW92ZShpbmRleDogbnVtYmVyKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy52YWx1ZXMuc3BsaWNlKGluZGV4LCAxKTtcclxuICAgICAgICB0aGlzLnNhdmUodHJ1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25CbHVyKHZhbHVlKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKHZhbHVlLmxlbmd0aCkge1xyXG4gICAgICAgICAgICB0aGlzLnNhdmUoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc2F2ZShyZW1vdmU/OiBib29sZWFuKTogdm9pZCB7XHJcbiAgICAgICAgY29uc3QgdmFsaWRWYWx1ZXMgPSB0aGlzLnZhbHVlcy5maWx0ZXIoaSA9PiBpLm5hbWUubGVuZ3RoICYmIGkudmFsdWUubGVuZ3RoKTtcclxuXHJcbiAgICAgICAgaWYgKHJlbW92ZSB8fCB2YWxpZFZhbHVlcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgdGhpcy5jYXJkVmlld1VwZGF0ZVNlcnZpY2UudXBkYXRlKHRoaXMucHJvcGVydHksIHZhbGlkVmFsdWVzKTtcclxuICAgICAgICAgICAgdGhpcy5wcm9wZXJ0eS52YWx1ZSA9IHZhbGlkVmFsdWVzO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=