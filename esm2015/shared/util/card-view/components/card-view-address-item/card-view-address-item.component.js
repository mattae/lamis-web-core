import * as tslib_1 from "tslib";
import { Component, Input, ViewChild } from '@angular/core';
import { CardViewUpdateService } from '@alfresco/adf-core';
import { Address } from '../../../../model/address.model';
import { LgaService } from '../../../../../services/lga.service';
import { StateService } from '../../../../../services/state.service';
let CardViewAddressItemComponent = class CardViewAddressItemComponent {
    constructor(cardViewUpdateService, stateService, lgaService) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.stateService = stateService;
        this.lgaService = lgaService;
        this.editable = false;
        this.displayEmpty = true;
        this.inEdit = false;
    }
    ngOnChanges() {
        this.editedStreet1 = this.property.value.street1;
        this.editedStreet2 = this.property.value.street2;
        this.editedCity = this.property.value.city;
        this.editedLga = this.property.value.lga;
        if (this.editedLga) {
            this.state = this.property.value.lga.state;
        }
    }
    ngOnInit() {
    }
    onChange(event) {
        this.lgaService.findByState(event.value.id).subscribe(res => this.lgas = res.body);
    }
    showProperty() {
        return this.displayEmpty || !this.property.isEmpty();
    }
    isEditable() {
        return this.editable && this.property.editable;
    }
    isClickable() {
        return this.property.clickable;
    }
    hasIcon() {
        return !!this.property.icon;
    }
    hasErrors() {
        return this.errorMessages && this.errorMessages.length;
    }
    setEditMode(editStatus) {
        if (editStatus) {
            this.stateService.getStates().subscribe(res => this.states = res.body);
        }
        this.inEdit = editStatus;
        setTimeout(() => {
            if (this.cityInput) {
                this.cityInput.nativeElement.click();
            }
        }, 0);
        setTimeout(() => {
            if (this.street1Input) {
                this.street1Input.nativeElement.click();
            }
        }, 0);
        setTimeout(() => {
            if (this.street2Input) {
                this.street2Input.nativeElement.click();
            }
        }, 0);
    }
    reset() {
        this.editedStreet1 = this.property.value.street1;
        this.editedStreet2 = this.property.value.street2;
        this.editedCity = this.property.value.city;
        this.editedLga = this.property.value.lga;
        if (this.editedLga) {
            this.state = this.property.value.lga.state;
        }
        this.setEditMode(false);
    }
    update() {
        if (this.property.isValid(new Address(this.editedStreet1, this.editedStreet2, this.editedCity, this.editedLga))) {
            this.cardViewUpdateService.update(this.property, new Address(this.editedStreet1, this.editedStreet2, this.editedCity, this.editedLga));
            this.property.value = new Address(this.editedStreet1, this.editedStreet2, this.editedCity, this.editedLga);
            this.setEditMode(false);
        }
        else {
            this.errorMessages = this.property.getValidationErrors(new Address(this.editedStreet1, this.editedStreet2, this.editedCity, this.editedLga));
        }
    }
    get displayValue() {
        return this.property.displayValue;
    }
    clicked() {
        this.cardViewUpdateService.clicked(this.property);
    }
    entityCompare(s1, s2) {
        return s1 && s2 ? s1.id == s2.id : s1 === s2;
    }
};
CardViewAddressItemComponent.ctorParameters = () => [
    { type: CardViewUpdateService },
    { type: StateService },
    { type: LgaService }
];
tslib_1.__decorate([
    Input()
], CardViewAddressItemComponent.prototype, "property", void 0);
tslib_1.__decorate([
    Input()
], CardViewAddressItemComponent.prototype, "editable", void 0);
tslib_1.__decorate([
    Input()
], CardViewAddressItemComponent.prototype, "displayEmpty", void 0);
tslib_1.__decorate([
    ViewChild('cityInput', { static: true })
], CardViewAddressItemComponent.prototype, "cityInput", void 0);
tslib_1.__decorate([
    ViewChild('street1Input', { static: true })
], CardViewAddressItemComponent.prototype, "street1Input", void 0);
tslib_1.__decorate([
    ViewChild('street2Input', { static: true })
], CardViewAddressItemComponent.prototype, "street2Input", void 0);
CardViewAddressItemComponent = tslib_1.__decorate([
    Component({
        selector: 'card-view-address-item',
        template: "<div [attr.data-automation-id]=\"'card-name-item-label-' + property.key\" class=\"adf-property-label\"\r\n     *ngIf=\"showProperty() || isEditable()\">{{ property.label | translate }}\r\n</div>\r\n<div class=\"adf-property-value\">\r\n    <span *ngIf=\"!isEditable()\">\r\n        <span *ngIf=\"!isClickable(); else elseBlock\"\r\n              [attr.data-automation-id]=\"'card-name-titem-value-' + property.key\">\r\n            <span *ngIf=\"showProperty()\">{{ displayValue }}</span>\r\n        </span>\r\n        <ng-template #elseBlock>\r\n        <div class=\"adf-textitem-clickable\" (click)=\"clicked()\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n            <span class=\"adf-textitem-clickable-value\"\r\n                  [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ displayValue }}</span>\r\n            </span>\r\n            <mat-icon *ngIf=\"hasIcon()\" fxFlex=\"0 0 auto\"\r\n                      [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.icon\" class=\"adf-textitem-icon\">{{ property.icon }}</mat-icon>\r\n        </div>\r\n        </ng-template>\r\n    </span>\r\n    <span *ngIf=\"isEditable()\">\r\n        <div *ngIf=\"!inEdit\" (click)=\"setEditMode(true)\" class=\"adf-textitem-readonly\"\r\n             [attr.data-automation-id]=\"'card-textitem-edit-toggle-' + property.key\" fxLayout=\"row\"\r\n             fxLayoutAlign=\"space-between center\">\r\n            <span [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ displayValue }}</span>\r\n            </span>\r\n            <mat-icon fxFlex=\"0 0 auto\"\r\n                      [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.key\"\r\n                      [attr.title]=\"'CORE.METADATA.ACTIONS.EDIT' | translate\"\r\n                      class=\"adf-textitem-icon\">create</mat-icon>\r\n        </div>\r\n        <div *ngIf=\"inEdit\" class=\"adf-textitem-editable\">\r\n            <div class=\"\" fxLayout=\"column\" fxLayoutAlign=\"space-between start\">\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\">\r\n                        <input #street1Input\r\n                               matInput\r\n                               [placeholder]=\"'Street Line 1'\"\r\n                               [(ngModel)]=\"editedStreet1\"\r\n                               [attr.data-automation-id]=\"'card-textitem-titleinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #street2Input\r\n                               matInput\r\n                               class=\"adf-input\"\r\n                               [placeholder]=\"'Street Line 2'\"\r\n                               [(ngModel)]=\"editedStreet2\"\r\n                               [attr.data-automation-id]=\"'card-textitem-firstnameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #cityInput\r\n                               matInput\r\n                               [placeholder]=\"'City'\"\r\n                               [(ngModel)]=\"editedCity\"\r\n                               [attr.data-automation-id]=\"'card-textitem-middlenameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field>\r\n                        <mat-select (selectionChange)=\"onChange($event)\"\r\n                                    placeholder=\"State\"\r\n                                    [(ngModel)]=\"state\"\r\n                                    [compareWith]=\"entityCompare\"\r\n                                    data-automation-class=\"select-box\">\r\n                            <mat-option *ngFor=\"let state of states \" [value]=\"state\">\r\n                                {{ state.name }}\r\n                            </mat-option>\r\n                        </mat-select>\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field>\r\n                        <mat-select [(ngModel)]=\"editedLga\"\r\n                                    placeholder=\"LGA\"\r\n                                    [compareWith]=\"entityCompare\"\r\n                                    data-automation-class=\"select-box\">\r\n                            <mat-option *ngFor=\"let lga of lgas \" [value]=\"lga\">\r\n                                {{ lga.name }}\r\n                            </mat-option>\r\n                        </mat-select>\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-icon\r\n                            class=\"adf-textitem-icon adf-update-icon\"\r\n                            (click)=\"update()\"\r\n                            [attr.title]=\"'CORE.METADATA.ACTIONS.SAVE' | translate\"\r\n                            [attr.data-automation-id]=\"'card-textitem-update-' + property.key\">done</mat-icon>\r\n                    <mat-icon\r\n                            class=\"adf-textitem-icon adf-reset-icon\"\r\n                            (click)=\"reset()\"\r\n                            [attr.title]=\"'CORE.METADATA.ACTIONS.CANCEL' | translate\"\r\n                            [attr.data-automation-id]=\"'card-textitem-reset-' + property.key\">clear</mat-icon>\r\n                </div>\r\n            </div>\r\n            <mat-error [attr.data-automation-id]=\"'card-textitem-error-' + property.key\"\r\n                       class=\"adf-textitem-editable-error\"\r\n                       *ngIf=\"hasErrors()\">\r\n                <ul>\r\n                    <li *ngFor=\"let errorMessage of errorMessages\">{{ errorMessage | translate }}</li>\r\n                </ul>\r\n            </mat-error>\r\n        </div>\r\n    </span>\r\n    <ng-template #elseEmptyValueBlock>\r\n        <span class=\"adf-textitem-default-value\">{{ property.default | translate }}</span>\r\n    </ng-template>\r\n</div>"
    })
], CardViewAddressItemComponent);
export { CardViewAddressItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWFkZHJlc3MtaXRlbS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvdXRpbC9jYXJkLXZpZXcvY29tcG9uZW50cy9jYXJkLXZpZXctYWRkcmVzcy1pdGVtL2NhcmQtdmlldy1hZGRyZXNzLWl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBcUIsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRzNELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUMxRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDakUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBUXJFLElBQWEsNEJBQTRCLEdBQXpDLE1BQWEsNEJBQTRCO0lBOEJyQyxZQUFvQixxQkFBNEMsRUFDNUMsWUFBMEIsRUFDMUIsVUFBc0I7UUFGdEIsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUF1QjtRQUM1QyxpQkFBWSxHQUFaLFlBQVksQ0FBYztRQUMxQixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBMUIxQyxhQUFRLEdBQVksS0FBSyxDQUFDO1FBRzFCLGlCQUFZLEdBQVksSUFBSSxDQUFDO1FBYzdCLFdBQU0sR0FBWSxLQUFLLENBQUM7SUFVeEIsQ0FBQztJQUVELFdBQVc7UUFDUCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztRQUNqRCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztRQUNqRCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztRQUMzQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztRQUN6QyxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDaEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDO1NBQzlDO0lBQ0wsQ0FBQztJQUVELFFBQVE7SUFDUixDQUFDO0lBRUQsUUFBUSxDQUFDLEtBQXNCO1FBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDdkYsQ0FBQztJQUVELFlBQVk7UUFDUixPQUFPLElBQUksQ0FBQyxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3pELENBQUM7SUFFRCxVQUFVO1FBQ04sT0FBTyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO0lBQ25ELENBQUM7SUFFRCxXQUFXO1FBQ1AsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQztJQUNuQyxDQUFDO0lBRUQsT0FBTztRQUNILE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO0lBQ2hDLENBQUM7SUFFRCxTQUFTO1FBQ0wsT0FBTyxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDO0lBQzNELENBQUM7SUFFRCxXQUFXLENBQUMsVUFBbUI7UUFDM0IsSUFBSSxVQUFVLEVBQUU7WUFDWixJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzFFO1FBQ0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUM7UUFDekIsVUFBVSxDQUFDLEdBQUcsRUFBRTtZQUNaLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtnQkFDaEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDeEM7UUFDTCxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDTixVQUFVLENBQUMsR0FBRyxFQUFFO1lBQ1osSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO2dCQUNuQixJQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUMzQztRQUNMLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNOLFVBQVUsQ0FBQyxHQUFHLEVBQUU7WUFDWixJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7Z0JBQ25CLElBQUksQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO2FBQzNDO1FBQ0wsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ1YsQ0FBQztJQUVELEtBQUs7UUFDRCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztRQUNqRCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztRQUNqRCxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztRQUMzQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztRQUN6QyxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDaEIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDO1NBQzlDO1FBQ0QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRUQsTUFBTTtRQUNGLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUU7WUFDN0csSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUMzQyxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQztZQUMxRixJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssR0FBRyxJQUFJLE9BQU8sQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDM0csSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMzQjthQUFNO1lBQ0gsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLElBQUksT0FBTyxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDO1NBQ2hKO0lBQ0wsQ0FBQztJQUVELElBQUksWUFBWTtRQUNaLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUM7SUFDdEMsQ0FBQztJQUVELE9BQU87UUFDSCxJQUFJLENBQUMscUJBQXFCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBRUQsYUFBYSxDQUFDLEVBQU8sRUFBRSxFQUFPO1FBQzFCLE9BQU8sRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFLEtBQUssRUFBRSxDQUFDO0lBQ2pELENBQUM7Q0FDSixDQUFBOztZQWpHOEMscUJBQXFCO1lBQzlCLFlBQVk7WUFDZCxVQUFVOztBQTdCMUM7SUFEQyxLQUFLLEVBQUU7OERBQzJCO0FBR25DO0lBREMsS0FBSyxFQUFFOzhEQUNrQjtBQUcxQjtJQURDLEtBQUssRUFBRTtrRUFDcUI7QUFHN0I7SUFEQyxTQUFTLENBQUMsV0FBVyxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQyxDQUFDOytEQUNoQjtBQUd2QjtJQURDLFNBQVMsQ0FBQyxjQUFjLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDLENBQUM7a0VBQ2hCO0FBRzFCO0lBREMsU0FBUyxDQUFDLGNBQWMsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUMsQ0FBQztrRUFDaEI7QUFsQmpCLDRCQUE0QjtJQUp4QyxTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsd0JBQXdCO1FBQ2xDLGc3TUFBc0Q7S0FDekQsQ0FBQztHQUNXLDRCQUE0QixDQStIeEM7U0EvSFksNEJBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25DaGFuZ2VzLCBPbkluaXQsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDYXJkVmlld1VwZGF0ZVNlcnZpY2UgfSBmcm9tICdAYWxmcmVzY28vYWRmLWNvcmUnO1xyXG5pbXBvcnQgeyBDYXJkVmlld0FkZHJlc3NJdGVtTW9kZWwgfSBmcm9tICcuLi8uLi9tb2RlbHMvY2FyZC12aWV3LWFkZHJlc3MtaXRlbS5tb2RlbCc7XHJcbmltcG9ydCB7IE1hdFNlbGVjdENoYW5nZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgQWRkcmVzcyB9IGZyb20gJy4uLy4uLy4uLy4uL21vZGVsL2FkZHJlc3MubW9kZWwnO1xyXG5pbXBvcnQgeyBMZ2FTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vc2VydmljZXMvbGdhLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTdGF0ZVNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi9zZXJ2aWNlcy9zdGF0ZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSUxHQSB9IGZyb20gJy4uLy4uLy4uLy4uL21vZGVsL2xnYS5tb2RlbCc7XHJcbmltcG9ydCB7IElTdGF0ZSB9IGZyb20gJy4uLy4uLy4uLy4uL21vZGVsL3N0YXRlLm1vZGVsJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdjYXJkLXZpZXctYWRkcmVzcy1pdGVtJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9jYXJkLXZpZXctYWRkcmVzcy1pdGVtLmNvbXBvbmVudC5odG1sJyxcclxufSlcclxuZXhwb3J0IGNsYXNzIENhcmRWaWV3QWRkcmVzc0l0ZW1Db21wb25lbnQgaW1wbGVtZW50cyBPbkNoYW5nZXMsIE9uSW5pdCB7XHJcbiAgICBzdGF0ZTogSVN0YXRlO1xyXG4gICAgQElucHV0KClcclxuICAgIHByb3BlcnR5OiBDYXJkVmlld0FkZHJlc3NJdGVtTW9kZWw7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIGVkaXRhYmxlOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIGRpc3BsYXlFbXB0eTogYm9vbGVhbiA9IHRydWU7XHJcblxyXG4gICAgQFZpZXdDaGlsZCgnY2l0eUlucHV0Jywge3N0YXRpYzogdHJ1ZX0pXHJcbiAgICBwcml2YXRlIGNpdHlJbnB1dDogYW55O1xyXG5cclxuICAgIEBWaWV3Q2hpbGQoJ3N0cmVldDFJbnB1dCcsIHtzdGF0aWM6IHRydWV9KVxyXG4gICAgcHJpdmF0ZSBzdHJlZXQxSW5wdXQ6IGFueTtcclxuXHJcbiAgICBAVmlld0NoaWxkKCdzdHJlZXQySW5wdXQnLCB7c3RhdGljOiB0cnVlfSlcclxuICAgIHByaXZhdGUgc3RyZWV0MklucHV0OiBhbnk7XHJcblxyXG4gICAgc3RhdGVzOiBJU3RhdGVbXTtcclxuICAgIGxnYXM6IElMR0FbXTtcclxuXHJcbiAgICBpbkVkaXQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGVkaXRlZFN0cmVldDE6IHN0cmluZztcclxuICAgIGVkaXRlZFN0cmVldDI6IHN0cmluZztcclxuICAgIGVkaXRlZENpdHk6IHN0cmluZztcclxuICAgIGVkaXRlZExnYTogSUxHQTtcclxuICAgIGVycm9yTWVzc2FnZXM6IHN0cmluZ1tdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgY2FyZFZpZXdVcGRhdGVTZXJ2aWNlOiBDYXJkVmlld1VwZGF0ZVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHN0YXRlU2VydmljZTogU3RhdGVTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBsZ2FTZXJ2aWNlOiBMZ2FTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkNoYW5nZXMoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5lZGl0ZWRTdHJlZXQxID0gdGhpcy5wcm9wZXJ0eS52YWx1ZS5zdHJlZXQxO1xyXG4gICAgICAgIHRoaXMuZWRpdGVkU3RyZWV0MiA9IHRoaXMucHJvcGVydHkudmFsdWUuc3RyZWV0MjtcclxuICAgICAgICB0aGlzLmVkaXRlZENpdHkgPSB0aGlzLnByb3BlcnR5LnZhbHVlLmNpdHk7XHJcbiAgICAgICAgdGhpcy5lZGl0ZWRMZ2EgPSB0aGlzLnByb3BlcnR5LnZhbHVlLmxnYTtcclxuICAgICAgICBpZiAodGhpcy5lZGl0ZWRMZ2EpIHtcclxuICAgICAgICAgICAgdGhpcy5zdGF0ZSA9IHRoaXMucHJvcGVydHkudmFsdWUubGdhLnN0YXRlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgIH1cclxuXHJcbiAgICBvbkNoYW5nZShldmVudDogTWF0U2VsZWN0Q2hhbmdlKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5sZ2FTZXJ2aWNlLmZpbmRCeVN0YXRlKGV2ZW50LnZhbHVlLmlkKS5zdWJzY3JpYmUocmVzID0+IHRoaXMubGdhcyA9IHJlcy5ib2R5KTtcclxuICAgIH1cclxuXHJcbiAgICBzaG93UHJvcGVydHkoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZGlzcGxheUVtcHR5IHx8ICF0aGlzLnByb3BlcnR5LmlzRW1wdHkoKTtcclxuICAgIH1cclxuXHJcbiAgICBpc0VkaXRhYmxlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmVkaXRhYmxlICYmIHRoaXMucHJvcGVydHkuZWRpdGFibGU7XHJcbiAgICB9XHJcblxyXG4gICAgaXNDbGlja2FibGUoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMucHJvcGVydHkuY2xpY2thYmxlO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc0ljb24oKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuICEhdGhpcy5wcm9wZXJ0eS5pY29uO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc0Vycm9ycygpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmVycm9yTWVzc2FnZXMgJiYgdGhpcy5lcnJvck1lc3NhZ2VzLmxlbmd0aDtcclxuICAgIH1cclxuXHJcbiAgICBzZXRFZGl0TW9kZShlZGl0U3RhdHVzOiBib29sZWFuKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKGVkaXRTdGF0dXMpIHtcclxuICAgICAgICAgICAgdGhpcy5zdGF0ZVNlcnZpY2UuZ2V0U3RhdGVzKCkuc3Vic2NyaWJlKHJlcyA9PiB0aGlzLnN0YXRlcyA9IHJlcy5ib2R5KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5pbkVkaXQgPSBlZGl0U3RhdHVzO1xyXG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5jaXR5SW5wdXQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2l0eUlucHV0Lm5hdGl2ZUVsZW1lbnQuY2xpY2soKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sIDApO1xyXG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5zdHJlZXQxSW5wdXQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc3RyZWV0MUlucHV0Lm5hdGl2ZUVsZW1lbnQuY2xpY2soKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sIDApO1xyXG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5zdHJlZXQySW5wdXQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc3RyZWV0MklucHV0Lm5hdGl2ZUVsZW1lbnQuY2xpY2soKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sIDApO1xyXG4gICAgfVxyXG5cclxuICAgIHJlc2V0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuZWRpdGVkU3RyZWV0MSA9IHRoaXMucHJvcGVydHkudmFsdWUuc3RyZWV0MTtcclxuICAgICAgICB0aGlzLmVkaXRlZFN0cmVldDIgPSB0aGlzLnByb3BlcnR5LnZhbHVlLnN0cmVldDI7XHJcbiAgICAgICAgdGhpcy5lZGl0ZWRDaXR5ID0gdGhpcy5wcm9wZXJ0eS52YWx1ZS5jaXR5O1xyXG4gICAgICAgIHRoaXMuZWRpdGVkTGdhID0gdGhpcy5wcm9wZXJ0eS52YWx1ZS5sZ2E7XHJcbiAgICAgICAgaWYgKHRoaXMuZWRpdGVkTGdhKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc3RhdGUgPSB0aGlzLnByb3BlcnR5LnZhbHVlLmxnYS5zdGF0ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zZXRFZGl0TW9kZShmYWxzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlKCk6IHZvaWQge1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BlcnR5LmlzVmFsaWQobmV3IEFkZHJlc3ModGhpcy5lZGl0ZWRTdHJlZXQxLCB0aGlzLmVkaXRlZFN0cmVldDIsIHRoaXMuZWRpdGVkQ2l0eSwgdGhpcy5lZGl0ZWRMZ2EpKSkge1xyXG4gICAgICAgICAgICB0aGlzLmNhcmRWaWV3VXBkYXRlU2VydmljZS51cGRhdGUodGhpcy5wcm9wZXJ0eSxcclxuICAgICAgICAgICAgICAgIG5ldyBBZGRyZXNzKHRoaXMuZWRpdGVkU3RyZWV0MSwgdGhpcy5lZGl0ZWRTdHJlZXQyLCB0aGlzLmVkaXRlZENpdHksIHRoaXMuZWRpdGVkTGdhKSk7XHJcbiAgICAgICAgICAgIHRoaXMucHJvcGVydHkudmFsdWUgPSBuZXcgQWRkcmVzcyh0aGlzLmVkaXRlZFN0cmVldDEsIHRoaXMuZWRpdGVkU3RyZWV0MiwgdGhpcy5lZGl0ZWRDaXR5LCB0aGlzLmVkaXRlZExnYSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0RWRpdE1vZGUoZmFsc2UpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuZXJyb3JNZXNzYWdlcyA9IHRoaXMucHJvcGVydHkuZ2V0VmFsaWRhdGlvbkVycm9ycyhuZXcgQWRkcmVzcyh0aGlzLmVkaXRlZFN0cmVldDEsIHRoaXMuZWRpdGVkU3RyZWV0MiwgdGhpcy5lZGl0ZWRDaXR5LCB0aGlzLmVkaXRlZExnYSkpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXQgZGlzcGxheVZhbHVlKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnByb3BlcnR5LmRpc3BsYXlWYWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBjbGlja2VkKCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuY2FyZFZpZXdVcGRhdGVTZXJ2aWNlLmNsaWNrZWQodGhpcy5wcm9wZXJ0eSk7XHJcbiAgICB9XHJcblxyXG4gICAgZW50aXR5Q29tcGFyZShzMTogYW55LCBzMjogYW55KTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHMxICYmIHMyID8gczEuaWQgPT0gczIuaWQgOiBzMSA9PT0gczI7XHJcbiAgICB9XHJcbn1cclxuIl19