import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CardItemTypeService, CoreModule } from '@alfresco/adf-core';
import { CardViewHtmlTextItemComponent } from './components/card-view-html-text-item/card-view-html-text-item.component';
import { CardViewNameItemComponent } from './components/card-view-name-item/card-view-name-item.component';
import { CardViewAddressItemComponent } from './components/card-view-address-item/card-view-address-item.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CardViewFixedKeyvaluepairsitemComponent } from './components/card-view-fixed-keyvaluepairsitem/card-view-fixed-keyvaluepairsitem.component';
import { CardViewPhoneItemComponent } from './components/card-view-phone-item/card-view-phone-item.component';
export function components() {
    return [
        CardViewHtmlTextItemComponent,
        CardViewNameItemComponent,
        CardViewAddressItemComponent,
        CardViewFixedKeyvaluepairsitemComponent,
        CardViewPhoneItemComponent
    ];
}
let CardViewModule = class CardViewModule {
    constructor(cardItemTypeService) {
        this.cardItemTypeService = cardItemTypeService;
        cardItemTypeService.setComponentTypeResolver('html-text', () => CardViewHtmlTextItemComponent);
        cardItemTypeService.setComponentTypeResolver('phone', () => CardViewPhoneItemComponent);
        cardItemTypeService.setComponentTypeResolver('name', () => CardViewNameItemComponent);
        cardItemTypeService.setComponentTypeResolver('address', () => CardViewAddressItemComponent);
        cardItemTypeService.setComponentTypeResolver('fixedkeyvaluepairs', () => CardViewFixedKeyvaluepairsitemComponent);
    }
};
CardViewModule.ctorParameters = () => [
    { type: CardItemTypeService }
];
CardViewModule = tslib_1.__decorate([
    NgModule({
        imports: [
            CoreModule,
            CommonModule,
            FormsModule,
            FlexLayoutModule
        ],
        declarations: components(),
        exports: components(),
        entryComponents: components(),
        providers: [
            CardItemTypeService
        ]
    })
], CardViewModule);
export { CardViewModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC91dGlsL2NhcmQtdmlldy9jYXJkLXZpZXcubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxVQUFVLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNyRSxPQUFPLEVBQUUsNkJBQTZCLEVBQUUsTUFBTSwwRUFBMEUsQ0FBQztBQUN6SCxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSxnRUFBZ0UsQ0FBQztBQUMzRyxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSxzRUFBc0UsQ0FBQztBQUNwSCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSx1Q0FBdUMsRUFBRSxNQUFNLDRGQUE0RixDQUFDO0FBQ3JKLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLGtFQUFrRSxDQUFDO0FBRTlHLE1BQU0sVUFBVSxVQUFVO0lBQ3RCLE9BQU87UUFDSCw2QkFBNkI7UUFDN0IseUJBQXlCO1FBQ3pCLDRCQUE0QjtRQUM1Qix1Q0FBdUM7UUFDdkMsMEJBQTBCO0tBQzdCLENBQUE7QUFDTCxDQUFDO0FBZ0JELElBQWEsY0FBYyxHQUEzQixNQUFhLGNBQWM7SUFDdkIsWUFBb0IsbUJBQXdDO1FBQXhDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEQsbUJBQW1CLENBQUMsd0JBQXdCLENBQUMsV0FBVyxFQUFFLEdBQUcsRUFBRSxDQUFDLDZCQUE2QixDQUFDLENBQUM7UUFDL0YsbUJBQW1CLENBQUMsd0JBQXdCLENBQUMsT0FBTyxFQUFFLEdBQUcsRUFBRSxDQUFDLDBCQUEwQixDQUFDLENBQUM7UUFDeEYsbUJBQW1CLENBQUMsd0JBQXdCLENBQUMsTUFBTSxFQUFFLEdBQUcsRUFBRSxDQUFDLHlCQUF5QixDQUFDLENBQUM7UUFDdEYsbUJBQW1CLENBQUMsd0JBQXdCLENBQUMsU0FBUyxFQUFFLEdBQUcsRUFBRSxDQUFDLDRCQUE0QixDQUFDLENBQUM7UUFDNUYsbUJBQW1CLENBQUMsd0JBQXdCLENBQUMsb0JBQW9CLEVBQUUsR0FBRyxFQUFFLENBQUMsdUNBQXVDLENBQUMsQ0FBQztJQUN0SCxDQUFDO0NBQ0osQ0FBQTs7WUFQNEMsbUJBQW1COztBQURuRCxjQUFjO0lBZDFCLFFBQVEsQ0FBQztRQUNOLE9BQU8sRUFBRTtZQUNMLFVBQVU7WUFDVixZQUFZO1lBQ1osV0FBVztZQUNYLGdCQUFnQjtTQUNuQjtRQUNELFlBQVksRUFBRSxVQUFVLEVBQUU7UUFDMUIsT0FBTyxFQUFFLFVBQVUsRUFBRTtRQUNyQixlQUFlLEVBQUUsVUFBVSxFQUFFO1FBQzdCLFNBQVMsRUFBRTtZQUNQLG1CQUFtQjtTQUN0QjtLQUNKLENBQUM7R0FDVyxjQUFjLENBUTFCO1NBUlksY0FBYyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENhcmRJdGVtVHlwZVNlcnZpY2UsIENvcmVNb2R1bGUgfSBmcm9tICdAYWxmcmVzY28vYWRmLWNvcmUnO1xyXG5pbXBvcnQgeyBDYXJkVmlld0h0bWxUZXh0SXRlbUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jYXJkLXZpZXctaHRtbC10ZXh0LWl0ZW0vY2FyZC12aWV3LWh0bWwtdGV4dC1pdGVtLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENhcmRWaWV3TmFtZUl0ZW1Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY2FyZC12aWV3LW5hbWUtaXRlbS9jYXJkLXZpZXctbmFtZS1pdGVtLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENhcmRWaWV3QWRkcmVzc0l0ZW1Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY2FyZC12aWV3LWFkZHJlc3MtaXRlbS9jYXJkLXZpZXctYWRkcmVzcy1pdGVtLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZsZXhMYXlvdXRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mbGV4LWxheW91dCc7XHJcbmltcG9ydCB7IEZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBDYXJkVmlld0ZpeGVkS2V5dmFsdWVwYWlyc2l0ZW1Db21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY2FyZC12aWV3LWZpeGVkLWtleXZhbHVlcGFpcnNpdGVtL2NhcmQtdmlldy1maXhlZC1rZXl2YWx1ZXBhaXJzaXRlbS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDYXJkVmlld1Bob25lSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jYXJkLXZpZXctcGhvbmUtaXRlbS9jYXJkLXZpZXctcGhvbmUtaXRlbS5jb21wb25lbnQnO1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIGNvbXBvbmVudHMoKSB7XHJcbiAgICByZXR1cm4gW1xyXG4gICAgICAgIENhcmRWaWV3SHRtbFRleHRJdGVtQ29tcG9uZW50LFxyXG4gICAgICAgIENhcmRWaWV3TmFtZUl0ZW1Db21wb25lbnQsXHJcbiAgICAgICAgQ2FyZFZpZXdBZGRyZXNzSXRlbUNvbXBvbmVudCxcclxuICAgICAgICBDYXJkVmlld0ZpeGVkS2V5dmFsdWVwYWlyc2l0ZW1Db21wb25lbnQsXHJcbiAgICAgICAgQ2FyZFZpZXdQaG9uZUl0ZW1Db21wb25lbnRcclxuICAgIF1cclxufVxyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBDb3JlTW9kdWxlLFxyXG4gICAgICAgIENvbW1vbk1vZHVsZSxcclxuICAgICAgICBGb3Jtc01vZHVsZSxcclxuICAgICAgICBGbGV4TGF5b3V0TW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBjb21wb25lbnRzKCksXHJcbiAgICBleHBvcnRzOiBjb21wb25lbnRzKCksXHJcbiAgICBlbnRyeUNvbXBvbmVudHM6IGNvbXBvbmVudHMoKSxcclxuICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgIENhcmRJdGVtVHlwZVNlcnZpY2VcclxuICAgIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIENhcmRWaWV3TW9kdWxlIHtcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgY2FyZEl0ZW1UeXBlU2VydmljZTogQ2FyZEl0ZW1UeXBlU2VydmljZSkge1xyXG4gICAgICAgIGNhcmRJdGVtVHlwZVNlcnZpY2Uuc2V0Q29tcG9uZW50VHlwZVJlc29sdmVyKCdodG1sLXRleHQnLCAoKSA9PiBDYXJkVmlld0h0bWxUZXh0SXRlbUNvbXBvbmVudCk7XHJcbiAgICAgICAgY2FyZEl0ZW1UeXBlU2VydmljZS5zZXRDb21wb25lbnRUeXBlUmVzb2x2ZXIoJ3Bob25lJywgKCkgPT4gQ2FyZFZpZXdQaG9uZUl0ZW1Db21wb25lbnQpO1xyXG4gICAgICAgIGNhcmRJdGVtVHlwZVNlcnZpY2Uuc2V0Q29tcG9uZW50VHlwZVJlc29sdmVyKCduYW1lJywgKCkgPT4gQ2FyZFZpZXdOYW1lSXRlbUNvbXBvbmVudCk7XHJcbiAgICAgICAgY2FyZEl0ZW1UeXBlU2VydmljZS5zZXRDb21wb25lbnRUeXBlUmVzb2x2ZXIoJ2FkZHJlc3MnLCAoKSA9PiBDYXJkVmlld0FkZHJlc3NJdGVtQ29tcG9uZW50KTtcclxuICAgICAgICBjYXJkSXRlbVR5cGVTZXJ2aWNlLnNldENvbXBvbmVudFR5cGVSZXNvbHZlcignZml4ZWRrZXl2YWx1ZXBhaXJzJywgKCkgPT4gQ2FyZFZpZXdGaXhlZEtleXZhbHVlcGFpcnNpdGVtQ29tcG9uZW50KTtcclxuICAgIH1cclxufVxyXG4iXX0=