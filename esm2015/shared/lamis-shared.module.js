import * as tslib_1 from "tslib";
import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatDialogModule, MatIconModule, MatProgressSpinnerModule, MatTooltipModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
import { SharedCommonModule } from './shared-common.module';
import { AppConfirmComponent } from './util/app-confirm/app-confirm.component';
import { AppLoaderComponent } from './util/app-loader/app-loader.component';
import { AppLoaderService } from './util/app-loader/app-loader.service';
import { ToggleFullscreenDirective } from './util/fullscreen/toggle.fullscreen.directive';
import { SpeedDialFabComponent } from './util/speed-dial-fab.component';
let LamisSharedModule = class LamisSharedModule {
};
LamisSharedModule = tslib_1.__decorate([
    NgModule({
        imports: [
            CommonModule,
            FormsModule,
            ReactiveFormsModule,
            RouterModule,
            SharedCommonModule,
            MatIconModule,
            MatButtonModule,
            MatTooltipModule,
            MatProgressSpinnerModule,
            MatDialogModule
        ],
        declarations: [
            HasAnyAuthorityDirective,
            SpeedDialFabComponent,
            AppConfirmComponent,
            AppLoaderComponent,
            ToggleFullscreenDirective
        ],
        entryComponents: [
            AppConfirmComponent,
            AppLoaderComponent
        ],
        exports: [
            SharedCommonModule,
            HasAnyAuthorityDirective,
            SpeedDialFabComponent,
            ToggleFullscreenDirective
        ],
        providers: [
            AppLoaderService,
            AppConfirmComponent
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
], LamisSharedModule);
export { LamisSharedModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGFtaXMtc2hhcmVkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC9sYW1pcy1zaGFyZWQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLHNCQUFzQixFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNqRSxPQUFPLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDbEUsT0FBTyxFQUFFLGVBQWUsRUFBRSxlQUFlLEVBQUUsYUFBYSxFQUFFLHdCQUF3QixFQUFFLGdCQUFnQixFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDaEksT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzVELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQzVFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQzFGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBc0N4RSxJQUFhLGlCQUFpQixHQUE5QixNQUFhLGlCQUFpQjtDQUM3QixDQUFBO0FBRFksaUJBQWlCO0lBcEM3QixRQUFRLENBQUM7UUFDTixPQUFPLEVBQUU7WUFDTCxZQUFZO1lBQ1osV0FBVztZQUNYLG1CQUFtQjtZQUNuQixZQUFZO1lBQ1osa0JBQWtCO1lBQ2xCLGFBQWE7WUFDYixlQUFlO1lBQ2YsZ0JBQWdCO1lBQ2hCLHdCQUF3QjtZQUN4QixlQUFlO1NBQ2xCO1FBQ0QsWUFBWSxFQUFFO1lBQ1Ysd0JBQXdCO1lBQ3hCLHFCQUFxQjtZQUNyQixtQkFBbUI7WUFDbkIsa0JBQWtCO1lBQ2xCLHlCQUF5QjtTQUM1QjtRQUNELGVBQWUsRUFBRTtZQUNiLG1CQUFtQjtZQUNuQixrQkFBa0I7U0FDckI7UUFDRCxPQUFPLEVBQUU7WUFDTCxrQkFBa0I7WUFDbEIsd0JBQXdCO1lBQ3hCLHFCQUFxQjtZQUNyQix5QkFBeUI7U0FDNUI7UUFDRCxTQUFTLEVBQUU7WUFDUCxnQkFBZ0I7WUFDaEIsbUJBQW1CO1NBQ3RCO1FBQ0QsT0FBTyxFQUFFLENBQUMsc0JBQXNCLENBQUM7S0FDcEMsQ0FBQztHQUNXLGlCQUFpQixDQUM3QjtTQURZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IENVU1RPTV9FTEVNRU5UU19TQ0hFTUEsIE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBNYXRCdXR0b25Nb2R1bGUsIE1hdERpYWxvZ01vZHVsZSwgTWF0SWNvbk1vZHVsZSwgTWF0UHJvZ3Jlc3NTcGlubmVyTW9kdWxlLCBNYXRUb29sdGlwTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBIYXNBbnlBdXRob3JpdHlEaXJlY3RpdmUgfSBmcm9tICcuL2F1dGgvaGFzLWFueS1hdXRob3JpdHkuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgU2hhcmVkQ29tbW9uTW9kdWxlIH0gZnJvbSAnLi9zaGFyZWQtY29tbW9uLm1vZHVsZSc7XHJcbmltcG9ydCB7IEFwcENvbmZpcm1Db21wb25lbnQgfSBmcm9tICcuL3V0aWwvYXBwLWNvbmZpcm0vYXBwLWNvbmZpcm0uY29tcG9uZW50JztcclxuaW1wb3J0IHsgQXBwTG9hZGVyQ29tcG9uZW50IH0gZnJvbSAnLi91dGlsL2FwcC1sb2FkZXIvYXBwLWxvYWRlci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBBcHBMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi91dGlsL2FwcC1sb2FkZXIvYXBwLWxvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVG9nZ2xlRnVsbHNjcmVlbkRpcmVjdGl2ZSB9IGZyb20gJy4vdXRpbC9mdWxsc2NyZWVuL3RvZ2dsZS5mdWxsc2NyZWVuLmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IFNwZWVkRGlhbEZhYkNvbXBvbmVudCB9IGZyb20gJy4vdXRpbC9zcGVlZC1kaWFsLWZhYi5jb21wb25lbnQnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgRm9ybXNNb2R1bGUsXHJcbiAgICAgICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcclxuICAgICAgICBSb3V0ZXJNb2R1bGUsXHJcbiAgICAgICAgU2hhcmVkQ29tbW9uTW9kdWxlLFxyXG4gICAgICAgIE1hdEljb25Nb2R1bGUsXHJcbiAgICAgICAgTWF0QnV0dG9uTW9kdWxlLFxyXG4gICAgICAgIE1hdFRvb2x0aXBNb2R1bGUsXHJcbiAgICAgICAgTWF0UHJvZ3Jlc3NTcGlubmVyTW9kdWxlLFxyXG4gICAgICAgIE1hdERpYWxvZ01vZHVsZVxyXG4gICAgXSxcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIEhhc0FueUF1dGhvcml0eURpcmVjdGl2ZSxcclxuICAgICAgICBTcGVlZERpYWxGYWJDb21wb25lbnQsXHJcbiAgICAgICAgQXBwQ29uZmlybUNvbXBvbmVudCxcclxuICAgICAgICBBcHBMb2FkZXJDb21wb25lbnQsXHJcbiAgICAgICAgVG9nZ2xlRnVsbHNjcmVlbkRpcmVjdGl2ZVxyXG4gICAgXSxcclxuICAgIGVudHJ5Q29tcG9uZW50czogW1xyXG4gICAgICAgIEFwcENvbmZpcm1Db21wb25lbnQsXHJcbiAgICAgICAgQXBwTG9hZGVyQ29tcG9uZW50XHJcbiAgICBdLFxyXG4gICAgZXhwb3J0czogW1xyXG4gICAgICAgIFNoYXJlZENvbW1vbk1vZHVsZSxcclxuICAgICAgICBIYXNBbnlBdXRob3JpdHlEaXJlY3RpdmUsXHJcbiAgICAgICAgU3BlZWREaWFsRmFiQ29tcG9uZW50LFxyXG4gICAgICAgIFRvZ2dsZUZ1bGxzY3JlZW5EaXJlY3RpdmVcclxuICAgIF0sXHJcbiAgICBwcm92aWRlcnM6IFtcclxuICAgICAgICBBcHBMb2FkZXJTZXJ2aWNlLFxyXG4gICAgICAgIEFwcENvbmZpcm1Db21wb25lbnRcclxuICAgIF0sXHJcbiAgICBzY2hlbWFzOiBbQ1VTVE9NX0VMRU1FTlRTX1NDSEVNQV1cclxufSlcclxuZXhwb3J0IGNsYXNzIExhbWlzU2hhcmVkTW9kdWxlIHtcclxufVxyXG4iXX0=