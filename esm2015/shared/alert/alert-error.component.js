import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { NotificationService } from '@alfresco/adf-core';
let AlertErrorComponent = class AlertErrorComponent {
    constructor(notification, eventManager) {
        this.notification = notification;
        this.eventManager = eventManager;
        /* tslint:enable */
        this.cleanHttpErrorListener = eventManager.subscribe('app.httpError', response => {
            let i;
            const httpErrorResponse = response.content;
            switch (httpErrorResponse.status) {
                // connection refused, server not reachable
                case 0:
                    this.addErrorAlert('Server not reachable', 'error.server.not.reachable');
                    break;
                case 400:
                    const arr = httpErrorResponse.headers.keys();
                    let errorHeader = null;
                    let entityKey = null;
                    arr.forEach(entry => {
                        if (entry.endsWith('app-error')) {
                            errorHeader = httpErrorResponse.headers.get(entry);
                        }
                        else if (entry.endsWith('app-params')) {
                            entityKey = httpErrorResponse.headers.get(entry);
                        }
                    });
                    if (errorHeader) {
                        this.addErrorAlert(errorHeader, errorHeader, { entityName: entityKey });
                    }
                    else if (httpErrorResponse.error !== '' && httpErrorResponse.error.fieldErrors) {
                        const fieldErrors = httpErrorResponse.error.fieldErrors;
                        for (i = 0; i < fieldErrors.length; i++) {
                            const fieldError = fieldErrors[i];
                            // convert 'something[14].other[4].id' to 'something[].other[].id' so translations can be written to it
                            const convertedField = fieldError.field.replace(/\[\d*\]/g, '[]');
                            const fieldName = convertedField.charAt(0).toUpperCase() + convertedField.slice(1);
                            this.addErrorAlert('Error on field "' + fieldName + '"', 'error.' + fieldError.message, { fieldName });
                        }
                    }
                    else if (httpErrorResponse.error !== '' && httpErrorResponse.error.message) {
                        this.addErrorAlert(httpErrorResponse.error.message, httpErrorResponse.error.message, httpErrorResponse.error.params);
                    }
                    else {
                        this.addErrorAlert(httpErrorResponse.error);
                    }
                    break;
                case 404:
                    this.addErrorAlert('Not found', 'error.url.not.found');
                    break;
                default:
                    if (httpErrorResponse.error !== '' && httpErrorResponse.error.message) {
                        this.addErrorAlert(httpErrorResponse.error.message);
                    }
                    else {
                        this.addErrorAlert(httpErrorResponse.error);
                    }
            }
        });
    }
    ngOnDestroy() {
        if (this.cleanHttpErrorListener !== undefined && this.cleanHttpErrorListener !== null) {
            this.eventManager.destroy(this.cleanHttpErrorListener);
        }
    }
    addErrorAlert(message, key, data) {
        this.notification.showError(message);
    }
};
AlertErrorComponent.ctorParameters = () => [
    { type: NotificationService },
    { type: JhiEventManager }
];
AlertErrorComponent = tslib_1.__decorate([
    Component({
        selector: 'alert-error',
        template: ``
    })
], AlertErrorComponent);
export { AlertErrorComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxlcnQtZXJyb3IuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL2FsZXJ0L2FsZXJ0LWVycm9yLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUNyRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBRTlDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBTXpELElBQWEsbUJBQW1CLEdBQWhDLE1BQWEsbUJBQW1CO0lBRzVCLFlBQW9CLFlBQWlDLEVBQVUsWUFBNkI7UUFBeEUsaUJBQVksR0FBWixZQUFZLENBQXFCO1FBQVUsaUJBQVksR0FBWixZQUFZLENBQWlCO1FBQ3hGLG1CQUFtQjtRQUNuQixJQUFJLENBQUMsc0JBQXNCLEdBQUcsWUFBWSxDQUFDLFNBQVMsQ0FBQyxlQUFlLEVBQUUsUUFBUSxDQUFDLEVBQUU7WUFDN0UsSUFBSSxDQUFDLENBQUM7WUFDTixNQUFNLGlCQUFpQixHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUM7WUFDM0MsUUFBUSxpQkFBaUIsQ0FBQyxNQUFNLEVBQUU7Z0JBQzlCLDJDQUEyQztnQkFDM0MsS0FBSyxDQUFDO29CQUNGLElBQUksQ0FBQyxhQUFhLENBQUMsc0JBQXNCLEVBQUUsNEJBQTRCLENBQUMsQ0FBQztvQkFDekUsTUFBTTtnQkFFVixLQUFLLEdBQUc7b0JBQ0osTUFBTSxHQUFHLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO29CQUM3QyxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUM7b0JBQ3ZCLElBQUksU0FBUyxHQUFHLElBQUksQ0FBQztvQkFDckIsR0FBRyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBRTt3QkFDaEIsSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxFQUFFOzRCQUM3QixXQUFXLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDdEQ7NkJBQU0sSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxFQUFFOzRCQUNyQyxTQUFTLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDcEQ7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxXQUFXLEVBQUU7d0JBQ2IsSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsV0FBVyxFQUFFLEVBQUMsVUFBVSxFQUFFLFNBQVMsRUFBQyxDQUFDLENBQUM7cUJBQ3pFO3lCQUFNLElBQUksaUJBQWlCLENBQUMsS0FBSyxLQUFLLEVBQUUsSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFO3dCQUM5RSxNQUFNLFdBQVcsR0FBRyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDO3dCQUN4RCxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7NEJBQ3JDLE1BQU0sVUFBVSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDbEMsdUdBQXVHOzRCQUN2RyxNQUFNLGNBQWMsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7NEJBQ2xFLE1BQU0sU0FBUyxHQUFHLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLEdBQUcsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDbkYsSUFBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsR0FBRyxTQUFTLEdBQUcsR0FBRyxFQUFFLFFBQVEsR0FBRyxVQUFVLENBQUMsT0FBTyxFQUFFLEVBQUMsU0FBUyxFQUFDLENBQUMsQ0FBQzt5QkFDeEc7cUJBQ0o7eUJBQU0sSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLEtBQUssRUFBRSxJQUFJLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUU7d0JBQzFFLElBQUksQ0FBQyxhQUFhLENBQ2QsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFDL0IsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFDL0IsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FDakMsQ0FBQztxQkFDTDt5QkFBTTt3QkFDSCxJQUFJLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUMvQztvQkFDRCxNQUFNO2dCQUVWLEtBQUssR0FBRztvQkFDSixJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxxQkFBcUIsQ0FBQyxDQUFDO29CQUN2RCxNQUFNO2dCQUVWO29CQUNJLElBQUksaUJBQWlCLENBQUMsS0FBSyxLQUFLLEVBQUUsSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFO3dCQUNuRSxJQUFJLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztxQkFDdkQ7eUJBQU07d0JBQ0gsSUFBSSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDL0M7YUFDUjtRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELFdBQVc7UUFDUCxJQUFJLElBQUksQ0FBQyxzQkFBc0IsS0FBSyxTQUFTLElBQUksSUFBSSxDQUFDLHNCQUFzQixLQUFLLElBQUksRUFBRTtZQUNuRixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsc0JBQXNCLENBQUMsQ0FBQztTQUMxRDtJQUNMLENBQUM7SUFFRCxhQUFhLENBQUMsT0FBTyxFQUFFLEdBQUksRUFBRSxJQUFLO1FBQzlCLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3pDLENBQUM7Q0FDSixDQUFBOztZQW5FcUMsbUJBQW1CO1lBQXdCLGVBQWU7O0FBSG5GLG1CQUFtQjtJQUovQixTQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsYUFBYTtRQUN2QixRQUFRLEVBQUUsRUFBRTtLQUNmLENBQUM7R0FDVyxtQkFBbUIsQ0FzRS9CO1NBdEVZLG1CQUFtQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEpoaUV2ZW50TWFuYWdlciB9IGZyb20gJ25nLWpoaXBzdGVyJztcclxuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICdAYWxmcmVzY28vYWRmLWNvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FsZXJ0LWVycm9yJyxcclxuICAgIHRlbXBsYXRlOiBgYFxyXG59KVxyXG5leHBvcnQgY2xhc3MgQWxlcnRFcnJvckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uRGVzdHJveSB7XHJcbiAgICBjbGVhbkh0dHBFcnJvckxpc3RlbmVyOiBTdWJzY3JpcHRpb247XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBub3RpZmljYXRpb246IE5vdGlmaWNhdGlvblNlcnZpY2UsIHByaXZhdGUgZXZlbnRNYW5hZ2VyOiBKaGlFdmVudE1hbmFnZXIpIHtcclxuICAgICAgICAvKiB0c2xpbnQ6ZW5hYmxlICovXHJcbiAgICAgICAgdGhpcy5jbGVhbkh0dHBFcnJvckxpc3RlbmVyID0gZXZlbnRNYW5hZ2VyLnN1YnNjcmliZSgnYXBwLmh0dHBFcnJvcicsIHJlc3BvbnNlID0+IHtcclxuICAgICAgICAgICAgbGV0IGk7XHJcbiAgICAgICAgICAgIGNvbnN0IGh0dHBFcnJvclJlc3BvbnNlID0gcmVzcG9uc2UuY29udGVudDtcclxuICAgICAgICAgICAgc3dpdGNoIChodHRwRXJyb3JSZXNwb25zZS5zdGF0dXMpIHtcclxuICAgICAgICAgICAgICAgIC8vIGNvbm5lY3Rpb24gcmVmdXNlZCwgc2VydmVyIG5vdCByZWFjaGFibGVcclxuICAgICAgICAgICAgICAgIGNhc2UgMDpcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZEVycm9yQWxlcnQoJ1NlcnZlciBub3QgcmVhY2hhYmxlJywgJ2Vycm9yLnNlcnZlci5ub3QucmVhY2hhYmxlJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICAgICAgY2FzZSA0MDA6XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgYXJyID0gaHR0cEVycm9yUmVzcG9uc2UuaGVhZGVycy5rZXlzKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IGVycm9ySGVhZGVyID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgZW50aXR5S2V5ID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICBhcnIuZm9yRWFjaChlbnRyeSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChlbnRyeS5lbmRzV2l0aCgnYXBwLWVycm9yJykpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVycm9ySGVhZGVyID0gaHR0cEVycm9yUmVzcG9uc2UuaGVhZGVycy5nZXQoZW50cnkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGVudHJ5LmVuZHNXaXRoKCdhcHAtcGFyYW1zJykpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVudGl0eUtleSA9IGh0dHBFcnJvclJlc3BvbnNlLmhlYWRlcnMuZ2V0KGVudHJ5KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChlcnJvckhlYWRlcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZEVycm9yQWxlcnQoZXJyb3JIZWFkZXIsIGVycm9ySGVhZGVyLCB7ZW50aXR5TmFtZTogZW50aXR5S2V5fSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChodHRwRXJyb3JSZXNwb25zZS5lcnJvciAhPT0gJycgJiYgaHR0cEVycm9yUmVzcG9uc2UuZXJyb3IuZmllbGRFcnJvcnMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZmllbGRFcnJvcnMgPSBodHRwRXJyb3JSZXNwb25zZS5lcnJvci5maWVsZEVycm9ycztcclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9yIChpID0gMDsgaSA8IGZpZWxkRXJyb3JzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBmaWVsZEVycm9yID0gZmllbGRFcnJvcnNbaV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvLyBjb252ZXJ0ICdzb21ldGhpbmdbMTRdLm90aGVyWzRdLmlkJyB0byAnc29tZXRoaW5nW10ub3RoZXJbXS5pZCcgc28gdHJhbnNsYXRpb25zIGNhbiBiZSB3cml0dGVuIHRvIGl0XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBjb252ZXJ0ZWRGaWVsZCA9IGZpZWxkRXJyb3IuZmllbGQucmVwbGFjZSgvXFxbXFxkKlxcXS9nLCAnW10nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGZpZWxkTmFtZSA9IGNvbnZlcnRlZEZpZWxkLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgY29udmVydGVkRmllbGQuc2xpY2UoMSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZEVycm9yQWxlcnQoJ0Vycm9yIG9uIGZpZWxkIFwiJyArIGZpZWxkTmFtZSArICdcIicsICdlcnJvci4nICsgZmllbGRFcnJvci5tZXNzYWdlLCB7ZmllbGROYW1lfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGh0dHBFcnJvclJlc3BvbnNlLmVycm9yICE9PSAnJyAmJiBodHRwRXJyb3JSZXNwb25zZS5lcnJvci5tZXNzYWdlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYWRkRXJyb3JBbGVydChcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGh0dHBFcnJvclJlc3BvbnNlLmVycm9yLm1lc3NhZ2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBodHRwRXJyb3JSZXNwb25zZS5lcnJvci5tZXNzYWdlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaHR0cEVycm9yUmVzcG9uc2UuZXJyb3IucGFyYW1zXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hZGRFcnJvckFsZXJ0KGh0dHBFcnJvclJlc3BvbnNlLmVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcblxyXG4gICAgICAgICAgICAgICAgY2FzZSA0MDQ6XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hZGRFcnJvckFsZXJ0KCdOb3QgZm91bmQnLCAnZXJyb3IudXJsLm5vdC5mb3VuZCcpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGh0dHBFcnJvclJlc3BvbnNlLmVycm9yICE9PSAnJyAmJiBodHRwRXJyb3JSZXNwb25zZS5lcnJvci5tZXNzYWdlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYWRkRXJyb3JBbGVydChodHRwRXJyb3JSZXNwb25zZS5lcnJvci5tZXNzYWdlKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZEVycm9yQWxlcnQoaHR0cEVycm9yUmVzcG9uc2UuZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25EZXN0cm95KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNsZWFuSHR0cEVycm9yTGlzdGVuZXIgIT09IHVuZGVmaW5lZCAmJiB0aGlzLmNsZWFuSHR0cEVycm9yTGlzdGVuZXIgIT09IG51bGwpIHtcclxuICAgICAgICAgICAgdGhpcy5ldmVudE1hbmFnZXIuZGVzdHJveSh0aGlzLmNsZWFuSHR0cEVycm9yTGlzdGVuZXIpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBhZGRFcnJvckFsZXJ0KG1lc3NhZ2UsIGtleT8sIGRhdGE/KSB7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb24uc2hvd0Vycm9yKG1lc3NhZ2UpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==