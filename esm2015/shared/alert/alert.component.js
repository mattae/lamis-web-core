import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { JhiAlertService } from 'ng-jhipster';
import { NotificationService } from '@alfresco/adf-core';
let AlertComponent = class AlertComponent {
    constructor(notification, alertService) {
        this.notification = notification;
        this.alertService = alertService;
    }
    ngOnInit() {
        this.alerts = this.alertService.get();
        this.alerts.forEach(alert => this.notification.openSnackMessage(alert.msg, 5000));
    }
    ngOnDestroy() {
        this.alerts = [];
    }
};
AlertComponent.ctorParameters = () => [
    { type: NotificationService },
    { type: JhiAlertService }
];
AlertComponent = tslib_1.__decorate([
    Component({
        selector: 'alert',
        template: ``
    })
], AlertComponent);
export { AlertComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxlcnQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL2FsZXJ0L2FsZXJ0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBcUIsTUFBTSxlQUFlLENBQUM7QUFDN0QsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUM5QyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQU16RCxJQUFhLGNBQWMsR0FBM0IsTUFBYSxjQUFjO0lBR3ZCLFlBQW9CLFlBQWlDLEVBQVUsWUFBNkI7UUFBeEUsaUJBQVksR0FBWixZQUFZLENBQXFCO1FBQVUsaUJBQVksR0FBWixZQUFZLENBQWlCO0lBQUcsQ0FBQztJQUVoRyxRQUFRO1FBQ0osSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDdEYsQ0FBQztJQUVELFdBQVc7UUFDUCxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztJQUNyQixDQUFDO0NBQ0osQ0FBQTs7WUFWcUMsbUJBQW1CO1lBQXdCLGVBQWU7O0FBSG5GLGNBQWM7SUFKMUIsU0FBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLE9BQU87UUFDakIsUUFBUSxFQUFFLEVBQUU7S0FDZixDQUFDO0dBQ1csY0FBYyxDQWExQjtTQWJZLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uRGVzdHJveSwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEpoaUFsZXJ0U2VydmljZSB9IGZyb20gJ25nLWpoaXBzdGVyJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJ0BhbGZyZXNjby9hZGYtY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWxlcnQnLFxyXG4gICAgdGVtcGxhdGU6IGBgXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBbGVydENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuICAgIGFsZXJ0czogYW55W107XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBub3RpZmljYXRpb246IE5vdGlmaWNhdGlvblNlcnZpY2UsIHByaXZhdGUgYWxlcnRTZXJ2aWNlOiBKaGlBbGVydFNlcnZpY2UpIHt9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5hbGVydHMgPSB0aGlzLmFsZXJ0U2VydmljZS5nZXQoKTtcclxuICAgICAgICB0aGlzLmFsZXJ0cy5mb3JFYWNoKGFsZXJ0ID0+IHRoaXMubm90aWZpY2F0aW9uLm9wZW5TbmFja01lc3NhZ2UoYWxlcnQubXNnLCA1MDAwKSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICAgICAgdGhpcy5hbGVydHMgPSBbXTtcclxuICAgIH1cclxufVxyXG4iXX0=