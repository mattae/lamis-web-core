import * as tslib_1 from "tslib";
import { JhiPaginationUtil } from 'ng-jhipster';
import { Injectable, Injector } from '@angular/core';
import * as i0 from "@angular/core";
let PagingParamsResolve = class PagingParamsResolve {
    constructor(injector) {
        this.injector = injector;
        this.paginationUtil = this.injector.get(JhiPaginationUtil);
    }
    resolve(route, state) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const query = route.queryParams['query'] ? route.queryParams['query'] : '';
        const filter = route.queryParams['filter'] ? route.queryParams['filter'] : '';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: +page,
            query: query,
            filter: filter,
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
};
PagingParamsResolve.ctorParameters = () => [
    { type: Injector }
];
PagingParamsResolve.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function PagingParamsResolve_Factory() { return new PagingParamsResolve(i0.ɵɵinject(i0.INJECTOR)); }, token: PagingParamsResolve, providedIn: "root" });
PagingParamsResolve = tslib_1.__decorate([
    Injectable({
        providedIn: 'root'
    })
], PagingParamsResolve);
export { PagingParamsResolve };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnaW5nLXBhcmFtLXJlc29sdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvcGFnaW5nLXBhcmFtLXJlc29sdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUNoRCxPQUFPLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFLckQsSUFBYSxtQkFBbUIsR0FBaEMsTUFBYSxtQkFBbUI7SUFHNUIsWUFBb0IsUUFBa0I7UUFBbEIsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQUNsQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELE9BQU8sQ0FBQyxLQUE2QixFQUFFLEtBQTBCO1FBQzdELE1BQU0sSUFBSSxHQUFHLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztRQUN6RSxNQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDM0UsTUFBTSxNQUFNLEdBQUcsS0FBSyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQzlFLE1BQU0sSUFBSSxHQUFHLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQztRQUM5RSxPQUFPO1lBQ0gsSUFBSSxFQUFFLENBQUMsSUFBSTtZQUNYLEtBQUssRUFBRSxLQUFLO1lBQ1osTUFBTSxFQUFFLE1BQU07WUFDZCxTQUFTLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO1lBQ25ELFNBQVMsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUM7U0FDdEQsQ0FBQztJQUNOLENBQUM7Q0FDSixDQUFBOztZQWpCaUMsUUFBUTs7O0FBSDdCLG1CQUFtQjtJQUgvQixVQUFVLENBQUM7UUFDUixVQUFVLEVBQUUsTUFBTTtLQUNyQixDQUFDO0dBQ1csbUJBQW1CLENBb0IvQjtTQXBCWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBSZXNvbHZlLCBSb3V0ZXJTdGF0ZVNuYXBzaG90IH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgSmhpUGFnaW5hdGlvblV0aWwgfSBmcm9tICduZy1qaGlwc3Rlcic7XHJcbmltcG9ydCB7IEluamVjdGFibGUsIEluamVjdG9yIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFBhZ2luZ1BhcmFtc1Jlc29sdmUgaW1wbGVtZW50cyBSZXNvbHZlPGFueT4ge1xyXG4gICAgcGFnaW5hdGlvblV0aWw6IGFueTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGluamVjdG9yOiBJbmplY3Rvcikge1xyXG4gICAgICAgIHRoaXMucGFnaW5hdGlvblV0aWwgPSB0aGlzLmluamVjdG9yLmdldChKaGlQYWdpbmF0aW9uVXRpbCk7XHJcbiAgICB9XHJcblxyXG4gICAgcmVzb2x2ZShyb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3QpIHtcclxuICAgICAgICBjb25zdCBwYWdlID0gcm91dGUucXVlcnlQYXJhbXNbJ3BhZ2UnXSA/IHJvdXRlLnF1ZXJ5UGFyYW1zWydwYWdlJ10gOiAnMSc7XHJcbiAgICAgICAgY29uc3QgcXVlcnkgPSByb3V0ZS5xdWVyeVBhcmFtc1sncXVlcnknXSA/IHJvdXRlLnF1ZXJ5UGFyYW1zWydxdWVyeSddIDogJyc7XHJcbiAgICAgICAgY29uc3QgZmlsdGVyID0gcm91dGUucXVlcnlQYXJhbXNbJ2ZpbHRlciddID8gcm91dGUucXVlcnlQYXJhbXNbJ2ZpbHRlciddIDogJyc7XHJcbiAgICAgICAgY29uc3Qgc29ydCA9IHJvdXRlLnF1ZXJ5UGFyYW1zWydzb3J0J10gPyByb3V0ZS5xdWVyeVBhcmFtc1snc29ydCddIDogJ2lkLGFzYyc7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgcGFnZTogK3BhZ2UsXHJcbiAgICAgICAgICAgIHF1ZXJ5OiBxdWVyeSxcclxuICAgICAgICAgICAgZmlsdGVyOiBmaWx0ZXIsXHJcbiAgICAgICAgICAgIHByZWRpY2F0ZTogdGhpcy5wYWdpbmF0aW9uVXRpbC5wYXJzZVByZWRpY2F0ZShzb3J0KSxcclxuICAgICAgICAgICAgYXNjZW5kaW5nOiB0aGlzLnBhZ2luYXRpb25VdGlsLnBhcnNlQXNjZW5kaW5nKHNvcnQpXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxufVxyXG4iXX0=