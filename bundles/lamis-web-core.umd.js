(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common/http'), require('rxjs/operators'), require('rxjs'), require('ngx-store'), require('@angular/router'), require('ng-jhipster'), require('@angular/common'), require('@angular/platform-browser'), require('@alfresco/adf-core'), require('@angular/forms'), require('@angular/material'), require('screenfull'), require('@angular/animations'), require('@stomp/ng2-stompjs'), require('lodash.get'), require('@mat-datetimepicker/core'), require('@mat-datetimepicker/moment'), require('moment'), require('moment-es6'), require('angular-material-formio'), require('formiojs'), require('util')) :
    typeof define === 'function' && define.amd ? define('@lamis/web-core', ['exports', '@angular/core', '@angular/common/http', 'rxjs/operators', 'rxjs', 'ngx-store', '@angular/router', 'ng-jhipster', '@angular/common', '@angular/platform-browser', '@alfresco/adf-core', '@angular/forms', '@angular/material', 'screenfull', '@angular/animations', '@stomp/ng2-stompjs', 'lodash.get', '@mat-datetimepicker/core', '@mat-datetimepicker/moment', 'moment', 'moment-es6', 'angular-material-formio', 'formiojs', 'util'], factory) :
    (global = global || self, factory((global.lamis = global.lamis || {}, global.lamis['web-core'] = {}), global.ng.core, global.ng.common.http, global.rxjs.operators, global.rxjs, global.ngxStore, global.ng.router, global.ngJhipster, global.ng.common, global.ng.platformBrowser, global.adfCore, global.ng.forms, global.ng.material, global.screenfull_, global.ng.animations, global.ng2Stompjs, global.get, global.core$1, global.moment, global.moment$1, global.moment$2, global.angularMaterialFormio, global.formiojs, global.util));
}(this, (function (exports, core, http, operators, rxjs, ngxStore, router, ngJhipster, common, platformBrowser, adfCore, forms, material, screenfull_, animations, ng2Stompjs, get, core$1, moment, moment$1, moment$2, angularMaterialFormio, formiojs, util) { 'use strict';

    get = get && get.hasOwnProperty('default') ? get['default'] : get;
    moment$2 = moment$2 && moment$2.hasOwnProperty('default') ? moment$2['default'] : moment$2;

    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0

    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.

    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */
    /* global Reflect, Promise */

    var extendStatics = function(d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };

    function __extends(d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var __assign = function() {
        __assign = Object.assign || function __assign(t) {
            for (var s, i = 1, n = arguments.length; i < n; i++) {
                s = arguments[i];
                for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
            }
            return t;
        };
        return __assign.apply(this, arguments);
    };

    function __rest(s, e) {
        var t = {};
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
            t[p] = s[p];
        if (s != null && typeof Object.getOwnPropertySymbols === "function")
            for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
                if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                    t[p[i]] = s[p[i]];
            }
        return t;
    }

    function __decorate(decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
        return function (target, key) { decorator(target, key, paramIndex); }
    }

    function __metadata(metadataKey, metadataValue) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
        return new (P || (P = Promise))(function (resolve, reject) {
            function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
            function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
            function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
            step((generator = generator.apply(thisArg, _arguments || [])).next());
        });
    }

    function __generator(thisArg, body) {
        var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
        return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
        function verb(n) { return function (v) { return step([n, v]); }; }
        function step(op) {
            if (f) throw new TypeError("Generator is already executing.");
            while (_) try {
                if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
                if (y = 0, t) op = [op[0] & 2, t.value];
                switch (op[0]) {
                    case 0: case 1: t = op; break;
                    case 4: _.label++; return { value: op[1], done: false };
                    case 5: _.label++; y = op[1]; op = [0]; continue;
                    case 7: op = _.ops.pop(); _.trys.pop(); continue;
                    default:
                        if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                        if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                        if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                        if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                        if (t[2]) _.ops.pop();
                        _.trys.pop(); continue;
                }
                op = body.call(thisArg, _);
            } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
            if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
        }
    }

    function __exportStar(m, exports) {
        for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
        var m = typeof Symbol === "function" && o[Symbol.iterator], i = 0;
        if (m) return m.call(o);
        return {
            next: function () {
                if (o && i >= o.length) o = void 0;
                return { value: o && o[i++], done: !o };
            }
        };
    }

    function __read(o, n) {
        var m = typeof Symbol === "function" && o[Symbol.iterator];
        if (!m) return o;
        var i = m.call(o), r, ar = [], e;
        try {
            while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
        }
        catch (error) { e = { error: error }; }
        finally {
            try {
                if (r && !r.done && (m = i["return"])) m.call(i);
            }
            finally { if (e) throw e.error; }
        }
        return ar;
    }

    function __spread() {
        for (var ar = [], i = 0; i < arguments.length; i++)
            ar = ar.concat(__read(arguments[i]));
        return ar;
    }

    function __spreadArrays() {
        for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
        for (var r = Array(s), k = 0, i = 0; i < il; i++)
            for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
                r[k] = a[j];
        return r;
    };

    function __await(v) {
        return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var g = generator.apply(thisArg, _arguments || []), i, q = [];
        return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
        function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
        function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
        function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
        function fulfill(value) { resume("next", value); }
        function reject(value) { resume("throw", value); }
        function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
    }

    function __asyncDelegator(o) {
        var i, p;
        return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
        function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
    }

    function __asyncValues(o) {
        if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
        var m = o[Symbol.asyncIterator], i;
        return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
        function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
        function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
    }

    function __makeTemplateObject(cooked, raw) {
        if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
        return cooked;
    };

    function __importStar(mod) {
        if (mod && mod.__esModule) return mod;
        var result = {};
        if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
        result.default = mod;
        return result;
    }

    function __importDefault(mod) {
        return (mod && mod.__esModule) ? mod : { default: mod };
    }

    var SERVER_API_URL_CONFIG = new core.InjectionToken('SERVER_API_URL_CONFIG');

    var AccountService = /** @class */ (function () {
        function AccountService(http, apiUrlConfig) {
            this.http = http;
            this.apiUrlConfig = apiUrlConfig;
            this.authenticated = false;
            this.authenticationState = new rxjs.Subject();
        }
        AccountService.prototype.fetch = function () {
            return this.http.get(this.apiUrlConfig.SERVER_API_URL + 'api/account', { observe: 'response' });
        };
        AccountService.prototype.save = function (account) {
            return this.http.post(this.apiUrlConfig.SERVER_API_URL + 'api/account', account, { observe: 'response' });
        };
        AccountService.prototype.authenticate = function (identity) {
            this.userIdentity = identity;
            this.authenticated = identity !== null;
            this.authenticationState.next(this.userIdentity);
        };
        AccountService.prototype.hasAnyAuthority = function (authorities) {
            if (authorities === undefined || authorities.length === 0) {
                return true;
            }
            if (!this.authenticated || !this.userIdentity || !this.userIdentity.authorities) {
                return false;
            }
            for (var i = 0; i < authorities.length; i++) {
                if (this.userIdentity.authorities.includes(authorities[i])) {
                    return true;
                }
            }
            return false;
        };
        AccountService.prototype.hasAuthority = function (authority) {
            if (!this.authenticated) {
                return Promise.resolve(false);
            }
            return this.identity().then(function (id) {
                return Promise.resolve(id.authorities && id.authorities.includes(authority));
            }, function () {
                return Promise.resolve(false);
            });
        };
        AccountService.prototype.identity = function (force) {
            var _this = this;
            if (force) {
                this.userIdentity = undefined;
            }
            // check and see if we have retrieved the userIdentity data from the server.
            // if we have, reuse it by immediately resolving
            if (this.userIdentity) {
                return Promise.resolve(this.userIdentity);
            }
            // retrieve the userIdentity data from the server, update the identity object, and then resolve.
            return this.fetch()
                .toPromise()
                .then(function (response) {
                var account = response.body;
                if (account) {
                    _this.userIdentity = account;
                    _this.authenticated = true;
                }
                else {
                    _this.userIdentity = null;
                    _this.authenticated = false;
                }
                _this.authenticationState.next(_this.userIdentity);
                return _this.userIdentity;
            })
                .catch(function (err) {
                _this.userIdentity = null;
                _this.authenticated = false;
                _this.authenticationState.next(_this.userIdentity);
                return null;
            });
        };
        AccountService.prototype.isAuthenticated = function () {
            return this.authenticated;
        };
        AccountService.prototype.isIdentityResolved = function () {
            return this.userIdentity !== undefined;
        };
        AccountService.prototype.getAuthenticationState = function () {
            return this.authenticationState.asObservable();
        };
        AccountService.prototype.getImageUrl = function () {
            return this.isIdentityResolved() ? this.userIdentity.imageUrl : null;
        };
        AccountService.ctorParameters = function () { return [
            { type: http.HttpClient },
            { type: undefined, decorators: [{ type: core.Inject, args: [SERVER_API_URL_CONFIG,] }] }
        ]; };
        AccountService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function AccountService_Factory() { return new AccountService(core.ɵɵinject(http.HttpClient), core.ɵɵinject(SERVER_API_URL_CONFIG)); }, token: AccountService, providedIn: "root" });
        AccountService = __decorate([
            core.Injectable({
                providedIn: 'root'
            }),
            __param(1, core.Inject(SERVER_API_URL_CONFIG))
        ], AccountService);
        return AccountService;
    }());

    var AuthServerProvider = /** @class */ (function () {
        function AuthServerProvider(http, injector, serverUrl) {
            this.http = http;
            this.injector = injector;
            this.serverUrl = serverUrl;
            this.$localStorage = this.injector.get(ngxStore.LocalStorageService);
            this.$sessionStorage = this.injector.get(ngxStore.SessionStorageService);
        }
        AuthServerProvider.prototype.getToken = function () {
            return this.$localStorage.get('authenticationToken') || this.$sessionStorage.get('authenticationToken');
        };
        AuthServerProvider.prototype.getAuthorizationToken = function () {
            return 'Bearer ' + this.getToken();
        };
        AuthServerProvider.prototype.login = function (credentials) {
            var _this = this;
            var data = {
                username: credentials.username,
                password: credentials.password,
                rememberMe: credentials.rememberMe
            };
            return this.http
                .post(this.serverUrl.SERVER_API_URL + 'api/authenticate', data)
                .pipe(operators.map(function (response) { return _this.authenticateSuccess(response, credentials.rememberMe); }));
        };
        AuthServerProvider.prototype.logout = function () {
            var _this = this;
            return new rxjs.Observable(function (observer) {
                _this.$localStorage.remove('authenticationToken');
                _this.$sessionStorage.remove('authenticationToken');
                observer.complete();
            });
        };
        AuthServerProvider.prototype.authenticateSuccess = function (response, rememberMe) {
            var jwt = response.id_token;
            if (rememberMe) {
                this.$localStorage.set('authenticationToken', jwt);
            }
            else {
                this.$sessionStorage.set('authenticationToken', jwt);
            }
        };
        AuthServerProvider.ctorParameters = function () { return [
            { type: http.HttpClient },
            { type: core.Injector },
            { type: undefined, decorators: [{ type: core.Inject, args: [SERVER_API_URL_CONFIG,] }] }
        ]; };
        AuthServerProvider.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function AuthServerProvider_Factory() { return new AuthServerProvider(core.ɵɵinject(http.HttpClient), core.ɵɵinject(core.INJECTOR), core.ɵɵinject(SERVER_API_URL_CONFIG)); }, token: AuthServerProvider, providedIn: "root" });
        AuthServerProvider = __decorate([
            core.Injectable({ providedIn: 'root' }),
            __param(2, core.Inject(SERVER_API_URL_CONFIG))
        ], AuthServerProvider);
        return AuthServerProvider;
    }());

    var LoginService = /** @class */ (function () {
        function LoginService(accountService, authServerProvider, router) {
            this.accountService = accountService;
            this.authServerProvider = authServerProvider;
            this.router = router;
        }
        LoginService.prototype.login = function (credentials) {
            var _this = this;
            return this.authServerProvider.login(credentials).pipe(operators.flatMap(function () { return _this.accountService.identity(true); }));
        };
        LoginService.prototype.logout = function () {
            this.authServerProvider.logout().subscribe();
            this.accountService.authenticate(null);
            this.router.navigateByUrl('/sessions/login');
        };
        LoginService.ctorParameters = function () { return [
            { type: AccountService },
            { type: AuthServerProvider },
            { type: router.Router }
        ]; };
        LoginService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function LoginService_Factory() { return new LoginService(core.ɵɵinject(AccountService), core.ɵɵinject(AuthServerProvider), core.ɵɵinject(router.Router)); }, token: LoginService, providedIn: "root" });
        LoginService = __decorate([
            core.Injectable({ providedIn: 'root' })
        ], LoginService);
        return LoginService;
    }());

    var AuthExpiredInterceptor = /** @class */ (function () {
        function AuthExpiredInterceptor(loginService) {
            this.loginService = loginService;
        }
        AuthExpiredInterceptor.prototype.intercept = function (request, next) {
            var _this = this;
            return next.handle(request).pipe(operators.tap(function (event) { }, function (err) {
                if (err instanceof http.HttpErrorResponse) {
                    if (err.status === 401) {
                        _this.loginService.logout();
                    }
                }
            }));
        };
        AuthExpiredInterceptor.ctorParameters = function () { return [
            { type: LoginService }
        ]; };
        AuthExpiredInterceptor = __decorate([
            core.Injectable()
        ], AuthExpiredInterceptor);
        return AuthExpiredInterceptor;
    }());

    var AuthInterceptor = /** @class */ (function () {
        function AuthInterceptor(localStorage, sessionStorage, serverUrl) {
            this.localStorage = localStorage;
            this.sessionStorage = sessionStorage;
            this.serverUrl = serverUrl;
        }
        AuthInterceptor.prototype.intercept = function (request, next) {
            // tslint:disable-next-line:max-line-length
            if (!request || !request.url || (/^http/.test(request.url) && !(this.serverUrl.SERVER_API_URL && request.url.startsWith(this.serverUrl.SERVER_API_URL)))) {
                return next.handle(request);
            }
            var token = this.localStorage.get('authenticationToken') || this.sessionStorage.get('authenticationToken');
            if (!!token) {
                request = request.clone({
                    setHeaders: {
                        Authorization: 'Bearer ' + token
                    }
                });
            }
            return next.handle(request);
        };
        AuthInterceptor.ctorParameters = function () { return [
            { type: ngxStore.LocalStorageService },
            { type: ngxStore.SessionStorageService },
            { type: undefined, decorators: [{ type: core.Inject, args: [SERVER_API_URL_CONFIG,] }] }
        ]; };
        AuthInterceptor = __decorate([
            core.Injectable(),
            __param(2, core.Inject(SERVER_API_URL_CONFIG))
        ], AuthInterceptor);
        return AuthInterceptor;
    }());

    var ErrorHandlerInterceptor = /** @class */ (function () {
        function ErrorHandlerInterceptor(eventManager) {
            this.eventManager = eventManager;
        }
        ErrorHandlerInterceptor.prototype.intercept = function (request, next) {
            var _this = this;
            return next.handle(request).pipe(operators.tap(function () { }, function (err) {
                if (err instanceof http.HttpErrorResponse) {
                    if (!(err.status === 401 && (err.message === '' || (err.url && err.url.includes('/api/account'))))) {
                        _this.eventManager.broadcast({ name: 'app.httpError', content: err });
                    }
                }
            }));
        };
        ErrorHandlerInterceptor.ctorParameters = function () { return [
            { type: ngJhipster.JhiEventManager }
        ]; };
        ErrorHandlerInterceptor = __decorate([
            core.Injectable()
        ], ErrorHandlerInterceptor);
        return ErrorHandlerInterceptor;
    }());

    var NotificationInterceptor = /** @class */ (function () {
        function NotificationInterceptor(alertService) {
            this.alertService = alertService;
        }
        NotificationInterceptor.prototype.intercept = function (request, next) {
            var _this = this;
            return next.handle(request).pipe(operators.tap(function (event) {
                if (event instanceof http.HttpResponse) {
                    var arr = event.headers.keys();
                    var alert_1 = null;
                    arr.forEach(function (entry) {
                        if (entry.toLowerCase().endsWith('app-alert')) {
                            alert_1 = event.headers.get(entry);
                        }
                    });
                    if (alert_1) {
                        if (typeof alert_1 === 'string') {
                            _this.alertService.success(alert_1, null, null);
                        }
                    }
                }
            }, function (err) { }));
        };
        NotificationInterceptor.ctorParameters = function () { return [
            { type: ngJhipster.JhiAlertService }
        ]; };
        NotificationInterceptor = __decorate([
            core.Injectable()
        ], NotificationInterceptor);
        return NotificationInterceptor;
    }());

    var StateStorageService = /** @class */ (function () {
        function StateStorageService(injector) {
            this.injector = injector;
            this.$sessionStorage = injector.get(ngxStore.SessionStorageService);
        }
        StateStorageService.prototype.getPreviousState = function () {
            return this.$sessionStorage.get('previousState');
        };
        StateStorageService.prototype.resetPreviousState = function () {
            this.$sessionStorage.remove('previousState');
        };
        StateStorageService.prototype.storePreviousState = function (previousStateName, previousStateParams) {
            var previousState = { name: previousStateName, params: previousStateParams };
            this.$sessionStorage.set('previousState', previousState);
        };
        StateStorageService.prototype.getDestinationState = function () {
            return this.$sessionStorage.get('destinationState');
        };
        StateStorageService.prototype.storeUrl = function (url) {
            this.$sessionStorage.set('previousUrl', url);
        };
        StateStorageService.prototype.getUrl = function () {
            return this.$sessionStorage.get('previousUrl');
        };
        StateStorageService.prototype.storeDestinationState = function (destinationState, destinationStateParams, fromState) {
            var destinationInfo = {
                destination: {
                    name: destinationState.name,
                    data: destinationState.data
                },
                params: destinationStateParams,
                from: {
                    name: fromState.name
                }
            };
            this.$sessionStorage.set('destinationState', destinationInfo);
        };
        StateStorageService.ctorParameters = function () { return [
            { type: core.Injector }
        ]; };
        StateStorageService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function StateStorageService_Factory() { return new StateStorageService(core.ɵɵinject(core.INJECTOR)); }, token: StateStorageService, providedIn: "root" });
        StateStorageService = __decorate([
            core.Injectable({ providedIn: 'root' })
        ], StateStorageService);
        return StateStorageService;
    }());

    var UserRouteAccessService = /** @class */ (function () {
        function UserRouteAccessService(router, accountService, stateStorageService) {
            this.router = router;
            this.accountService = accountService;
            this.stateStorageService = stateStorageService;
        }
        UserRouteAccessService.prototype.canActivate = function (route, state) {
            var authorities = route.data['authorities'];
            // We need to call the checkLogin / and so the accountService.identity() function, to ensure,
            // that the client has a principal too, if they already logged in by the server.
            // This could happen on a page refresh.
            return this.checkLogin(authorities, state.url);
        };
        UserRouteAccessService.prototype.checkLogin = function (authorities, url) {
            var _this = this;
            return this.accountService.identity().then(function (account) {
                if (!authorities || authorities.length === 0) {
                    return true;
                }
                if (account && account.login !== 'anonymoususer') {
                    var hasAnyAuthority = _this.accountService.hasAnyAuthority(authorities);
                    if (hasAnyAuthority) {
                        return true;
                    }
                    _this.router.navigate(['sessions/accessdenied']);
                    return false;
                }
                _this.stateStorageService.storeUrl(url);
                _this.router.navigate(['sessions/login']);
                return false;
            });
        };
        UserRouteAccessService.ctorParameters = function () { return [
            { type: router.Router },
            { type: AccountService },
            { type: StateStorageService }
        ]; };
        UserRouteAccessService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function UserRouteAccessService_Factory() { return new UserRouteAccessService(core.ɵɵinject(router.Router), core.ɵɵinject(AccountService), core.ɵɵinject(StateStorageService)); }, token: UserRouteAccessService, providedIn: "root" });
        UserRouteAccessService = __decorate([
            core.Injectable({ providedIn: 'root' })
        ], UserRouteAccessService);
        return UserRouteAccessService;
    }());

    var Account = /** @class */ (function () {
        function Account(activated, authorities, email, firstName, langKey, lastName, login, imageUrl) {
            this.activated = activated;
            this.authorities = authorities;
            this.email = email;
            this.firstName = firstName;
            this.langKey = langKey;
            this.lastName = lastName;
            this.login = login;
            this.imageUrl = imageUrl;
        }
        return Account;
    }());

    var User = /** @class */ (function () {
        function User(id, login, firstName, lastName, email, activated, langKey, authorities, createdBy, createdDate, lastModifiedBy, lastModifiedDate, password) {
            this.id = id;
            this.login = login;
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.activated = activated;
            this.langKey = langKey;
            this.authorities = authorities;
            this.createdBy = createdBy;
            this.createdDate = createdDate;
            this.lastModifiedBy = lastModifiedBy;
            this.lastModifiedDate = lastModifiedDate;
            this.password = password;
            this.id = id ? id : null;
            this.login = login ? login : null;
            this.firstName = firstName ? firstName : null;
            this.lastName = lastName ? lastName : null;
            this.email = email ? email : null;
            this.activated = activated ? activated : false;
            this.langKey = langKey ? langKey : null;
            this.authorities = authorities ? authorities : null;
            this.createdBy = createdBy ? createdBy : null;
            this.createdDate = createdDate ? createdDate : null;
            this.lastModifiedBy = lastModifiedBy ? lastModifiedBy : null;
            this.lastModifiedDate = lastModifiedDate ? lastModifiedDate : null;
            this.password = password ? password : null;
        }
        return User;
    }());

    var createRequestOption = function (req) {
        var options = new http.HttpParams();
        if (req) {
            Object.keys(req).forEach(function (key) {
                if (key !== 'sort') {
                    options = options.set(key, req[key]);
                }
            });
            if (req.sort) {
                req.sort.forEach(function (val) {
                    options = options.append('sort', val);
                });
            }
        }
        return options;
    };

    var UserService = /** @class */ (function () {
        function UserService(http, serverUrl) {
            this.http = http;
            this.serverUrl = serverUrl;
            this.resourceUrl = serverUrl.SERVER_API_URL + 'api/users';
        }
        UserService.prototype.create = function (user) {
            return this.http.post(this.resourceUrl, user, { observe: 'response' });
        };
        UserService.prototype.update = function (user) {
            return this.http.put(this.resourceUrl, user, { observe: 'response' });
        };
        UserService.prototype.find = function (login) {
            return this.http.get(this.resourceUrl + "/" + login, { observe: 'response' });
        };
        UserService.prototype.query = function (req) {
            var options = createRequestOption(req);
            return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
        };
        UserService.prototype.delete = function (login) {
            return this.http.delete(this.resourceUrl + "/" + login, { observe: 'response' });
        };
        UserService.prototype.authorities = function () {
            return this.http.get(this.serverUrl + 'api/users/authorities');
        };
        UserService.ctorParameters = function () { return [
            { type: http.HttpClient },
            { type: undefined, decorators: [{ type: core.Inject, args: [SERVER_API_URL_CONFIG,] }] }
        ]; };
        UserService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function UserService_Factory() { return new UserService(core.ɵɵinject(http.HttpClient), core.ɵɵinject(SERVER_API_URL_CONFIG)); }, token: UserService, providedIn: "root" });
        UserService = __decorate([
            core.Injectable({ providedIn: 'root' }),
            __param(1, core.Inject(SERVER_API_URL_CONFIG))
        ], UserService);
        return UserService;
    }());

    var MenuService = /** @class */ (function () {
        function MenuService(http, serverUrl) {
            this.http = http;
            this.serverUrl = serverUrl;
        }
        MenuService.prototype.getMenus = function () {
            return this.http.get(this.serverUrl.SERVER_API_URL + 'api/modules/menus', {});
        };
        MenuService.ctorParameters = function () { return [
            { type: http.HttpClient },
            { type: undefined, decorators: [{ type: core.Inject, args: [SERVER_API_URL_CONFIG,] }] }
        ]; };
        MenuService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function MenuService_Factory() { return new MenuService(core.ɵɵinject(http.HttpClient), core.ɵɵinject(SERVER_API_URL_CONFIG)); }, token: MenuService, providedIn: "root" });
        MenuService = __decorate([
            core.Injectable({
                providedIn: 'root'
            }),
            __param(1, core.Inject(SERVER_API_URL_CONFIG))
        ], MenuService);
        return MenuService;
    }());

    var CoreModule = /** @class */ (function () {
        function CoreModule() {
            //registerLocaleData(locale);
        }
        CoreModule = __decorate([
            core.NgModule({
                imports: [http.HttpClientModule],
                exports: [],
                declarations: [],
                providers: [
                    platformBrowser.Title,
                    {
                        provide: core.LOCALE_ID,
                        useValue: 'en'
                    },
                    common.DatePipe
                ]
            })
        ], CoreModule);
        return CoreModule;
    }());

    var LgaService = /** @class */ (function () {
        function LgaService(http, serverUrl) {
            this.http = http;
            this.serverUrl = serverUrl;
            this.resourceUrl = '';
            this.resourceUrl = serverUrl.SERVER_API_URL + '/api/lgas';
        }
        LgaService.prototype.find = function (id) {
            return this.http.get(this.resourceUrl + "/" + id, { observe: 'response' });
        };
        LgaService.prototype.findByState = function (id) {
            return this.http.get(this.resourceUrl + "/state/" + id, { observe: 'response' });
        };
        LgaService.ctorParameters = function () { return [
            { type: http.HttpClient },
            { type: undefined, decorators: [{ type: core.Inject, args: [SERVER_API_URL_CONFIG,] }] }
        ]; };
        LgaService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function LgaService_Factory() { return new LgaService(core.ɵɵinject(http.HttpClient), core.ɵɵinject(SERVER_API_URL_CONFIG)); }, token: LgaService, providedIn: "root" });
        LgaService = __decorate([
            core.Injectable({
                providedIn: 'root'
            }),
            __param(1, core.Inject(SERVER_API_URL_CONFIG))
        ], LgaService);
        return LgaService;
    }());

    var FacilityService = /** @class */ (function () {
        function FacilityService(http, serverUrl) {
            this.http = http;
            this.serverUrl = serverUrl;
            this.resourceUrl = '';
            this.resourceUrl = serverUrl.SERVER_API_URL + '/api/facilities';
        }
        FacilityService.prototype.create = function (facility) {
            return this.http
                .post(this.resourceUrl, facility, { observe: 'response' });
        };
        FacilityService.prototype.update = function (facility) {
            return this.http
                .put(this.resourceUrl, facility, { observe: 'response' });
        };
        FacilityService.prototype.delete = function (id) {
            return this.http.delete(this.resourceUrl + "/" + id, { observe: 'response' });
        };
        FacilityService.prototype.find = function (id) {
            return this.http.get(this.resourceUrl + "/" + id, { observe: 'response' });
        };
        FacilityService.prototype.findByLga = function (id) {
            return this.http.get(this.resourceUrl + "/lga/" + id, { observe: 'response' });
        };
        FacilityService.ctorParameters = function () { return [
            { type: http.HttpClient },
            { type: undefined, decorators: [{ type: core.Inject, args: [SERVER_API_URL_CONFIG,] }] }
        ]; };
        FacilityService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function FacilityService_Factory() { return new FacilityService(core.ɵɵinject(http.HttpClient), core.ɵɵinject(SERVER_API_URL_CONFIG)); }, token: FacilityService, providedIn: "root" });
        FacilityService = __decorate([
            core.Injectable({
                providedIn: 'root'
            }),
            __param(1, core.Inject(SERVER_API_URL_CONFIG))
        ], FacilityService);
        return FacilityService;
    }());

    var LoginAuthenticationService = /** @class */ (function () {
        function LoginAuthenticationService(router, loginService, $localStorage, $sessionStorage, eventManager, stateStorageService) {
            this.router = router;
            this.loginService = loginService;
            this.$localStorage = $localStorage;
            this.$sessionStorage = $sessionStorage;
            this.eventManager = eventManager;
            this.stateStorageService = stateStorageService;
        }
        LoginAuthenticationService.prototype.setRedirect = function (value) {
        };
        LoginAuthenticationService.prototype.isEcmLoggedIn = function () {
            return false;
        };
        LoginAuthenticationService.prototype.isBpmLoggedIn = function () {
            return false;
        };
        LoginAuthenticationService.prototype.isOauth = function () {
            return false;
        };
        LoginAuthenticationService.prototype.getRedirect = function () {
            return null;
        };
        LoginAuthenticationService.prototype.login = function (username, password, rememberMe) {
            var _this = this;
            if (rememberMe === void 0) { rememberMe = false; }
            return this.loginService.login({
                username: username,
                password: password,
                rememberMe: rememberMe
            }).pipe(operators.map(function () {
                if (_this.router.url === '/account/register' || _this.router.url.startsWith('/account/activate') ||
                    _this.router.url.startsWith('/account/reset/')) {
                    _this.router.navigate(['']);
                }
                _this.eventManager.broadcast({
                    name: 'authenticationSuccess',
                    content: 'Sending Authentication Success'
                });
                // previousState was set in the authExpiredInterceptor before being redirected to login modal.
                // since login is successful, go to stored previousState and clear previousState
                var redirect = _this.stateStorageService.getUrl();
                if (redirect) {
                    _this.stateStorageService.storeUrl('');
                    _this.router.navigate([redirect]);
                }
                else {
                    _this.router.navigate(['/dashboard']);
                }
            }));
        };
        LoginAuthenticationService.ctorParameters = function () { return [
            { type: router.Router },
            { type: LoginService },
            { type: ngxStore.LocalStorageService },
            { type: ngxStore.SessionStorageService },
            { type: ngJhipster.JhiEventManager },
            { type: StateStorageService }
        ]; };
        LoginAuthenticationService = __decorate([
            core.Injectable()
        ], LoginAuthenticationService);
        return LoginAuthenticationService;
    }());

    var StateService = /** @class */ (function () {
        function StateService(http, serverUrl) {
            this.http = http;
            this.serverUrl = serverUrl;
            this.resourceUrl = '';
            this.resourceUrl = serverUrl.SERVER_API_URL + '/api/states';
        }
        StateService.prototype.find = function (id) {
            return this.http.get(this.resourceUrl + "/" + id, { observe: 'response' });
        };
        StateService.prototype.getStates = function () {
            return this.http.get("" + this.resourceUrl, { observe: 'response' });
        };
        StateService.ctorParameters = function () { return [
            { type: http.HttpClient },
            { type: undefined, decorators: [{ type: core.Inject, args: [SERVER_API_URL_CONFIG,] }] }
        ]; };
        StateService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function StateService_Factory() { return new StateService(core.ɵɵinject(http.HttpClient), core.ɵɵinject(SERVER_API_URL_CONFIG)); }, token: StateService, providedIn: "root" });
        StateService = __decorate([
            core.Injectable({
                providedIn: 'root'
            }),
            __param(1, core.Inject(SERVER_API_URL_CONFIG))
        ], StateService);
        return StateService;
    }());

    function _window() {
        // return the global native browser window object
        return window;
    }
    var WindowRef = /** @class */ (function () {
        function WindowRef() {
        }
        Object.defineProperty(WindowRef.prototype, "nativeWindow", {
            get: function () {
                return _window();
            },
            enumerable: true,
            configurable: true
        });
        WindowRef.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function WindowRef_Factory() { return new WindowRef(); }, token: WindowRef, providedIn: "root" });
        WindowRef = __decorate([
            core.Injectable({ providedIn: 'root' })
        ], WindowRef);
        return WindowRef;
    }());

    var PROBLEM_BASE_URL = 'https://www.jhipster.tech/problem';
    var EMAIL_ALREADY_USED_TYPE = PROBLEM_BASE_URL + '/email-already-used';
    var LOGIN_ALREADY_USED_TYPE = PROBLEM_BASE_URL + '/login-already-used';
    var EMAIL_NOT_FOUND_TYPE = PROBLEM_BASE_URL + '/email-not-found';

    var ITEMS_PER_PAGE = 20;

    var DATE_FORMAT = 'YYYY-MM-DD';
    var DATE_TIME_FORMAT = 'YYYY-MM-DDTHH:mm';

    var AlertComponent = /** @class */ (function () {
        function AlertComponent(notification, alertService) {
            this.notification = notification;
            this.alertService = alertService;
        }
        AlertComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.alerts = this.alertService.get();
            this.alerts.forEach(function (alert) { return _this.notification.openSnackMessage(alert.msg, 5000); });
        };
        AlertComponent.prototype.ngOnDestroy = function () {
            this.alerts = [];
        };
        AlertComponent.ctorParameters = function () { return [
            { type: adfCore.NotificationService },
            { type: ngJhipster.JhiAlertService }
        ]; };
        AlertComponent = __decorate([
            core.Component({
                selector: 'alert',
                template: ""
            })
        ], AlertComponent);
        return AlertComponent;
    }());

    var AlertErrorComponent = /** @class */ (function () {
        function AlertErrorComponent(notification, eventManager) {
            var _this = this;
            this.notification = notification;
            this.eventManager = eventManager;
            /* tslint:enable */
            this.cleanHttpErrorListener = eventManager.subscribe('app.httpError', function (response) {
                var i;
                var httpErrorResponse = response.content;
                switch (httpErrorResponse.status) {
                    // connection refused, server not reachable
                    case 0:
                        _this.addErrorAlert('Server not reachable', 'error.server.not.reachable');
                        break;
                    case 400:
                        var arr = httpErrorResponse.headers.keys();
                        var errorHeader_1 = null;
                        var entityKey_1 = null;
                        arr.forEach(function (entry) {
                            if (entry.endsWith('app-error')) {
                                errorHeader_1 = httpErrorResponse.headers.get(entry);
                            }
                            else if (entry.endsWith('app-params')) {
                                entityKey_1 = httpErrorResponse.headers.get(entry);
                            }
                        });
                        if (errorHeader_1) {
                            _this.addErrorAlert(errorHeader_1, errorHeader_1, { entityName: entityKey_1 });
                        }
                        else if (httpErrorResponse.error !== '' && httpErrorResponse.error.fieldErrors) {
                            var fieldErrors = httpErrorResponse.error.fieldErrors;
                            for (i = 0; i < fieldErrors.length; i++) {
                                var fieldError = fieldErrors[i];
                                // convert 'something[14].other[4].id' to 'something[].other[].id' so translations can be written to it
                                var convertedField = fieldError.field.replace(/\[\d*\]/g, '[]');
                                var fieldName = convertedField.charAt(0).toUpperCase() + convertedField.slice(1);
                                _this.addErrorAlert('Error on field "' + fieldName + '"', 'error.' + fieldError.message, { fieldName: fieldName });
                            }
                        }
                        else if (httpErrorResponse.error !== '' && httpErrorResponse.error.message) {
                            _this.addErrorAlert(httpErrorResponse.error.message, httpErrorResponse.error.message, httpErrorResponse.error.params);
                        }
                        else {
                            _this.addErrorAlert(httpErrorResponse.error);
                        }
                        break;
                    case 404:
                        _this.addErrorAlert('Not found', 'error.url.not.found');
                        break;
                    default:
                        if (httpErrorResponse.error !== '' && httpErrorResponse.error.message) {
                            _this.addErrorAlert(httpErrorResponse.error.message);
                        }
                        else {
                            _this.addErrorAlert(httpErrorResponse.error);
                        }
                }
            });
        }
        AlertErrorComponent.prototype.ngOnDestroy = function () {
            if (this.cleanHttpErrorListener !== undefined && this.cleanHttpErrorListener !== null) {
                this.eventManager.destroy(this.cleanHttpErrorListener);
            }
        };
        AlertErrorComponent.prototype.addErrorAlert = function (message, key, data) {
            this.notification.showError(message);
        };
        AlertErrorComponent.ctorParameters = function () { return [
            { type: adfCore.NotificationService },
            { type: ngJhipster.JhiEventManager }
        ]; };
        AlertErrorComponent = __decorate([
            core.Component({
                selector: 'alert-error',
                template: ""
            })
        ], AlertErrorComponent);
        return AlertErrorComponent;
    }());

    /**
     * @whatItDoes Conditionally includes an HTML element if current user has any
     * of the authorities passed as the `expression`.
     *
     * @howToUse
     * ```
     *     <some-element *jhiHasAnyAuthority="'ROLE_ADMIN'">...</some-element>
     *
     *     <some-element *jhiHasAnyAuthority="['ROLE_ADMIN', 'ROLE_USER']">...</some-element>
     * ```
     */
    var HasAnyAuthorityDirective = /** @class */ (function () {
        function HasAnyAuthorityDirective(accountService, templateRef, viewContainerRef) {
            this.accountService = accountService;
            this.templateRef = templateRef;
            this.viewContainerRef = viewContainerRef;
        }
        Object.defineProperty(HasAnyAuthorityDirective.prototype, "jhiHasAnyAuthority", {
            set: function (value) {
                var _this = this;
                this.authorities = typeof value === 'string' ? [value] : value;
                this.updateView();
                // Get notified each time authentication state changes.
                this.accountService.getAuthenticationState().subscribe(function (identity) { return _this.updateView(); });
            },
            enumerable: true,
            configurable: true
        });
        HasAnyAuthorityDirective.prototype.updateView = function () {
            var hasAnyAuthority = this.accountService.hasAnyAuthority(this.authorities);
            this.viewContainerRef.clear();
            if (hasAnyAuthority) {
                this.viewContainerRef.createEmbeddedView(this.templateRef);
            }
        };
        HasAnyAuthorityDirective.ctorParameters = function () { return [
            { type: AccountService },
            { type: core.TemplateRef },
            { type: core.ViewContainerRef }
        ]; };
        __decorate([
            core.Input()
        ], HasAnyAuthorityDirective.prototype, "jhiHasAnyAuthority", null);
        HasAnyAuthorityDirective = __decorate([
            core.Directive({
                selector: '[jhiHasAnyAuthority]'
            })
        ], HasAnyAuthorityDirective);
        return HasAnyAuthorityDirective;
    }());

    var PhoneNumberValidator = /** @class */ (function () {
        function PhoneNumberValidator() {
            // tslint:disable-next-line:max-line-length
            this.prefixes = '0703|0706|0803|0806|0810|0813|0814|0816|0903|0906|0705|0805|0807|0811|0815|0905|0701|0708|0802|0808|0812|0902|0907|0901|0809|0817|0818|0908|0909|07028|07029|0819|07025|07026|0704|07027|0709|0707|0804|0702';
        }
        PhoneNumberValidator_1 = PhoneNumberValidator;
        PhoneNumberValidator.prototype.validate = function (control) {
            var phone = control.value;
            if (!phone) {
                return null;
            }
            if (phone.length !== 11) {
                return {
                    invalidPhone: true
                };
            }
            var prefix = phone.substr(0, 4);
            if (!this.prefixes.includes(prefix)) {
                return {
                    invalidPhone: true
                };
            }
        };
        var PhoneNumberValidator_1;
        PhoneNumberValidator = PhoneNumberValidator_1 = __decorate([
            core.Directive({
                selector: '[phoneNumber]',
                providers: [{
                        provide: forms.NG_VALIDATORS,
                        useExisting: PhoneNumberValidator_1,
                        multi: true
                    }]
            })
        ], PhoneNumberValidator);
        return PhoneNumberValidator;
    }());

    var RelativeTimePipe = /** @class */ (function () {
        function RelativeTimePipe() {
        }
        RelativeTimePipe.prototype.transform = function (value) {
            if (!(value instanceof Date))
                value = new Date(value);
            var seconds = Math.floor(((new Date()).getTime() - value.getTime()) / 1000);
            var interval = Math.floor(seconds / 31536000);
            if (interval > 1) {
                return interval + " years ago";
            }
            interval = Math.floor(seconds / 2592000);
            if (interval > 1) {
                return interval + " months ago";
            }
            interval = Math.floor(seconds / 86400);
            if (interval > 1) {
                return interval + " days ago";
            }
            interval = Math.floor(seconds / 3600);
            if (interval > 1) {
                return interval + " hours ago";
            }
            interval = Math.floor(seconds / 60);
            if (interval > 1) {
                return interval + " minutes ago";
            }
            return Math.floor(seconds) + " seconds ago";
        };
        RelativeTimePipe = __decorate([
            core.Pipe({ name: 'relativeTime' })
        ], RelativeTimePipe);
        return RelativeTimePipe;
    }());

    var ExcerptPipe = /** @class */ (function () {
        function ExcerptPipe() {
        }
        ExcerptPipe.prototype.transform = function (text, limit) {
            if (limit === void 0) { limit = 5; }
            if (text.length <= limit)
                return text;
            return text.substring(0, limit) + '...';
        };
        ExcerptPipe = __decorate([
            core.Pipe({ name: 'excerpt' })
        ], ExcerptPipe);
        return ExcerptPipe;
    }());

    var KeysPipe = /** @class */ (function () {
        function KeysPipe() {
        }
        KeysPipe.prototype.transform = function (value, args) {
            var keys = [];
            for (var enumMember in value) {
                if (!isNaN(parseInt(enumMember, 10))) {
                    keys.push({ key: enumMember, value: value[enumMember] });
                }
            }
            console.log('Keys', keys);
            return keys;
        };
        KeysPipe = __decorate([
            core.Pipe({ name: 'keys1' })
        ], KeysPipe);
        return KeysPipe;
    }());

    var MapValuesPipe = /** @class */ (function () {
        function MapValuesPipe() {
        }
        MapValuesPipe.prototype.transform = function (value, args) {
            var returnArray = [];
            value.forEach(function (entryVal, entryKey) {
                returnArray.push({
                    key: entryKey,
                    val: entryVal
                });
            });
            return returnArray;
        };
        MapValuesPipe = __decorate([
            core.Pipe({ name: 'mapValues' })
        ], MapValuesPipe);
        return MapValuesPipe;
    }());

    var NairaPipe = /** @class */ (function () {
        function NairaPipe() {
            this.pipe = new common.CurrencyPipe('en');
        }
        NairaPipe.prototype.transform = function (value) {
            var args = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                args[_i - 1] = arguments[_i];
            }
            return this.pipe.transform(value, '₦');
        };
        NairaPipe = __decorate([
            core.Pipe({
                name: 'naira'
            })
        ], NairaPipe);
        return NairaPipe;
    }());

    var CommonPipesModule = /** @class */ (function () {
        function CommonPipesModule() {
        }
        CommonPipesModule = __decorate([
            core.NgModule({
                declarations: [
                    RelativeTimePipe,
                    ExcerptPipe,
                    KeysPipe,
                    MapValuesPipe,
                    NairaPipe
                ],
                exports: [
                    RelativeTimePipe,
                    ExcerptPipe,
                    KeysPipe,
                    MapValuesPipe,
                    NairaPipe
                ]
            })
        ], CommonPipesModule);
        return CommonPipesModule;
    }());

    var SharedCommonModule = /** @class */ (function () {
        function SharedCommonModule() {
        }
        SharedCommonModule = __decorate([
            core.NgModule({
                imports: [
                    common.CommonModule,
                    ngJhipster.NgJhipsterModule,
                    CommonPipesModule,
                ],
                declarations: [
                    AlertComponent,
                    AlertErrorComponent,
                    PhoneNumberValidator
                ],
                exports: [
                    AlertComponent,
                    AlertErrorComponent,
                    CommonPipesModule,
                    PhoneNumberValidator
                ]
            })
        ], SharedCommonModule);
        return SharedCommonModule;
    }());

    var AppConfirmComponent = /** @class */ (function () {
        function AppConfirmComponent(dialogRef, data) {
            this.dialogRef = dialogRef;
            this.data = data;
        }
        AppConfirmComponent.ctorParameters = function () { return [
            { type: material.MatDialogRef },
            { type: undefined, decorators: [{ type: core.Inject, args: [material.MAT_DIALOG_DATA,] }] }
        ]; };
        AppConfirmComponent = __decorate([
            core.Component({
                selector: 'app-confirm',
                template: "<h1 matDialogTitle class=\"mb-05\">{{ data.title }}</h1>\n    <div mat-dialog-content class=\"mb-1\">{{ data.message }}</div>\n    <div mat-dialog-actions>\n        <button\n                type=\"button\"\n                mat-raised-button\n                color=\"primary\"\n                (click)=\"dialogRef.close(true)\">OK\n        </button>\n        &nbsp;\n        <span fxFlex></span>\n        <button\n                type=\"button\"\n                color=\"accent\"\n                mat-raised-button\n                (click)=\"dialogRef.close(false)\">Cancel\n        </button>\n    </div>"
            }),
            __param(1, core.Inject(material.MAT_DIALOG_DATA))
        ], AppConfirmComponent);
        return AppConfirmComponent;
    }());

    var AppLoaderComponent = /** @class */ (function () {
        function AppLoaderComponent(dialogRef) {
            this.dialogRef = dialogRef;
        }
        AppLoaderComponent.prototype.ngOnInit = function () {
        };
        AppLoaderComponent.ctorParameters = function () { return [
            { type: material.MatDialogRef }
        ]; };
        AppLoaderComponent = __decorate([
            core.Component({
                selector: 'app-app-loader',
                template: "<div class=\"text-center\">\r\n    <h6 class=\"m-0 pb-1\">{{ title }}</h6>\r\n    <div mat-dialog-content>\r\n        <mat-spinner [style.margin]=\"'auto'\"></mat-spinner>\r\n    </div>\r\n</div>\r\n",
                styles: [".mat-dialog-content{min-height:122px}"]
            })
        ], AppLoaderComponent);
        return AppLoaderComponent;
    }());

    var AppLoaderService = /** @class */ (function () {
        function AppLoaderService(dialog) {
            this.dialog = dialog;
        }
        AppLoaderService.prototype.open = function (title) {
            if (title === void 0) { title = 'Please wait'; }
            this.dialogRef = this.dialog.open(AppLoaderComponent, { disableClose: true, backdropClass: 'light-backdrop' });
            this.dialogRef.updateSize('200px');
            this.dialogRef.componentInstance.title = title;
            return this.dialogRef.afterClosed();
        };
        AppLoaderService.prototype.close = function () {
            if (this.dialogRef) {
                this.dialogRef.close();
            }
        };
        AppLoaderService.ctorParameters = function () { return [
            { type: material.MatDialog }
        ]; };
        AppLoaderService = __decorate([
            core.Injectable()
        ], AppLoaderService);
        return AppLoaderService;
    }());

    var screenfull = screenfull_;
    var ToggleFullscreenDirective = /** @class */ (function () {
        function ToggleFullscreenDirective() {
        }
        ToggleFullscreenDirective.prototype.onClick = function () {
            if (screenfull.isEnabled) {
                screenfull.toggle();
            }
        };
        __decorate([
            core.HostListener('click')
        ], ToggleFullscreenDirective.prototype, "onClick", null);
        ToggleFullscreenDirective = __decorate([
            core.Directive({
                selector: '[toggleFullscreen]'
            })
        ], ToggleFullscreenDirective);
        return ToggleFullscreenDirective;
    }());

    var speedDialFabAnimations = [
        animations.trigger('fabToggler', [
            animations.state('inactive', animations.style({
                transform: 'rotate(0deg)'
            })),
            animations.state('active', animations.style({
                transform: 'rotate(225deg)'
            })),
            animations.transition('* <=> *', animations.animate('200ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
        ]),
        animations.trigger('speedDialStagger', [
            animations.transition('* => *', [
                animations.query(':enter', animations.style({ opacity: 0 }), { optional: true }),
                animations.query(':enter', animations.stagger('40ms', [
                    animations.animate('200ms cubic-bezier(0.4, 0.0, 0.2, 1)', animations.keyframes([
                        animations.style({ opacity: 0, transform: 'translateY(10px)' }),
                        animations.style({ opacity: 1, transform: 'translateY(0)' }),
                    ]))
                ]), { optional: true }),
                animations.query(':leave', animations.animate('200ms cubic-bezier(0.4, 0.0, 0.2, 1)', animations.keyframes([
                    animations.style({ opacity: 1 }),
                    animations.style({ opacity: 0 }),
                ])), { optional: true })
            ])
        ])
    ];

    var SpeedDialFabComponent = /** @class */ (function () {
        function SpeedDialFabComponent() {
            this.fabTogglerState = 'inactive';
            this.buttons = [];
            this.buttonState = [];
        }
        SpeedDialFabComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.links.forEach(function (link) { return _this.buttonState.push(link); });
        };
        SpeedDialFabComponent.prototype.showItems = function () {
            this.fabTogglerState = 'active';
            this.buttons = this.buttonState;
        };
        SpeedDialFabComponent.prototype.hideItems = function () {
            this.fabTogglerState = 'inactive';
            this.buttons = [];
        };
        SpeedDialFabComponent.prototype.onToggleFab = function () {
            this.buttons.length ? this.hideItems() : this.showItems();
        };
        __decorate([
            core.Input()
        ], SpeedDialFabComponent.prototype, "links", void 0);
        SpeedDialFabComponent = __decorate([
            core.Component({
                selector: 'speed-dial',
                template: "<div class=\"fab-container\">\r\n    <button mat-fab class=\"fab-toggler\"\r\n            (click)=\"onToggleFab()\">\r\n        <mat-icon [@fabToggler]=\"{value: fabTogglerState}\">add</mat-icon>\r\n    </button>\r\n    <div [@speedDialStagger]=\"buttons.length\">\r\n        <button mat-mini-fab *ngFor=\"let btn of buttons\"\r\n                matTooltip=\"{{btn.tooltip}}\"\r\n                [routerLink]=\"['.', btn.state, 'new']\"\r\n                class=\"fab-secondary\"\r\n                color=\"accent\">\r\n            <mat-icon>{{btn.icon}}</mat-icon>\r\n        </button>\r\n    </div>\r\n</div>\r\n\r\n<div id=\"fab-dismiss\"\r\n     *ngIf=\"fabTogglerState==='active'\"\r\n     (click)=\"onToggleFab()\">\r\n</div>\r\n",
                animations: speedDialFabAnimations,
                styles: [""]
            })
        ], SpeedDialFabComponent);
        return SpeedDialFabComponent;
    }());

    var LamisSharedModule = /** @class */ (function () {
        function LamisSharedModule() {
        }
        LamisSharedModule = __decorate([
            core.NgModule({
                imports: [
                    common.CommonModule,
                    forms.FormsModule,
                    forms.ReactiveFormsModule,
                    router.RouterModule,
                    SharedCommonModule,
                    material.MatIconModule,
                    material.MatButtonModule,
                    material.MatTooltipModule,
                    material.MatProgressSpinnerModule,
                    material.MatDialogModule
                ],
                declarations: [
                    HasAnyAuthorityDirective,
                    SpeedDialFabComponent,
                    AppConfirmComponent,
                    AppLoaderComponent,
                    ToggleFullscreenDirective
                ],
                entryComponents: [
                    AppConfirmComponent,
                    AppLoaderComponent
                ],
                exports: [
                    SharedCommonModule,
                    HasAnyAuthorityDirective,
                    SpeedDialFabComponent,
                    ToggleFullscreenDirective
                ],
                providers: [
                    AppLoaderService,
                    AppConfirmComponent
                ],
                schemas: [core.CUSTOM_ELEMENTS_SCHEMA]
            })
        ], LamisSharedModule);
        return LamisSharedModule;
    }());

    var Address = /** @class */ (function () {
        function Address(street1, street2, city, lga) {
            this.street1 = street1;
            this.street2 = street2;
            this.city = city;
            this.lga = lga;
            this.street1 = street1 ? street1 : null;
            this.street2 = street2 ? street2 : null;
            this.city = city ? city : null;
            this.lga = lga ? lga : null;
        }
        return Address;
    }());
    var PersonName = /** @class */ (function () {
        function PersonName(title, firstName, middleName, surname) {
            this.title = title;
            this.firstName = firstName;
            this.middleName = middleName;
            this.surname = surname;
        }
        return PersonName;
    }());
    var Phone = /** @class */ (function () {
        function Phone(phone1, phone2) {
            this.phone1 = phone1;
            this.phone2 = phone2;
        }
        return Phone;
    }());


    (function (FacilityType) {
        FacilityType[FacilityType["DOCTOR_OFFICE"] = 0] = "DOCTOR_OFFICE";
        FacilityType[FacilityType["PRIMARY_CARE"] = 1] = "PRIMARY_CARE";
        FacilityType[FacilityType["CLINIC"] = 2] = "CLINIC";
        FacilityType[FacilityType["HOSPITAL"] = 3] = "HOSPITAL";
        FacilityType[FacilityType["SPECIALIZED"] = 4] = "SPECIALIZED";
        FacilityType[FacilityType["NURSING_HOME"] = 5] = "NURSING_HOME";
        FacilityType[FacilityType["HOSPICE"] = 6] = "HOSPICE";
        FacilityType[FacilityType["RURAL"] = 7] = "RURAL";
    })(exports.FacilityType || (exports.FacilityType = {}));

    (function (PublicLevel) {
        PublicLevel[PublicLevel["PRIVATE"] = 0] = "PRIVATE";
        PublicLevel[PublicLevel["PUBLIC"] = 1] = "PUBLIC";
        PublicLevel[PublicLevel["MIXED"] = 2] = "MIXED";
    })(exports.PublicLevel || (exports.PublicLevel = {}));

    var LGA = /** @class */ (function () {
        function LGA(id, name, state) {
            this.id = id;
            this.name = name;
            this.state = state;
            this.id = id ? id : null;
            this.name = name ? name : null;
            this.state = state ? state : null;
        }
        return LGA;
    }());

    var State = /** @class */ (function () {
        function State(id, name) {
            this.id = id;
            this.name = name;
            this.id = id ? id : null;
            this.name = name ? name : null;
        }
        return State;
    }());

    var Aggregate = /** @class */ (function () {
        function Aggregate(field, key) {
            this.field = field;
            this.key = key;
        }
        return Aggregate;
    }());
    function remove(array, element) {
        return array.filter(function (e) { return e.id !== element.id; });
    }
    function clear(array) {
        array.length = 0;
        return array;
    }
    function contains(array, element) {
        return array.filter(function (e) { return e.id === element.id; }).length > 0;
    }
    function entityCompare(e1, e2) {
        return e1 && e2 ? e1.id == e2.id : e1 === e2;
    }
    function enumCompare(e1, e2) {
        return (e1 !== undefined && e2 !== undefined) ? e1.valueOf() == e2.valueOf() : e1 === e2;
    }
    function replace(array, element) {
        var result = remove(array, element);
        result.push(element);
        return result;
    }

    var source = 'http://' + window.location.host + '/websocket';
    var ɵ0 = function () {
        return new SockJS('' + source);
    };
    var RxStompConfig = {
        // Which server?
        brokerURL: '' + source,
        webSocketFactory: ɵ0,
        // Headers
        // Typical keys: login, passcode, host
        connectHeaders: {
            login: 'guest',
            passcode: 'guest'
        },
        // How often to heartbeat?
        // Interval in milliseconds, set to 0 to disable
        heartbeatIncoming: 0,
        // Typical value 0 - disabled
        heartbeatOutgoing: 20000,
    };

    var LamisCoreModule = /** @class */ (function () {
        function LamisCoreModule() {
        }
        LamisCoreModule_1 = LamisCoreModule;
        LamisCoreModule.forRoot = function (serverApiUrlConfig, dateTimeConfig) {
            // MomentDateFormat.DATE_FORMAT = dateTimeConfig.DATE_FORMAT;
            return {
                ngModule: LamisCoreModule_1,
                providers: [
                    AuthExpiredInterceptor,
                    AuthInterceptor,
                    ErrorHandlerInterceptor,
                    NotificationInterceptor,
                    {
                        provide: SERVER_API_URL_CONFIG,
                        useValue: serverApiUrlConfig
                    },
                    {
                        provide: ng2Stompjs.InjectableRxStompConfig,
                        useValue: RxStompConfig
                    },
                    {
                        provide: ng2Stompjs.RxStompService,
                        useFactory: ng2Stompjs.rxStompServiceFactory,
                        deps: [ng2Stompjs.InjectableRxStompConfig]
                    }
                ]
            };
        };
        var LamisCoreModule_1;
        LamisCoreModule = LamisCoreModule_1 = __decorate([
            core.NgModule({
                declarations: [],
                imports: [
                    common.CommonModule,
                    LamisSharedModule
                ],
                exports: [
                    LamisSharedModule
                ],
                providers: []
            })
        ], LamisCoreModule);
        return LamisCoreModule;
    }());

    var PagingParamsResolve = /** @class */ (function () {
        function PagingParamsResolve(injector) {
            this.injector = injector;
            this.paginationUtil = this.injector.get(ngJhipster.JhiPaginationUtil);
        }
        PagingParamsResolve.prototype.resolve = function (route, state) {
            var page = route.queryParams['page'] ? route.queryParams['page'] : '1';
            var query = route.queryParams['query'] ? route.queryParams['query'] : '';
            var filter = route.queryParams['filter'] ? route.queryParams['filter'] : '';
            var sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
            return {
                page: +page,
                query: query,
                filter: filter,
                predicate: this.paginationUtil.parsePredicate(sort),
                ascending: this.paginationUtil.parseAscending(sort)
            };
        };
        PagingParamsResolve.ctorParameters = function () { return [
            { type: core.Injector }
        ]; };
        PagingParamsResolve.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function PagingParamsResolve_Factory() { return new PagingParamsResolve(core.ɵɵinject(core.INJECTOR)); }, token: PagingParamsResolve, providedIn: "root" });
        PagingParamsResolve = __decorate([
            core.Injectable({
                providedIn: 'root'
            })
        ], PagingParamsResolve);
        return PagingParamsResolve;
    }());

    var AppConfirmService = /** @class */ (function () {
        function AppConfirmService(dialog) {
            this.dialog = dialog;
        }
        AppConfirmService.prototype.confirm = function (data) {
            if (data === void 0) { data = {}; }
            data.title = data.title || 'Confirm';
            data.message = data.message || 'Are you sure?';
            var dialogRef;
            dialogRef = this.dialog.open(AppConfirmComponent, {
                width: '380px',
                disableClose: true,
                data: { title: data.title, message: data.message }
            });
            return dialogRef.afterClosed();
        };
        AppConfirmService.ctorParameters = function () { return [
            { type: material.MatDialog }
        ]; };
        AppConfirmService = __decorate([
            core.Injectable()
        ], AppConfirmService);
        return AppConfirmService;
    }());

    var LayoutTemplateService = /** @class */ (function () {
        function LayoutTemplateService(http, serverUrl) {
            this.http = http;
            this.serverUrl = serverUrl;
            this.resourceUrl = '';
            this.resourceUrl = serverUrl.SERVER_API_URL + '/api/forms';
        }
        LayoutTemplateService.prototype.getTemplate = function (templateId) {
            return this.http.get(this.resourceUrl + "/by-name/" + templateId, { observe: 'body' });
        };
        LayoutTemplateService.ctorParameters = function () { return [
            { type: http.HttpClient },
            { type: undefined, decorators: [{ type: core.Inject, args: [SERVER_API_URL_CONFIG,] }] }
        ]; };
        LayoutTemplateService.ngInjectableDef = core.ɵɵdefineInjectable({ factory: function LayoutTemplateService_Factory() { return new LayoutTemplateService(core.ɵɵinject(http.HttpClient), core.ɵɵinject(SERVER_API_URL_CONFIG)); }, token: LayoutTemplateService, providedIn: "root" });
        LayoutTemplateService = __decorate([
            core.Injectable({
                providedIn: 'root'
            }),
            __param(1, core.Inject(SERVER_API_URL_CONFIG))
        ], LayoutTemplateService);
        return LayoutTemplateService;
    }());


    (function (FieldType) {
        FieldType["date"] = "date";
        FieldType["datetime"] = "datetime";
        FieldType["text"] = "text";
        FieldType["boolean"] = "boolean";
        FieldType["int"] = "int";
        FieldType["float"] = "float";
    })(exports.FieldType || (exports.FieldType = {}));
    var DetailsComponent = /** @class */ (function () {
        function DetailsComponent(templateService) {
            this.templateService = templateService;
        }
        DetailsComponent.prototype.ngOnInit = function () {
            var _this = this;
            this.templateService.getTemplate(this.template).subscribe(function (json) {
                _this.details = json.template;
            });
        };
        DetailsComponent.prototype.propertiesForDetail = function (detail) {
            var e_1, _a;
            var properties = [];
            try {
                for (var _b = __values(detail.fields), _c = _b.next(); !_c.done; _c = _b.next()) {
                    var field = _c.value;
                    var dataType = field.type;
                    var item = void 0;
                    switch (dataType) {
                        case exports.FieldType.boolean:
                            item = new adfCore.CardViewBoolItemModel({
                                value: this.getValueForKey(field.key),
                                key: '',
                                label: field.label
                            });
                            break;
                        case exports.FieldType.int:
                            item = new adfCore.CardViewIntItemModel({
                                value: this.getValueForKey(field.key),
                                key: '',
                                label: field.label,
                            });
                            break;
                        case exports.FieldType.float:
                            item = new adfCore.CardViewFloatItemModel({
                                value: this.getValueForKey(field.key),
                                key: '',
                                label: field.label,
                            });
                            break;
                        case exports.FieldType.date:
                            item = new adfCore.CardViewDateItemModel({
                                value: this.getValueForKey(field.key),
                                key: '',
                                label: field.label,
                                format: 'dd MMM, yyyy'
                            });
                            break;
                        case exports.FieldType.datetime:
                            item = new adfCore.CardViewDatetimeItemModel({
                                value: this.getValueForKey(field.key),
                                key: '',
                                label: field.label,
                                format: 'dd MMM, yyyy HH:mm'
                            });
                            break;
                        default:
                            item = new adfCore.CardViewTextItemModel({
                                value: this.getValueForKey(field.key),
                                key: '',
                                label: field.label,
                            });
                    }
                    properties.push(item);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
            return properties;
        };
        DetailsComponent.prototype.getValueForKey = function (key) {
            return get(this.model, key);
        };
        DetailsComponent.ctorParameters = function () { return [
            { type: LayoutTemplateService }
        ]; };
        __decorate([
            core.Input()
        ], DetailsComponent.prototype, "template", void 0);
        __decorate([
            core.Input()
        ], DetailsComponent.prototype, "model", void 0);
        DetailsComponent = __decorate([
            core.Component({
                selector: 'details-component',
                template: "<ng-container *ngIf=\"model && details\">\r\n    <mat-card *ngFor=\"let detail of details\" class=\"default mb-1 pb-0\">\r\n        <ng-container *ngIf=\"!!detail.header\">\r\n            <mat-card-title>{{detail.header}}</mat-card-title>\r\n            <mat-divider></mat-divider>\r\n        </ng-container>\r\n        <mat-card-content>\r\n            <adf-card-view [properties]=\"propertiesForDetail(detail)\"></adf-card-view>\r\n        </mat-card-content>\r\n    </mat-card>\r\n</ng-container>\r\n",
                encapsulation: core.ViewEncapsulation.None,
                styles: [""]
            })
        ], DetailsComponent);
        return DetailsComponent;
    }());

    /*!
     * @license
     * Copyright 2016 Alfresco Software, Ltd.
     *
     * Licensed under the Apache License, Version 2.0 (the "License");
     * you may not use this file except in compliance with the License.
     * You may obtain a copy of the License at
     *
     *     http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing, software
     * distributed under the License is distributed on an "AS IS" BASIS,
     * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     * See the License for the specific language governing permissions and
     * limitations under the License.
     */
    var MOMENT_DATE_FORMATS = {
        parse: {
            dateInput: 'DD MMM YYYY'
        },
        display: {
            dateInput: 'DD MMM YYYY',
            monthYearLabel: 'MMMM Y',
            dateA11yLabel: 'LL',
            monthYearA11yLabel: 'MMMM Y'
        }
    };
    var dateNames = [];
    for (var date = 1; date <= 31; date++) {
        dateNames.push(String(date));
    }

    /*!
     * @license
     * Copyright 2016 Alfresco Software, Ltd.
     *
     * Licensed under the Apache License, Version 2.0 (the "License");
     * you may not use this file except in compliance with the License.
     * You may obtain a copy of the License at
     *
     *     http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing, software
     * distributed under the License is distributed on an "AS IS" BASIS,
     * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     * See the License for the specific language governing permissions and
     * limitations under the License.
     */
    var MomentDateAdapter = /** @class */ (function (_super) {
        __extends(MomentDateAdapter, _super);
        function MomentDateAdapter() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.localeData = moment$2.localeData();
            return _this;
        }
        MomentDateAdapter.prototype.getYear = function (date) {
            return date.year();
        };
        MomentDateAdapter.prototype.getMonth = function (date) {
            return date.month();
        };
        MomentDateAdapter.prototype.getDate = function (date) {
            return date.date();
        };
        MomentDateAdapter.prototype.getDayOfWeek = function (date) {
            return date.day();
        };
        MomentDateAdapter.prototype.getMonthNames = function (style) {
            switch (style) {
                case 'long':
                    return this.localeData.months();
                case 'short':
                    return this.localeData.monthsShort();
                case 'narrow':
                    return this.localeData.monthsShort().map(function (month) { return month[0]; });
                default:
                    return;
            }
        };
        MomentDateAdapter.prototype.getDateNames = function () {
            var dateNames = [];
            for (var date = 1; date <= 31; date++) {
                dateNames.push(String(date));
            }
            return dateNames;
        };
        MomentDateAdapter.prototype.getDayOfWeekNames = function (style) {
            switch (style) {
                case 'long':
                    return this.localeData.weekdays();
                case 'short':
                    return this.localeData.weekdaysShort();
                case 'narrow':
                    return this.localeData.weekdaysShort();
                default:
                    return;
            }
        };
        MomentDateAdapter.prototype.getYearName = function (date) {
            return String(date.year());
        };
        MomentDateAdapter.prototype.getFirstDayOfWeek = function () {
            return this.localeData.firstDayOfWeek();
        };
        MomentDateAdapter.prototype.getNumDaysInMonth = function (date) {
            return date.daysInMonth();
        };
        MomentDateAdapter.prototype.clone = function (date) {
            var locale = this.locale || 'en';
            return date.clone().locale(locale);
        };
        MomentDateAdapter.prototype.createDate = function (year, month, date) {
            return moment$2([year, month, date]);
        };
        MomentDateAdapter.prototype.today = function () {
            var locale = this.locale || 'en';
            return moment$2().locale(locale);
        };
        MomentDateAdapter.prototype.parse = function (value, parseFormat) {
            var locale = this.locale || 'en';
            if (value && typeof value === 'string') {
                var m = moment$2(value, parseFormat, locale, true);
                if (!m.isValid()) {
                    // use strict parsing because Moment's parser is very forgiving, and this can lead to undesired behavior.
                    m = moment$2(value, this.overrideDisplayFormat, locale, true);
                }
                if (m.isValid()) {
                    // if user omits year, it defaults to 2001, so check for that issue.
                    if (m.year() === 2001 && value.indexOf('2001') === -1) {
                        // if 2001 not actually in the value string, change to current year
                        var currentYear = new Date().getFullYear();
                        m.set('year', currentYear);
                        // if date is in the future, set previous year
                        if (m.isAfter(moment$2())) {
                            m.set('year', currentYear - 1);
                        }
                    }
                }
                return m;
            }
            return value ? moment$2(value).locale(locale) : null;
        };
        MomentDateAdapter.prototype.format = function (date, displayFormat) {
            date = this.clone(date);
            displayFormat = this.overrideDisplayFormat ? this.overrideDisplayFormat : displayFormat;
            if (date && date.format) {
                return date.format(displayFormat);
            }
            else {
                return '';
            }
        };
        MomentDateAdapter.prototype.addCalendarYears = function (date, years) {
            return date.clone().add(years, 'y');
        };
        MomentDateAdapter.prototype.addCalendarMonths = function (date, months) {
            return date.clone().add(months, 'M');
        };
        MomentDateAdapter.prototype.addCalendarDays = function (date, days) {
            return date.clone().add(days, 'd');
        };
        MomentDateAdapter.prototype.getISODateString = function (date) {
            return date.toISOString();
        };
        MomentDateAdapter.prototype.setLocale = function (locale) {
            _super.prototype.setLocale.call(this, locale);
            this.localeData = moment$2.localeData(locale);
        };
        MomentDateAdapter.prototype.compareDate = function (first, second) {
            return first.diff(second, 'seconds', true);
        };
        MomentDateAdapter.prototype.sameDate = function (first, second) {
            if (first == null) {
                // same if both null
                return second == null;
            }
            else if (moment$1.isMoment(first)) {
                return first.isSame(second);
            }
            else {
                var isSame = _super.prototype.sameDate.call(this, first, second);
                return isSame;
            }
        };
        MomentDateAdapter.prototype.clampDate = function (date, min, max) {
            if (min && date.isBefore(min)) {
                return min;
            }
            else if (max && date.isAfter(max)) {
                return max;
            }
            else {
                return date;
            }
        };
        MomentDateAdapter.prototype.isDateInstance = function (date) {
            var isValidDateInstance = false;
            if (date) {
                isValidDateInstance = date._isAMomentObject;
            }
            return isValidDateInstance;
        };
        MomentDateAdapter.prototype.isValid = function (date) {
            return date.isValid();
        };
        MomentDateAdapter.prototype.toIso8601 = function (date) {
            return this.clone(date).format();
        };
        MomentDateAdapter.prototype.fromIso8601 = function (iso8601String) {
            var locale = this.locale || 'en';
            var d = moment$2(iso8601String, moment$2.ISO_8601).locale(locale);
            return this.isValid(d) ? d : null;
        };
        MomentDateAdapter.prototype.invalid = function () {
            return moment$2.invalid();
        };
        return MomentDateAdapter;
    }(material.DateAdapter));

    var ɵ0$1 = MOMENT_DATE_FORMATS, ɵ1 = moment.MAT_MOMENT_DATETIME_FORMATS;
    var MatDateFormatModule = /** @class */ (function () {
        function MatDateFormatModule() {
        }
        MatDateFormatModule = __decorate([
            core.NgModule({
                providers: [
                    { provide: material.DateAdapter, useClass: MomentDateAdapter },
                    { provide: material.MAT_DATE_FORMATS, useValue: ɵ0$1 },
                    { provide: core$1.DatetimeAdapter, useClass: moment.MomentDatetimeAdapter },
                    { provide: core$1.MAT_DATETIME_FORMATS, useValue: ɵ1 }
                ]
            })
        ], MatDateFormatModule);
        return MatDateFormatModule;
    }());

    var JsonFormComponent = /** @class */ (function () {
        function JsonFormComponent(templateService, localStorage, sessionStorage) {
            this.templateService = templateService;
            this.localStorage = localStorage;
            this.sessionStorage = sessionStorage;
            this.dataEvent = new core.EventEmitter(true);
            this.customEvent = new core.EventEmitter(true);
            this.form = {};
            this.isValid = false;
        }
        JsonFormComponent.prototype.ngOnInit = function () {
            var _this = this;
            if (this.templateId) {
                this.templateService.getTemplate(this.templateId).subscribe(function (json) {
                    _this.form = json.form;
                });
            }
            else {
                this.form = this.template;
            }
        };
        JsonFormComponent.prototype.reset = function () {
            this.formio.onRefresh({
                submission: this.model
            });
        };
        JsonFormComponent.prototype.onCustomEvent = function (event) {
            this.customEvent.emit(event);
        };
        JsonFormComponent.prototype.change = function (event) {
            if (event.hasOwnProperty('isValid')) {
                this.isValid = event.isValid;
                if (this.isValid) {
                    this.dataEvent.emit(event.data);
                }
            }
        };
        JsonFormComponent.prototype.ngOnChanges = function (changes) {
            if (changes['model']) {
                var token = this.localStorage.get('authenticationToken') || this.sessionStorage.get('authenticationToken');
                this.model = {
                    data: Object.assign({}, changes['model'].currentValue, { authorization: token })
                };
            }
        };
        JsonFormComponent.ctorParameters = function () { return [
            { type: LayoutTemplateService },
            { type: ngxStore.LocalStorageService },
            { type: ngxStore.SessionStorageService }
        ]; };
        __decorate([
            core.ViewChild(angularMaterialFormio.FormioComponent, { static: true })
        ], JsonFormComponent.prototype, "formio", void 0);
        __decorate([
            core.Input()
        ], JsonFormComponent.prototype, "template", void 0);
        __decorate([
            core.Input()
        ], JsonFormComponent.prototype, "templateId", void 0);
        __decorate([
            core.Input()
        ], JsonFormComponent.prototype, "model", void 0);
        __decorate([
            core.Output()
        ], JsonFormComponent.prototype, "dataEvent", void 0);
        JsonFormComponent = __decorate([
            core.Component({
                selector: 'json-form',
                template: "\n        <mat-formio [form]=\"form\"\n                    (ready)=\"reset()\"\n                    (customEvent)=\"onCustomEvent($event)\"\n                    (change)=\"change($event)\">\n        </mat-formio>\n    "
            })
        ], JsonFormComponent);
        return JsonFormComponent;
    }());

    var JsonFormModule = /** @class */ (function () {
        function JsonFormModule(localStorage, sessionStorage) {
            this.localStorage = localStorage;
            this.sessionStorage = sessionStorage;
            var token = this.localStorage.get('authenticationToken') || this.sessionStorage.get('authenticationToken');
            formiojs.Formio.getRequestArgs = function (formio, type, url, method, data, opts) {
                method = (method || 'GET').toUpperCase();
                if (!opts || !util.isObject(opts)) {
                    opts = {};
                }
                opts['Authorization'] = token;
                var requestArgs = {
                    url: url,
                    method: method,
                    data: data || null,
                    opts: opts
                };
                if (type) {
                    requestArgs['type'] = type;
                }
                if (formio) {
                    requestArgs['formio'] = formio;
                }
                return requestArgs;
            };
        }
        JsonFormModule.ctorParameters = function () { return [
            { type: ngxStore.LocalStorageService },
            { type: ngxStore.SessionStorageService }
        ]; };
        JsonFormModule = __decorate([
            core.NgModule({
                imports: [angularMaterialFormio.MatFormioModule, common.CommonModule, adfCore.CardViewModule],
                declarations: [JsonFormComponent, DetailsComponent],
                exports: [JsonFormComponent, DetailsComponent, angularMaterialFormio.MatFormioModule],
                entryComponents: [JsonFormComponent]
            })
        ], JsonFormModule);
        return JsonFormModule;
    }());

    exports.Account = Account;
    exports.AccountService = AccountService;
    exports.Address = Address;
    exports.Aggregate = Aggregate;
    exports.AlertComponent = AlertComponent;
    exports.AlertErrorComponent = AlertErrorComponent;
    exports.AppConfirmService = AppConfirmService;
    exports.AppLoaderService = AppLoaderService;
    exports.AuthExpiredInterceptor = AuthExpiredInterceptor;
    exports.AuthInterceptor = AuthInterceptor;
    exports.AuthServerProvider = AuthServerProvider;
    exports.CommonPipesModule = CommonPipesModule;
    exports.CoreModule = CoreModule;
    exports.DATE_FORMAT = DATE_FORMAT;
    exports.DATE_TIME_FORMAT = DATE_TIME_FORMAT;
    exports.DetailsComponent = DetailsComponent;
    exports.EMAIL_ALREADY_USED_TYPE = EMAIL_ALREADY_USED_TYPE;
    exports.EMAIL_NOT_FOUND_TYPE = EMAIL_NOT_FOUND_TYPE;
    exports.ErrorHandlerInterceptor = ErrorHandlerInterceptor;
    exports.ExcerptPipe = ExcerptPipe;
    exports.FacilityService = FacilityService;
    exports.HasAnyAuthorityDirective = HasAnyAuthorityDirective;
    exports.ITEMS_PER_PAGE = ITEMS_PER_PAGE;
    exports.JsonFormComponent = JsonFormComponent;
    exports.JsonFormModule = JsonFormModule;
    exports.KeysPipe = KeysPipe;
    exports.LGA = LGA;
    exports.LOGIN_ALREADY_USED_TYPE = LOGIN_ALREADY_USED_TYPE;
    exports.LamisCoreModule = LamisCoreModule;
    exports.LamisSharedModule = LamisSharedModule;
    exports.LgaService = LgaService;
    exports.LoginAuthenticationService = LoginAuthenticationService;
    exports.LoginService = LoginService;
    exports.MOMENT_DATE_FORMATS = MOMENT_DATE_FORMATS;
    exports.MapValuesPipe = MapValuesPipe;
    exports.MatDateFormatModule = MatDateFormatModule;
    exports.MenuService = MenuService;
    exports.MomentDateAdapter = MomentDateAdapter;
    exports.NairaPipe = NairaPipe;
    exports.NotificationInterceptor = NotificationInterceptor;
    exports.PROBLEM_BASE_URL = PROBLEM_BASE_URL;
    exports.PagingParamsResolve = PagingParamsResolve;
    exports.PersonName = PersonName;
    exports.Phone = Phone;
    exports.RelativeTimePipe = RelativeTimePipe;
    exports.SERVER_API_URL_CONFIG = SERVER_API_URL_CONFIG;
    exports.SharedCommonModule = SharedCommonModule;
    exports.SpeedDialFabComponent = SpeedDialFabComponent;
    exports.State = State;
    exports.StateService = StateService;
    exports.StateStorageService = StateStorageService;
    exports.ToggleFullscreenDirective = ToggleFullscreenDirective;
    exports.User = User;
    exports.UserRouteAccessService = UserRouteAccessService;
    exports.UserService = UserService;
    exports.WindowRef = WindowRef;
    exports.clear = clear;
    exports.contains = contains;
    exports.createRequestOption = createRequestOption;
    exports.entityCompare = entityCompare;
    exports.enumCompare = enumCompare;
    exports.remove = remove;
    exports.replace = replace;
    exports.speedDialFabAnimations = speedDialFabAnimations;
    exports.ɵ0 = ɵ0$1;
    exports.ɵ1 = ɵ1;
    exports.ɵa = PhoneNumberValidator;
    exports.ɵb = AppConfirmComponent;
    exports.ɵc = AppLoaderComponent;
    exports.ɵd = RxStompConfig;
    exports.ɵe = LayoutTemplateService;

    Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=lamis-web-core.umd.js.map
