import { EventEmitter, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { FormioComponent } from 'angular-material-formio';
import { LocalStorageService, SessionStorageService } from 'ngx-store';
import { LayoutTemplateService } from '../../services/layout.template.service';
export declare class JsonFormComponent implements OnInit, OnChanges {
    private templateService;
    private localStorage;
    private sessionStorage;
    formio: FormioComponent;
    template: string;
    templateId: string;
    model: any;
    dataEvent: EventEmitter<any>;
    customEvent: EventEmitter<any>;
    form: any;
    isValid: boolean;
    constructor(templateService: LayoutTemplateService, localStorage: LocalStorageService, sessionStorage: SessionStorageService);
    ngOnInit(): void;
    reset(): void;
    onCustomEvent(event: any): void;
    change(event: any): void;
    ngOnChanges(changes: SimpleChanges): void;
}
