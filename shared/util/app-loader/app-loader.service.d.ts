import { MatDialog, MatDialogRef } from '@angular/material';
import { Observable } from 'rxjs';
import { AppLoaderComponent } from './app-loader.component';
export declare class AppLoaderService {
    private dialog;
    dialogRef: MatDialogRef<AppLoaderComponent>;
    constructor(dialog: MatDialog);
    open(title?: string): Observable<boolean>;
    close(): void;
}
