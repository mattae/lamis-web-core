import { OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
export declare class AppLoaderComponent implements OnInit {
    dialogRef: MatDialogRef<AppLoaderComponent>;
    title: any;
    message: any;
    constructor(dialogRef: MatDialogRef<AppLoaderComponent>);
    ngOnInit(): void;
}
