import { MatDialogRef } from '@angular/material';
export declare class AppConfirmComponent {
    dialogRef: MatDialogRef<AppConfirmComponent>;
    data: any;
    constructor(dialogRef: MatDialogRef<AppConfirmComponent>, data: any);
}
