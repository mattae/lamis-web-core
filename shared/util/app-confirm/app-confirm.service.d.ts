import { Observable } from 'rxjs';
import { MatDialog } from '@angular/material';
interface ConfirmData {
    title?: string;
    message?: string;
}
export declare class AppConfirmService {
    private dialog;
    constructor(dialog: MatDialog);
    confirm(data?: ConfirmData): Observable<boolean>;
}
export {};
