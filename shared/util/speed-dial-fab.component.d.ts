import { OnInit } from '@angular/core';
export declare class SpeedDialFabComponent implements OnInit {
    ngOnInit(): void;
    links: {
        state: string;
        icon: string;
        tooltip: string;
    }[];
    fabTogglerState: string;
    buttons: any[];
    buttonState: any[];
    constructor();
    showItems(): void;
    hideItems(): void;
    onToggleFab(): void;
}
