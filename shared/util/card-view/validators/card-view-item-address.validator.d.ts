import { CardViewItemValidator } from '@alfresco/adf-core';
export declare class CardViewItemAddressValidator implements CardViewItemValidator {
    message: string;
    isValid(value: any): boolean;
}
