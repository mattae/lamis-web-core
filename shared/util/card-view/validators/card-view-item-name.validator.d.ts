import { CardViewItemValidator } from '@alfresco/adf-core';
export declare class CardViewItemNameValidator implements CardViewItemValidator {
    message: string;
    isValid(value: any): boolean;
}
