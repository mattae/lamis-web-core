import { OnChanges, OnInit } from '@angular/core';
import { CardViewUpdateService } from '@alfresco/adf-core';
import { CardViewAddressItemModel } from '../../models/card-view-address-item.model';
import { MatSelectChange } from '@angular/material';
import { LgaService } from '../../../../../services/lga.service';
import { StateService } from '../../../../../services/state.service';
import { ILGA } from '../../../../model/lga.model';
import { IState } from '../../../../model/state.model';
export declare class CardViewAddressItemComponent implements OnChanges, OnInit {
    private cardViewUpdateService;
    private stateService;
    private lgaService;
    state: IState;
    property: CardViewAddressItemModel;
    editable: boolean;
    displayEmpty: boolean;
    private cityInput;
    private street1Input;
    private street2Input;
    states: IState[];
    lgas: ILGA[];
    inEdit: boolean;
    editedStreet1: string;
    editedStreet2: string;
    editedCity: string;
    editedLga: ILGA;
    errorMessages: string[];
    constructor(cardViewUpdateService: CardViewUpdateService, stateService: StateService, lgaService: LgaService);
    ngOnChanges(): void;
    ngOnInit(): void;
    onChange(event: MatSelectChange): void;
    showProperty(): boolean;
    isEditable(): boolean;
    isClickable(): boolean;
    hasIcon(): boolean;
    hasErrors(): number;
    setEditMode(editStatus: boolean): void;
    reset(): void;
    update(): void;
    readonly displayValue: any;
    clicked(): void;
    entityCompare(s1: any, s2: any): boolean;
}
