import { CardViewBaseItemModel, CardViewItem, DynamicComponentModel } from '@alfresco/adf-core';
import { CardViewNameItemProperties } from './card-view-name-item.properties';
export declare class CardViewNameItemModel extends CardViewBaseItemModel implements CardViewItem, DynamicComponentModel {
    type: string;
    constructor(obj: CardViewNameItemProperties);
    readonly displayValue: any;
    getValue(): string;
}
