import { OnInit } from '@angular/core';
import { LayoutTemplateService } from '../../services/layout.template.service';
import { CardViewItem } from '@alfresco/adf-core';
export interface Detail {
    header?: string;
    headerClass?: string;
    fields: Field[];
}
export interface Field {
    type: FieldType;
    key: string;
    label: string;
}
export declare enum FieldType {
    date = "date",
    datetime = "datetime",
    text = "text",
    boolean = "boolean",
    int = "int",
    float = "float"
}
export declare class DetailsComponent implements OnInit {
    private templateService;
    template: string;
    model: any;
    details: Detail[];
    constructor(templateService: LayoutTemplateService);
    ngOnInit(): void;
    propertiesForDetail(detail: Detail): Array<CardViewItem>;
    private getValueForKey;
}
