import { PipeTransform } from "@angular/core";
export declare class NairaPipe implements PipeTransform {
    private pipe;
    transform(value: any, ...args: any[]): string;
}
