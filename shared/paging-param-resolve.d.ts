import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Injector } from '@angular/core';
export declare class PagingParamsResolve implements Resolve<any> {
    private injector;
    paginationUtil: any;
    constructor(injector: Injector);
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): {
        page: number;
        query: any;
        filter: any;
        predicate: any;
        ascending: any;
    };
}
