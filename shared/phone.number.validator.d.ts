import { AbstractControl, ValidationErrors, Validator } from '@angular/forms';
export declare class PhoneNumberValidator implements Validator {
    prefixes: string;
    validate(control: AbstractControl): ValidationErrors | null;
}
