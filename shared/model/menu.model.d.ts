export interface IMenuItem {
    type: string;
    name?: string;
    state?: string;
    icon?: string;
    tooltip?: string;
    disabled?: boolean;
    subs?: IChildItem[];
    badges?: IBadge[];
    authorities?: string[];
}
export interface IChildItem {
    type?: string;
    name: string;
    state?: string;
    icon?: string;
    subs?: IChildItem[];
    authorities?: string[];
}
interface IBadge {
    color: string;
    value: string;
}
export {};
