import { ILGA } from './lga.model';
export interface IAddress {
    street1?: string;
    street2?: string;
    city?: string;
    lga?: ILGA;
}
export declare class Address implements IAddress {
    street1?: string;
    street2?: string;
    city?: string;
    lga?: ILGA;
    constructor(street1?: string, street2?: string, city?: string, lga?: ILGA);
}
export declare class PersonName {
    private title?;
    private firstName?;
    private middleName?;
    private surname?;
    constructor(title?: string, firstName?: string, middleName?: string, surname?: string);
}
export declare class Phone {
    private phone1?;
    private phone2?;
    constructor(phone1?: string, phone2?: string);
}
