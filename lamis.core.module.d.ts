import { ModuleWithProviders } from '@angular/core';
import { DateTimeConfig, ServerApiUrlConfig } from './app.constants';
export declare class LamisCoreModule {
    static forRoot(serverApiUrlConfig: ServerApiUrlConfig, dateTimeConfig?: DateTimeConfig): ModuleWithProviders;
}
