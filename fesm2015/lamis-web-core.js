import { InjectionToken, Inject, ɵɵdefineInjectable, ɵɵinject, Injectable, Injector, INJECTOR, NgModule, LOCALE_ID, Component, TemplateRef, ViewContainerRef, Input, Directive, Pipe, HostListener, CUSTOM_ELEMENTS_SCHEMA, ViewEncapsulation, EventEmitter, ViewChild, Output } from '@angular/core';
import { __decorate, __param } from 'tslib';
import { HttpClient, HttpErrorResponse, HttpResponse, HttpParams, HttpClientModule } from '@angular/common/http';
import { map, flatMap, tap } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import { LocalStorageService, SessionStorageService } from 'ngx-store';
import { Router, RouterModule } from '@angular/router';
import { JhiEventManager, JhiAlertService, NgJhipsterModule, JhiPaginationUtil } from 'ng-jhipster';
import { DatePipe, CurrencyPipe, CommonModule } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { NotificationService, CardViewTextItemModel, CardViewDatetimeItemModel, CardViewDateItemModel, CardViewFloatItemModel, CardViewIntItemModel, CardViewBoolItemModel, CardViewModule } from '@alfresco/adf-core';
import { NG_VALIDATORS, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatIconModule, MatButtonModule, MatTooltipModule, MatProgressSpinnerModule, MatDialogModule, DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import * as screenfull_ from 'screenfull';
import { trigger, state, style, transition, animate, query, stagger, keyframes } from '@angular/animations';
import { InjectableRxStompConfig, RxStompService, rxStompServiceFactory } from '@stomp/ng2-stompjs';
import get from 'lodash.get';
import { DatetimeAdapter, MAT_DATETIME_FORMATS } from '@mat-datetimepicker/core';
import { MAT_MOMENT_DATETIME_FORMATS, MomentDatetimeAdapter } from '@mat-datetimepicker/moment';
import { isMoment } from 'moment';
import moment from 'moment-es6';
import { FormioComponent, MatFormioModule } from 'angular-material-formio';
import { Formio } from 'formiojs';
import { isObject } from 'util';

const SERVER_API_URL_CONFIG = new InjectionToken('SERVER_API_URL_CONFIG');

let AccountService = class AccountService {
    constructor(http, apiUrlConfig) {
        this.http = http;
        this.apiUrlConfig = apiUrlConfig;
        this.authenticated = false;
        this.authenticationState = new Subject();
    }
    fetch() {
        return this.http.get(this.apiUrlConfig.SERVER_API_URL + 'api/account', { observe: 'response' });
    }
    save(account) {
        return this.http.post(this.apiUrlConfig.SERVER_API_URL + 'api/account', account, { observe: 'response' });
    }
    authenticate(identity) {
        this.userIdentity = identity;
        this.authenticated = identity !== null;
        this.authenticationState.next(this.userIdentity);
    }
    hasAnyAuthority(authorities) {
        if (authorities === undefined || authorities.length === 0) {
            return true;
        }
        if (!this.authenticated || !this.userIdentity || !this.userIdentity.authorities) {
            return false;
        }
        for (let i = 0; i < authorities.length; i++) {
            if (this.userIdentity.authorities.includes(authorities[i])) {
                return true;
            }
        }
        return false;
    }
    hasAuthority(authority) {
        if (!this.authenticated) {
            return Promise.resolve(false);
        }
        return this.identity().then(id => {
            return Promise.resolve(id.authorities && id.authorities.includes(authority));
        }, () => {
            return Promise.resolve(false);
        });
    }
    identity(force) {
        if (force) {
            this.userIdentity = undefined;
        }
        // check and see if we have retrieved the userIdentity data from the server.
        // if we have, reuse it by immediately resolving
        if (this.userIdentity) {
            return Promise.resolve(this.userIdentity);
        }
        // retrieve the userIdentity data from the server, update the identity object, and then resolve.
        return this.fetch()
            .toPromise()
            .then(response => {
            const account = response.body;
            if (account) {
                this.userIdentity = account;
                this.authenticated = true;
            }
            else {
                this.userIdentity = null;
                this.authenticated = false;
            }
            this.authenticationState.next(this.userIdentity);
            return this.userIdentity;
        })
            .catch(err => {
            this.userIdentity = null;
            this.authenticated = false;
            this.authenticationState.next(this.userIdentity);
            return null;
        });
    }
    isAuthenticated() {
        return this.authenticated;
    }
    isIdentityResolved() {
        return this.userIdentity !== undefined;
    }
    getAuthenticationState() {
        return this.authenticationState.asObservable();
    }
    getImageUrl() {
        return this.isIdentityResolved() ? this.userIdentity.imageUrl : null;
    }
};
AccountService.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
AccountService.ngInjectableDef = ɵɵdefineInjectable({ factory: function AccountService_Factory() { return new AccountService(ɵɵinject(HttpClient), ɵɵinject(SERVER_API_URL_CONFIG)); }, token: AccountService, providedIn: "root" });
AccountService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(1, Inject(SERVER_API_URL_CONFIG))
], AccountService);

let AuthServerProvider = class AuthServerProvider {
    constructor(http, injector, serverUrl) {
        this.http = http;
        this.injector = injector;
        this.serverUrl = serverUrl;
        this.$localStorage = this.injector.get(LocalStorageService);
        this.$sessionStorage = this.injector.get(SessionStorageService);
    }
    getToken() {
        return this.$localStorage.get('authenticationToken') || this.$sessionStorage.get('authenticationToken');
    }
    getAuthorizationToken() {
        return 'Bearer ' + this.getToken();
    }
    login(credentials) {
        const data = {
            username: credentials.username,
            password: credentials.password,
            rememberMe: credentials.rememberMe
        };
        return this.http
            .post(this.serverUrl.SERVER_API_URL + 'api/authenticate', data)
            .pipe(map(response => this.authenticateSuccess(response, credentials.rememberMe)));
    }
    logout() {
        return new Observable(observer => {
            this.$localStorage.remove('authenticationToken');
            this.$sessionStorage.remove('authenticationToken');
            observer.complete();
        });
    }
    authenticateSuccess(response, rememberMe) {
        const jwt = response.id_token;
        if (rememberMe) {
            this.$localStorage.set('authenticationToken', jwt);
        }
        else {
            this.$sessionStorage.set('authenticationToken', jwt);
        }
    }
};
AuthServerProvider.ctorParameters = () => [
    { type: HttpClient },
    { type: Injector },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
AuthServerProvider.ngInjectableDef = ɵɵdefineInjectable({ factory: function AuthServerProvider_Factory() { return new AuthServerProvider(ɵɵinject(HttpClient), ɵɵinject(INJECTOR), ɵɵinject(SERVER_API_URL_CONFIG)); }, token: AuthServerProvider, providedIn: "root" });
AuthServerProvider = __decorate([
    Injectable({ providedIn: 'root' }),
    __param(2, Inject(SERVER_API_URL_CONFIG))
], AuthServerProvider);

let LoginService = class LoginService {
    constructor(accountService, authServerProvider, router) {
        this.accountService = accountService;
        this.authServerProvider = authServerProvider;
        this.router = router;
    }
    login(credentials) {
        return this.authServerProvider.login(credentials).pipe(flatMap(() => this.accountService.identity(true)));
    }
    logout() {
        this.authServerProvider.logout().subscribe();
        this.accountService.authenticate(null);
        this.router.navigateByUrl('/sessions/login');
    }
};
LoginService.ctorParameters = () => [
    { type: AccountService },
    { type: AuthServerProvider },
    { type: Router }
];
LoginService.ngInjectableDef = ɵɵdefineInjectable({ factory: function LoginService_Factory() { return new LoginService(ɵɵinject(AccountService), ɵɵinject(AuthServerProvider), ɵɵinject(Router)); }, token: LoginService, providedIn: "root" });
LoginService = __decorate([
    Injectable({ providedIn: 'root' })
], LoginService);

let AuthExpiredInterceptor = class AuthExpiredInterceptor {
    constructor(loginService) {
        this.loginService = loginService;
    }
    intercept(request, next) {
        return next.handle(request).pipe(tap((event) => { }, (err) => {
            if (err instanceof HttpErrorResponse) {
                if (err.status === 401) {
                    this.loginService.logout();
                }
            }
        }));
    }
};
AuthExpiredInterceptor.ctorParameters = () => [
    { type: LoginService }
];
AuthExpiredInterceptor = __decorate([
    Injectable()
], AuthExpiredInterceptor);

let AuthInterceptor = class AuthInterceptor {
    constructor(localStorage, sessionStorage, serverUrl) {
        this.localStorage = localStorage;
        this.sessionStorage = sessionStorage;
        this.serverUrl = serverUrl;
    }
    intercept(request, next) {
        // tslint:disable-next-line:max-line-length
        if (!request || !request.url || (/^http/.test(request.url) && !(this.serverUrl.SERVER_API_URL && request.url.startsWith(this.serverUrl.SERVER_API_URL)))) {
            return next.handle(request);
        }
        const token = this.localStorage.get('authenticationToken') || this.sessionStorage.get('authenticationToken');
        if (!!token) {
            request = request.clone({
                setHeaders: {
                    Authorization: 'Bearer ' + token
                }
            });
        }
        return next.handle(request);
    }
};
AuthInterceptor.ctorParameters = () => [
    { type: LocalStorageService },
    { type: SessionStorageService },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
AuthInterceptor = __decorate([
    Injectable(),
    __param(2, Inject(SERVER_API_URL_CONFIG))
], AuthInterceptor);

let ErrorHandlerInterceptor = class ErrorHandlerInterceptor {
    constructor(eventManager) {
        this.eventManager = eventManager;
    }
    intercept(request, next) {
        return next.handle(request).pipe(tap(() => { }, (err) => {
            if (err instanceof HttpErrorResponse) {
                if (!(err.status === 401 && (err.message === '' || (err.url && err.url.includes('/api/account'))))) {
                    this.eventManager.broadcast({ name: 'app.httpError', content: err });
                }
            }
        }));
    }
};
ErrorHandlerInterceptor.ctorParameters = () => [
    { type: JhiEventManager }
];
ErrorHandlerInterceptor = __decorate([
    Injectable()
], ErrorHandlerInterceptor);

let NotificationInterceptor = class NotificationInterceptor {
    constructor(alertService) {
        this.alertService = alertService;
    }
    intercept(request, next) {
        return next.handle(request).pipe(tap((event) => {
            if (event instanceof HttpResponse) {
                const arr = event.headers.keys();
                let alert = null;
                arr.forEach(entry => {
                    if (entry.toLowerCase().endsWith('app-alert')) {
                        alert = event.headers.get(entry);
                    }
                });
                if (alert) {
                    if (typeof alert === 'string') {
                        this.alertService.success(alert, null, null);
                    }
                }
            }
        }, (err) => { }));
    }
};
NotificationInterceptor.ctorParameters = () => [
    { type: JhiAlertService }
];
NotificationInterceptor = __decorate([
    Injectable()
], NotificationInterceptor);

let StateStorageService = class StateStorageService {
    constructor(injector) {
        this.injector = injector;
        this.$sessionStorage = injector.get(SessionStorageService);
    }
    getPreviousState() {
        return this.$sessionStorage.get('previousState');
    }
    resetPreviousState() {
        this.$sessionStorage.remove('previousState');
    }
    storePreviousState(previousStateName, previousStateParams) {
        const previousState = { name: previousStateName, params: previousStateParams };
        this.$sessionStorage.set('previousState', previousState);
    }
    getDestinationState() {
        return this.$sessionStorage.get('destinationState');
    }
    storeUrl(url) {
        this.$sessionStorage.set('previousUrl', url);
    }
    getUrl() {
        return this.$sessionStorage.get('previousUrl');
    }
    storeDestinationState(destinationState, destinationStateParams, fromState) {
        const destinationInfo = {
            destination: {
                name: destinationState.name,
                data: destinationState.data
            },
            params: destinationStateParams,
            from: {
                name: fromState.name
            }
        };
        this.$sessionStorage.set('destinationState', destinationInfo);
    }
};
StateStorageService.ctorParameters = () => [
    { type: Injector }
];
StateStorageService.ngInjectableDef = ɵɵdefineInjectable({ factory: function StateStorageService_Factory() { return new StateStorageService(ɵɵinject(INJECTOR)); }, token: StateStorageService, providedIn: "root" });
StateStorageService = __decorate([
    Injectable({ providedIn: 'root' })
], StateStorageService);

let UserRouteAccessService = class UserRouteAccessService {
    constructor(router, accountService, stateStorageService) {
        this.router = router;
        this.accountService = accountService;
        this.stateStorageService = stateStorageService;
    }
    canActivate(route, state) {
        const authorities = route.data['authorities'];
        // We need to call the checkLogin / and so the accountService.identity() function, to ensure,
        // that the client has a principal too, if they already logged in by the server.
        // This could happen on a page refresh.
        return this.checkLogin(authorities, state.url);
    }
    checkLogin(authorities, url) {
        return this.accountService.identity().then(account => {
            if (!authorities || authorities.length === 0) {
                return true;
            }
            if (account && account.login !== 'anonymoususer') {
                const hasAnyAuthority = this.accountService.hasAnyAuthority(authorities);
                if (hasAnyAuthority) {
                    return true;
                }
                this.router.navigate(['sessions/accessdenied']);
                return false;
            }
            this.stateStorageService.storeUrl(url);
            this.router.navigate(['sessions/login']);
            return false;
        });
    }
};
UserRouteAccessService.ctorParameters = () => [
    { type: Router },
    { type: AccountService },
    { type: StateStorageService }
];
UserRouteAccessService.ngInjectableDef = ɵɵdefineInjectable({ factory: function UserRouteAccessService_Factory() { return new UserRouteAccessService(ɵɵinject(Router), ɵɵinject(AccountService), ɵɵinject(StateStorageService)); }, token: UserRouteAccessService, providedIn: "root" });
UserRouteAccessService = __decorate([
    Injectable({ providedIn: 'root' })
], UserRouteAccessService);

class Account {
    constructor(activated, authorities, email, firstName, langKey, lastName, login, imageUrl) {
        this.activated = activated;
        this.authorities = authorities;
        this.email = email;
        this.firstName = firstName;
        this.langKey = langKey;
        this.lastName = lastName;
        this.login = login;
        this.imageUrl = imageUrl;
    }
}

class User {
    constructor(id, login, firstName, lastName, email, activated, langKey, authorities, createdBy, createdDate, lastModifiedBy, lastModifiedDate, password) {
        this.id = id;
        this.login = login;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.activated = activated;
        this.langKey = langKey;
        this.authorities = authorities;
        this.createdBy = createdBy;
        this.createdDate = createdDate;
        this.lastModifiedBy = lastModifiedBy;
        this.lastModifiedDate = lastModifiedDate;
        this.password = password;
        this.id = id ? id : null;
        this.login = login ? login : null;
        this.firstName = firstName ? firstName : null;
        this.lastName = lastName ? lastName : null;
        this.email = email ? email : null;
        this.activated = activated ? activated : false;
        this.langKey = langKey ? langKey : null;
        this.authorities = authorities ? authorities : null;
        this.createdBy = createdBy ? createdBy : null;
        this.createdDate = createdDate ? createdDate : null;
        this.lastModifiedBy = lastModifiedBy ? lastModifiedBy : null;
        this.lastModifiedDate = lastModifiedDate ? lastModifiedDate : null;
        this.password = password ? password : null;
    }
}

const createRequestOption = (req) => {
    let options = new HttpParams();
    if (req) {
        Object.keys(req).forEach(key => {
            if (key !== 'sort') {
                options = options.set(key, req[key]);
            }
        });
        if (req.sort) {
            req.sort.forEach(val => {
                options = options.append('sort', val);
            });
        }
    }
    return options;
};

let UserService = class UserService {
    constructor(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
        this.resourceUrl = serverUrl.SERVER_API_URL + 'api/users';
    }
    create(user) {
        return this.http.post(this.resourceUrl, user, { observe: 'response' });
    }
    update(user) {
        return this.http.put(this.resourceUrl, user, { observe: 'response' });
    }
    find(login) {
        return this.http.get(`${this.resourceUrl}/${login}`, { observe: 'response' });
    }
    query(req) {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, { params: options, observe: 'response' });
    }
    delete(login) {
        return this.http.delete(`${this.resourceUrl}/${login}`, { observe: 'response' });
    }
    authorities() {
        return this.http.get(this.serverUrl + 'api/users/authorities');
    }
};
UserService.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
UserService.ngInjectableDef = ɵɵdefineInjectable({ factory: function UserService_Factory() { return new UserService(ɵɵinject(HttpClient), ɵɵinject(SERVER_API_URL_CONFIG)); }, token: UserService, providedIn: "root" });
UserService = __decorate([
    Injectable({ providedIn: 'root' }),
    __param(1, Inject(SERVER_API_URL_CONFIG))
], UserService);

let MenuService = class MenuService {
    constructor(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
    }
    getMenus() {
        return this.http.get(this.serverUrl.SERVER_API_URL + 'api/modules/menus', {});
    }
};
MenuService.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
MenuService.ngInjectableDef = ɵɵdefineInjectable({ factory: function MenuService_Factory() { return new MenuService(ɵɵinject(HttpClient), ɵɵinject(SERVER_API_URL_CONFIG)); }, token: MenuService, providedIn: "root" });
MenuService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(1, Inject(SERVER_API_URL_CONFIG))
], MenuService);

let CoreModule = class CoreModule {
    constructor() {
        //registerLocaleData(locale);
    }
};
CoreModule = __decorate([
    NgModule({
        imports: [HttpClientModule],
        exports: [],
        declarations: [],
        providers: [
            Title,
            {
                provide: LOCALE_ID,
                useValue: 'en'
            },
            DatePipe
        ]
    })
], CoreModule);

let LgaService = class LgaService {
    constructor(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
        this.resourceUrl = '';
        this.resourceUrl = serverUrl.SERVER_API_URL + '/api/lgas';
    }
    find(id) {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    findByState(id) {
        return this.http.get(`${this.resourceUrl}/state/${id}`, { observe: 'response' });
    }
};
LgaService.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
LgaService.ngInjectableDef = ɵɵdefineInjectable({ factory: function LgaService_Factory() { return new LgaService(ɵɵinject(HttpClient), ɵɵinject(SERVER_API_URL_CONFIG)); }, token: LgaService, providedIn: "root" });
LgaService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(1, Inject(SERVER_API_URL_CONFIG))
], LgaService);

let FacilityService = class FacilityService {
    constructor(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
        this.resourceUrl = '';
        this.resourceUrl = serverUrl.SERVER_API_URL + '/api/facilities';
    }
    create(facility) {
        return this.http
            .post(this.resourceUrl, facility, { observe: 'response' });
    }
    update(facility) {
        return this.http
            .put(this.resourceUrl, facility, { observe: 'response' });
    }
    delete(id) {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    find(id) {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    findByLga(id) {
        return this.http.get(`${this.resourceUrl}/lga/${id}`, { observe: 'response' });
    }
};
FacilityService.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
FacilityService.ngInjectableDef = ɵɵdefineInjectable({ factory: function FacilityService_Factory() { return new FacilityService(ɵɵinject(HttpClient), ɵɵinject(SERVER_API_URL_CONFIG)); }, token: FacilityService, providedIn: "root" });
FacilityService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(1, Inject(SERVER_API_URL_CONFIG))
], FacilityService);

let LoginAuthenticationService = class LoginAuthenticationService {
    constructor(router, loginService, $localStorage, $sessionStorage, eventManager, stateStorageService) {
        this.router = router;
        this.loginService = loginService;
        this.$localStorage = $localStorage;
        this.$sessionStorage = $sessionStorage;
        this.eventManager = eventManager;
        this.stateStorageService = stateStorageService;
    }
    setRedirect(value) {
    }
    isEcmLoggedIn() {
        return false;
    }
    isBpmLoggedIn() {
        return false;
    }
    isOauth() {
        return false;
    }
    getRedirect() {
        return null;
    }
    login(username, password, rememberMe = false) {
        return this.loginService.login({
            username: username,
            password: password,
            rememberMe: rememberMe
        }).pipe(map(() => {
            if (this.router.url === '/account/register' || this.router.url.startsWith('/account/activate') ||
                this.router.url.startsWith('/account/reset/')) {
                this.router.navigate(['']);
            }
            this.eventManager.broadcast({
                name: 'authenticationSuccess',
                content: 'Sending Authentication Success'
            });
            // previousState was set in the authExpiredInterceptor before being redirected to login modal.
            // since login is successful, go to stored previousState and clear previousState
            const redirect = this.stateStorageService.getUrl();
            if (redirect) {
                this.stateStorageService.storeUrl('');
                this.router.navigate([redirect]);
            }
            else {
                this.router.navigate(['/dashboard']);
            }
        }));
    }
};
LoginAuthenticationService.ctorParameters = () => [
    { type: Router },
    { type: LoginService },
    { type: LocalStorageService },
    { type: SessionStorageService },
    { type: JhiEventManager },
    { type: StateStorageService }
];
LoginAuthenticationService = __decorate([
    Injectable()
], LoginAuthenticationService);

let StateService = class StateService {
    constructor(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
        this.resourceUrl = '';
        this.resourceUrl = serverUrl.SERVER_API_URL + '/api/states';
    }
    find(id) {
        return this.http.get(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }
    getStates() {
        return this.http.get(`${this.resourceUrl}`, { observe: 'response' });
    }
};
StateService.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
StateService.ngInjectableDef = ɵɵdefineInjectable({ factory: function StateService_Factory() { return new StateService(ɵɵinject(HttpClient), ɵɵinject(SERVER_API_URL_CONFIG)); }, token: StateService, providedIn: "root" });
StateService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(1, Inject(SERVER_API_URL_CONFIG))
], StateService);

function _window() {
    // return the global native browser window object
    return window;
}
let WindowRef = class WindowRef {
    get nativeWindow() {
        return _window();
    }
};
WindowRef.ngInjectableDef = ɵɵdefineInjectable({ factory: function WindowRef_Factory() { return new WindowRef(); }, token: WindowRef, providedIn: "root" });
WindowRef = __decorate([
    Injectable({ providedIn: 'root' })
], WindowRef);

const PROBLEM_BASE_URL = 'https://www.jhipster.tech/problem';
const EMAIL_ALREADY_USED_TYPE = PROBLEM_BASE_URL + '/email-already-used';
const LOGIN_ALREADY_USED_TYPE = PROBLEM_BASE_URL + '/login-already-used';
const EMAIL_NOT_FOUND_TYPE = PROBLEM_BASE_URL + '/email-not-found';

const ITEMS_PER_PAGE = 20;

const DATE_FORMAT = 'YYYY-MM-DD';
const DATE_TIME_FORMAT = 'YYYY-MM-DDTHH:mm';

let AlertComponent = class AlertComponent {
    constructor(notification, alertService) {
        this.notification = notification;
        this.alertService = alertService;
    }
    ngOnInit() {
        this.alerts = this.alertService.get();
        this.alerts.forEach(alert => this.notification.openSnackMessage(alert.msg, 5000));
    }
    ngOnDestroy() {
        this.alerts = [];
    }
};
AlertComponent.ctorParameters = () => [
    { type: NotificationService },
    { type: JhiAlertService }
];
AlertComponent = __decorate([
    Component({
        selector: 'alert',
        template: ``
    })
], AlertComponent);

let AlertErrorComponent = class AlertErrorComponent {
    constructor(notification, eventManager) {
        this.notification = notification;
        this.eventManager = eventManager;
        /* tslint:enable */
        this.cleanHttpErrorListener = eventManager.subscribe('app.httpError', response => {
            let i;
            const httpErrorResponse = response.content;
            switch (httpErrorResponse.status) {
                // connection refused, server not reachable
                case 0:
                    this.addErrorAlert('Server not reachable', 'error.server.not.reachable');
                    break;
                case 400:
                    const arr = httpErrorResponse.headers.keys();
                    let errorHeader = null;
                    let entityKey = null;
                    arr.forEach(entry => {
                        if (entry.endsWith('app-error')) {
                            errorHeader = httpErrorResponse.headers.get(entry);
                        }
                        else if (entry.endsWith('app-params')) {
                            entityKey = httpErrorResponse.headers.get(entry);
                        }
                    });
                    if (errorHeader) {
                        this.addErrorAlert(errorHeader, errorHeader, { entityName: entityKey });
                    }
                    else if (httpErrorResponse.error !== '' && httpErrorResponse.error.fieldErrors) {
                        const fieldErrors = httpErrorResponse.error.fieldErrors;
                        for (i = 0; i < fieldErrors.length; i++) {
                            const fieldError = fieldErrors[i];
                            // convert 'something[14].other[4].id' to 'something[].other[].id' so translations can be written to it
                            const convertedField = fieldError.field.replace(/\[\d*\]/g, '[]');
                            const fieldName = convertedField.charAt(0).toUpperCase() + convertedField.slice(1);
                            this.addErrorAlert('Error on field "' + fieldName + '"', 'error.' + fieldError.message, { fieldName });
                        }
                    }
                    else if (httpErrorResponse.error !== '' && httpErrorResponse.error.message) {
                        this.addErrorAlert(httpErrorResponse.error.message, httpErrorResponse.error.message, httpErrorResponse.error.params);
                    }
                    else {
                        this.addErrorAlert(httpErrorResponse.error);
                    }
                    break;
                case 404:
                    this.addErrorAlert('Not found', 'error.url.not.found');
                    break;
                default:
                    if (httpErrorResponse.error !== '' && httpErrorResponse.error.message) {
                        this.addErrorAlert(httpErrorResponse.error.message);
                    }
                    else {
                        this.addErrorAlert(httpErrorResponse.error);
                    }
            }
        });
    }
    ngOnDestroy() {
        if (this.cleanHttpErrorListener !== undefined && this.cleanHttpErrorListener !== null) {
            this.eventManager.destroy(this.cleanHttpErrorListener);
        }
    }
    addErrorAlert(message, key, data) {
        this.notification.showError(message);
    }
};
AlertErrorComponent.ctorParameters = () => [
    { type: NotificationService },
    { type: JhiEventManager }
];
AlertErrorComponent = __decorate([
    Component({
        selector: 'alert-error',
        template: ``
    })
], AlertErrorComponent);

/**
 * @whatItDoes Conditionally includes an HTML element if current user has any
 * of the authorities passed as the `expression`.
 *
 * @howToUse
 * ```
 *     <some-element *jhiHasAnyAuthority="'ROLE_ADMIN'">...</some-element>
 *
 *     <some-element *jhiHasAnyAuthority="['ROLE_ADMIN', 'ROLE_USER']">...</some-element>
 * ```
 */
let HasAnyAuthorityDirective = class HasAnyAuthorityDirective {
    constructor(accountService, templateRef, viewContainerRef) {
        this.accountService = accountService;
        this.templateRef = templateRef;
        this.viewContainerRef = viewContainerRef;
    }
    set jhiHasAnyAuthority(value) {
        this.authorities = typeof value === 'string' ? [value] : value;
        this.updateView();
        // Get notified each time authentication state changes.
        this.accountService.getAuthenticationState().subscribe(identity => this.updateView());
    }
    updateView() {
        const hasAnyAuthority = this.accountService.hasAnyAuthority(this.authorities);
        this.viewContainerRef.clear();
        if (hasAnyAuthority) {
            this.viewContainerRef.createEmbeddedView(this.templateRef);
        }
    }
};
HasAnyAuthorityDirective.ctorParameters = () => [
    { type: AccountService },
    { type: TemplateRef },
    { type: ViewContainerRef }
];
__decorate([
    Input()
], HasAnyAuthorityDirective.prototype, "jhiHasAnyAuthority", null);
HasAnyAuthorityDirective = __decorate([
    Directive({
        selector: '[jhiHasAnyAuthority]'
    })
], HasAnyAuthorityDirective);

var PhoneNumberValidator_1;
let PhoneNumberValidator = PhoneNumberValidator_1 = class PhoneNumberValidator {
    constructor() {
        // tslint:disable-next-line:max-line-length
        this.prefixes = '0703|0706|0803|0806|0810|0813|0814|0816|0903|0906|0705|0805|0807|0811|0815|0905|0701|0708|0802|0808|0812|0902|0907|0901|0809|0817|0818|0908|0909|07028|07029|0819|07025|07026|0704|07027|0709|0707|0804|0702';
    }
    validate(control) {
        const phone = control.value;
        if (!phone) {
            return null;
        }
        if (phone.length !== 11) {
            return {
                invalidPhone: true
            };
        }
        const prefix = phone.substr(0, 4);
        if (!this.prefixes.includes(prefix)) {
            return {
                invalidPhone: true
            };
        }
    }
};
PhoneNumberValidator = PhoneNumberValidator_1 = __decorate([
    Directive({
        selector: '[phoneNumber]',
        providers: [{
                provide: NG_VALIDATORS,
                useExisting: PhoneNumberValidator_1,
                multi: true
            }]
    })
], PhoneNumberValidator);

let RelativeTimePipe = class RelativeTimePipe {
    transform(value) {
        if (!(value instanceof Date))
            value = new Date(value);
        let seconds = Math.floor(((new Date()).getTime() - value.getTime()) / 1000);
        let interval = Math.floor(seconds / 31536000);
        if (interval > 1) {
            return interval + " years ago";
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return interval + " months ago";
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return interval + " days ago";
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return interval + " hours ago";
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return interval + " minutes ago";
        }
        return Math.floor(seconds) + " seconds ago";
    }
};
RelativeTimePipe = __decorate([
    Pipe({ name: 'relativeTime' })
], RelativeTimePipe);

let ExcerptPipe = class ExcerptPipe {
    transform(text, limit = 5) {
        if (text.length <= limit)
            return text;
        return text.substring(0, limit) + '...';
    }
};
ExcerptPipe = __decorate([
    Pipe({ name: 'excerpt' })
], ExcerptPipe);

let KeysPipe = class KeysPipe {
    transform(value, args) {
        let keys = [];
        for (let enumMember in value) {
            if (!isNaN(parseInt(enumMember, 10))) {
                keys.push({ key: enumMember, value: value[enumMember] });
            }
        }
        console.log('Keys', keys);
        return keys;
    }
};
KeysPipe = __decorate([
    Pipe({ name: 'keys1' })
], KeysPipe);

let MapValuesPipe = class MapValuesPipe {
    transform(value, args) {
        let returnArray = [];
        value.forEach((entryVal, entryKey) => {
            returnArray.push({
                key: entryKey,
                val: entryVal
            });
        });
        return returnArray;
    }
};
MapValuesPipe = __decorate([
    Pipe({ name: 'mapValues' })
], MapValuesPipe);

let NairaPipe = class NairaPipe {
    constructor() {
        this.pipe = new CurrencyPipe('en');
    }
    transform(value, ...args) {
        return this.pipe.transform(value, '₦');
    }
};
NairaPipe = __decorate([
    Pipe({
        name: 'naira'
    })
], NairaPipe);

let CommonPipesModule = class CommonPipesModule {
};
CommonPipesModule = __decorate([
    NgModule({
        declarations: [
            RelativeTimePipe,
            ExcerptPipe,
            KeysPipe,
            MapValuesPipe,
            NairaPipe
        ],
        exports: [
            RelativeTimePipe,
            ExcerptPipe,
            KeysPipe,
            MapValuesPipe,
            NairaPipe
        ]
    })
], CommonPipesModule);

let SharedCommonModule = class SharedCommonModule {
};
SharedCommonModule = __decorate([
    NgModule({
        imports: [
            CommonModule,
            NgJhipsterModule,
            CommonPipesModule,
        ],
        declarations: [
            AlertComponent,
            AlertErrorComponent,
            PhoneNumberValidator
        ],
        exports: [
            AlertComponent,
            AlertErrorComponent,
            CommonPipesModule,
            PhoneNumberValidator
        ]
    })
], SharedCommonModule);

let AppConfirmComponent = class AppConfirmComponent {
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
};
AppConfirmComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
];
AppConfirmComponent = __decorate([
    Component({
        selector: 'app-confirm',
        template: `<h1 matDialogTitle class="mb-05">{{ data.title }}</h1>
    <div mat-dialog-content class="mb-1">{{ data.message }}</div>
    <div mat-dialog-actions>
        <button
                type="button"
                mat-raised-button
                color="primary"
                (click)="dialogRef.close(true)">OK
        </button>
        &nbsp;
        <span fxFlex></span>
        <button
                type="button"
                color="accent"
                mat-raised-button
                (click)="dialogRef.close(false)">Cancel
        </button>
    </div>`
    }),
    __param(1, Inject(MAT_DIALOG_DATA))
], AppConfirmComponent);

let AppLoaderComponent = class AppLoaderComponent {
    constructor(dialogRef) {
        this.dialogRef = dialogRef;
    }
    ngOnInit() {
    }
};
AppLoaderComponent.ctorParameters = () => [
    { type: MatDialogRef }
];
AppLoaderComponent = __decorate([
    Component({
        selector: 'app-app-loader',
        template: "<div class=\"text-center\">\r\n    <h6 class=\"m-0 pb-1\">{{ title }}</h6>\r\n    <div mat-dialog-content>\r\n        <mat-spinner [style.margin]=\"'auto'\"></mat-spinner>\r\n    </div>\r\n</div>\r\n",
        styles: [".mat-dialog-content{min-height:122px}"]
    })
], AppLoaderComponent);

let AppLoaderService = class AppLoaderService {
    constructor(dialog) {
        this.dialog = dialog;
    }
    open(title = 'Please wait') {
        this.dialogRef = this.dialog.open(AppLoaderComponent, { disableClose: true, backdropClass: 'light-backdrop' });
        this.dialogRef.updateSize('200px');
        this.dialogRef.componentInstance.title = title;
        return this.dialogRef.afterClosed();
    }
    close() {
        if (this.dialogRef) {
            this.dialogRef.close();
        }
    }
};
AppLoaderService.ctorParameters = () => [
    { type: MatDialog }
];
AppLoaderService = __decorate([
    Injectable()
], AppLoaderService);

const screenfull = screenfull_;
let ToggleFullscreenDirective = class ToggleFullscreenDirective {
    onClick() {
        if (screenfull.isEnabled) {
            screenfull.toggle();
        }
    }
};
__decorate([
    HostListener('click')
], ToggleFullscreenDirective.prototype, "onClick", null);
ToggleFullscreenDirective = __decorate([
    Directive({
        selector: '[toggleFullscreen]'
    })
], ToggleFullscreenDirective);

const speedDialFabAnimations = [
    trigger('fabToggler', [
        state('inactive', style({
            transform: 'rotate(0deg)'
        })),
        state('active', style({
            transform: 'rotate(225deg)'
        })),
        transition('* <=> *', animate('200ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
    trigger('speedDialStagger', [
        transition('* => *', [
            query(':enter', style({ opacity: 0 }), { optional: true }),
            query(':enter', stagger('40ms', [
                animate('200ms cubic-bezier(0.4, 0.0, 0.2, 1)', keyframes([
                    style({ opacity: 0, transform: 'translateY(10px)' }),
                    style({ opacity: 1, transform: 'translateY(0)' }),
                ]))
            ]), { optional: true }),
            query(':leave', animate('200ms cubic-bezier(0.4, 0.0, 0.2, 1)', keyframes([
                style({ opacity: 1 }),
                style({ opacity: 0 }),
            ])), { optional: true })
        ])
    ])
];

let SpeedDialFabComponent = class SpeedDialFabComponent {
    constructor() {
        this.fabTogglerState = 'inactive';
        this.buttons = [];
        this.buttonState = [];
    }
    ngOnInit() {
        this.links.forEach(link => this.buttonState.push(link));
    }
    showItems() {
        this.fabTogglerState = 'active';
        this.buttons = this.buttonState;
    }
    hideItems() {
        this.fabTogglerState = 'inactive';
        this.buttons = [];
    }
    onToggleFab() {
        this.buttons.length ? this.hideItems() : this.showItems();
    }
};
__decorate([
    Input()
], SpeedDialFabComponent.prototype, "links", void 0);
SpeedDialFabComponent = __decorate([
    Component({
        selector: 'speed-dial',
        template: "<div class=\"fab-container\">\r\n    <button mat-fab class=\"fab-toggler\"\r\n            (click)=\"onToggleFab()\">\r\n        <mat-icon [@fabToggler]=\"{value: fabTogglerState}\">add</mat-icon>\r\n    </button>\r\n    <div [@speedDialStagger]=\"buttons.length\">\r\n        <button mat-mini-fab *ngFor=\"let btn of buttons\"\r\n                matTooltip=\"{{btn.tooltip}}\"\r\n                [routerLink]=\"['.', btn.state, 'new']\"\r\n                class=\"fab-secondary\"\r\n                color=\"accent\">\r\n            <mat-icon>{{btn.icon}}</mat-icon>\r\n        </button>\r\n    </div>\r\n</div>\r\n\r\n<div id=\"fab-dismiss\"\r\n     *ngIf=\"fabTogglerState==='active'\"\r\n     (click)=\"onToggleFab()\">\r\n</div>\r\n",
        animations: speedDialFabAnimations,
        styles: [""]
    })
], SpeedDialFabComponent);

let LamisSharedModule = class LamisSharedModule {
};
LamisSharedModule = __decorate([
    NgModule({
        imports: [
            CommonModule,
            FormsModule,
            ReactiveFormsModule,
            RouterModule,
            SharedCommonModule,
            MatIconModule,
            MatButtonModule,
            MatTooltipModule,
            MatProgressSpinnerModule,
            MatDialogModule
        ],
        declarations: [
            HasAnyAuthorityDirective,
            SpeedDialFabComponent,
            AppConfirmComponent,
            AppLoaderComponent,
            ToggleFullscreenDirective
        ],
        entryComponents: [
            AppConfirmComponent,
            AppLoaderComponent
        ],
        exports: [
            SharedCommonModule,
            HasAnyAuthorityDirective,
            SpeedDialFabComponent,
            ToggleFullscreenDirective
        ],
        providers: [
            AppLoaderService,
            AppConfirmComponent
        ],
        schemas: [CUSTOM_ELEMENTS_SCHEMA]
    })
], LamisSharedModule);

class Address {
    constructor(street1, street2, city, lga) {
        this.street1 = street1;
        this.street2 = street2;
        this.city = city;
        this.lga = lga;
        this.street1 = street1 ? street1 : null;
        this.street2 = street2 ? street2 : null;
        this.city = city ? city : null;
        this.lga = lga ? lga : null;
    }
}
class PersonName {
    constructor(title, firstName, middleName, surname) {
        this.title = title;
        this.firstName = firstName;
        this.middleName = middleName;
        this.surname = surname;
    }
}
class Phone {
    constructor(phone1, phone2) {
        this.phone1 = phone1;
        this.phone2 = phone2;
    }
}

var FacilityType;
(function (FacilityType) {
    FacilityType[FacilityType["DOCTOR_OFFICE"] = 0] = "DOCTOR_OFFICE";
    FacilityType[FacilityType["PRIMARY_CARE"] = 1] = "PRIMARY_CARE";
    FacilityType[FacilityType["CLINIC"] = 2] = "CLINIC";
    FacilityType[FacilityType["HOSPITAL"] = 3] = "HOSPITAL";
    FacilityType[FacilityType["SPECIALIZED"] = 4] = "SPECIALIZED";
    FacilityType[FacilityType["NURSING_HOME"] = 5] = "NURSING_HOME";
    FacilityType[FacilityType["HOSPICE"] = 6] = "HOSPICE";
    FacilityType[FacilityType["RURAL"] = 7] = "RURAL";
})(FacilityType || (FacilityType = {}));
var PublicLevel;
(function (PublicLevel) {
    PublicLevel[PublicLevel["PRIVATE"] = 0] = "PRIVATE";
    PublicLevel[PublicLevel["PUBLIC"] = 1] = "PUBLIC";
    PublicLevel[PublicLevel["MIXED"] = 2] = "MIXED";
})(PublicLevel || (PublicLevel = {}));

class LGA {
    constructor(id, name, state) {
        this.id = id;
        this.name = name;
        this.state = state;
        this.id = id ? id : null;
        this.name = name ? name : null;
        this.state = state ? state : null;
    }
}

class State {
    constructor(id, name) {
        this.id = id;
        this.name = name;
        this.id = id ? id : null;
        this.name = name ? name : null;
    }
}

class Aggregate {
    constructor(field, key) {
        this.field = field;
        this.key = key;
    }
}
function remove(array, element) {
    return array.filter(e => e.id !== element.id);
}
function clear(array) {
    array.length = 0;
    return array;
}
function contains(array, element) {
    return array.filter(e => e.id === element.id).length > 0;
}
function entityCompare(e1, e2) {
    return e1 && e2 ? e1.id == e2.id : e1 === e2;
}
function enumCompare(e1, e2) {
    return (e1 !== undefined && e2 !== undefined) ? e1.valueOf() == e2.valueOf() : e1 === e2;
}
function replace(array, element) {
    let result = remove(array, element);
    result.push(element);
    return result;
}

const source = 'http://' + window.location.host + '/websocket';
const ɵ0 = () => {
    return new SockJS('' + source);
};
const RxStompConfig = {
    // Which server?
    brokerURL: '' + source,
    webSocketFactory: ɵ0,
    // Headers
    // Typical keys: login, passcode, host
    connectHeaders: {
        login: 'guest',
        passcode: 'guest'
    },
    // How often to heartbeat?
    // Interval in milliseconds, set to 0 to disable
    heartbeatIncoming: 0,
    // Typical value 0 - disabled
    heartbeatOutgoing: 20000,
};

var LamisCoreModule_1;
let LamisCoreModule = LamisCoreModule_1 = class LamisCoreModule {
    static forRoot(serverApiUrlConfig, dateTimeConfig) {
        // MomentDateFormat.DATE_FORMAT = dateTimeConfig.DATE_FORMAT;
        return {
            ngModule: LamisCoreModule_1,
            providers: [
                AuthExpiredInterceptor,
                AuthInterceptor,
                ErrorHandlerInterceptor,
                NotificationInterceptor,
                {
                    provide: SERVER_API_URL_CONFIG,
                    useValue: serverApiUrlConfig
                },
                {
                    provide: InjectableRxStompConfig,
                    useValue: RxStompConfig
                },
                {
                    provide: RxStompService,
                    useFactory: rxStompServiceFactory,
                    deps: [InjectableRxStompConfig]
                }
            ]
        };
    }
};
LamisCoreModule = LamisCoreModule_1 = __decorate([
    NgModule({
        declarations: [],
        imports: [
            CommonModule,
            LamisSharedModule
        ],
        exports: [
            LamisSharedModule
        ],
        providers: []
    })
], LamisCoreModule);

let PagingParamsResolve = class PagingParamsResolve {
    constructor(injector) {
        this.injector = injector;
        this.paginationUtil = this.injector.get(JhiPaginationUtil);
    }
    resolve(route, state) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const query = route.queryParams['query'] ? route.queryParams['query'] : '';
        const filter = route.queryParams['filter'] ? route.queryParams['filter'] : '';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: +page,
            query: query,
            filter: filter,
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    }
};
PagingParamsResolve.ctorParameters = () => [
    { type: Injector }
];
PagingParamsResolve.ngInjectableDef = ɵɵdefineInjectable({ factory: function PagingParamsResolve_Factory() { return new PagingParamsResolve(ɵɵinject(INJECTOR)); }, token: PagingParamsResolve, providedIn: "root" });
PagingParamsResolve = __decorate([
    Injectable({
        providedIn: 'root'
    })
], PagingParamsResolve);

let AppConfirmService = class AppConfirmService {
    constructor(dialog) {
        this.dialog = dialog;
    }
    confirm(data = {}) {
        data.title = data.title || 'Confirm';
        data.message = data.message || 'Are you sure?';
        let dialogRef;
        dialogRef = this.dialog.open(AppConfirmComponent, {
            width: '380px',
            disableClose: true,
            data: { title: data.title, message: data.message }
        });
        return dialogRef.afterClosed();
    }
};
AppConfirmService.ctorParameters = () => [
    { type: MatDialog }
];
AppConfirmService = __decorate([
    Injectable()
], AppConfirmService);

let LayoutTemplateService = class LayoutTemplateService {
    constructor(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
        this.resourceUrl = '';
        this.resourceUrl = serverUrl.SERVER_API_URL + '/api/forms';
    }
    getTemplate(templateId) {
        return this.http.get(`${this.resourceUrl}/by-name/${templateId}`, { observe: 'body' });
    }
};
LayoutTemplateService.ctorParameters = () => [
    { type: HttpClient },
    { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
];
LayoutTemplateService.ngInjectableDef = ɵɵdefineInjectable({ factory: function LayoutTemplateService_Factory() { return new LayoutTemplateService(ɵɵinject(HttpClient), ɵɵinject(SERVER_API_URL_CONFIG)); }, token: LayoutTemplateService, providedIn: "root" });
LayoutTemplateService = __decorate([
    Injectable({
        providedIn: 'root'
    }),
    __param(1, Inject(SERVER_API_URL_CONFIG))
], LayoutTemplateService);

var FieldType;
(function (FieldType) {
    FieldType["date"] = "date";
    FieldType["datetime"] = "datetime";
    FieldType["text"] = "text";
    FieldType["boolean"] = "boolean";
    FieldType["int"] = "int";
    FieldType["float"] = "float";
})(FieldType || (FieldType = {}));
let DetailsComponent = class DetailsComponent {
    constructor(templateService) {
        this.templateService = templateService;
    }
    ngOnInit() {
        this.templateService.getTemplate(this.template).subscribe((json) => {
            this.details = json.template;
        });
    }
    propertiesForDetail(detail) {
        const properties = [];
        for (const field of detail.fields) {
            const dataType = field.type;
            let item;
            switch (dataType) {
                case FieldType.boolean:
                    item = new CardViewBoolItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label
                    });
                    break;
                case FieldType.int:
                    item = new CardViewIntItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label,
                    });
                    break;
                case FieldType.float:
                    item = new CardViewFloatItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label,
                    });
                    break;
                case FieldType.date:
                    item = new CardViewDateItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label,
                        format: 'dd MMM, yyyy'
                    });
                    break;
                case FieldType.datetime:
                    item = new CardViewDatetimeItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label,
                        format: 'dd MMM, yyyy HH:mm'
                    });
                    break;
                default:
                    item = new CardViewTextItemModel({
                        value: this.getValueForKey(field.key),
                        key: '',
                        label: field.label,
                    });
            }
            properties.push(item);
        }
        return properties;
    }
    getValueForKey(key) {
        return get(this.model, key);
    }
};
DetailsComponent.ctorParameters = () => [
    { type: LayoutTemplateService }
];
__decorate([
    Input()
], DetailsComponent.prototype, "template", void 0);
__decorate([
    Input()
], DetailsComponent.prototype, "model", void 0);
DetailsComponent = __decorate([
    Component({
        selector: 'details-component',
        template: "<ng-container *ngIf=\"model && details\">\r\n    <mat-card *ngFor=\"let detail of details\" class=\"default mb-1 pb-0\">\r\n        <ng-container *ngIf=\"!!detail.header\">\r\n            <mat-card-title>{{detail.header}}</mat-card-title>\r\n            <mat-divider></mat-divider>\r\n        </ng-container>\r\n        <mat-card-content>\r\n            <adf-card-view [properties]=\"propertiesForDetail(detail)\"></adf-card-view>\r\n        </mat-card-content>\r\n    </mat-card>\r\n</ng-container>\r\n",
        encapsulation: ViewEncapsulation.None,
        styles: [""]
    })
], DetailsComponent);

/*!
 * @license
 * Copyright 2016 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const MOMENT_DATE_FORMATS = {
    parse: {
        dateInput: 'DD MMM YYYY'
    },
    display: {
        dateInput: 'DD MMM YYYY',
        monthYearLabel: 'MMMM Y',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM Y'
    }
};
const dateNames = [];
for (let date = 1; date <= 31; date++) {
    dateNames.push(String(date));
}

/*!
 * @license
 * Copyright 2016 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class MomentDateAdapter extends DateAdapter {
    constructor() {
        super(...arguments);
        this.localeData = moment.localeData();
    }
    getYear(date) {
        return date.year();
    }
    getMonth(date) {
        return date.month();
    }
    getDate(date) {
        return date.date();
    }
    getDayOfWeek(date) {
        return date.day();
    }
    getMonthNames(style) {
        switch (style) {
            case 'long':
                return this.localeData.months();
            case 'short':
                return this.localeData.monthsShort();
            case 'narrow':
                return this.localeData.monthsShort().map(month => month[0]);
            default:
                return;
        }
    }
    getDateNames() {
        const dateNames = [];
        for (let date = 1; date <= 31; date++) {
            dateNames.push(String(date));
        }
        return dateNames;
    }
    getDayOfWeekNames(style) {
        switch (style) {
            case 'long':
                return this.localeData.weekdays();
            case 'short':
                return this.localeData.weekdaysShort();
            case 'narrow':
                return this.localeData.weekdaysShort();
            default:
                return;
        }
    }
    getYearName(date) {
        return String(date.year());
    }
    getFirstDayOfWeek() {
        return this.localeData.firstDayOfWeek();
    }
    getNumDaysInMonth(date) {
        return date.daysInMonth();
    }
    clone(date) {
        const locale = this.locale || 'en';
        return date.clone().locale(locale);
    }
    createDate(year, month, date) {
        return moment([year, month, date]);
    }
    today() {
        const locale = this.locale || 'en';
        return moment().locale(locale);
    }
    parse(value, parseFormat) {
        const locale = this.locale || 'en';
        if (value && typeof value === 'string') {
            let m = moment(value, parseFormat, locale, true);
            if (!m.isValid()) {
                // use strict parsing because Moment's parser is very forgiving, and this can lead to undesired behavior.
                m = moment(value, this.overrideDisplayFormat, locale, true);
            }
            if (m.isValid()) {
                // if user omits year, it defaults to 2001, so check for that issue.
                if (m.year() === 2001 && value.indexOf('2001') === -1) {
                    // if 2001 not actually in the value string, change to current year
                    const currentYear = new Date().getFullYear();
                    m.set('year', currentYear);
                    // if date is in the future, set previous year
                    if (m.isAfter(moment())) {
                        m.set('year', currentYear - 1);
                    }
                }
            }
            return m;
        }
        return value ? moment(value).locale(locale) : null;
    }
    format(date, displayFormat) {
        date = this.clone(date);
        displayFormat = this.overrideDisplayFormat ? this.overrideDisplayFormat : displayFormat;
        if (date && date.format) {
            return date.format(displayFormat);
        }
        else {
            return '';
        }
    }
    addCalendarYears(date, years) {
        return date.clone().add(years, 'y');
    }
    addCalendarMonths(date, months) {
        return date.clone().add(months, 'M');
    }
    addCalendarDays(date, days) {
        return date.clone().add(days, 'd');
    }
    getISODateString(date) {
        return date.toISOString();
    }
    setLocale(locale) {
        super.setLocale(locale);
        this.localeData = moment.localeData(locale);
    }
    compareDate(first, second) {
        return first.diff(second, 'seconds', true);
    }
    sameDate(first, second) {
        if (first == null) {
            // same if both null
            return second == null;
        }
        else if (isMoment(first)) {
            return first.isSame(second);
        }
        else {
            const isSame = super.sameDate(first, second);
            return isSame;
        }
    }
    clampDate(date, min, max) {
        if (min && date.isBefore(min)) {
            return min;
        }
        else if (max && date.isAfter(max)) {
            return max;
        }
        else {
            return date;
        }
    }
    isDateInstance(date) {
        let isValidDateInstance = false;
        if (date) {
            isValidDateInstance = date._isAMomentObject;
        }
        return isValidDateInstance;
    }
    isValid(date) {
        return date.isValid();
    }
    toIso8601(date) {
        return this.clone(date).format();
    }
    fromIso8601(iso8601String) {
        const locale = this.locale || 'en';
        const d = moment(iso8601String, moment.ISO_8601).locale(locale);
        return this.isValid(d) ? d : null;
    }
    invalid() {
        return moment.invalid();
    }
}

const ɵ0$1 = MOMENT_DATE_FORMATS, ɵ1 = MAT_MOMENT_DATETIME_FORMATS;
let MatDateFormatModule = class MatDateFormatModule {
};
MatDateFormatModule = __decorate([
    NgModule({
        providers: [
            { provide: DateAdapter, useClass: MomentDateAdapter },
            { provide: MAT_DATE_FORMATS, useValue: ɵ0$1 },
            { provide: DatetimeAdapter, useClass: MomentDatetimeAdapter },
            { provide: MAT_DATETIME_FORMATS, useValue: ɵ1 }
        ]
    })
], MatDateFormatModule);

let JsonFormComponent = class JsonFormComponent {
    constructor(templateService, localStorage, sessionStorage) {
        this.templateService = templateService;
        this.localStorage = localStorage;
        this.sessionStorage = sessionStorage;
        this.dataEvent = new EventEmitter(true);
        this.customEvent = new EventEmitter(true);
        this.form = {};
        this.isValid = false;
    }
    ngOnInit() {
        if (this.templateId) {
            this.templateService.getTemplate(this.templateId).subscribe((json) => {
                this.form = json.form;
            });
        }
        else {
            this.form = this.template;
        }
    }
    reset() {
        this.formio.onRefresh({
            submission: this.model
        });
    }
    onCustomEvent(event) {
        this.customEvent.emit(event);
    }
    change(event) {
        if (event.hasOwnProperty('isValid')) {
            this.isValid = event.isValid;
            if (this.isValid) {
                this.dataEvent.emit(event.data);
            }
        }
    }
    ngOnChanges(changes) {
        if (changes['model']) {
            const token = this.localStorage.get('authenticationToken') || this.sessionStorage.get('authenticationToken');
            this.model = {
                data: Object.assign({}, changes['model'].currentValue, { authorization: token })
            };
        }
    }
};
JsonFormComponent.ctorParameters = () => [
    { type: LayoutTemplateService },
    { type: LocalStorageService },
    { type: SessionStorageService }
];
__decorate([
    ViewChild(FormioComponent, { static: true })
], JsonFormComponent.prototype, "formio", void 0);
__decorate([
    Input()
], JsonFormComponent.prototype, "template", void 0);
__decorate([
    Input()
], JsonFormComponent.prototype, "templateId", void 0);
__decorate([
    Input()
], JsonFormComponent.prototype, "model", void 0);
__decorate([
    Output()
], JsonFormComponent.prototype, "dataEvent", void 0);
JsonFormComponent = __decorate([
    Component({
        selector: 'json-form',
        template: `
        <mat-formio [form]="form"
                    (ready)="reset()"
                    (customEvent)="onCustomEvent($event)"
                    (change)="change($event)">
        </mat-formio>
    `
    })
], JsonFormComponent);

let JsonFormModule = class JsonFormModule {
    constructor(localStorage, sessionStorage) {
        this.localStorage = localStorage;
        this.sessionStorage = sessionStorage;
        const token = this.localStorage.get('authenticationToken') || this.sessionStorage.get('authenticationToken');
        Formio.getRequestArgs = (formio, type, url, method, data, opts) => {
            method = (method || 'GET').toUpperCase();
            if (!opts || !isObject(opts)) {
                opts = {};
            }
            opts['Authorization'] = token;
            const requestArgs = {
                url,
                method,
                data: data || null,
                opts
            };
            if (type) {
                requestArgs['type'] = type;
            }
            if (formio) {
                requestArgs['formio'] = formio;
            }
            return requestArgs;
        };
    }
};
JsonFormModule.ctorParameters = () => [
    { type: LocalStorageService },
    { type: SessionStorageService }
];
JsonFormModule = __decorate([
    NgModule({
        imports: [MatFormioModule, CommonModule, CardViewModule],
        declarations: [JsonFormComponent, DetailsComponent],
        exports: [JsonFormComponent, DetailsComponent, MatFormioModule],
        entryComponents: [JsonFormComponent]
    })
], JsonFormModule);

/*
 * Public API Surface of LAMIS Web Core
 */

/**
 * Generated bundle index. Do not edit.
 */

export { Account, AccountService, Address, Aggregate, AlertComponent, AlertErrorComponent, AppConfirmService, AppLoaderService, AuthExpiredInterceptor, AuthInterceptor, AuthServerProvider, CommonPipesModule, CoreModule, DATE_FORMAT, DATE_TIME_FORMAT, DetailsComponent, EMAIL_ALREADY_USED_TYPE, EMAIL_NOT_FOUND_TYPE, ErrorHandlerInterceptor, ExcerptPipe, FacilityService, FacilityType, FieldType, HasAnyAuthorityDirective, ITEMS_PER_PAGE, JsonFormComponent, JsonFormModule, KeysPipe, LGA, LOGIN_ALREADY_USED_TYPE, LamisCoreModule, LamisSharedModule, LgaService, LoginAuthenticationService, LoginService, MOMENT_DATE_FORMATS, MapValuesPipe, MatDateFormatModule, MenuService, MomentDateAdapter, NairaPipe, NotificationInterceptor, PROBLEM_BASE_URL, PagingParamsResolve, PersonName, Phone, PublicLevel, RelativeTimePipe, SERVER_API_URL_CONFIG, SharedCommonModule, SpeedDialFabComponent, State, StateService, StateStorageService, ToggleFullscreenDirective, User, UserRouteAccessService, UserService, WindowRef, clear, contains, createRequestOption, entityCompare, enumCompare, remove, replace, speedDialFabAnimations, ɵ0$1 as ɵ0, ɵ1, PhoneNumberValidator as ɵa, AppConfirmComponent as ɵb, AppLoaderComponent as ɵc, RxStompConfig as ɵd, LayoutTemplateService as ɵe };
//# sourceMappingURL=lamis-web-core.js.map
