import * as tslib_1 from "tslib";
import { Inject, Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LocalStorageService, SessionStorageService } from 'ngx-store';
import { SERVER_API_URL_CONFIG } from '../../app.constants';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../../app.constants";
var AuthServerProvider = /** @class */ (function () {
    function AuthServerProvider(http, injector, serverUrl) {
        this.http = http;
        this.injector = injector;
        this.serverUrl = serverUrl;
        this.$localStorage = this.injector.get(LocalStorageService);
        this.$sessionStorage = this.injector.get(SessionStorageService);
    }
    AuthServerProvider.prototype.getToken = function () {
        return this.$localStorage.get('authenticationToken') || this.$sessionStorage.get('authenticationToken');
    };
    AuthServerProvider.prototype.getAuthorizationToken = function () {
        return 'Bearer ' + this.getToken();
    };
    AuthServerProvider.prototype.login = function (credentials) {
        var _this = this;
        var data = {
            username: credentials.username,
            password: credentials.password,
            rememberMe: credentials.rememberMe
        };
        return this.http
            .post(this.serverUrl.SERVER_API_URL + 'api/authenticate', data)
            .pipe(map(function (response) { return _this.authenticateSuccess(response, credentials.rememberMe); }));
    };
    AuthServerProvider.prototype.logout = function () {
        var _this = this;
        return new Observable(function (observer) {
            _this.$localStorage.remove('authenticationToken');
            _this.$sessionStorage.remove('authenticationToken');
            observer.complete();
        });
    };
    AuthServerProvider.prototype.authenticateSuccess = function (response, rememberMe) {
        var jwt = response.id_token;
        if (rememberMe) {
            this.$localStorage.set('authenticationToken', jwt);
        }
        else {
            this.$sessionStorage.set('authenticationToken', jwt);
        }
    };
    AuthServerProvider.ctorParameters = function () { return [
        { type: HttpClient },
        { type: Injector },
        { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
    ]; };
    AuthServerProvider.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AuthServerProvider_Factory() { return new AuthServerProvider(i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i0.INJECTOR), i0.ɵɵinject(i2.SERVER_API_URL_CONFIG)); }, token: AuthServerProvider, providedIn: "root" });
    AuthServerProvider = tslib_1.__decorate([
        Injectable({ providedIn: 'root' }),
        tslib_1.__param(2, Inject(SERVER_API_URL_CONFIG))
    ], AuthServerProvider);
    return AuthServerProvider;
}());
export { AuthServerProvider };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1qd3Quc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbImNvcmUvYXV0aC9hdXRoLWp3dC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDN0QsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDbEMsT0FBTyxFQUFFLEdBQUcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3JDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxxQkFBcUIsRUFBRSxNQUFNLFdBQVcsQ0FBQztBQUN2RSxPQUFPLEVBQUUscUJBQXFCLEVBQXNCLE1BQU0scUJBQXFCLENBQUM7Ozs7QUFHaEY7SUFJSSw0QkFBb0IsSUFBZ0IsRUFDaEIsUUFBa0IsRUFDYSxTQUE2QjtRQUY1RCxTQUFJLEdBQUosSUFBSSxDQUFZO1FBQ2hCLGFBQVEsR0FBUixRQUFRLENBQVU7UUFDYSxjQUFTLEdBQVQsU0FBUyxDQUFvQjtRQUM1RSxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO0lBQ3BFLENBQUM7SUFFRCxxQ0FBUSxHQUFSO1FBQ0ksT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUM7SUFDNUcsQ0FBQztJQUVELGtEQUFxQixHQUFyQjtRQUNJLE9BQU8sU0FBUyxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUN2QyxDQUFDO0lBRUQsa0NBQUssR0FBTCxVQUFNLFdBQWdCO1FBQXRCLGlCQVNDO1FBUkcsSUFBTSxJQUFJLEdBQUc7WUFDVCxRQUFRLEVBQUUsV0FBVyxDQUFDLFFBQVE7WUFDOUIsUUFBUSxFQUFFLFdBQVcsQ0FBQyxRQUFRO1lBQzlCLFVBQVUsRUFBRSxXQUFXLENBQUMsVUFBVTtTQUNyQyxDQUFDO1FBQ0YsT0FBTyxJQUFJLENBQUMsSUFBSTthQUNYLElBQUksQ0FBTSxJQUFJLENBQUMsU0FBUyxDQUFDLGNBQWMsR0FBRyxrQkFBa0IsRUFBRSxJQUFJLENBQUM7YUFDbkUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFBLFFBQVEsSUFBSSxPQUFBLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLEVBQUUsV0FBVyxDQUFDLFVBQVUsQ0FBQyxFQUExRCxDQUEwRCxDQUFDLENBQUMsQ0FBQztJQUMzRixDQUFDO0lBRUQsbUNBQU0sR0FBTjtRQUFBLGlCQU1DO1FBTEcsT0FBTyxJQUFJLFVBQVUsQ0FBQyxVQUFBLFFBQVE7WUFDMUIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMscUJBQXFCLENBQUMsQ0FBQztZQUNqRCxLQUFJLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQ25ELFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUN4QixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTyxnREFBbUIsR0FBM0IsVUFBNEIsUUFBYSxFQUFFLFVBQW1CO1FBQzFELElBQU0sR0FBRyxHQUFHLFFBQVEsQ0FBQyxRQUFRLENBQUM7UUFDOUIsSUFBSSxVQUFVLEVBQUU7WUFDWixJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxHQUFHLENBQUMsQ0FBQztTQUN0RDthQUFNO1lBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUUsR0FBRyxDQUFDLENBQUM7U0FDeEQ7SUFDTCxDQUFDOztnQkF6Q3lCLFVBQVU7Z0JBQ04sUUFBUTtnREFDekIsTUFBTSxTQUFDLHFCQUFxQjs7O0lBTmhDLGtCQUFrQjtRQUQ5QixVQUFVLENBQUMsRUFBQyxVQUFVLEVBQUUsTUFBTSxFQUFDLENBQUM7UUFPaEIsbUJBQUEsTUFBTSxDQUFDLHFCQUFxQixDQUFDLENBQUE7T0FOakMsa0JBQWtCLENBOEM5Qjs2QkF0REQ7Q0FzREMsQUE5Q0QsSUE4Q0M7U0E5Q1ksa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlLCBJbmplY3RvciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgTG9jYWxTdG9yYWdlU2VydmljZSwgU2Vzc2lvblN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnbmd4LXN0b3JlJztcclxuaW1wb3J0IHsgU0VSVkVSX0FQSV9VUkxfQ09ORklHLCBTZXJ2ZXJBcGlVcmxDb25maWcgfSBmcm9tICcuLi8uLi9hcHAuY29uc3RhbnRzJztcclxuXHJcbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxyXG5leHBvcnQgY2xhc3MgQXV0aFNlcnZlclByb3ZpZGVyIHtcclxuICAgIHByaXZhdGUgJGxvY2FsU3RvcmFnZTogTG9jYWxTdG9yYWdlU2VydmljZTtcclxuICAgIHByaXZhdGUgJHNlc3Npb25TdG9yYWdlOiBTZXNzaW9uU3RvcmFnZVNlcnZpY2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50LFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBpbmplY3RvcjogSW5qZWN0b3IsXHJcbiAgICAgICAgICAgICAgICBASW5qZWN0KFNFUlZFUl9BUElfVVJMX0NPTkZJRykgcHJpdmF0ZSBzZXJ2ZXJVcmw6IFNlcnZlckFwaVVybENvbmZpZykge1xyXG4gICAgICAgIHRoaXMuJGxvY2FsU3RvcmFnZSA9IHRoaXMuaW5qZWN0b3IuZ2V0KExvY2FsU3RvcmFnZVNlcnZpY2UpO1xyXG4gICAgICAgIHRoaXMuJHNlc3Npb25TdG9yYWdlID0gdGhpcy5pbmplY3Rvci5nZXQoU2Vzc2lvblN0b3JhZ2VTZXJ2aWNlKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRUb2tlbigpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy4kbG9jYWxTdG9yYWdlLmdldCgnYXV0aGVudGljYXRpb25Ub2tlbicpIHx8IHRoaXMuJHNlc3Npb25TdG9yYWdlLmdldCgnYXV0aGVudGljYXRpb25Ub2tlbicpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEF1dGhvcml6YXRpb25Ub2tlbigpIHtcclxuICAgICAgICByZXR1cm4gJ0JlYXJlciAnICsgdGhpcy5nZXRUb2tlbigpO1xyXG4gICAgfVxyXG5cclxuICAgIGxvZ2luKGNyZWRlbnRpYWxzOiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IGRhdGEgPSB7XHJcbiAgICAgICAgICAgIHVzZXJuYW1lOiBjcmVkZW50aWFscy51c2VybmFtZSxcclxuICAgICAgICAgICAgcGFzc3dvcmQ6IGNyZWRlbnRpYWxzLnBhc3N3b3JkLFxyXG4gICAgICAgICAgICByZW1lbWJlck1lOiBjcmVkZW50aWFscy5yZW1lbWJlck1lXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwXHJcbiAgICAgICAgICAgIC5wb3N0PGFueT4odGhpcy5zZXJ2ZXJVcmwuU0VSVkVSX0FQSV9VUkwgKyAnYXBpL2F1dGhlbnRpY2F0ZScsIGRhdGEpXHJcbiAgICAgICAgICAgIC5waXBlKG1hcChyZXNwb25zZSA9PiB0aGlzLmF1dGhlbnRpY2F0ZVN1Y2Nlc3MocmVzcG9uc2UsIGNyZWRlbnRpYWxzLnJlbWVtYmVyTWUpKSk7XHJcbiAgICB9XHJcblxyXG4gICAgbG9nb3V0KCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKG9ic2VydmVyID0+IHtcclxuICAgICAgICAgICAgdGhpcy4kbG9jYWxTdG9yYWdlLnJlbW92ZSgnYXV0aGVudGljYXRpb25Ub2tlbicpO1xyXG4gICAgICAgICAgICB0aGlzLiRzZXNzaW9uU3RvcmFnZS5yZW1vdmUoJ2F1dGhlbnRpY2F0aW9uVG9rZW4nKTtcclxuICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGF1dGhlbnRpY2F0ZVN1Y2Nlc3MocmVzcG9uc2U6IGFueSwgcmVtZW1iZXJNZTogYm9vbGVhbik6IHZvaWQge1xyXG4gICAgICAgIGNvbnN0IGp3dCA9IHJlc3BvbnNlLmlkX3Rva2VuO1xyXG4gICAgICAgIGlmIChyZW1lbWJlck1lKSB7XHJcbiAgICAgICAgICAgIHRoaXMuJGxvY2FsU3RvcmFnZS5zZXQoJ2F1dGhlbnRpY2F0aW9uVG9rZW4nLCBqd3QpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuJHNlc3Npb25TdG9yYWdlLnNldCgnYXV0aGVudGljYXRpb25Ub2tlbicsIGp3dCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==