import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { AccountService } from './account.service';
import { StateStorageService } from './state-storage.service';
import * as i0 from "@angular/core";
import * as i1 from "@angular/router";
import * as i2 from "./account.service";
import * as i3 from "./state-storage.service";
var UserRouteAccessService = /** @class */ (function () {
    function UserRouteAccessService(router, accountService, stateStorageService) {
        this.router = router;
        this.accountService = accountService;
        this.stateStorageService = stateStorageService;
    }
    UserRouteAccessService.prototype.canActivate = function (route, state) {
        var authorities = route.data['authorities'];
        // We need to call the checkLogin / and so the accountService.identity() function, to ensure,
        // that the client has a principal too, if they already logged in by the server.
        // This could happen on a page refresh.
        return this.checkLogin(authorities, state.url);
    };
    UserRouteAccessService.prototype.checkLogin = function (authorities, url) {
        var _this = this;
        return this.accountService.identity().then(function (account) {
            if (!authorities || authorities.length === 0) {
                return true;
            }
            if (account && account.login !== 'anonymoususer') {
                var hasAnyAuthority = _this.accountService.hasAnyAuthority(authorities);
                if (hasAnyAuthority) {
                    return true;
                }
                _this.router.navigate(['sessions/accessdenied']);
                return false;
            }
            _this.stateStorageService.storeUrl(url);
            _this.router.navigate(['sessions/login']);
            return false;
        });
    };
    UserRouteAccessService.ctorParameters = function () { return [
        { type: Router },
        { type: AccountService },
        { type: StateStorageService }
    ]; };
    UserRouteAccessService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function UserRouteAccessService_Factory() { return new UserRouteAccessService(i0.ɵɵinject(i1.Router), i0.ɵɵinject(i2.AccountService), i0.ɵɵinject(i3.StateStorageService)); }, token: UserRouteAccessService, providedIn: "root" });
    UserRouteAccessService = tslib_1.__decorate([
        Injectable({ providedIn: 'root' })
    ], UserRouteAccessService);
    return UserRouteAccessService;
}());
export { UserRouteAccessService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci1yb3V0ZS1hY2Nlc3Mtc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbImNvcmUvYXV0aC91c2VyLXJvdXRlLWFjY2Vzcy1zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxXQUFXLEVBQUUsTUFBTSxFQUFFLG1CQUFtQixFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFFbkcsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlCQUF5QixDQUFDOzs7OztBQUc5RDtJQUNJLGdDQUNZLE1BQWMsRUFDZCxjQUE4QixFQUM5QixtQkFBd0M7UUFGeEMsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQUNkLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5Qix3QkFBbUIsR0FBbkIsbUJBQW1CLENBQXFCO0lBQ2pELENBQUM7SUFFSiw0Q0FBVyxHQUFYLFVBQVksS0FBNkIsRUFBRSxLQUEwQjtRQUNqRSxJQUFNLFdBQVcsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzlDLDZGQUE2RjtRQUM3RixnRkFBZ0Y7UUFDaEYsdUNBQXVDO1FBQ3ZDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ25ELENBQUM7SUFFRCwyQ0FBVSxHQUFWLFVBQVcsV0FBcUIsRUFBRSxHQUFXO1FBQTdDLGlCQW1CQztRQWxCRyxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQUEsT0FBTztZQUM5QyxJQUFJLENBQUMsV0FBVyxJQUFJLFdBQVcsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO2dCQUMxQyxPQUFPLElBQUksQ0FBQzthQUNmO1lBRUQsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLEtBQUssS0FBSyxlQUFlLEVBQUU7Z0JBQzlDLElBQU0sZUFBZSxHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUN6RSxJQUFJLGVBQWUsRUFBRTtvQkFDakIsT0FBTyxJQUFJLENBQUM7aUJBQ2Y7Z0JBQ0QsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hELE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1lBRUQsS0FBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN2QyxLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztZQUN6QyxPQUFPLEtBQUssQ0FBQztRQUNqQixDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7O2dCQWhDbUIsTUFBTTtnQkFDRSxjQUFjO2dCQUNULG1CQUFtQjs7O0lBSjNDLHNCQUFzQjtRQURsQyxVQUFVLENBQUMsRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLENBQUM7T0FDdEIsc0JBQXNCLENBbUNsQztpQ0ExQ0Q7Q0EwQ0MsQUFuQ0QsSUFtQ0M7U0FuQ1ksc0JBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBDYW5BY3RpdmF0ZSwgUm91dGVyLCBSb3V0ZXJTdGF0ZVNuYXBzaG90IH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuXHJcbmltcG9ydCB7IEFjY291bnRTZXJ2aWNlIH0gZnJvbSAnLi9hY2NvdW50LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTdGF0ZVN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnLi9zdGF0ZS1zdG9yYWdlLnNlcnZpY2UnO1xyXG5cclxuQEluamVjdGFibGUoeyBwcm92aWRlZEluOiAncm9vdCcgfSlcclxuZXhwb3J0IGNsYXNzIFVzZXJSb3V0ZUFjY2Vzc1NlcnZpY2UgaW1wbGVtZW50cyBDYW5BY3RpdmF0ZSB7XHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxyXG4gICAgICAgIHByaXZhdGUgYWNjb3VudFNlcnZpY2U6IEFjY291bnRTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgc3RhdGVTdG9yYWdlU2VydmljZTogU3RhdGVTdG9yYWdlU2VydmljZVxyXG4gICAgKSB7fVxyXG5cclxuICAgIGNhbkFjdGl2YXRlKHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdCk6IGJvb2xlYW4gfCBQcm9taXNlPGJvb2xlYW4+IHtcclxuICAgICAgICBjb25zdCBhdXRob3JpdGllcyA9IHJvdXRlLmRhdGFbJ2F1dGhvcml0aWVzJ107XHJcbiAgICAgICAgLy8gV2UgbmVlZCB0byBjYWxsIHRoZSBjaGVja0xvZ2luIC8gYW5kIHNvIHRoZSBhY2NvdW50U2VydmljZS5pZGVudGl0eSgpIGZ1bmN0aW9uLCB0byBlbnN1cmUsXHJcbiAgICAgICAgLy8gdGhhdCB0aGUgY2xpZW50IGhhcyBhIHByaW5jaXBhbCB0b28sIGlmIHRoZXkgYWxyZWFkeSBsb2dnZWQgaW4gYnkgdGhlIHNlcnZlci5cclxuICAgICAgICAvLyBUaGlzIGNvdWxkIGhhcHBlbiBvbiBhIHBhZ2UgcmVmcmVzaC5cclxuICAgICAgICByZXR1cm4gdGhpcy5jaGVja0xvZ2luKGF1dGhvcml0aWVzLCBzdGF0ZS51cmwpO1xyXG4gICAgfVxyXG5cclxuICAgIGNoZWNrTG9naW4oYXV0aG9yaXRpZXM6IHN0cmluZ1tdLCB1cmw6IHN0cmluZyk6IFByb21pc2U8Ym9vbGVhbj4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFjY291bnRTZXJ2aWNlLmlkZW50aXR5KCkudGhlbihhY2NvdW50ID0+IHtcclxuICAgICAgICAgICAgaWYgKCFhdXRob3JpdGllcyB8fCBhdXRob3JpdGllcy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoYWNjb3VudCAmJiBhY2NvdW50LmxvZ2luICE9PSAnYW5vbnltb3VzdXNlcicpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGhhc0FueUF1dGhvcml0eSA9IHRoaXMuYWNjb3VudFNlcnZpY2UuaGFzQW55QXV0aG9yaXR5KGF1dGhvcml0aWVzKTtcclxuICAgICAgICAgICAgICAgIGlmIChoYXNBbnlBdXRob3JpdHkpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnc2Vzc2lvbnMvYWNjZXNzZGVuaWVkJ10pO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLnN0YXRlU3RvcmFnZVNlcnZpY2Uuc3RvcmVVcmwodXJsKTtcclxuICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWydzZXNzaW9ucy9sb2dpbiddKTtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==