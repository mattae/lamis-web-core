import * as tslib_1 from "tslib";
import { LOCALE_ID, NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Title } from '@angular/platform-browser';
var CoreModule = /** @class */ (function () {
    function CoreModule() {
        //registerLocaleData(locale);
    }
    CoreModule = tslib_1.__decorate([
        NgModule({
            imports: [HttpClientModule],
            exports: [],
            declarations: [],
            providers: [
                Title,
                {
                    provide: LOCALE_ID,
                    useValue: 'en'
                },
                DatePipe
            ]
        })
    ], CoreModule);
    return CoreModule;
}());
export { CoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJjb3JlL2NvcmUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNwRCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDM0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBZWxEO0lBQ0k7UUFDSSw2QkFBNkI7SUFDakMsQ0FBQztJQUhRLFVBQVU7UUFidEIsUUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFLENBQUMsZ0JBQWdCLENBQUM7WUFDM0IsT0FBTyxFQUFFLEVBQUU7WUFDWCxZQUFZLEVBQUUsRUFBRTtZQUNoQixTQUFTLEVBQUU7Z0JBQ1AsS0FBSztnQkFDTDtvQkFDSSxPQUFPLEVBQUUsU0FBUztvQkFDbEIsUUFBUSxFQUFFLElBQUk7aUJBQ2pCO2dCQUNELFFBQVE7YUFDWDtTQUNKLENBQUM7T0FDVyxVQUFVLENBSXRCO0lBQUQsaUJBQUM7Q0FBQSxBQUpELElBSUM7U0FKWSxVQUFVIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTE9DQUxFX0lELCBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEYXRlUGlwZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IFRpdGxlIH0gZnJvbSAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlcic7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgaW1wb3J0czogW0h0dHBDbGllbnRNb2R1bGVdLFxyXG4gICAgZXhwb3J0czogW10sXHJcbiAgICBkZWNsYXJhdGlvbnM6IFtdLFxyXG4gICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAgVGl0bGUsXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBwcm92aWRlOiBMT0NBTEVfSUQsXHJcbiAgICAgICAgICAgIHVzZVZhbHVlOiAnZW4nXHJcbiAgICAgICAgfSxcclxuICAgICAgICBEYXRlUGlwZVxyXG4gICAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29yZU1vZHVsZSB7XHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICAvL3JlZ2lzdGVyTG9jYWxlRGF0YShsb2NhbGUpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==