import * as tslib_1 from "tslib";
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { InjectableRxStompConfig, RxStompService, rxStompServiceFactory } from '@stomp/ng2-stompjs';
import { SERVER_API_URL_CONFIG } from './app.constants';
import { AuthExpiredInterceptor } from './blocks/interceptor/auth-expired.interceptor';
import { AuthInterceptor } from './blocks/interceptor/auth.interceptor';
import { ErrorHandlerInterceptor } from './blocks/interceptor/errorhandler.interceptor';
import { NotificationInterceptor } from './blocks/interceptor/notification.interceptor';
import { RxStompConfig } from './services/rx-stomp.config';
import { LamisSharedModule } from './shared/lamis-shared.module';
var LamisCoreModule = /** @class */ (function () {
    function LamisCoreModule() {
    }
    LamisCoreModule_1 = LamisCoreModule;
    LamisCoreModule.forRoot = function (serverApiUrlConfig, dateTimeConfig) {
        // MomentDateFormat.DATE_FORMAT = dateTimeConfig.DATE_FORMAT;
        return {
            ngModule: LamisCoreModule_1,
            providers: [
                AuthExpiredInterceptor,
                AuthInterceptor,
                ErrorHandlerInterceptor,
                NotificationInterceptor,
                {
                    provide: SERVER_API_URL_CONFIG,
                    useValue: serverApiUrlConfig
                },
                {
                    provide: InjectableRxStompConfig,
                    useValue: RxStompConfig
                },
                {
                    provide: RxStompService,
                    useFactory: rxStompServiceFactory,
                    deps: [InjectableRxStompConfig]
                }
            ]
        };
    };
    var LamisCoreModule_1;
    LamisCoreModule = LamisCoreModule_1 = tslib_1.__decorate([
        NgModule({
            declarations: [],
            imports: [
                CommonModule,
                LamisSharedModule
            ],
            exports: [
                LamisSharedModule
            ],
            providers: []
        })
    ], LamisCoreModule);
    return LamisCoreModule;
}());
export { LamisCoreModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGFtaXMuY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJsYW1pcy5jb3JlLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBdUIsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzlELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxjQUFjLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNwRyxPQUFPLEVBQWtCLHFCQUFxQixFQUFzQixNQUFNLGlCQUFpQixDQUFDO0FBQzVGLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQ3ZGLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUN4RSxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUN4RixPQUFPLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUN4RixPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDM0QsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFjakU7SUFBQTtJQTBCQSxDQUFDO3dCQTFCWSxlQUFlO0lBQ2pCLHVCQUFPLEdBQWQsVUFBZSxrQkFBc0MsRUFBRSxjQUErQjtRQUNsRiw2REFBNkQ7UUFDN0QsT0FBTztZQUNILFFBQVEsRUFBRSxpQkFBZTtZQUN6QixTQUFTLEVBQUU7Z0JBQ1Asc0JBQXNCO2dCQUN0QixlQUFlO2dCQUNmLHVCQUF1QjtnQkFDdkIsdUJBQXVCO2dCQUN2QjtvQkFDSSxPQUFPLEVBQUUscUJBQXFCO29CQUM5QixRQUFRLEVBQUUsa0JBQWtCO2lCQUMvQjtnQkFDRDtvQkFDSSxPQUFPLEVBQUUsdUJBQXVCO29CQUNoQyxRQUFRLEVBQUUsYUFBYTtpQkFDMUI7Z0JBQ0Q7b0JBQ0ksT0FBTyxFQUFFLGNBQWM7b0JBQ3ZCLFVBQVUsRUFBRSxxQkFBcUI7b0JBQ2pDLElBQUksRUFBRSxDQUFDLHVCQUF1QixDQUFDO2lCQUNsQzthQUNKO1NBQ0osQ0FBQztJQUNOLENBQUM7O0lBekJRLGVBQWU7UUFYM0IsUUFBUSxDQUFDO1lBQ04sWUFBWSxFQUFFLEVBQUU7WUFDaEIsT0FBTyxFQUFFO2dCQUNMLFlBQVk7Z0JBQ1osaUJBQWlCO2FBQ3BCO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLGlCQUFpQjthQUNwQjtZQUNELFNBQVMsRUFBRSxFQUFFO1NBQ2hCLENBQUM7T0FDVyxlQUFlLENBMEIzQjtJQUFELHNCQUFDO0NBQUEsQUExQkQsSUEwQkM7U0ExQlksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IE1vZHVsZVdpdGhQcm92aWRlcnMsIE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEluamVjdGFibGVSeFN0b21wQ29uZmlnLCBSeFN0b21wU2VydmljZSwgcnhTdG9tcFNlcnZpY2VGYWN0b3J5IH0gZnJvbSAnQHN0b21wL25nMi1zdG9tcGpzJztcclxuaW1wb3J0IHsgRGF0ZVRpbWVDb25maWcsIFNFUlZFUl9BUElfVVJMX0NPTkZJRywgU2VydmVyQXBpVXJsQ29uZmlnIH0gZnJvbSAnLi9hcHAuY29uc3RhbnRzJztcclxuaW1wb3J0IHsgQXV0aEV4cGlyZWRJbnRlcmNlcHRvciB9IGZyb20gJy4vYmxvY2tzL2ludGVyY2VwdG9yL2F1dGgtZXhwaXJlZC5pbnRlcmNlcHRvcic7XHJcbmltcG9ydCB7IEF1dGhJbnRlcmNlcHRvciB9IGZyb20gJy4vYmxvY2tzL2ludGVyY2VwdG9yL2F1dGguaW50ZXJjZXB0b3InO1xyXG5pbXBvcnQgeyBFcnJvckhhbmRsZXJJbnRlcmNlcHRvciB9IGZyb20gJy4vYmxvY2tzL2ludGVyY2VwdG9yL2Vycm9yaGFuZGxlci5pbnRlcmNlcHRvcic7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvbkludGVyY2VwdG9yIH0gZnJvbSAnLi9ibG9ja3MvaW50ZXJjZXB0b3Ivbm90aWZpY2F0aW9uLmludGVyY2VwdG9yJztcclxuaW1wb3J0IHsgUnhTdG9tcENvbmZpZyB9IGZyb20gJy4vc2VydmljZXMvcngtc3RvbXAuY29uZmlnJztcclxuaW1wb3J0IHsgTGFtaXNTaGFyZWRNb2R1bGUgfSBmcm9tICcuL3NoYXJlZC9sYW1pcy1zaGFyZWQubW9kdWxlJztcclxuXHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgZGVjbGFyYXRpb25zOiBbXSxcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgTGFtaXNTaGFyZWRNb2R1bGVcclxuICAgIF0sXHJcbiAgICBleHBvcnRzOiBbXHJcbiAgICAgICAgTGFtaXNTaGFyZWRNb2R1bGVcclxuICAgIF0sXHJcbiAgICBwcm92aWRlcnM6IFtdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMYW1pc0NvcmVNb2R1bGUge1xyXG4gICAgc3RhdGljIGZvclJvb3Qoc2VydmVyQXBpVXJsQ29uZmlnOiBTZXJ2ZXJBcGlVcmxDb25maWcsIGRhdGVUaW1lQ29uZmlnPzogRGF0ZVRpbWVDb25maWcpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgICAgICAvLyBNb21lbnREYXRlRm9ybWF0LkRBVEVfRk9STUFUID0gZGF0ZVRpbWVDb25maWcuREFURV9GT1JNQVQ7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgbmdNb2R1bGU6IExhbWlzQ29yZU1vZHVsZSxcclxuICAgICAgICAgICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAgICAgICAgICBBdXRoRXhwaXJlZEludGVyY2VwdG9yLFxyXG4gICAgICAgICAgICAgICAgQXV0aEludGVyY2VwdG9yLFxyXG4gICAgICAgICAgICAgICAgRXJyb3JIYW5kbGVySW50ZXJjZXB0b3IsXHJcbiAgICAgICAgICAgICAgICBOb3RpZmljYXRpb25JbnRlcmNlcHRvcixcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBwcm92aWRlOiBTRVJWRVJfQVBJX1VSTF9DT05GSUcsXHJcbiAgICAgICAgICAgICAgICAgICAgdXNlVmFsdWU6IHNlcnZlckFwaVVybENvbmZpZ1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBwcm92aWRlOiBJbmplY3RhYmxlUnhTdG9tcENvbmZpZyxcclxuICAgICAgICAgICAgICAgICAgICB1c2VWYWx1ZTogUnhTdG9tcENvbmZpZ1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBwcm92aWRlOiBSeFN0b21wU2VydmljZSxcclxuICAgICAgICAgICAgICAgICAgICB1c2VGYWN0b3J5OiByeFN0b21wU2VydmljZUZhY3RvcnksXHJcbiAgICAgICAgICAgICAgICAgICAgZGVwczogW0luamVjdGFibGVSeFN0b21wQ29uZmlnXVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBdXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxufVxyXG4iXX0=