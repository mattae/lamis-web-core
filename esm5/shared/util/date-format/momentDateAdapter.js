/*!
 * @license
 * Copyright 2016 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import * as tslib_1 from "tslib";
import { DateAdapter } from '@angular/material';
import { isMoment } from 'moment';
import moment from 'moment-es6';
var MomentDateAdapter = /** @class */ (function (_super) {
    tslib_1.__extends(MomentDateAdapter, _super);
    function MomentDateAdapter() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.localeData = moment.localeData();
        return _this;
    }
    MomentDateAdapter.prototype.getYear = function (date) {
        return date.year();
    };
    MomentDateAdapter.prototype.getMonth = function (date) {
        return date.month();
    };
    MomentDateAdapter.prototype.getDate = function (date) {
        return date.date();
    };
    MomentDateAdapter.prototype.getDayOfWeek = function (date) {
        return date.day();
    };
    MomentDateAdapter.prototype.getMonthNames = function (style) {
        switch (style) {
            case 'long':
                return this.localeData.months();
            case 'short':
                return this.localeData.monthsShort();
            case 'narrow':
                return this.localeData.monthsShort().map(function (month) { return month[0]; });
            default:
                return;
        }
    };
    MomentDateAdapter.prototype.getDateNames = function () {
        var dateNames = [];
        for (var date = 1; date <= 31; date++) {
            dateNames.push(String(date));
        }
        return dateNames;
    };
    MomentDateAdapter.prototype.getDayOfWeekNames = function (style) {
        switch (style) {
            case 'long':
                return this.localeData.weekdays();
            case 'short':
                return this.localeData.weekdaysShort();
            case 'narrow':
                return this.localeData.weekdaysShort();
            default:
                return;
        }
    };
    MomentDateAdapter.prototype.getYearName = function (date) {
        return String(date.year());
    };
    MomentDateAdapter.prototype.getFirstDayOfWeek = function () {
        return this.localeData.firstDayOfWeek();
    };
    MomentDateAdapter.prototype.getNumDaysInMonth = function (date) {
        return date.daysInMonth();
    };
    MomentDateAdapter.prototype.clone = function (date) {
        var locale = this.locale || 'en';
        return date.clone().locale(locale);
    };
    MomentDateAdapter.prototype.createDate = function (year, month, date) {
        return moment([year, month, date]);
    };
    MomentDateAdapter.prototype.today = function () {
        var locale = this.locale || 'en';
        return moment().locale(locale);
    };
    MomentDateAdapter.prototype.parse = function (value, parseFormat) {
        var locale = this.locale || 'en';
        if (value && typeof value === 'string') {
            var m = moment(value, parseFormat, locale, true);
            if (!m.isValid()) {
                // use strict parsing because Moment's parser is very forgiving, and this can lead to undesired behavior.
                m = moment(value, this.overrideDisplayFormat, locale, true);
            }
            if (m.isValid()) {
                // if user omits year, it defaults to 2001, so check for that issue.
                if (m.year() === 2001 && value.indexOf('2001') === -1) {
                    // if 2001 not actually in the value string, change to current year
                    var currentYear = new Date().getFullYear();
                    m.set('year', currentYear);
                    // if date is in the future, set previous year
                    if (m.isAfter(moment())) {
                        m.set('year', currentYear - 1);
                    }
                }
            }
            return m;
        }
        return value ? moment(value).locale(locale) : null;
    };
    MomentDateAdapter.prototype.format = function (date, displayFormat) {
        date = this.clone(date);
        displayFormat = this.overrideDisplayFormat ? this.overrideDisplayFormat : displayFormat;
        if (date && date.format) {
            return date.format(displayFormat);
        }
        else {
            return '';
        }
    };
    MomentDateAdapter.prototype.addCalendarYears = function (date, years) {
        return date.clone().add(years, 'y');
    };
    MomentDateAdapter.prototype.addCalendarMonths = function (date, months) {
        return date.clone().add(months, 'M');
    };
    MomentDateAdapter.prototype.addCalendarDays = function (date, days) {
        return date.clone().add(days, 'd');
    };
    MomentDateAdapter.prototype.getISODateString = function (date) {
        return date.toISOString();
    };
    MomentDateAdapter.prototype.setLocale = function (locale) {
        _super.prototype.setLocale.call(this, locale);
        this.localeData = moment.localeData(locale);
    };
    MomentDateAdapter.prototype.compareDate = function (first, second) {
        return first.diff(second, 'seconds', true);
    };
    MomentDateAdapter.prototype.sameDate = function (first, second) {
        if (first == null) {
            // same if both null
            return second == null;
        }
        else if (isMoment(first)) {
            return first.isSame(second);
        }
        else {
            var isSame = _super.prototype.sameDate.call(this, first, second);
            return isSame;
        }
    };
    MomentDateAdapter.prototype.clampDate = function (date, min, max) {
        if (min && date.isBefore(min)) {
            return min;
        }
        else if (max && date.isAfter(max)) {
            return max;
        }
        else {
            return date;
        }
    };
    MomentDateAdapter.prototype.isDateInstance = function (date) {
        var isValidDateInstance = false;
        if (date) {
            isValidDateInstance = date._isAMomentObject;
        }
        return isValidDateInstance;
    };
    MomentDateAdapter.prototype.isValid = function (date) {
        return date.isValid();
    };
    MomentDateAdapter.prototype.toIso8601 = function (date) {
        return this.clone(date).format();
    };
    MomentDateAdapter.prototype.fromIso8601 = function (iso8601String) {
        var locale = this.locale || 'en';
        var d = moment(iso8601String, moment.ISO_8601).locale(locale);
        return this.isValid(d) ? d : null;
    };
    MomentDateAdapter.prototype.invalid = function () {
        return moment.invalid();
    };
    return MomentDateAdapter;
}(DateAdapter));
export { MomentDateAdapter };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9tZW50RGF0ZUFkYXB0ZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvdXRpbC9kYXRlLWZvcm1hdC9tb21lbnREYXRlQWRhcHRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7Ozs7O0dBZUc7O0FBRUgsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ2hELE9BQU8sRUFBRSxRQUFRLEVBQVUsTUFBTSxRQUFRLENBQUM7QUFDMUMsT0FBTyxNQUFNLE1BQU0sWUFBWSxDQUFDO0FBRWhDO0lBQXVDLDZDQUFtQjtJQUExRDtRQUFBLHFFQW9NQztRQWxNVyxnQkFBVSxHQUFRLE1BQU0sQ0FBQyxVQUFVLEVBQUUsQ0FBQzs7SUFrTWxELENBQUM7SUE5TEcsbUNBQU8sR0FBUCxVQUFRLElBQVk7UUFDaEIsT0FBTyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQUVELG9DQUFRLEdBQVIsVUFBUyxJQUFZO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3hCLENBQUM7SUFFRCxtQ0FBTyxHQUFQLFVBQVEsSUFBWTtRQUNoQixPQUFPLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN2QixDQUFDO0lBRUQsd0NBQVksR0FBWixVQUFhLElBQVk7UUFDckIsT0FBTyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQztJQUVELHlDQUFhLEdBQWIsVUFBYyxLQUFrQztRQUM1QyxRQUFRLEtBQUssRUFBRTtZQUNYLEtBQUssTUFBTTtnQkFDUCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUM7WUFDcEMsS0FBSyxPQUFPO2dCQUNSLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUN6QyxLQUFLLFFBQVE7Z0JBQ1QsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLEdBQUcsQ0FBQyxVQUFBLEtBQUssSUFBSSxPQUFBLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBUixDQUFRLENBQUMsQ0FBQztZQUNoRTtnQkFDSSxPQUFPO1NBQ2Q7SUFDTCxDQUFDO0lBRUQsd0NBQVksR0FBWjtRQUNJLElBQU0sU0FBUyxHQUFhLEVBQUUsQ0FBQztRQUMvQixLQUFLLElBQUksSUFBSSxHQUFHLENBQUMsRUFBRSxJQUFJLElBQUksRUFBRSxFQUFFLElBQUksRUFBRSxFQUFFO1lBQ25DLFNBQVMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7U0FDaEM7UUFFRCxPQUFPLFNBQVMsQ0FBQztJQUNyQixDQUFDO0lBRUQsNkNBQWlCLEdBQWpCLFVBQWtCLEtBQWtDO1FBQ2hELFFBQVEsS0FBSyxFQUFFO1lBQ1gsS0FBSyxNQUFNO2dCQUNQLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QyxLQUFLLE9BQU87Z0JBQ1IsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQzNDLEtBQUssUUFBUTtnQkFDVCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLENBQUM7WUFDM0M7Z0JBQ0ksT0FBTztTQUNkO0lBQ0wsQ0FBQztJQUVELHVDQUFXLEdBQVgsVUFBWSxJQUFZO1FBQ3BCLE9BQU8sTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDO0lBQy9CLENBQUM7SUFFRCw2Q0FBaUIsR0FBakI7UUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDNUMsQ0FBQztJQUVELDZDQUFpQixHQUFqQixVQUFrQixJQUFZO1FBQzFCLE9BQU8sSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQzlCLENBQUM7SUFFRCxpQ0FBSyxHQUFMLFVBQU0sSUFBWTtRQUNkLElBQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDO1FBQ25DLE9BQU8sSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRUQsc0NBQVUsR0FBVixVQUFXLElBQVksRUFBRSxLQUFhLEVBQUUsSUFBWTtRQUNoRCxPQUFPLE1BQU0sQ0FBQyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRUQsaUNBQUssR0FBTDtRQUNJLElBQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDO1FBQ25DLE9BQU8sTUFBTSxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFRCxpQ0FBSyxHQUFMLFVBQU0sS0FBVSxFQUFFLFdBQWdCO1FBQzlCLElBQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDO1FBRW5DLElBQUksS0FBSyxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsRUFBRTtZQUNwQyxJQUFJLENBQUMsR0FBRyxNQUFNLENBQUMsS0FBSyxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDakQsSUFBSSxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTtnQkFDZCx5R0FBeUc7Z0JBQ3pHLENBQUMsR0FBRyxNQUFNLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7YUFDL0Q7WUFDRCxJQUFJLENBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTtnQkFDYixvRUFBb0U7Z0JBQ3BFLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxLQUFLLElBQUksSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFO29CQUNuRCxtRUFBbUU7b0JBQ25FLElBQU0sV0FBVyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQzdDLENBQUMsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLFdBQVcsQ0FBQyxDQUFDO29CQUMzQiw4Q0FBOEM7b0JBQzlDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFO3dCQUNyQixDQUFDLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxXQUFXLEdBQUcsQ0FBQyxDQUFDLENBQUM7cUJBQ2xDO2lCQUNKO2FBQ0o7WUFDRCxPQUFPLENBQUMsQ0FBQztTQUNaO1FBRUQsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUN2RCxDQUFDO0lBRUQsa0NBQU0sR0FBTixVQUFPLElBQVksRUFBRSxhQUFrQjtRQUNuQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4QixhQUFhLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQztRQUV4RixJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ3JCLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUNyQzthQUFNO1lBQ0gsT0FBTyxFQUFFLENBQUM7U0FDYjtJQUNMLENBQUM7SUFFRCw0Q0FBZ0IsR0FBaEIsVUFBaUIsSUFBWSxFQUFFLEtBQWE7UUFDeEMsT0FBTyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztJQUN4QyxDQUFDO0lBRUQsNkNBQWlCLEdBQWpCLFVBQWtCLElBQVksRUFBRSxNQUFjO1FBQzFDLE9BQU8sSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDekMsQ0FBQztJQUVELDJDQUFlLEdBQWYsVUFBZ0IsSUFBWSxFQUFFLElBQVk7UUFDdEMsT0FBTyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRUQsNENBQWdCLEdBQWhCLFVBQWlCLElBQVk7UUFDekIsT0FBTyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDOUIsQ0FBQztJQUVELHFDQUFTLEdBQVQsVUFBVSxNQUFXO1FBQ2pCLGlCQUFNLFNBQVMsWUFBQyxNQUFNLENBQUMsQ0FBQztRQUV4QixJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDaEQsQ0FBQztJQUVELHVDQUFXLEdBQVgsVUFBWSxLQUFhLEVBQUUsTUFBYztRQUNyQyxPQUFPLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBRUQsb0NBQVEsR0FBUixVQUFTLEtBQW1CLEVBQUUsTUFBb0I7UUFDOUMsSUFBSSxLQUFLLElBQUksSUFBSSxFQUFFO1lBQ2Ysb0JBQW9CO1lBQ3BCLE9BQU8sTUFBTSxJQUFJLElBQUksQ0FBQztTQUN6QjthQUFNLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFO1lBQ3hCLE9BQU8sS0FBSyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUMvQjthQUFNO1lBQ0gsSUFBTSxNQUFNLEdBQUcsaUJBQU0sUUFBUSxZQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQztZQUM3QyxPQUFPLE1BQU0sQ0FBQztTQUNqQjtJQUNMLENBQUM7SUFFRCxxQ0FBUyxHQUFULFVBQVUsSUFBWSxFQUFFLEdBQWtCLEVBQUUsR0FBa0I7UUFDMUQsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUMzQixPQUFPLEdBQUcsQ0FBQztTQUNkO2FBQU0sSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUNqQyxPQUFPLEdBQUcsQ0FBQztTQUNkO2FBQU07WUFDSCxPQUFPLElBQUksQ0FBQztTQUNmO0lBQ0wsQ0FBQztJQUVELDBDQUFjLEdBQWQsVUFBZSxJQUFTO1FBQ3BCLElBQUksbUJBQW1CLEdBQUcsS0FBSyxDQUFDO1FBRWhDLElBQUksSUFBSSxFQUFFO1lBQ04sbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO1NBQy9DO1FBRUQsT0FBTyxtQkFBbUIsQ0FBQztJQUMvQixDQUFDO0lBRUQsbUNBQU8sR0FBUCxVQUFRLElBQVk7UUFDaEIsT0FBTyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDMUIsQ0FBQztJQUVELHFDQUFTLEdBQVQsVUFBVSxJQUFZO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUNyQyxDQUFDO0lBRUQsdUNBQVcsR0FBWCxVQUFZLGFBQXFCO1FBQzdCLElBQU0sTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDO1FBQ25DLElBQU0sQ0FBQyxHQUFHLE1BQU0sQ0FBQyxhQUFhLEVBQUUsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNoRSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO0lBQ3RDLENBQUM7SUFFRCxtQ0FBTyxHQUFQO1FBQ0ksT0FBTyxNQUFNLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDNUIsQ0FBQztJQUNMLHdCQUFDO0FBQUQsQ0FBQyxBQXBNRCxDQUF1QyxXQUFXLEdBb01qRCIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxNiBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBEYXRlQWRhcHRlciB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgaXNNb21lbnQsIE1vbWVudCB9IGZyb20gJ21vbWVudCc7XHJcbmltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50LWVzNic7XHJcblxyXG5leHBvcnQgY2xhc3MgTW9tZW50RGF0ZUFkYXB0ZXIgZXh0ZW5kcyBEYXRlQWRhcHRlcjxNb21lbnQ+IHtcclxuXHJcbiAgICBwcml2YXRlIGxvY2FsZURhdGE6IGFueSA9IG1vbWVudC5sb2NhbGVEYXRhKCk7XHJcblxyXG4gICAgb3ZlcnJpZGVEaXNwbGF5Rm9ybWF0OiBzdHJpbmc7XHJcblxyXG4gICAgZ2V0WWVhcihkYXRlOiBNb21lbnQpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiBkYXRlLnllYXIoKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRNb250aChkYXRlOiBNb21lbnQpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiBkYXRlLm1vbnRoKCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RGF0ZShkYXRlOiBNb21lbnQpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiBkYXRlLmRhdGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXREYXlPZldlZWsoZGF0ZTogTW9tZW50KTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gZGF0ZS5kYXkoKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRNb250aE5hbWVzKHN0eWxlOiAnbG9uZycgfCAnc2hvcnQnIHwgJ25hcnJvdycpOiBzdHJpbmdbXSB7XHJcbiAgICAgICAgc3dpdGNoIChzdHlsZSkge1xyXG4gICAgICAgICAgICBjYXNlICdsb25nJzpcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmxvY2FsZURhdGEubW9udGhzKCk7XHJcbiAgICAgICAgICAgIGNhc2UgJ3Nob3J0JzpcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmxvY2FsZURhdGEubW9udGhzU2hvcnQoKTtcclxuICAgICAgICAgICAgY2FzZSAnbmFycm93JzpcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmxvY2FsZURhdGEubW9udGhzU2hvcnQoKS5tYXAobW9udGggPT4gbW9udGhbMF0pO1xyXG4gICAgICAgICAgICBkZWZhdWx0IDpcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RGF0ZU5hbWVzKCk6IHN0cmluZ1tdIHtcclxuICAgICAgICBjb25zdCBkYXRlTmFtZXM6IHN0cmluZ1tdID0gW107XHJcbiAgICAgICAgZm9yIChsZXQgZGF0ZSA9IDE7IGRhdGUgPD0gMzE7IGRhdGUrKykge1xyXG4gICAgICAgICAgICBkYXRlTmFtZXMucHVzaChTdHJpbmcoZGF0ZSkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGRhdGVOYW1lcztcclxuICAgIH1cclxuXHJcbiAgICBnZXREYXlPZldlZWtOYW1lcyhzdHlsZTogJ2xvbmcnIHwgJ3Nob3J0JyB8ICduYXJyb3cnKTogc3RyaW5nW10ge1xyXG4gICAgICAgIHN3aXRjaCAoc3R5bGUpIHtcclxuICAgICAgICAgICAgY2FzZSAnbG9uZyc6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5sb2NhbGVEYXRhLndlZWtkYXlzKCk7XHJcbiAgICAgICAgICAgIGNhc2UgJ3Nob3J0JzpcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmxvY2FsZURhdGEud2Vla2RheXNTaG9ydCgpO1xyXG4gICAgICAgICAgICBjYXNlICduYXJyb3cnOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMubG9jYWxlRGF0YS53ZWVrZGF5c1Nob3J0KCk7XHJcbiAgICAgICAgICAgIGRlZmF1bHQgOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRZZWFyTmFtZShkYXRlOiBNb21lbnQpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBTdHJpbmcoZGF0ZS55ZWFyKCkpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEZpcnN0RGF5T2ZXZWVrKCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMubG9jYWxlRGF0YS5maXJzdERheU9mV2VlaygpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE51bURheXNJbk1vbnRoKGRhdGU6IE1vbWVudCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIGRhdGUuZGF5c0luTW9udGgoKTtcclxuICAgIH1cclxuXHJcbiAgICBjbG9uZShkYXRlOiBNb21lbnQpOiBNb21lbnQge1xyXG4gICAgICAgIGNvbnN0IGxvY2FsZSA9IHRoaXMubG9jYWxlIHx8ICdlbic7XHJcbiAgICAgICAgcmV0dXJuIGRhdGUuY2xvbmUoKS5sb2NhbGUobG9jYWxlKTtcclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVEYXRlKHllYXI6IG51bWJlciwgbW9udGg6IG51bWJlciwgZGF0ZTogbnVtYmVyKTogTW9tZW50IHtcclxuICAgICAgICByZXR1cm4gbW9tZW50KFt5ZWFyLCBtb250aCwgZGF0ZV0pO1xyXG4gICAgfVxyXG5cclxuICAgIHRvZGF5KCk6IE1vbWVudCB7XHJcbiAgICAgICAgY29uc3QgbG9jYWxlID0gdGhpcy5sb2NhbGUgfHwgJ2VuJztcclxuICAgICAgICByZXR1cm4gbW9tZW50KCkubG9jYWxlKGxvY2FsZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcGFyc2UodmFsdWU6IGFueSwgcGFyc2VGb3JtYXQ6IGFueSk6IE1vbWVudCB7XHJcbiAgICAgICAgY29uc3QgbG9jYWxlID0gdGhpcy5sb2NhbGUgfHwgJ2VuJztcclxuXHJcbiAgICAgICAgaWYgKHZhbHVlICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgbGV0IG0gPSBtb21lbnQodmFsdWUsIHBhcnNlRm9ybWF0LCBsb2NhbGUsIHRydWUpO1xyXG4gICAgICAgICAgICBpZiAoIW0uaXNWYWxpZCgpKSB7XHJcbiAgICAgICAgICAgICAgICAvLyB1c2Ugc3RyaWN0IHBhcnNpbmcgYmVjYXVzZSBNb21lbnQncyBwYXJzZXIgaXMgdmVyeSBmb3JnaXZpbmcsIGFuZCB0aGlzIGNhbiBsZWFkIHRvIHVuZGVzaXJlZCBiZWhhdmlvci5cclxuICAgICAgICAgICAgICAgIG0gPSBtb21lbnQodmFsdWUsIHRoaXMub3ZlcnJpZGVEaXNwbGF5Rm9ybWF0LCBsb2NhbGUsIHRydWUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChtLmlzVmFsaWQoKSkge1xyXG4gICAgICAgICAgICAgICAgLy8gaWYgdXNlciBvbWl0cyB5ZWFyLCBpdCBkZWZhdWx0cyB0byAyMDAxLCBzbyBjaGVjayBmb3IgdGhhdCBpc3N1ZS5cclxuICAgICAgICAgICAgICAgIGlmIChtLnllYXIoKSA9PT0gMjAwMSAmJiB2YWx1ZS5pbmRleE9mKCcyMDAxJykgPT09IC0xKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gaWYgMjAwMSBub3QgYWN0dWFsbHkgaW4gdGhlIHZhbHVlIHN0cmluZywgY2hhbmdlIHRvIGN1cnJlbnQgeWVhclxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGN1cnJlbnRZZWFyID0gbmV3IERhdGUoKS5nZXRGdWxsWWVhcigpO1xyXG4gICAgICAgICAgICAgICAgICAgIG0uc2V0KCd5ZWFyJywgY3VycmVudFllYXIpO1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIGlmIGRhdGUgaXMgaW4gdGhlIGZ1dHVyZSwgc2V0IHByZXZpb3VzIHllYXJcclxuICAgICAgICAgICAgICAgICAgICBpZiAobS5pc0FmdGVyKG1vbWVudCgpKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtLnNldCgneWVhcicsIGN1cnJlbnRZZWFyIC0gMSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBtO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHZhbHVlID8gbW9tZW50KHZhbHVlKS5sb2NhbGUobG9jYWxlKSA6IG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgZm9ybWF0KGRhdGU6IE1vbWVudCwgZGlzcGxheUZvcm1hdDogYW55KTogc3RyaW5nIHtcclxuICAgICAgICBkYXRlID0gdGhpcy5jbG9uZShkYXRlKTtcclxuICAgICAgICBkaXNwbGF5Rm9ybWF0ID0gdGhpcy5vdmVycmlkZURpc3BsYXlGb3JtYXQgPyB0aGlzLm92ZXJyaWRlRGlzcGxheUZvcm1hdCA6IGRpc3BsYXlGb3JtYXQ7XHJcblxyXG4gICAgICAgIGlmIChkYXRlICYmIGRhdGUuZm9ybWF0KSB7XHJcbiAgICAgICAgICAgIHJldHVybiBkYXRlLmZvcm1hdChkaXNwbGF5Rm9ybWF0KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gJyc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGFkZENhbGVuZGFyWWVhcnMoZGF0ZTogTW9tZW50LCB5ZWFyczogbnVtYmVyKTogTW9tZW50IHtcclxuICAgICAgICByZXR1cm4gZGF0ZS5jbG9uZSgpLmFkZCh5ZWFycywgJ3knKTtcclxuICAgIH1cclxuXHJcbiAgICBhZGRDYWxlbmRhck1vbnRocyhkYXRlOiBNb21lbnQsIG1vbnRoczogbnVtYmVyKTogTW9tZW50IHtcclxuICAgICAgICByZXR1cm4gZGF0ZS5jbG9uZSgpLmFkZChtb250aHMsICdNJyk7XHJcbiAgICB9XHJcblxyXG4gICAgYWRkQ2FsZW5kYXJEYXlzKGRhdGU6IE1vbWVudCwgZGF5czogbnVtYmVyKTogTW9tZW50IHtcclxuICAgICAgICByZXR1cm4gZGF0ZS5jbG9uZSgpLmFkZChkYXlzLCAnZCcpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldElTT0RhdGVTdHJpbmcoZGF0ZTogTW9tZW50KTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gZGF0ZS50b0lTT1N0cmluZygpO1xyXG4gICAgfVxyXG5cclxuICAgIHNldExvY2FsZShsb2NhbGU6IGFueSk6IHZvaWQge1xyXG4gICAgICAgIHN1cGVyLnNldExvY2FsZShsb2NhbGUpO1xyXG5cclxuICAgICAgICB0aGlzLmxvY2FsZURhdGEgPSBtb21lbnQubG9jYWxlRGF0YShsb2NhbGUpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbXBhcmVEYXRlKGZpcnN0OiBNb21lbnQsIHNlY29uZDogTW9tZW50KTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gZmlyc3QuZGlmZihzZWNvbmQsICdzZWNvbmRzJywgdHJ1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgc2FtZURhdGUoZmlyc3Q6IGFueSB8IE1vbWVudCwgc2Vjb25kOiBhbnkgfCBNb21lbnQpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAoZmlyc3QgPT0gbnVsbCkge1xyXG4gICAgICAgICAgICAvLyBzYW1lIGlmIGJvdGggbnVsbFxyXG4gICAgICAgICAgICByZXR1cm4gc2Vjb25kID09IG51bGw7XHJcbiAgICAgICAgfSBlbHNlIGlmIChpc01vbWVudChmaXJzdCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZpcnN0LmlzU2FtZShzZWNvbmQpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGlzU2FtZSA9IHN1cGVyLnNhbWVEYXRlKGZpcnN0LCBzZWNvbmQpO1xyXG4gICAgICAgICAgICByZXR1cm4gaXNTYW1lO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjbGFtcERhdGUoZGF0ZTogTW9tZW50LCBtaW4/OiBhbnkgfCBNb21lbnQsIG1heD86IGFueSB8IE1vbWVudCk6IE1vbWVudCB7XHJcbiAgICAgICAgaWYgKG1pbiAmJiBkYXRlLmlzQmVmb3JlKG1pbikpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG1pbjtcclxuICAgICAgICB9IGVsc2UgaWYgKG1heCAmJiBkYXRlLmlzQWZ0ZXIobWF4KSkge1xyXG4gICAgICAgICAgICByZXR1cm4gbWF4O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBkYXRlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpc0RhdGVJbnN0YW5jZShkYXRlOiBhbnkpIHtcclxuICAgICAgICBsZXQgaXNWYWxpZERhdGVJbnN0YW5jZSA9IGZhbHNlO1xyXG5cclxuICAgICAgICBpZiAoZGF0ZSkge1xyXG4gICAgICAgICAgICBpc1ZhbGlkRGF0ZUluc3RhbmNlID0gZGF0ZS5faXNBTW9tZW50T2JqZWN0O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGlzVmFsaWREYXRlSW5zdGFuY2U7XHJcbiAgICB9XHJcblxyXG4gICAgaXNWYWxpZChkYXRlOiBNb21lbnQpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gZGF0ZS5pc1ZhbGlkKCk7XHJcbiAgICB9XHJcblxyXG4gICAgdG9Jc284NjAxKGRhdGU6IE1vbWVudCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY2xvbmUoZGF0ZSkuZm9ybWF0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgZnJvbUlzbzg2MDEoaXNvODYwMVN0cmluZzogc3RyaW5nKTogTW9tZW50IHwgbnVsbCB7XHJcbiAgICAgICAgY29uc3QgbG9jYWxlID0gdGhpcy5sb2NhbGUgfHwgJ2VuJztcclxuICAgICAgICBjb25zdCBkID0gbW9tZW50KGlzbzg2MDFTdHJpbmcsIG1vbWVudC5JU09fODYwMSkubG9jYWxlKGxvY2FsZSk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNWYWxpZChkKSA/IGQgOiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIGludmFsaWQoKTogTW9tZW50IHtcclxuICAgICAgICByZXR1cm4gbW9tZW50LmludmFsaWQoKTtcclxuICAgIH1cclxufVxyXG4iXX0=