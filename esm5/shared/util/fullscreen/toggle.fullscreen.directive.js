import * as tslib_1 from "tslib";
import { Directive, HostListener } from '@angular/core';
import * as screenfull_ from 'screenfull';
var screenfull = screenfull_;
var ToggleFullscreenDirective = /** @class */ (function () {
    function ToggleFullscreenDirective() {
    }
    ToggleFullscreenDirective.prototype.onClick = function () {
        if (screenfull.isEnabled) {
            screenfull.toggle();
        }
    };
    tslib_1.__decorate([
        HostListener('click')
    ], ToggleFullscreenDirective.prototype, "onClick", null);
    ToggleFullscreenDirective = tslib_1.__decorate([
        Directive({
            selector: '[toggleFullscreen]'
        })
    ], ToggleFullscreenDirective);
    return ToggleFullscreenDirective;
}());
export { ToggleFullscreenDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidG9nZ2xlLmZ1bGxzY3JlZW4uZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL3V0aWwvZnVsbHNjcmVlbi90b2dnbGUuZnVsbHNjcmVlbi5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBQyxTQUFTLEVBQUUsWUFBWSxFQUFDLE1BQU0sZUFBZSxDQUFDO0FBQ3RELE9BQU8sS0FBSyxXQUFXLE1BQU0sWUFBWSxDQUFDO0FBRTFDLElBQU0sVUFBVSxHQUFHLFdBQVcsQ0FBQztBQUsvQjtJQUFBO0lBTUEsQ0FBQztJQUwwQiwyQ0FBTyxHQUFQO1FBQ25CLElBQUksVUFBVSxDQUFDLFNBQVMsRUFBRTtZQUN0QixVQUFVLENBQUMsTUFBTSxFQUFFLENBQUM7U0FDdkI7SUFDTCxDQUFDO0lBSnNCO1FBQXRCLFlBQVksQ0FBQyxPQUFPLENBQUM7NERBSXJCO0lBTFEseUJBQXlCO1FBSHJDLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxvQkFBb0I7U0FDakMsQ0FBQztPQUNXLHlCQUF5QixDQU1yQztJQUFELGdDQUFDO0NBQUEsQUFORCxJQU1DO1NBTlkseUJBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtEaXJlY3RpdmUsIEhvc3RMaXN0ZW5lcn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgKiBhcyBzY3JlZW5mdWxsXyBmcm9tICdzY3JlZW5mdWxsJztcblxuY29uc3Qgc2NyZWVuZnVsbCA9IHNjcmVlbmZ1bGxfO1xuXG5ARGlyZWN0aXZlKHtcbiAgICBzZWxlY3RvcjogJ1t0b2dnbGVGdWxsc2NyZWVuXSdcbn0pXG5leHBvcnQgY2xhc3MgVG9nZ2xlRnVsbHNjcmVlbkRpcmVjdGl2ZSB7XG4gICAgQEhvc3RMaXN0ZW5lcignY2xpY2snKSBvbkNsaWNrKCkge1xuICAgICAgICBpZiAoc2NyZWVuZnVsbC5pc0VuYWJsZWQpIHtcbiAgICAgICAgICAgIHNjcmVlbmZ1bGwudG9nZ2xlKCk7XG4gICAgICAgIH1cbiAgICB9XG59XG4iXX0=