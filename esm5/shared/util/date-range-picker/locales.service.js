import * as tslib_1 from "tslib";
import { Inject, Injectable } from '@angular/core';
import { DefaultLocaleConfig, LOCALE_CONFIG } from './date-range-picker.config';
var LocaleService = /** @class */ (function () {
    function LocaleService(_config) {
        this._config = _config;
    }
    Object.defineProperty(LocaleService.prototype, "config", {
        get: function () {
            if (!this._config) {
                return DefaultLocaleConfig;
            }
            return tslib_1.__assign({}, DefaultLocaleConfig, this._config);
        },
        enumerable: true,
        configurable: true
    });
    LocaleService.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [LOCALE_CONFIG,] }] }
    ]; };
    LocaleService = tslib_1.__decorate([
        Injectable(),
        tslib_1.__param(0, Inject(LOCALE_CONFIG))
    ], LocaleService);
    return LocaleService;
}());
export { LocaleService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYWxlcy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL3V0aWwvZGF0ZS1yYW5nZS1waWNrZXIvbG9jYWxlcy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsYUFBYSxFQUFnQixNQUFNLDRCQUE0QixDQUFDO0FBRzlGO0lBQ0MsdUJBQTJDLE9BQXFCO1FBQXJCLFlBQU8sR0FBUCxPQUFPLENBQWM7SUFDaEUsQ0FBQztJQUVELHNCQUFJLGlDQUFNO2FBQVY7WUFDQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDbEIsT0FBTyxtQkFBbUIsQ0FBQzthQUMzQjtZQUVELDRCQUFXLG1CQUFtQixFQUFLLElBQUksQ0FBQyxPQUFPLEVBQUM7UUFDakQsQ0FBQzs7O09BQUE7O2dEQVRZLE1BQU0sU0FBQyxhQUFhOztJQURyQixhQUFhO1FBRHpCLFVBQVUsRUFBRTtRQUVDLG1CQUFBLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQTtPQUR0QixhQUFhLENBV3pCO0lBQUQsb0JBQUM7Q0FBQSxBQVhELElBV0M7U0FYWSxhQUFhIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERlZmF1bHRMb2NhbGVDb25maWcsIExPQ0FMRV9DT05GSUcsIExvY2FsZUNvbmZpZyB9IGZyb20gJy4vZGF0ZS1yYW5nZS1waWNrZXIuY29uZmlnJztcclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIExvY2FsZVNlcnZpY2Uge1xyXG5cdGNvbnN0cnVjdG9yKEBJbmplY3QoTE9DQUxFX0NPTkZJRykgcHJpdmF0ZSBfY29uZmlnOiBMb2NhbGVDb25maWcpIHtcclxuXHR9XHJcblxyXG5cdGdldCBjb25maWcoKSB7XHJcblx0XHRpZiAoIXRoaXMuX2NvbmZpZykge1xyXG5cdFx0XHRyZXR1cm4gRGVmYXVsdExvY2FsZUNvbmZpZztcclxuXHRcdH1cclxuXHJcblx0XHRyZXR1cm4gey4uLkRlZmF1bHRMb2NhbGVDb25maWcsIC4uLnRoaXMuX2NvbmZpZ31cclxuXHR9XHJcbn1cclxuIl19