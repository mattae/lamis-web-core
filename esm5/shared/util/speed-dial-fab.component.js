import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { speedDialFabAnimations } from './speed-dial-fab-animation';
var SpeedDialFabComponent = /** @class */ (function () {
    function SpeedDialFabComponent() {
        this.fabTogglerState = 'inactive';
        this.buttons = [];
        this.buttonState = [];
    }
    SpeedDialFabComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.links.forEach(function (link) { return _this.buttonState.push(link); });
    };
    SpeedDialFabComponent.prototype.showItems = function () {
        this.fabTogglerState = 'active';
        this.buttons = this.buttonState;
    };
    SpeedDialFabComponent.prototype.hideItems = function () {
        this.fabTogglerState = 'inactive';
        this.buttons = [];
    };
    SpeedDialFabComponent.prototype.onToggleFab = function () {
        this.buttons.length ? this.hideItems() : this.showItems();
    };
    tslib_1.__decorate([
        Input()
    ], SpeedDialFabComponent.prototype, "links", void 0);
    SpeedDialFabComponent = tslib_1.__decorate([
        Component({
            selector: 'speed-dial',
            template: "<div class=\"fab-container\">\r\n    <button mat-fab class=\"fab-toggler\"\r\n            (click)=\"onToggleFab()\">\r\n        <mat-icon [@fabToggler]=\"{value: fabTogglerState}\">add</mat-icon>\r\n    </button>\r\n    <div [@speedDialStagger]=\"buttons.length\">\r\n        <button mat-mini-fab *ngFor=\"let btn of buttons\"\r\n                matTooltip=\"{{btn.tooltip}}\"\r\n                [routerLink]=\"['.', btn.state, 'new']\"\r\n                class=\"fab-secondary\"\r\n                color=\"accent\">\r\n            <mat-icon>{{btn.icon}}</mat-icon>\r\n        </button>\r\n    </div>\r\n</div>\r\n\r\n<div id=\"fab-dismiss\"\r\n     *ngIf=\"fabTogglerState==='active'\"\r\n     (click)=\"onToggleFab()\">\r\n</div>\r\n",
            animations: speedDialFabAnimations,
            styles: [""]
        })
    ], SpeedDialFabComponent);
    return SpeedDialFabComponent;
}());
export { SpeedDialFabComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3BlZWQtZGlhbC1mYWIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL3V0aWwvc3BlZWQtZGlhbC1mYWIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBVSxNQUFNLGVBQWUsQ0FBQztBQUN6RCxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQVFwRTtJQVlJO1FBSkEsb0JBQWUsR0FBRyxVQUFVLENBQUM7UUFDN0IsWUFBTyxHQUFVLEVBQUUsQ0FBQztRQUNwQixnQkFBVyxHQUFVLEVBQUUsQ0FBQztJQUl4QixDQUFDO0lBYkQsd0NBQVEsR0FBUjtRQUFBLGlCQUVDO1FBREcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBM0IsQ0FBMkIsQ0FBQyxDQUFDO0lBQzVELENBQUM7SUFhRCx5Q0FBUyxHQUFUO1FBQ0ksSUFBSSxDQUFDLGVBQWUsR0FBRyxRQUFRLENBQUM7UUFDaEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQ3BDLENBQUM7SUFFRCx5Q0FBUyxHQUFUO1FBQ0ksSUFBSSxDQUFDLGVBQWUsR0FBRyxVQUFVLENBQUM7UUFDbEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUE7SUFDckIsQ0FBQztJQUVELDJDQUFXLEdBQVg7UUFDSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDOUQsQ0FBQztJQXRCRDtRQURDLEtBQUssRUFBRTt3REFDa0Q7SUFOakQscUJBQXFCO1FBTmpDLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxZQUFZO1lBQ3RCLDJ1QkFBOEM7WUFFOUMsVUFBVSxFQUFFLHNCQUFzQjs7U0FDckMsQ0FBQztPQUNXLHFCQUFxQixDQTZCakM7SUFBRCw0QkFBQztDQUFBLEFBN0JELElBNkJDO1NBN0JZLHFCQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBzcGVlZERpYWxGYWJBbmltYXRpb25zIH0gZnJvbSAnLi9zcGVlZC1kaWFsLWZhYi1hbmltYXRpb24nO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ3NwZWVkLWRpYWwnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3NwZWVkLWRpYWwtZmFiLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL3NwZWVkLWRpYWwtZmFiLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgICBhbmltYXRpb25zOiBzcGVlZERpYWxGYWJBbmltYXRpb25zXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTcGVlZERpYWxGYWJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5saW5rcy5mb3JFYWNoKGxpbmsgPT4gdGhpcy5idXR0b25TdGF0ZS5wdXNoKGxpbmspKTtcclxuICAgIH1cclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgbGlua3M6IHsgc3RhdGU6IHN0cmluZywgaWNvbjogc3RyaW5nLCB0b29sdGlwOiBzdHJpbmcgfVtdO1xyXG5cclxuICAgIGZhYlRvZ2dsZXJTdGF0ZSA9ICdpbmFjdGl2ZSc7XHJcbiAgICBidXR0b25zOiBhbnlbXSA9IFtdO1xyXG4gICAgYnV0dG9uU3RhdGU6IGFueVtdID0gW107XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIHNob3dJdGVtcygpIHtcclxuICAgICAgICB0aGlzLmZhYlRvZ2dsZXJTdGF0ZSA9ICdhY3RpdmUnO1xyXG4gICAgICAgIHRoaXMuYnV0dG9ucyA9IHRoaXMuYnV0dG9uU3RhdGU7XHJcbiAgICB9XHJcblxyXG4gICAgaGlkZUl0ZW1zKCkge1xyXG4gICAgICAgIHRoaXMuZmFiVG9nZ2xlclN0YXRlID0gJ2luYWN0aXZlJztcclxuICAgICAgICB0aGlzLmJ1dHRvbnMgPSBbXVxyXG4gICAgfVxyXG5cclxuICAgIG9uVG9nZ2xlRmFiKCkge1xyXG4gICAgICAgIHRoaXMuYnV0dG9ucy5sZW5ndGggPyB0aGlzLmhpZGVJdGVtcygpIDogdGhpcy5zaG93SXRlbXMoKTtcclxuICAgIH1cclxufVxyXG4iXX0=