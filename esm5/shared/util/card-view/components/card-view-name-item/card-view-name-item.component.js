import * as tslib_1 from "tslib";
import { Component, Input, ViewChild } from '@angular/core';
import { CardViewUpdateService } from '@alfresco/adf-core';
import { PersonName } from '../../../../model/address.model';
var CardViewNameItemComponent = /** @class */ (function () {
    function CardViewNameItemComponent(cardViewUpdateService) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.editable = false;
        this.displayEmpty = true;
        this.inEdit = false;
    }
    CardViewNameItemComponent.prototype.ngOnChanges = function () {
        this.editedTitle = this.property.value.title;
        this.editedFirstName = this.property.value.firstName;
        this.editedMiddleName = this.property.value.middleName;
        this.editedSurname = this.property.value.surname;
    };
    CardViewNameItemComponent.prototype.showProperty = function () {
        return this.displayEmpty || !this.property.isEmpty();
    };
    CardViewNameItemComponent.prototype.isEditable = function () {
        return this.editable && this.property.editable;
    };
    CardViewNameItemComponent.prototype.isClickable = function () {
        return this.property.clickable;
    };
    CardViewNameItemComponent.prototype.hasIcon = function () {
        return !!this.property.icon;
    };
    CardViewNameItemComponent.prototype.hasErrors = function () {
        return this.errorMessages && this.errorMessages.length;
    };
    CardViewNameItemComponent.prototype.setEditMode = function (editStatus) {
        var _this = this;
        this.inEdit = editStatus;
        setTimeout(function () {
            if (_this.titleInput) {
                _this.titleInput.nativeElement.click();
            }
        }, 0);
        setTimeout(function () {
            if (_this.firstNameInput) {
                _this.firstNameInput.nativeElement.click();
            }
        }, 0);
        setTimeout(function () {
            if (_this.middleNameInput) {
                _this.middleNameInput.nativeElement.click();
            }
        }, 0);
        setTimeout(function () {
            if (_this.surnameInput) {
                _this.surnameInput.nativeElement.click();
            }
        }, 0);
    };
    CardViewNameItemComponent.prototype.reset = function () {
        this.editedTitle = this.property.value.title;
        this.editedFirstName = this.property.value.firstName;
        this.editedMiddleName = this.property.value.middleName;
        this.editedSurname = this.property.value.surname;
        this.setEditMode(false);
    };
    CardViewNameItemComponent.prototype.update = function () {
        console.log('Property', this.property);
        if (this.property.isValid(new PersonName(this.editedTitle, this.editedFirstName, this.editedMiddleName, this.editedSurname))) {
            this.cardViewUpdateService.update(this.property, new PersonName(this.editedTitle, this.editedFirstName, this.editedMiddleName, this.editedSurname));
            this.property.value = new PersonName(this.editedTitle, this.editedFirstName, this.editedMiddleName, this.editedSurname);
            this.setEditMode(false);
        }
        else {
            this.errorMessages = this.property.getValidationErrors(new PersonName(this.editedTitle, this.editedFirstName, this.editedMiddleName, this.editedSurname));
        }
    };
    Object.defineProperty(CardViewNameItemComponent.prototype, "displayValue", {
        get: function () {
            return this.property.displayValue;
        },
        enumerable: true,
        configurable: true
    });
    CardViewNameItemComponent.prototype.clicked = function () {
        this.cardViewUpdateService.clicked(this.property);
    };
    CardViewNameItemComponent.ctorParameters = function () { return [
        { type: CardViewUpdateService }
    ]; };
    tslib_1.__decorate([
        Input()
    ], CardViewNameItemComponent.prototype, "property", void 0);
    tslib_1.__decorate([
        Input()
    ], CardViewNameItemComponent.prototype, "editable", void 0);
    tslib_1.__decorate([
        Input()
    ], CardViewNameItemComponent.prototype, "displayEmpty", void 0);
    tslib_1.__decorate([
        ViewChild('titleInput', { static: true })
    ], CardViewNameItemComponent.prototype, "titleInput", void 0);
    tslib_1.__decorate([
        ViewChild('firstNameInput', { static: true })
    ], CardViewNameItemComponent.prototype, "firstNameInput", void 0);
    tslib_1.__decorate([
        ViewChild('middleNameInput', { static: true })
    ], CardViewNameItemComponent.prototype, "middleNameInput", void 0);
    tslib_1.__decorate([
        ViewChild('titleInput', { static: true })
    ], CardViewNameItemComponent.prototype, "surnameInput", void 0);
    CardViewNameItemComponent = tslib_1.__decorate([
        Component({
            selector: 'card-view-name-item',
            template: "<div [attr.data-automation-id]=\"'card-name-item-label-' + property.key\" class=\"adf-property-label\"\r\n     *ngIf=\"showProperty() || isEditable()\">{{ property.label | translate }}\r\n</div>\r\n<div class=\"adf-property-value\">\r\n    <span *ngIf=\"!isEditable()\">\r\n        <span *ngIf=\"!isClickable(); else elseBlock\"\r\n              [attr.data-automation-id]=\"'card-name-titem-value-' + property.key\">\r\n            <span *ngIf=\"showProperty()\">{{ displayValue }}</span>\r\n        </span>\r\n        <ng-template #elseBlock>\r\n        <div class=\"adf-textitem-clickable\" (click)=\"clicked()\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n            <span class=\"adf-textitem-clickable-value\"\r\n                  [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ displayValue }}</span>\r\n            </span>\r\n            <mat-icon *ngIf=\"hasIcon()\" fxFlex=\"0 0 auto\"\r\n                      [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.icon\" class=\"adf-textitem-icon\">{{ property.icon }}</mat-icon>\r\n        </div>\r\n        </ng-template>\r\n    </span>\r\n    <span *ngIf=\"isEditable()\">\r\n        <div *ngIf=\"!inEdit\" (click)=\"setEditMode(true)\" class=\"adf-textitem-readonly\"\r\n             [attr.data-automation-id]=\"'card-textitem-edit-toggle-' + property.key\" fxLayout=\"row\"\r\n             fxLayoutAlign=\"space-between center\">\r\n            <span [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ displayValue }}</span>\r\n            </span>\r\n            <mat-icon fxFlex=\"0 0 auto\"\r\n                      [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.key\"\r\n                      [attr.title]=\"'CORE.METADATA.ACTIONS.EDIT' | translate\"\r\n                      class=\"adf-textitem-icon\">create</mat-icon>\r\n        </div>\r\n        <div *ngIf=\"inEdit\" class=\"adf-textitem-editable\">\r\n            <div class=\"\" fxLayout=\"column\" fxLayoutAlign=\"space-between start\">\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\">\r\n                        <input #titleInput\r\n                               matInput\r\n                               [placeholder]=\"'Title'\"\r\n                               [(ngModel)]=\"editedTitle\"\r\n                               [attr.data-automation-id]=\"'card-textitem-titleinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #firstNameInput\r\n                               matInput\r\n                               class=\"adf-input\"\r\n                               [placeholder]=\"'First name'\"\r\n                               [(ngModel)]=\"editedFirstName\"\r\n                               [attr.data-automation-id]=\"'card-textitem-firstnameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #middleNameInput\r\n                               matInput\r\n                               [placeholder]=\"'Middle name'\"\r\n                               [(ngModel)]=\"editedMiddleName\"\r\n                               [attr.data-automation-id]=\"'card-textitem-middlenameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #surnameInput\r\n                               matInput\r\n                               [placeholder]=\"'Surname'\"\r\n                               [(ngModel)]=\"editedSurname\"\r\n                               [attr.data-automation-id]=\"'card-textitem-surnameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-icon\r\n                            class=\"adf-textitem-icon adf-update-icon\"\r\n                            (click)=\"update()\"\r\n                            [attr.title]=\"'CORE.METADATA.ACTIONS.SAVE' | translate\"\r\n                            [attr.data-automation-id]=\"'card-textitem-update-' + property.key\">done</mat-icon>\r\n                    <mat-icon\r\n                            class=\"adf-textitem-icon adf-reset-icon\"\r\n                            (click)=\"reset()\"\r\n                            [attr.title]=\"'CORE.METADATA.ACTIONS.CANCEL' | translate\"\r\n                            [attr.data-automation-id]=\"'card-textitem-reset-' + property.key\">clear</mat-icon>\r\n                </div>\r\n            </div>\r\n            <mat-error [attr.data-automation-id]=\"'card-textitem-error-' + property.key\"\r\n                       class=\"adf-textitem-editable-error\"\r\n                       *ngIf=\"hasErrors()\">\r\n                <ul>\r\n                    <li *ngFor=\"let errorMessage of errorMessages\">{{ errorMessage | translate }}</li>\r\n                </ul>\r\n            </mat-error>\r\n        </div>\r\n    </span>\r\n    <ng-template #elseEmptyValueBlock>\r\n        <span class=\"adf-textitem-default-value\">{{ property.default | translate }}</span>\r\n    </ng-template>\r\n</div>"
        })
    ], CardViewNameItemComponent);
    return CardViewNameItemComponent;
}());
export { CardViewNameItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LW5hbWUtaXRlbS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvdXRpbC9jYXJkLXZpZXcvY29tcG9uZW50cy9jYXJkLXZpZXctbmFtZS1pdGVtL2NhcmQtdmlldy1uYW1lLWl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBYSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFFM0QsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBTTdEO0lBNkJJLG1DQUFvQixxQkFBNEM7UUFBNUMsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUF1QjtRQXhCaEUsYUFBUSxHQUFZLEtBQUssQ0FBQztRQUcxQixpQkFBWSxHQUFZLElBQUksQ0FBQztRQWM3QixXQUFNLEdBQVksS0FBSyxDQUFDO0lBUXhCLENBQUM7SUFFRCwrQ0FBVyxHQUFYO1FBQ0ksSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFDN0MsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7UUFDckQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQztRQUN2RCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztJQUNyRCxDQUFDO0lBRUQsZ0RBQVksR0FBWjtRQUNJLE9BQU8sSUFBSSxDQUFDLFlBQVksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDekQsQ0FBQztJQUVELDhDQUFVLEdBQVY7UUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7SUFDbkQsQ0FBQztJQUVELCtDQUFXLEdBQVg7UUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDO0lBQ25DLENBQUM7SUFFRCwyQ0FBTyxHQUFQO1FBQ0ksT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7SUFDaEMsQ0FBQztJQUVELDZDQUFTLEdBQVQ7UUFDSSxPQUFPLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7SUFDM0QsQ0FBQztJQUVELCtDQUFXLEdBQVgsVUFBWSxVQUFtQjtRQUEvQixpQkFzQkM7UUFyQkcsSUFBSSxDQUFDLE1BQU0sR0FBRyxVQUFVLENBQUM7UUFDekIsVUFBVSxDQUFDO1lBQ1AsSUFBSSxLQUFJLENBQUMsVUFBVSxFQUFFO2dCQUNqQixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUN6QztRQUNMLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUNOLFVBQVUsQ0FBQztZQUNQLElBQUksS0FBSSxDQUFDLGNBQWMsRUFBRTtnQkFDckIsS0FBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDN0M7UUFDTCxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7UUFDTixVQUFVLENBQUM7WUFDUCxJQUFJLEtBQUksQ0FBQyxlQUFlLEVBQUU7Z0JBQ3RCLEtBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO2FBQzlDO1FBQ0wsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ04sVUFBVSxDQUFDO1lBQ1AsSUFBSSxLQUFJLENBQUMsWUFBWSxFQUFFO2dCQUNuQixLQUFJLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUMzQztRQUNMLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNWLENBQUM7SUFFRCx5Q0FBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFDN0MsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUM7UUFDckQsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQztRQUN2RCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztRQUNqRCxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzVCLENBQUM7SUFFRCwwQ0FBTSxHQUFOO1FBQ0ksT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ3ZDLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRTtZQUMxSCxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQzNDLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7WUFDdkcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsSUFBSSxVQUFVLENBQUMsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDeEgsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMzQjthQUFNO1lBQ0gsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLElBQUksVUFBVSxDQUFDLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUM7U0FDN0o7SUFDTCxDQUFDO0lBRUQsc0JBQUksbURBQVk7YUFBaEI7WUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDO1FBQ3RDLENBQUM7OztPQUFBO0lBRUQsMkNBQU8sR0FBUDtRQUNJLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3RELENBQUM7O2dCQWhGMEMscUJBQXFCOztJQTNCaEU7UUFEQyxLQUFLLEVBQUU7K0RBQ3dCO0lBR2hDO1FBREMsS0FBSyxFQUFFOytEQUNrQjtJQUcxQjtRQURDLEtBQUssRUFBRTttRUFDcUI7SUFHN0I7UUFEQyxTQUFTLENBQUMsWUFBWSxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQyxDQUFDO2lFQUNoQjtJQUd4QjtRQURDLFNBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUMsQ0FBQztxRUFDaEI7SUFHNUI7UUFEQyxTQUFTLENBQUMsaUJBQWlCLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDLENBQUM7c0VBQ2hCO0lBRzdCO1FBREMsU0FBUyxDQUFDLFlBQVksRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUMsQ0FBQzttRUFDZDtJQXBCakIseUJBQXlCO1FBSnJDLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxxQkFBcUI7WUFDL0Isb3BMQUFtRDtTQUN0RCxDQUFDO09BQ1cseUJBQXlCLENBOEdyQztJQUFELGdDQUFDO0NBQUEsQUE5R0QsSUE4R0M7U0E5R1kseUJBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25DaGFuZ2VzLCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdVcGRhdGVTZXJ2aWNlIH0gZnJvbSAnQGFsZnJlc2NvL2FkZi1jb3JlJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdOYW1lSXRlbU1vZGVsIH0gZnJvbSAnLi4vLi4vbW9kZWxzL2NhcmQtdmlldy1uYW1lLWl0ZW0ubW9kZWwnO1xyXG5pbXBvcnQgeyBQZXJzb25OYW1lIH0gZnJvbSAnLi4vLi4vLi4vLi4vbW9kZWwvYWRkcmVzcy5tb2RlbCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnY2FyZC12aWV3LW5hbWUtaXRlbScsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vY2FyZC12aWV3LW5hbWUtaXRlbS5jb21wb25lbnQuaHRtbCdcclxufSlcclxuZXhwb3J0IGNsYXNzIENhcmRWaWV3TmFtZUl0ZW1Db21wb25lbnQgaW1wbGVtZW50cyBPbkNoYW5nZXMge1xyXG4gICAgQElucHV0KClcclxuICAgIHByb3BlcnR5OiBDYXJkVmlld05hbWVJdGVtTW9kZWw7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIGVkaXRhYmxlOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIGRpc3BsYXlFbXB0eTogYm9vbGVhbiA9IHRydWU7XHJcblxyXG4gICAgQFZpZXdDaGlsZCgndGl0bGVJbnB1dCcsIHtzdGF0aWM6IHRydWV9KVxyXG4gICAgcHJpdmF0ZSB0aXRsZUlucHV0OiBhbnk7XHJcblxyXG4gICAgQFZpZXdDaGlsZCgnZmlyc3ROYW1lSW5wdXQnLCB7c3RhdGljOiB0cnVlfSlcclxuICAgIHByaXZhdGUgZmlyc3ROYW1lSW5wdXQ6IGFueTtcclxuXHJcbiAgICBAVmlld0NoaWxkKCdtaWRkbGVOYW1lSW5wdXQnLCB7c3RhdGljOiB0cnVlfSlcclxuICAgIHByaXZhdGUgbWlkZGxlTmFtZUlucHV0OiBhbnk7XHJcblxyXG4gICAgQFZpZXdDaGlsZCgndGl0bGVJbnB1dCcsIHtzdGF0aWM6IHRydWV9KVxyXG4gICAgcHJpdmF0ZSBzdXJuYW1lSW5wdXQ6IGFueTtcclxuXHJcbiAgICBpbkVkaXQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGVkaXRlZFRpdGxlOiBzdHJpbmc7XHJcbiAgICBlZGl0ZWRTdXJuYW1lOiBzdHJpbmc7XHJcbiAgICBlZGl0ZWRGaXJzdE5hbWU6IHN0cmluZztcclxuICAgIGVkaXRlZE1pZGRsZU5hbWU6IHN0cmluZztcclxuICAgIGVycm9yTWVzc2FnZXM6IHN0cmluZ1tdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgY2FyZFZpZXdVcGRhdGVTZXJ2aWNlOiBDYXJkVmlld1VwZGF0ZVNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uQ2hhbmdlcygpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmVkaXRlZFRpdGxlID0gdGhpcy5wcm9wZXJ0eS52YWx1ZS50aXRsZTtcclxuICAgICAgICB0aGlzLmVkaXRlZEZpcnN0TmFtZSA9IHRoaXMucHJvcGVydHkudmFsdWUuZmlyc3ROYW1lO1xyXG4gICAgICAgIHRoaXMuZWRpdGVkTWlkZGxlTmFtZSA9IHRoaXMucHJvcGVydHkudmFsdWUubWlkZGxlTmFtZTtcclxuICAgICAgICB0aGlzLmVkaXRlZFN1cm5hbWUgPSB0aGlzLnByb3BlcnR5LnZhbHVlLnN1cm5hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgc2hvd1Byb3BlcnR5KCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmRpc3BsYXlFbXB0eSB8fCAhdGhpcy5wcm9wZXJ0eS5pc0VtcHR5KCk7XHJcbiAgICB9XHJcblxyXG4gICAgaXNFZGl0YWJsZSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lZGl0YWJsZSAmJiB0aGlzLnByb3BlcnR5LmVkaXRhYmxlO1xyXG4gICAgfVxyXG5cclxuICAgIGlzQ2xpY2thYmxlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnByb3BlcnR5LmNsaWNrYWJsZTtcclxuICAgIH1cclxuXHJcbiAgICBoYXNJY29uKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiAhIXRoaXMucHJvcGVydHkuaWNvbjtcclxuICAgIH1cclxuXHJcbiAgICBoYXNFcnJvcnMoKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lcnJvck1lc3NhZ2VzICYmIHRoaXMuZXJyb3JNZXNzYWdlcy5sZW5ndGg7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0RWRpdE1vZGUoZWRpdFN0YXR1czogYm9vbGVhbik6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuaW5FZGl0ID0gZWRpdFN0YXR1cztcclxuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMudGl0bGVJbnB1dCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50aXRsZUlucHV0Lm5hdGl2ZUVsZW1lbnQuY2xpY2soKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sIDApO1xyXG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5maXJzdE5hbWVJbnB1dCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5maXJzdE5hbWVJbnB1dC5uYXRpdmVFbGVtZW50LmNsaWNrKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LCAwKTtcclxuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMubWlkZGxlTmFtZUlucHV0KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1pZGRsZU5hbWVJbnB1dC5uYXRpdmVFbGVtZW50LmNsaWNrKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LCAwKTtcclxuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuc3VybmFtZUlucHV0KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnN1cm5hbWVJbnB1dC5uYXRpdmVFbGVtZW50LmNsaWNrKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LCAwKTtcclxuICAgIH1cclxuXHJcbiAgICByZXNldCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmVkaXRlZFRpdGxlID0gdGhpcy5wcm9wZXJ0eS52YWx1ZS50aXRsZTtcclxuICAgICAgICB0aGlzLmVkaXRlZEZpcnN0TmFtZSA9IHRoaXMucHJvcGVydHkudmFsdWUuZmlyc3ROYW1lO1xyXG4gICAgICAgIHRoaXMuZWRpdGVkTWlkZGxlTmFtZSA9IHRoaXMucHJvcGVydHkudmFsdWUubWlkZGxlTmFtZTtcclxuICAgICAgICB0aGlzLmVkaXRlZFN1cm5hbWUgPSB0aGlzLnByb3BlcnR5LnZhbHVlLnN1cm5hbWU7XHJcbiAgICAgICAgdGhpcy5zZXRFZGl0TW9kZShmYWxzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlKCk6IHZvaWQge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCdQcm9wZXJ0eScsIHRoaXMucHJvcGVydHkpO1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BlcnR5LmlzVmFsaWQobmV3IFBlcnNvbk5hbWUodGhpcy5lZGl0ZWRUaXRsZSwgdGhpcy5lZGl0ZWRGaXJzdE5hbWUsIHRoaXMuZWRpdGVkTWlkZGxlTmFtZSwgdGhpcy5lZGl0ZWRTdXJuYW1lKSkpIHtcclxuICAgICAgICAgICAgdGhpcy5jYXJkVmlld1VwZGF0ZVNlcnZpY2UudXBkYXRlKHRoaXMucHJvcGVydHksXHJcbiAgICAgICAgICAgICAgICBuZXcgUGVyc29uTmFtZSh0aGlzLmVkaXRlZFRpdGxlLCB0aGlzLmVkaXRlZEZpcnN0TmFtZSwgdGhpcy5lZGl0ZWRNaWRkbGVOYW1lLCB0aGlzLmVkaXRlZFN1cm5hbWUpKTtcclxuICAgICAgICAgICAgdGhpcy5wcm9wZXJ0eS52YWx1ZSA9IG5ldyBQZXJzb25OYW1lKHRoaXMuZWRpdGVkVGl0bGUsIHRoaXMuZWRpdGVkRmlyc3ROYW1lLCB0aGlzLmVkaXRlZE1pZGRsZU5hbWUsIHRoaXMuZWRpdGVkU3VybmFtZSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0RWRpdE1vZGUoZmFsc2UpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuZXJyb3JNZXNzYWdlcyA9IHRoaXMucHJvcGVydHkuZ2V0VmFsaWRhdGlvbkVycm9ycyhuZXcgUGVyc29uTmFtZSh0aGlzLmVkaXRlZFRpdGxlLCB0aGlzLmVkaXRlZEZpcnN0TmFtZSwgdGhpcy5lZGl0ZWRNaWRkbGVOYW1lLCB0aGlzLmVkaXRlZFN1cm5hbWUpKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRpc3BsYXlWYWx1ZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm9wZXJ0eS5kaXNwbGF5VmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgY2xpY2tlZCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmNhcmRWaWV3VXBkYXRlU2VydmljZS5jbGlja2VkKHRoaXMucHJvcGVydHkpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==