import * as tslib_1 from "tslib";
import { Component, Input, ViewChild } from '@angular/core';
import { CardViewUpdateService } from '@alfresco/adf-core';
import { Phone } from '../../../../model/address.model';
var CardViewPhoneItemComponent = /** @class */ (function () {
    function CardViewPhoneItemComponent(cardViewUpdateService) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.editable = false;
        this.displayEmpty = true;
        this.inEdit = false;
    }
    CardViewPhoneItemComponent.prototype.ngOnChanges = function () {
        this.editedPhone1 = this.property.value.phone1;
        this.editedPhone2 = this.property.value.phone2;
    };
    CardViewPhoneItemComponent.prototype.ngOnInit = function () {
    };
    CardViewPhoneItemComponent.prototype.showProperty = function () {
        return this.displayEmpty || !this.property.isEmpty();
    };
    CardViewPhoneItemComponent.prototype.isEditable = function () {
        return this.editable && this.property.editable;
    };
    CardViewPhoneItemComponent.prototype.isClickable = function () {
        return this.property.clickable;
    };
    CardViewPhoneItemComponent.prototype.hasIcon = function () {
        return !!this.property.icon;
    };
    CardViewPhoneItemComponent.prototype.hasErrors = function () {
        return this.errorMessages && this.errorMessages.length;
    };
    CardViewPhoneItemComponent.prototype.setEditMode = function (editStatus) {
        var _this = this;
        this.inEdit = editStatus;
        setTimeout(function () {
            if (_this.phone1Input) {
                _this.phone1Input.nativeElement.click();
            }
        }, 0);
        setTimeout(function () {
            if (_this.phone2Input) {
                _this.phone2Input.nativeElement.click();
            }
        }, 0);
    };
    CardViewPhoneItemComponent.prototype.reset = function () {
        this.editedPhone1 = this.property.value.phone1;
        this.editedPhone2 = this.property.value.phone2;
        this.setEditMode(false);
    };
    CardViewPhoneItemComponent.prototype.update = function () {
        if (this.property.isValid(new Phone(this.editedPhone1, this.editedPhone2))) {
            this.cardViewUpdateService.update(this.property, new Phone(this.editedPhone1, this.editedPhone2));
            this.property.value = new Phone(this.editedPhone1, this.editedPhone2);
            this.setEditMode(false);
        }
        else {
            this.errorMessages = this.property.getValidationErrors(new Phone(this.editedPhone1, this.editedPhone2));
        }
    };
    Object.defineProperty(CardViewPhoneItemComponent.prototype, "displayValue", {
        get: function () {
            return this.property.displayValue;
        },
        enumerable: true,
        configurable: true
    });
    CardViewPhoneItemComponent.prototype.clicked = function () {
        this.cardViewUpdateService.clicked(this.property);
    };
    CardViewPhoneItemComponent.ctorParameters = function () { return [
        { type: CardViewUpdateService }
    ]; };
    tslib_1.__decorate([
        Input()
    ], CardViewPhoneItemComponent.prototype, "property", void 0);
    tslib_1.__decorate([
        Input()
    ], CardViewPhoneItemComponent.prototype, "editable", void 0);
    tslib_1.__decorate([
        Input()
    ], CardViewPhoneItemComponent.prototype, "displayEmpty", void 0);
    tslib_1.__decorate([
        ViewChild('phone1Input', { static: true })
    ], CardViewPhoneItemComponent.prototype, "phone1Input", void 0);
    tslib_1.__decorate([
        ViewChild('phone2Input', { static: true })
    ], CardViewPhoneItemComponent.prototype, "phone2Input", void 0);
    CardViewPhoneItemComponent = tslib_1.__decorate([
        Component({
            selector: 'card-view-phone-item',
            template: "<div [attr.data-automation-id]=\"'card-name-item-label-' + property.key\" class=\"adf-property-label\"\r\n     *ngIf=\"showProperty() || isEditable()\">{{ property.label | translate }}\r\n</div>\r\n<div class=\"adf-property-value\">\r\n    <span *ngIf=\"!isEditable()\">\r\n        <span *ngIf=\"!isClickable(); else elseBlock\"\r\n              [attr.data-automation-id]=\"'card-name-titem-value-' + property.key\">\r\n            <span *ngIf=\"showProperty()\">{{ displayValue }}</span>\r\n        </span>\r\n        <ng-template #elseBlock>\r\n        <div class=\"adf-textitem-clickable\" (click)=\"clicked()\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n            <span class=\"adf-textitem-clickable-value\"\r\n                  [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ displayValue }}</span>\r\n            </span>\r\n            <mat-icon *ngIf=\"hasIcon()\" fxFlex=\"0 0 auto\"\r\n                      [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.icon\" class=\"adf-textitem-icon\">{{ property.icon }}</mat-icon>\r\n        </div>\r\n        </ng-template>\r\n    </span>\r\n    <span *ngIf=\"isEditable()\">\r\n        <div *ngIf=\"!inEdit\" (click)=\"setEditMode(true)\" class=\"adf-textitem-readonly\"\r\n             [attr.data-automation-id]=\"'card-textitem-edit-toggle-' + property.key\" fxLayout=\"row\"\r\n             fxLayoutAlign=\"space-between center\">\r\n            <span [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ displayValue }}</span>\r\n            </span>\r\n            <mat-icon fxFlex=\"0 0 auto\"\r\n                      [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.key\"\r\n                      [attr.title]=\"'CORE.METADATA.ACTIONS.EDIT' | translate\"\r\n                      class=\"adf-textitem-icon\">create</mat-icon>\r\n        </div>\r\n        <div *ngIf=\"inEdit\" class=\"adf-textitem-editable\">\r\n            <div class=\"\" fxLayout=\"column\" fxLayoutAlign=\"space-between start\">\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\">\r\n                        <input #phonr1Input\r\n                               matInput\r\n                               [placeholder]=\"'Phone 1'\"\r\n                               [(ngModel)]=\"editedPhone1\"\r\n                               [attr.data-automation-id]=\"'card-textitem-titleinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-form-field floatPlaceholder=\"never\" fxFlex=\"3 3 auto\" class=\"adf-input-container\">\r\n                        <input #phone2Input\r\n                               matInput\r\n                               class=\"adf-input\"\r\n                               [placeholder]=\"'Phone 2'\"\r\n                               [(ngModel)]=\"editedPhone2\"\r\n                               [attr.data-automation-id]=\"'card-textitem-firstnameinput-' + property.key\">\r\n                    </mat-form-field>\r\n                </div>\r\n                <div>\r\n                    <mat-icon\r\n                            class=\"adf-textitem-icon adf-update-icon\"\r\n                            (click)=\"update()\"\r\n                            [attr.title]=\"'CORE.METADATA.ACTIONS.SAVE' | translate\"\r\n                            [attr.data-automation-id]=\"'card-textitem-update-' + property.key\">done</mat-icon>\r\n                    <mat-icon\r\n                            class=\"adf-textitem-icon adf-reset-icon\"\r\n                            (click)=\"reset()\"\r\n                            [attr.title]=\"'CORE.METADATA.ACTIONS.CANCEL' | translate\"\r\n                            [attr.data-automation-id]=\"'card-textitem-reset-' + property.key\">clear</mat-icon>\r\n                </div>\r\n            </div>\r\n            <mat-error [attr.data-automation-id]=\"'card-textitem-error-' + property.key\"\r\n                       class=\"adf-textitem-editable-error\"\r\n                       *ngIf=\"hasErrors()\">\r\n                <ul>\r\n                    <li *ngFor=\"let errorMessage of errorMessages\">{{ errorMessage | translate }}</li>\r\n                </ul>\r\n            </mat-error>\r\n        </div>\r\n    </span>\r\n    <ng-template #elseEmptyValueBlock>\r\n        <span class=\"adf-textitem-default-value\">{{ property.default | translate }}</span>\r\n    </ng-template>\r\n</div>"
        })
    ], CardViewPhoneItemComponent);
    return CardViewPhoneItemComponent;
}());
export { CardViewPhoneItemComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LXBob25lLWl0ZW0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL3V0aWwvY2FyZC12aWV3L2NvbXBvbmVudHMvY2FyZC12aWV3LXBob25lLWl0ZW0vY2FyZC12aWV3LXBob25lLWl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBcUIsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRTNELE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQU14RDtJQXNCSSxvQ0FBb0IscUJBQTRDO1FBQTVDLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBdUI7UUFoQmhFLGFBQVEsR0FBWSxLQUFLLENBQUM7UUFHMUIsaUJBQVksR0FBWSxJQUFJLENBQUM7UUFRN0IsV0FBTSxHQUFZLEtBQUssQ0FBQztJQU14QixDQUFDO0lBRUQsZ0RBQVcsR0FBWDtRQUNJLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO1FBQy9DLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDO0lBQ25ELENBQUM7SUFFRCw2Q0FBUSxHQUFSO0lBQ0EsQ0FBQztJQUVELGlEQUFZLEdBQVo7UUFDSSxPQUFPLElBQUksQ0FBQyxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3pELENBQUM7SUFFRCwrQ0FBVSxHQUFWO1FBQ0ksT0FBTyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO0lBQ25ELENBQUM7SUFFRCxnREFBVyxHQUFYO1FBQ0ksT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQztJQUNuQyxDQUFDO0lBRUQsNENBQU8sR0FBUDtRQUNJLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDO0lBQ2hDLENBQUM7SUFFRCw4Q0FBUyxHQUFUO1FBQ0ksT0FBTyxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDO0lBQzNELENBQUM7SUFFRCxnREFBVyxHQUFYLFVBQVksVUFBbUI7UUFBL0IsaUJBWUM7UUFYRyxJQUFJLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQztRQUN6QixVQUFVLENBQUM7WUFDUCxJQUFJLEtBQUksQ0FBQyxXQUFXLEVBQUU7Z0JBQ2xCLEtBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxDQUFDO2FBQzFDO1FBQ0wsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ04sVUFBVSxDQUFDO1lBQ1AsSUFBSSxLQUFJLENBQUMsV0FBVyxFQUFFO2dCQUNsQixLQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsQ0FBQzthQUMxQztRQUNMLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNWLENBQUM7SUFFRCwwQ0FBSyxHQUFMO1FBQ0ksSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7UUFDL0MsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUM7UUFDL0MsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRUQsMkNBQU0sR0FBTjtRQUNJLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsRUFBRTtZQUN4RSxJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQzNDLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDckQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsSUFBSSxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDdEUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMzQjthQUFNO1lBQ0gsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLG1CQUFtQixDQUFDLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7U0FDM0c7SUFDTCxDQUFDO0lBRUQsc0JBQUksb0RBQVk7YUFBaEI7WUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDO1FBQ3RDLENBQUM7OztPQUFBO0lBRUQsNENBQU8sR0FBUDtRQUNJLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3RELENBQUM7O2dCQXBFMEMscUJBQXFCOztJQW5CaEU7UUFEQyxLQUFLLEVBQUU7Z0VBQ3lCO0lBR2pDO1FBREMsS0FBSyxFQUFFO2dFQUNrQjtJQUcxQjtRQURDLEtBQUssRUFBRTtvRUFDcUI7SUFHN0I7UUFEQyxTQUFTLENBQUMsYUFBYSxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQyxDQUFDO21FQUNoQjtJQUd6QjtRQURDLFNBQVMsQ0FBQyxhQUFhLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDLENBQUM7bUVBQ2hCO0lBZmhCLDBCQUEwQjtRQUp0QyxTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsc0JBQXNCO1lBQ2hDLGtsSkFBb0Q7U0FDdkQsQ0FBQztPQUNXLDBCQUEwQixDQTJGdEM7SUFBRCxpQ0FBQztDQUFBLEFBM0ZELElBMkZDO1NBM0ZZLDBCQUEwQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uQ2hhbmdlcywgT25Jbml0LCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdVcGRhdGVTZXJ2aWNlIH0gZnJvbSAnQGFsZnJlc2NvL2FkZi1jb3JlJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdQaG9uZUl0ZW1Nb2RlbCB9IGZyb20gJy4uLy4uL21vZGVscy9jYXJkLXZpZXctcGhvbmUtaXRlbS5tb2RlbCc7XHJcbmltcG9ydCB7IFBob25lIH0gZnJvbSAnLi4vLi4vLi4vLi4vbW9kZWwvYWRkcmVzcy5tb2RlbCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnY2FyZC12aWV3LXBob25lLWl0ZW0nLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2NhcmQtdmlldy1waG9uZS1pdGVtLmNvbXBvbmVudC5odG1sJyxcclxufSlcclxuZXhwb3J0IGNsYXNzIENhcmRWaWV3UGhvbmVJdGVtQ29tcG9uZW50IGltcGxlbWVudHMgT25DaGFuZ2VzLCBPbkluaXQge1xyXG4gICAgcGhvbmU6IFBob25lO1xyXG4gICAgQElucHV0KClcclxuICAgIHByb3BlcnR5OiBDYXJkVmlld1Bob25lSXRlbU1vZGVsO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBlZGl0YWJsZTogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBkaXNwbGF5RW1wdHk6IGJvb2xlYW4gPSB0cnVlO1xyXG5cclxuICAgIEBWaWV3Q2hpbGQoJ3Bob25lMUlucHV0Jywge3N0YXRpYzogdHJ1ZX0pXHJcbiAgICBwcml2YXRlIHBob25lMUlucHV0OiBhbnk7XHJcblxyXG4gICAgQFZpZXdDaGlsZCgncGhvbmUySW5wdXQnLCB7c3RhdGljOiB0cnVlfSlcclxuICAgIHByaXZhdGUgcGhvbmUySW5wdXQ6IGFueTtcclxuXHJcbiAgICBpbkVkaXQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGVkaXRlZFBob25lMTogc3RyaW5nO1xyXG4gICAgZWRpdGVkUGhvbmUyOiBzdHJpbmc7XHJcbiAgICBlcnJvck1lc3NhZ2VzOiBzdHJpbmdbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNhcmRWaWV3VXBkYXRlU2VydmljZTogQ2FyZFZpZXdVcGRhdGVTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkNoYW5nZXMoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5lZGl0ZWRQaG9uZTEgPSB0aGlzLnByb3BlcnR5LnZhbHVlLnBob25lMTtcclxuICAgICAgICB0aGlzLmVkaXRlZFBob25lMiA9IHRoaXMucHJvcGVydHkudmFsdWUucGhvbmUyO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgfVxyXG5cclxuICAgIHNob3dQcm9wZXJ0eSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5kaXNwbGF5RW1wdHkgfHwgIXRoaXMucHJvcGVydHkuaXNFbXB0eSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGlzRWRpdGFibGUoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZWRpdGFibGUgJiYgdGhpcy5wcm9wZXJ0eS5lZGl0YWJsZTtcclxuICAgIH1cclxuXHJcbiAgICBpc0NsaWNrYWJsZSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm9wZXJ0eS5jbGlja2FibGU7XHJcbiAgICB9XHJcblxyXG4gICAgaGFzSWNvbigpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gISF0aGlzLnByb3BlcnR5Lmljb247XHJcbiAgICB9XHJcblxyXG4gICAgaGFzRXJyb3JzKCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZXJyb3JNZXNzYWdlcyAmJiB0aGlzLmVycm9yTWVzc2FnZXMubGVuZ3RoO1xyXG4gICAgfVxyXG5cclxuICAgIHNldEVkaXRNb2RlKGVkaXRTdGF0dXM6IGJvb2xlYW4pOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmluRWRpdCA9IGVkaXRTdGF0dXM7XHJcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnBob25lMUlucHV0KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnBob25lMUlucHV0Lm5hdGl2ZUVsZW1lbnQuY2xpY2soKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sIDApO1xyXG4gICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5waG9uZTJJbnB1dCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5waG9uZTJJbnB1dC5uYXRpdmVFbGVtZW50LmNsaWNrKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LCAwKTtcclxuICAgIH1cclxuXHJcbiAgICByZXNldCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmVkaXRlZFBob25lMSA9IHRoaXMucHJvcGVydHkudmFsdWUucGhvbmUxO1xyXG4gICAgICAgIHRoaXMuZWRpdGVkUGhvbmUyID0gdGhpcy5wcm9wZXJ0eS52YWx1ZS5waG9uZTI7XHJcbiAgICAgICAgdGhpcy5zZXRFZGl0TW9kZShmYWxzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlKCk6IHZvaWQge1xyXG4gICAgICAgIGlmICh0aGlzLnByb3BlcnR5LmlzVmFsaWQobmV3IFBob25lKHRoaXMuZWRpdGVkUGhvbmUxLCB0aGlzLmVkaXRlZFBob25lMikpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2FyZFZpZXdVcGRhdGVTZXJ2aWNlLnVwZGF0ZSh0aGlzLnByb3BlcnR5LFxyXG4gICAgICAgICAgICAgICAgbmV3IFBob25lKHRoaXMuZWRpdGVkUGhvbmUxLCB0aGlzLmVkaXRlZFBob25lMikpO1xyXG4gICAgICAgICAgICB0aGlzLnByb3BlcnR5LnZhbHVlID0gbmV3IFBob25lKHRoaXMuZWRpdGVkUGhvbmUxLCB0aGlzLmVkaXRlZFBob25lMik7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0RWRpdE1vZGUoZmFsc2UpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuZXJyb3JNZXNzYWdlcyA9IHRoaXMucHJvcGVydHkuZ2V0VmFsaWRhdGlvbkVycm9ycyhuZXcgUGhvbmUodGhpcy5lZGl0ZWRQaG9uZTEsIHRoaXMuZWRpdGVkUGhvbmUyKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldCBkaXNwbGF5VmFsdWUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMucHJvcGVydHkuZGlzcGxheVZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIGNsaWNrZWQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5jYXJkVmlld1VwZGF0ZVNlcnZpY2UuY2xpY2tlZCh0aGlzLnByb3BlcnR5KTtcclxuICAgIH1cclxufVxyXG4iXX0=