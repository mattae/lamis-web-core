import * as tslib_1 from "tslib";
import { CardViewBaseItemModel } from '@alfresco/adf-core';
var CardViewHtmlTextItemModel = /** @class */ (function (_super) {
    tslib_1.__extends(CardViewHtmlTextItemModel, _super);
    function CardViewHtmlTextItemModel(obj) {
        var _this = _super.call(this, obj) || this;
        _this.type = 'html-text';
        return _this;
    }
    Object.defineProperty(CardViewHtmlTextItemModel.prototype, "displayValue", {
        get: function () {
            if (this.isEmpty()) {
                return this.default;
            }
            else {
                return this.value;
            }
        },
        enumerable: true,
        configurable: true
    });
    return CardViewHtmlTextItemModel;
}(CardViewBaseItemModel));
export { CardViewHtmlTextItemModel };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWh0bWwtdGV4dC1pdGVtLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL3V0aWwvY2FyZC12aWV3L21vZGVscy9jYXJkLXZpZXctaHRtbC10ZXh0LWl0ZW0ubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFDSCxxQkFBcUIsRUFJeEIsTUFBTSxvQkFBb0IsQ0FBQztBQUU1QjtJQUErQyxxREFBcUI7SUFHaEUsbUNBQVksR0FBK0I7UUFBM0MsWUFDSSxrQkFBTSxHQUFHLENBQUMsU0FDYjtRQUpELFVBQUksR0FBVyxXQUFXLENBQUM7O0lBSTNCLENBQUM7SUFFRCxzQkFBSSxtREFBWTthQUFoQjtZQUNJLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRSxFQUFFO2dCQUNoQixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7YUFDdkI7aUJBQU07Z0JBQ0gsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO2FBQ3JCO1FBQ0wsQ0FBQzs7O09BQUE7SUFDTCxnQ0FBQztBQUFELENBQUMsQUFkRCxDQUErQyxxQkFBcUIsR0FjbkUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xyXG4gICAgQ2FyZFZpZXdCYXNlSXRlbU1vZGVsLFxyXG4gICAgQ2FyZFZpZXdJdGVtLFxyXG4gICAgQ2FyZFZpZXdUZXh0SXRlbVByb3BlcnRpZXMsXHJcbiAgICBEeW5hbWljQ29tcG9uZW50TW9kZWxcclxufSBmcm9tICdAYWxmcmVzY28vYWRmLWNvcmUnO1xyXG5cclxuZXhwb3J0IGNsYXNzIENhcmRWaWV3SHRtbFRleHRJdGVtTW9kZWwgZXh0ZW5kcyBDYXJkVmlld0Jhc2VJdGVtTW9kZWwgaW1wbGVtZW50cyBDYXJkVmlld0l0ZW0sIER5bmFtaWNDb21wb25lbnRNb2RlbCB7XHJcbiAgICB0eXBlOiBzdHJpbmcgPSAnaHRtbC10ZXh0JztcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihvYmo6IENhcmRWaWV3VGV4dEl0ZW1Qcm9wZXJ0aWVzKSB7XHJcbiAgICAgICAgc3VwZXIob2JqKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgZGlzcGxheVZhbHVlKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmlzRW1wdHkoKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5kZWZhdWx0O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnZhbHVlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=