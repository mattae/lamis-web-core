/*!
 * @license
 * Copyright 2016 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import * as tslib_1 from "tslib";
import { CardViewBaseItemModel } from '@alfresco/adf-core';
var CardViewFixedKeyValuePairsItemModel = /** @class */ (function (_super) {
    tslib_1.__extends(CardViewFixedKeyValuePairsItemModel, _super);
    function CardViewFixedKeyValuePairsItemModel(obj) {
        var _this = _super.call(this, obj) || this;
        _this.type = 'fixedkeyvaluepairs';
        return _this;
    }
    Object.defineProperty(CardViewFixedKeyValuePairsItemModel.prototype, "displayValue", {
        get: function () {
            return this.value;
        },
        enumerable: true,
        configurable: true
    });
    return CardViewFixedKeyValuePairsItemModel;
}(CardViewBaseItemModel));
export { CardViewFixedKeyValuePairsItemModel };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWZpeGVkLWtleXZhbHVlcGFpcnMubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvdXRpbC9jYXJkLXZpZXcvbW9kZWxzL2NhcmQtdmlldy1maXhlZC1rZXl2YWx1ZXBhaXJzLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOzs7Ozs7Ozs7Ozs7Ozs7R0FlRzs7QUFHSCxPQUFPLEVBQ0gscUJBQXFCLEVBSXhCLE1BQU0sb0JBQW9CLENBQUM7QUFFNUI7SUFBeUQsK0RBQXFCO0lBRzFFLDZDQUFZLEdBQXdDO1FBQXBELFlBQ0ksa0JBQU0sR0FBRyxDQUFDLFNBQ2I7UUFKRCxVQUFJLEdBQVcsb0JBQW9CLENBQUM7O0lBSXBDLENBQUM7SUFFRCxzQkFBSSw2REFBWTthQUFoQjtZQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztRQUN0QixDQUFDOzs7T0FBQTtJQUNMLDBDQUFDO0FBQUQsQ0FBQyxBQVZELENBQXlELHFCQUFxQixHQVU3RSIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxNiBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5cclxuaW1wb3J0IHtcclxuICAgIENhcmRWaWV3QmFzZUl0ZW1Nb2RlbCxcclxuICAgIENhcmRWaWV3SXRlbSxcclxuICAgIENhcmRWaWV3S2V5VmFsdWVQYWlyc0l0ZW1Qcm9wZXJ0aWVzLFxyXG4gICAgRHluYW1pY0NvbXBvbmVudE1vZGVsXHJcbn0gZnJvbSAnQGFsZnJlc2NvL2FkZi1jb3JlJztcclxuXHJcbmV4cG9ydCBjbGFzcyBDYXJkVmlld0ZpeGVkS2V5VmFsdWVQYWlyc0l0ZW1Nb2RlbCBleHRlbmRzIENhcmRWaWV3QmFzZUl0ZW1Nb2RlbCBpbXBsZW1lbnRzIENhcmRWaWV3SXRlbSwgRHluYW1pY0NvbXBvbmVudE1vZGVsIHtcclxuICAgIHR5cGU6IHN0cmluZyA9ICdmaXhlZGtleXZhbHVlcGFpcnMnO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKG9iajogQ2FyZFZpZXdLZXlWYWx1ZVBhaXJzSXRlbVByb3BlcnRpZXMpIHtcclxuICAgICAgICBzdXBlcihvYmopO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBkaXNwbGF5VmFsdWUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudmFsdWU7XHJcbiAgICB9XHJcbn1cclxuIl19