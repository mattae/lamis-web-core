import * as tslib_1 from "tslib";
import { CardViewBaseItemModel } from '@alfresco/adf-core';
var CardViewNameItemModel = /** @class */ (function (_super) {
    tslib_1.__extends(CardViewNameItemModel, _super);
    function CardViewNameItemModel(obj) {
        var _this = _super.call(this, obj) || this;
        _this.type = 'name';
        return _this;
    }
    Object.defineProperty(CardViewNameItemModel.prototype, "displayValue", {
        get: function () {
            if (this.isEmpty()) {
                return this.default;
            }
            else {
                return this.getValue();
            }
        },
        enumerable: true,
        configurable: true
    });
    CardViewNameItemModel.prototype.getValue = function () {
        return ((!!this.value.title ? this.value.title + ' ' : '') + "\n        " + (!!this.value.firstName ? this.value.firstName + ' ' : '') + "\n        " + (!!this.value.middleName ? this.value.middleName + ' ' : '') + "\n        " + (!!this.value.surname ? this.value.surname + ' ' : '')).trim();
    };
    return CardViewNameItemModel;
}(CardViewBaseItemModel));
export { CardViewNameItemModel };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LW5hbWUtaXRlbS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC91dGlsL2NhcmQtdmlldy9tb2RlbHMvY2FyZC12aWV3LW5hbWUtaXRlbS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLHFCQUFxQixFQUF1QyxNQUFNLG9CQUFvQixDQUFDO0FBR2hHO0lBQTJDLGlEQUFxQjtJQUc1RCwrQkFBWSxHQUErQjtRQUEzQyxZQUNJLGtCQUFNLEdBQUcsQ0FBQyxTQUNiO1FBSkQsVUFBSSxHQUFXLE1BQU0sQ0FBQzs7SUFJdEIsQ0FBQztJQUVELHNCQUFJLCtDQUFZO2FBQWhCO1lBQ0ksSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFLEVBQUU7Z0JBQ2hCLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQzthQUN2QjtpQkFBTTtnQkFDSCxPQUFPLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQzthQUMxQjtRQUNMLENBQUM7OztPQUFBO0lBRUQsd0NBQVEsR0FBUjtRQUNJLE9BQU8sQ0FBQSxDQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFBLENBQUMsQ0FBQyxFQUFFLG9CQUN2RCxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQSxDQUFDLENBQUMsRUFBRSxvQkFDdkQsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUEsQ0FBQyxDQUFDLEVBQUUsb0JBQ3pELENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFBLENBQUMsQ0FBQyxFQUFFLENBQUUsQ0FBQSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ25FLENBQUM7SUFDTCw0QkFBQztBQUFELENBQUMsQUFyQkQsQ0FBMkMscUJBQXFCLEdBcUIvRCIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENhcmRWaWV3QmFzZUl0ZW1Nb2RlbCwgQ2FyZFZpZXdJdGVtLCBEeW5hbWljQ29tcG9uZW50TW9kZWwgfSBmcm9tICdAYWxmcmVzY28vYWRmLWNvcmUnO1xyXG5pbXBvcnQgeyBDYXJkVmlld05hbWVJdGVtUHJvcGVydGllcyB9IGZyb20gJy4vY2FyZC12aWV3LW5hbWUtaXRlbS5wcm9wZXJ0aWVzJztcclxuXHJcbmV4cG9ydCBjbGFzcyBDYXJkVmlld05hbWVJdGVtTW9kZWwgZXh0ZW5kcyBDYXJkVmlld0Jhc2VJdGVtTW9kZWwgaW1wbGVtZW50cyBDYXJkVmlld0l0ZW0sIER5bmFtaWNDb21wb25lbnRNb2RlbCB7XHJcbiAgICB0eXBlOiBzdHJpbmcgPSAnbmFtZSc7XHJcblxyXG4gICAgY29uc3RydWN0b3Iob2JqOiBDYXJkVmlld05hbWVJdGVtUHJvcGVydGllcykge1xyXG4gICAgICAgIHN1cGVyKG9iaik7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRpc3BsYXlWYWx1ZSgpIHtcclxuICAgICAgICBpZiAodGhpcy5pc0VtcHR5KCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZGVmYXVsdDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5nZXRWYWx1ZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRWYWx1ZSgpIHtcclxuICAgICAgICByZXR1cm4gYCR7ISF0aGlzLnZhbHVlLnRpdGxlID8gdGhpcy52YWx1ZS50aXRsZSArICcgJzogJyd9XHJcbiAgICAgICAgJHshIXRoaXMudmFsdWUuZmlyc3ROYW1lID8gdGhpcy52YWx1ZS5maXJzdE5hbWUgKyAnICc6ICcnfVxyXG4gICAgICAgICR7ISF0aGlzLnZhbHVlLm1pZGRsZU5hbWUgPyB0aGlzLnZhbHVlLm1pZGRsZU5hbWUgKyAnICc6ICcnfVxyXG4gICAgICAgICR7ISF0aGlzLnZhbHVlLnN1cm5hbWUgPyB0aGlzLnZhbHVlLnN1cm5hbWUgKyAnICc6ICcnfWAudHJpbSgpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==