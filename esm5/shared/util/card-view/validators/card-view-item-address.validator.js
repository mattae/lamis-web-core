var CardViewItemAddressValidator = /** @class */ (function () {
    function CardViewItemAddressValidator() {
        this.message = '';
    }
    CardViewItemAddressValidator.prototype.isValid = function (value) {
        var streetError = false;
        var cityError = false;
        var lgaError = false;
        if (!value.street1 || value.street1.length < 3) {
            streetError = true;
        }
        if (!value.city || value.city.length < 3) {
            cityError = true;
        }
        if (!!value.lga) {
            lgaError = true;
        }
        if (streetError && cityError && lgaError) {
            this.message = 'Street, city and LGA are all required';
        }
        else if (streetError && cityError) {
            this.message = 'Street and city are required';
        }
        else if (streetError && lgaError) {
            this.message = 'Street and LGA are required';
        }
        else if (lgaError && cityError) {
            this.message = 'City and LGA are required';
        }
        else if (streetError) {
            this.message = 'Street is required';
        }
        else if (cityError) {
            this.message = 'City is required';
        }
        else if (lgaError) {
            this.message = 'LGA is required';
        }
        return !(streetError || cityError || lgaError);
    };
    return CardViewItemAddressValidator;
}());
export { CardViewItemAddressValidator };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWl0ZW0tYWRkcmVzcy52YWxpZGF0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvdXRpbC9jYXJkLXZpZXcvdmFsaWRhdG9ycy9jYXJkLXZpZXctaXRlbS1hZGRyZXNzLnZhbGlkYXRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtJQUFBO1FBQ0ksWUFBTyxHQUFHLEVBQUUsQ0FBQztJQWlDakIsQ0FBQztJQS9CRyw4Q0FBTyxHQUFQLFVBQVEsS0FBVTtRQUNkLElBQUksV0FBVyxHQUFHLEtBQUssQ0FBQztRQUN4QixJQUFJLFNBQVMsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxJQUFJLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUM1QyxXQUFXLEdBQUcsSUFBSSxDQUFDO1NBQ3RCO1FBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3RDLFNBQVMsR0FBRyxJQUFJLENBQUM7U0FDcEI7UUFDRCxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFO1lBQ2IsUUFBUSxHQUFHLElBQUksQ0FBQztTQUNuQjtRQUNELElBQUksV0FBVyxJQUFJLFNBQVMsSUFBSSxRQUFRLEVBQUU7WUFDdEMsSUFBSSxDQUFDLE9BQU8sR0FBRyx1Q0FBdUMsQ0FBQztTQUMxRDthQUFNLElBQUksV0FBVyxJQUFJLFNBQVMsRUFBRTtZQUNqQyxJQUFJLENBQUMsT0FBTyxHQUFHLDhCQUE4QixDQUFDO1NBQ2pEO2FBQU0sSUFBSSxXQUFXLElBQUksUUFBUSxFQUFFO1lBQ2hDLElBQUksQ0FBQyxPQUFPLEdBQUcsNkJBQTZCLENBQUM7U0FDaEQ7YUFBTSxJQUFJLFFBQVEsSUFBSSxTQUFTLEVBQUU7WUFDOUIsSUFBSSxDQUFDLE9BQU8sR0FBRywyQkFBMkIsQ0FBQztTQUM5QzthQUFNLElBQUksV0FBVyxFQUFFO1lBQ3BCLElBQUksQ0FBQyxPQUFPLEdBQUcsb0JBQW9CLENBQUM7U0FDdkM7YUFBTSxJQUFJLFNBQVMsRUFBRTtZQUNsQixJQUFJLENBQUMsT0FBTyxHQUFHLGtCQUFrQixDQUFBO1NBQ3BDO2FBQU0sSUFBSSxRQUFRLEVBQUU7WUFDakIsSUFBSSxDQUFDLE9BQU8sR0FBRyxpQkFBaUIsQ0FBQTtTQUNuQztRQUNELE9BQU8sQ0FBQyxDQUFDLFdBQVcsSUFBSSxTQUFTLElBQUksUUFBUSxDQUFDLENBQUM7SUFDbkQsQ0FBQztJQUVMLG1DQUFDO0FBQUQsQ0FBQyxBQWxDRCxJQWtDQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENhcmRWaWV3SXRlbVZhbGlkYXRvciB9IGZyb20gJ0BhbGZyZXNjby9hZGYtY29yZSc7XHJcblxyXG5leHBvcnQgY2xhc3MgQ2FyZFZpZXdJdGVtQWRkcmVzc1ZhbGlkYXRvciBpbXBsZW1lbnRzIENhcmRWaWV3SXRlbVZhbGlkYXRvciB7XHJcbiAgICBtZXNzYWdlID0gJyc7XHJcblxyXG4gICAgaXNWYWxpZCh2YWx1ZTogYW55KTogYm9vbGVhbiB7XHJcbiAgICAgICAgbGV0IHN0cmVldEVycm9yID0gZmFsc2U7XHJcbiAgICAgICAgbGV0IGNpdHlFcnJvciA9IGZhbHNlO1xyXG4gICAgICAgIGxldCBsZ2FFcnJvciA9IGZhbHNlO1xyXG4gICAgICAgIGlmICghdmFsdWUuc3RyZWV0MSB8fCB2YWx1ZS5zdHJlZXQxLmxlbmd0aCA8IDMpIHtcclxuICAgICAgICAgICAgc3RyZWV0RXJyb3IgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIXZhbHVlLmNpdHkgfHwgdmFsdWUuY2l0eS5sZW5ndGggPCAzKSB7XHJcbiAgICAgICAgICAgIGNpdHlFcnJvciA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghIXZhbHVlLmxnYSkge1xyXG4gICAgICAgICAgICBsZ2FFcnJvciA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChzdHJlZXRFcnJvciAmJiBjaXR5RXJyb3IgJiYgbGdhRXJyb3IpIHtcclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlID0gJ1N0cmVldCwgY2l0eSBhbmQgTEdBIGFyZSBhbGwgcmVxdWlyZWQnO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoc3RyZWV0RXJyb3IgJiYgY2l0eUVycm9yKSB7XHJcbiAgICAgICAgICAgIHRoaXMubWVzc2FnZSA9ICdTdHJlZXQgYW5kIGNpdHkgYXJlIHJlcXVpcmVkJztcclxuICAgICAgICB9IGVsc2UgaWYgKHN0cmVldEVycm9yICYmIGxnYUVycm9yKSB7XHJcbiAgICAgICAgICAgIHRoaXMubWVzc2FnZSA9ICdTdHJlZXQgYW5kIExHQSBhcmUgcmVxdWlyZWQnO1xyXG4gICAgICAgIH0gZWxzZSBpZiAobGdhRXJyb3IgJiYgY2l0eUVycm9yKSB7XHJcbiAgICAgICAgICAgIHRoaXMubWVzc2FnZSA9ICdDaXR5IGFuZCBMR0EgYXJlIHJlcXVpcmVkJztcclxuICAgICAgICB9IGVsc2UgaWYgKHN0cmVldEVycm9yKSB7XHJcbiAgICAgICAgICAgIHRoaXMubWVzc2FnZSA9ICdTdHJlZXQgaXMgcmVxdWlyZWQnO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoY2l0eUVycm9yKSB7XHJcbiAgICAgICAgICAgIHRoaXMubWVzc2FnZSA9ICdDaXR5IGlzIHJlcXVpcmVkJ1xyXG4gICAgICAgIH0gZWxzZSBpZiAobGdhRXJyb3IpIHtcclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlID0gJ0xHQSBpcyByZXF1aXJlZCdcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuICEoc3RyZWV0RXJyb3IgfHwgY2l0eUVycm9yIHx8IGxnYUVycm9yKTtcclxuICAgIH1cclxuXHJcbn0iXX0=