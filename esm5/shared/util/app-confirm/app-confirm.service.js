import * as tslib_1 from "tslib";
import { MatDialogRef, MatDialog, MatDialogConfig } from '@angular/material';
import { Injectable } from '@angular/core';
import { AppConfirmComponent } from './app-confirm.component';
var AppConfirmService = /** @class */ (function () {
    function AppConfirmService(dialog) {
        this.dialog = dialog;
    }
    AppConfirmService.prototype.confirm = function (data) {
        if (data === void 0) { data = {}; }
        data.title = data.title || 'Confirm';
        data.message = data.message || 'Are you sure?';
        var dialogRef;
        dialogRef = this.dialog.open(AppConfirmComponent, {
            width: '380px',
            disableClose: true,
            data: { title: data.title, message: data.message }
        });
        return dialogRef.afterClosed();
    };
    AppConfirmService.ctorParameters = function () { return [
        { type: MatDialog }
    ]; };
    AppConfirmService = tslib_1.__decorate([
        Injectable()
    ], AppConfirmService);
    return AppConfirmService;
}());
export { AppConfirmService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLWNvbmZpcm0uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC91dGlsL2FwcC1jb25maXJtL2FwcC1jb25maXJtLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBLE9BQU8sRUFBRSxZQUFZLEVBQUUsU0FBUyxFQUFFLGVBQWUsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQzdFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFROUQ7SUFFSSwyQkFBb0IsTUFBaUI7UUFBakIsV0FBTSxHQUFOLE1BQU0sQ0FBVztJQUNyQyxDQUFDO0lBRU0sbUNBQU8sR0FBZCxVQUFlLElBQXNCO1FBQXRCLHFCQUFBLEVBQUEsU0FBc0I7UUFDakMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxJQUFJLFNBQVMsQ0FBQztRQUNyQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLElBQUksZUFBZSxDQUFDO1FBQy9DLElBQUksU0FBNEMsQ0FBQztRQUNqRCxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUU7WUFDOUMsS0FBSyxFQUFFLE9BQU87WUFDZCxZQUFZLEVBQUUsSUFBSTtZQUNsQixJQUFJLEVBQUUsRUFBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBQztTQUNuRCxDQUFDLENBQUM7UUFDSCxPQUFPLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUNuQyxDQUFDOztnQkFiMkIsU0FBUzs7SUFGNUIsaUJBQWlCO1FBRDdCLFVBQVUsRUFBRTtPQUNBLGlCQUFpQixDQWdCN0I7SUFBRCx3QkFBQztDQUFBLEFBaEJELElBZ0JDO1NBaEJZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgTWF0RGlhbG9nUmVmLCBNYXREaWFsb2csIE1hdERpYWxvZ0NvbmZpZyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgQXBwQ29uZmlybUNvbXBvbmVudCB9IGZyb20gJy4vYXBwLWNvbmZpcm0uY29tcG9uZW50JztcclxuXHJcbmludGVyZmFjZSBDb25maXJtRGF0YSB7XHJcbiAgICB0aXRsZT86IHN0cmluZztcclxuICAgIG1lc3NhZ2U/OiBzdHJpbmc7XHJcbn1cclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIEFwcENvbmZpcm1TZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGRpYWxvZzogTWF0RGlhbG9nKSB7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGNvbmZpcm0oZGF0YTogQ29uZmlybURhdGEgPSB7fSk6IE9ic2VydmFibGU8Ym9vbGVhbj4ge1xyXG4gICAgICAgIGRhdGEudGl0bGUgPSBkYXRhLnRpdGxlIHx8ICdDb25maXJtJztcclxuICAgICAgICBkYXRhLm1lc3NhZ2UgPSBkYXRhLm1lc3NhZ2UgfHwgJ0FyZSB5b3Ugc3VyZT8nO1xyXG4gICAgICAgIGxldCBkaWFsb2dSZWY6IE1hdERpYWxvZ1JlZjxBcHBDb25maXJtQ29tcG9uZW50PjtcclxuICAgICAgICBkaWFsb2dSZWYgPSB0aGlzLmRpYWxvZy5vcGVuKEFwcENvbmZpcm1Db21wb25lbnQsIHtcclxuICAgICAgICAgICAgd2lkdGg6ICczODBweCcsXHJcbiAgICAgICAgICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgICAgICAgICAgZGF0YToge3RpdGxlOiBkYXRhLnRpdGxlLCBtZXNzYWdlOiBkYXRhLm1lc3NhZ2V9XHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgcmV0dXJuIGRpYWxvZ1JlZi5hZnRlckNsb3NlZCgpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==