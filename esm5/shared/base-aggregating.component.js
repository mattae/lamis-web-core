import { ObjectDataTableAdapter } from '@alfresco/adf-core';
import { ITEMS_PER_PAGE } from '../shared/constants/pagination.constants';
var BaseAggregatingComponent = /** @class */ (function () {
    function BaseAggregatingComponent(router, activatedRoute, notification, media) {
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.notification = notification;
        this.media = media;
        this.filter = [];
        this.totalItems = 0;
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.page = 1;
        this.selectedAggregates = [];
        this.sortOrder = 'asc';
        this.sortBy = 'id';
    }
    BaseAggregatingComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.routeData = this.activatedRoute.data.subscribe(function (data) {
            if (data['pagingParams']) {
                _this.page = data['pagingParams'].page;
                _this.previousPage = data['pagingParams'].page;
                _this.currentSearch = data['pagingParams'].query;
                _this.reverse = data['pagingParams'].ascending;
                _this.predicate = data['pagingParams'].predicate;
                if (data['pagingParams'].filter) {
                    _this.filter = typeof data['pagingParams'].filter === 'string' ?
                        [data['pagingParams'].filter] : data['pagingParams'].filter;
                }
            }
        });
        this.transition();
    };
    BaseAggregatingComponent.prototype.searchEntities = function (keyword) {
        this.selectedAggregates = [];
        this.currentSearch = keyword;
        this.transition();
    };
    BaseAggregatingComponent.prototype.entitySelected = function (event) {
        var row = event.obj;
        this.router.navigate(['..', this.getPath(), row.id, 'view'], { relativeTo: this.activatedRoute });
    };
    BaseAggregatingComponent.prototype.getSort = function () {
        var result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    };
    BaseAggregatingComponent.prototype.sort = function (sortEvent) {
        this.sortBy = sortEvent.key;
        this.sortOrder = sortEvent.direction;
        this.predicate = this.sortBy;
        this.reverse = this.sortOrder === 'asc';
        this.transition();
    };
    BaseAggregatingComponent.prototype.changed = function () {
        this.previousPage = 1;
        this.page = 1;
        this.transition();
    };
    BaseAggregatingComponent.prototype.changeLinks = function (event) {
        this.page = event;
        this.loadPage(event);
    };
    BaseAggregatingComponent.prototype.loadPage = function (page) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    };
    BaseAggregatingComponent.prototype.transition = function () {
        var params = {};
        if ((this.page) || (this.currentSearch)) {
            params.page = this.page;
        }
        if (this.currentSearch) {
            params.query = this.currentSearch;
        }
        if (this.predicate && this.predicate !== 'id') {
            params.sort = this.predicate + ',' + (this.reverse ? 'asc' : 'desc');
        }
        if (this.filter) {
            params.filter = this.filter;
        }
        this.router.navigate(['..', this.getPath()], { relativeTo: this.activatedRoute, queryParams: params });
        this.loadAll();
    };
    BaseAggregatingComponent.prototype.loadAll = function () {
        var _this = this;
        var aggs = [];
        if (this.filter) {
            if (this.selectedAggregates.length == 0) {
                this.filter.forEach(function (filter) {
                    var parts = filter.split(':');
                    _this.selectedAggregates.push({ field: parts[0], key: parts[1] });
                });
            }
        }
        if (this.selectedAggregates) {
            this.selectedAggregates.forEach(function (agg) {
                aggs.push(agg);
            });
        }
        this.getService().search({
            query: this.currentSearch,
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.getSort(),
            aggs: this.selectedAggregates
        }).subscribe(function (res) { return _this.onSuccess(res.body, res.headers); }, function (res) { return _this.onError(res.statusText); });
    };
    BaseAggregatingComponent.prototype.onSuccess = function (data, headers) {
        var _this = this;
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        this.rawEntities = data;
        this.entities = new ObjectDataTableAdapter(data);
        var result = this.buildMap(JSON.parse(headers.get('aggregates')));
        var map = new Map();
        result.forEach(function (entryVal, entryKey) {
            var aggs = [];
            if (_this.selectedAggregates && _this.selectedAggregates.length !== 0) {
                entryVal.forEach(function (entry) {
                    var selectedAggregate = {};
                    _this.selectedAggregates.filter(function (e) {
                        if (e.key == entry.key) {
                            selectedAggregate = e;
                        }
                    });
                    if (entry.field == selectedAggregate.field && entry.key == selectedAggregate.key) {
                        aggs.push({
                            field: entry.field,
                            key: entry.key,
                            count: entry.count,
                            selected: true
                        });
                    }
                    else {
                        aggs.push({
                            field: entry.field,
                            key: entry.key,
                            count: entry.count,
                            selected: false
                        });
                    }
                });
            }
            else {
                entryVal.forEach(function (entry) {
                    aggs.push({
                        field: entry.field,
                        key: entry.key,
                        count: entry.count,
                        selected: false
                    });
                });
            }
            map.set(entryKey, aggs);
        });
        this.aggregates = map;
        this.querySuccess();
    };
    BaseAggregatingComponent.prototype.onError = function (error) {
        this.notification.openSnackMessage(error.message);
    };
    BaseAggregatingComponent.prototype.addFilter = function (field, val) {
        this.aggregateSelected(field, val, true);
    };
    BaseAggregatingComponent.prototype.removeFilter = function (field, val) {
        this.aggregateSelected(field, val, false);
    };
    BaseAggregatingComponent.prototype.aggregateSelected = function (field, val, state) {
        var _this = this;
        var selection = { 'field': field, 'key': val };
        if (state) {
            this.selectedAggregates.push(selection);
        }
        else {
            var found_1 = false;
            this.selectedAggregates.filter(function (e) {
                if (e.field === selection.field && e.key === selection.key) {
                    found_1 = true;
                }
            });
            if (found_1) {
                this.selectedAggregates = this.selectedAggregates
                    .filter(function (e) { return !(e.field === selection.field && e.key === selection.key); });
            }
        }
        this.filter = [];
        this.selectedAggregates.forEach(function (agg) {
            _this.filter.push(agg.field + ':' + agg.key);
        });
        this.transition();
    };
    BaseAggregatingComponent.prototype.buildMap = function (obj) {
        var map = new Map();
        Object.keys(obj).forEach(function (key) {
            map.set(key, obj[key]);
        });
        return map;
    };
    return BaseAggregatingComponent;
}());
export { BaseAggregatingComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS1hZ2dyZWdhdGluZy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvYmFzZS1hZ2dyZWdhdGluZy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxFQUF1QixzQkFBc0IsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBSWpGLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSwwQ0FBMEMsQ0FBQztBQUkxRTtJQWtCQyxrQ0FBc0IsTUFBYyxFQUNkLGNBQThCLEVBQzlCLFlBQWlDLEVBQ2pDLEtBQW9CO1FBSHBCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsaUJBQVksR0FBWixZQUFZLENBQXFCO1FBQ2pDLFVBQUssR0FBTCxLQUFLLENBQWU7UUFmaEMsV0FBTSxHQUFVLEVBQUUsQ0FBQztRQUM3QixlQUFVLEdBQUcsQ0FBQyxDQUFDO1FBRWYsaUJBQVksR0FBRyxjQUFjLENBQUM7UUFDOUIsU0FBSSxHQUFHLENBQUMsQ0FBQztRQUVDLHVCQUFrQixHQUFVLEVBQUUsQ0FBQztRQUN6QyxjQUFTLEdBQUcsS0FBSyxDQUFDO1FBQ2xCLFdBQU0sR0FBVyxJQUFJLENBQUM7SUFRdEIsQ0FBQztJQUVELDJDQUFRLEdBQVI7UUFBQSxpQkFlQztRQWRBLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQUMsSUFBSTtZQUN4RCxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsRUFBRTtnQkFDekIsS0FBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsSUFBSSxDQUFDO2dCQUN0QyxLQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxJQUFJLENBQUM7Z0JBQzlDLEtBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEtBQUssQ0FBQztnQkFDaEQsS0FBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsU0FBUyxDQUFDO2dCQUM5QyxLQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxTQUFTLENBQUM7Z0JBQ2hELElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLE1BQU0sRUFBRTtvQkFDaEMsS0FBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQyxNQUFNLEtBQUssUUFBUSxDQUFDLENBQUM7d0JBQzlELENBQVUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBUyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUMsTUFBTSxDQUFDO2lCQUM5RTthQUNEO1FBQ0YsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDbkIsQ0FBQztJQUVELGlEQUFjLEdBQWQsVUFBZSxPQUFlO1FBQzdCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxFQUFFLENBQUM7UUFDN0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxPQUFPLENBQUM7UUFDN0IsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQ25CLENBQUM7SUFFRCxpREFBYyxHQUFkLFVBQWUsS0FBVTtRQUN4QixJQUFJLEdBQUcsR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDO1FBQ3BCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBRSxHQUFHLENBQUMsRUFBRSxFQUFFLE1BQU0sQ0FBQyxFQUFFLEVBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUMsQ0FBQyxDQUFDO0lBQ2pHLENBQUM7SUFFRCwwQ0FBTyxHQUFQO1FBQ0MsSUFBTSxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztRQUN4RSxJQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssSUFBSSxFQUFFO1lBQzVCLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDbEI7UUFDRCxPQUFPLE1BQU0sQ0FBQztJQUNmLENBQUM7SUFFRCx1Q0FBSSxHQUFKLFVBQUssU0FBYztRQUNsQixJQUFJLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQyxHQUFHLENBQUM7UUFDNUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUMsU0FBUyxDQUFDO1FBQ3JDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUM3QixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLEtBQUssS0FBSyxDQUFDO1FBQ3hDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUNuQixDQUFDO0lBRUQsMENBQU8sR0FBUDtRQUNDLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDO1FBQ2QsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQ25CLENBQUM7SUFFRCw4Q0FBVyxHQUFYLFVBQVksS0FBVTtRQUNyQixJQUFJLENBQUMsSUFBSSxHQUFHLEtBQUssQ0FBQztRQUNsQixJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3RCLENBQUM7SUFFRCwyQ0FBUSxHQUFSLFVBQVMsSUFBWTtRQUNwQixJQUFJLElBQUksS0FBSyxJQUFJLENBQUMsWUFBWSxFQUFFO1lBQy9CLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztTQUNsQjtJQUNGLENBQUM7SUFFRCw2Q0FBVSxHQUFWO1FBQ0MsSUFBSSxNQUFNLEdBQVEsRUFBRSxDQUFDO1FBQ3JCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUU7WUFDeEMsTUFBTSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ3hCO1FBQ0QsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3ZCLE1BQU0sQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQztTQUNsQztRQUNELElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLElBQUksRUFBRTtZQUM5QyxNQUFNLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQTtTQUNwRTtRQUNELElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNoQixNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7U0FDNUI7UUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsRUFBRSxFQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUMsQ0FBQyxDQUFDO1FBQ3JHLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUNoQixDQUFDO0lBRUQsMENBQU8sR0FBUDtRQUFBLGlCQXlCQztRQXhCQSxJQUFNLElBQUksR0FBRyxFQUFFLENBQUM7UUFDaEIsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2hCLElBQUksSUFBSSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUU7Z0JBQ3hDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUMsTUFBYztvQkFDbEMsSUFBSSxLQUFLLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDOUIsS0FBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxFQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLEVBQUUsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBQyxDQUFDLENBQUM7Z0JBQ2hFLENBQUMsQ0FBQyxDQUFBO2FBQ0Y7U0FDRDtRQUNELElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQzVCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsVUFBQyxHQUFHO2dCQUNuQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ2hCLENBQUMsQ0FBQyxDQUFDO1NBQ0g7UUFDRCxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsTUFBTSxDQUFDO1lBQ3hCLEtBQUssRUFBRSxJQUFJLENBQUMsYUFBYTtZQUN6QixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDO1lBQ25CLElBQUksRUFBRSxJQUFJLENBQUMsWUFBWTtZQUN2QixJQUFJLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNwQixJQUFJLEVBQUUsSUFBSSxDQUFDLGtCQUFrQjtTQUM3QixDQUFDLENBQUMsU0FBUyxDQUNYLFVBQUMsR0FBUSxJQUFLLE9BQUEsS0FBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxPQUFPLENBQUMsRUFBckMsQ0FBcUMsRUFDbkQsVUFBQyxHQUFRLElBQUssT0FBQSxLQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsRUFBNUIsQ0FBNEIsQ0FDMUMsQ0FBQztJQUNILENBQUM7SUFFUyw0Q0FBUyxHQUFuQixVQUFvQixJQUFTLEVBQUUsT0FBWTtRQUEzQyxpQkFpREM7UUFoREEsSUFBSSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUNsQyxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztRQUN4QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksc0JBQXNCLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDakQsSUFBSSxNQUFNLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQ2xFLElBQUksR0FBRyxHQUFHLElBQUksR0FBRyxFQUFFLENBQUM7UUFDcEIsTUFBTSxDQUFDLE9BQU8sQ0FBQyxVQUFDLFFBQVEsRUFBRSxRQUFRO1lBQ2pDLElBQUksSUFBSSxHQUFVLEVBQUUsQ0FBQztZQUNyQixJQUFJLEtBQUksQ0FBQyxrQkFBa0IsSUFBSSxLQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtnQkFDcEUsUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFDLEtBQVU7b0JBQzNCLElBQUksaUJBQWlCLEdBQVEsRUFBRSxDQUFDO29CQUNoQyxLQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxDQUFDLFVBQUMsQ0FBQzt3QkFDaEMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLEtBQUssQ0FBQyxHQUFHLEVBQUU7NEJBQ3ZCLGlCQUFpQixHQUFHLENBQUMsQ0FBQzt5QkFDdEI7b0JBQ0YsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxLQUFLLENBQUMsS0FBSyxJQUFJLGlCQUFpQixDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsR0FBRyxJQUFJLGlCQUFpQixDQUFDLEdBQUcsRUFBRTt3QkFDakYsSUFBSSxDQUFDLElBQUksQ0FBQzs0QkFDVCxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUs7NEJBQ2xCLEdBQUcsRUFBRSxLQUFLLENBQUMsR0FBRzs0QkFDZCxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUs7NEJBQ2xCLFFBQVEsRUFBRSxJQUFJO3lCQUNkLENBQUMsQ0FBQztxQkFDSDt5QkFDSTt3QkFDSixJQUFJLENBQUMsSUFBSSxDQUFDOzRCQUNULEtBQUssRUFBRSxLQUFLLENBQUMsS0FBSzs0QkFDbEIsR0FBRyxFQUFFLEtBQUssQ0FBQyxHQUFHOzRCQUNkLEtBQUssRUFBRSxLQUFLLENBQUMsS0FBSzs0QkFDbEIsUUFBUSxFQUFFLEtBQUs7eUJBQ2YsQ0FBQyxDQUFDO3FCQUNIO2dCQUNGLENBQUMsQ0FBQyxDQUFDO2FBQ0g7aUJBQ0k7Z0JBQ0osUUFBUSxDQUFDLE9BQU8sQ0FBQyxVQUFDLEtBQVU7b0JBQzNCLElBQUksQ0FBQyxJQUFJLENBQUM7d0JBQ1QsS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLO3dCQUNsQixHQUFHLEVBQUUsS0FBSyxDQUFDLEdBQUc7d0JBQ2QsS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLO3dCQUNsQixRQUFRLEVBQUUsS0FBSztxQkFDZixDQUFDLENBQUE7Z0JBQ0gsQ0FBQyxDQUFDLENBQUM7YUFDSDtZQUNELEdBQUcsQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3pCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUM7UUFDdEIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFFTywwQ0FBTyxHQUFmLFVBQWdCLEtBQVU7UUFDekIsSUFBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDbkQsQ0FBQztJQUVELDRDQUFTLEdBQVQsVUFBVSxLQUFhLEVBQUUsR0FBVztRQUNuQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBRUQsK0NBQVksR0FBWixVQUFhLEtBQWEsRUFBRSxHQUFXO1FBQ3RDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLEVBQUUsR0FBRyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFFRCxvREFBaUIsR0FBakIsVUFBa0IsS0FBYSxFQUFFLEdBQVcsRUFBRSxLQUFjO1FBQTVELGlCQXNCQztRQXJCQSxJQUFNLFNBQVMsR0FBYyxFQUFDLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBQyxDQUFDO1FBQzFELElBQUksS0FBSyxFQUFFO1lBQ1YsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztTQUN4QzthQUNJO1lBQ0osSUFBSSxPQUFLLEdBQUcsS0FBSyxDQUFDO1lBQ2xCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsVUFBQSxDQUFDO2dCQUMvQixJQUFJLENBQUMsQ0FBQyxLQUFLLEtBQUssU0FBUyxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUMsR0FBRyxLQUFLLFNBQVMsQ0FBQyxHQUFHLEVBQUU7b0JBQzNELE9BQUssR0FBRyxJQUFJLENBQUM7aUJBQ2I7WUFDRixDQUFDLENBQUMsQ0FBQztZQUNILElBQUksT0FBSyxFQUFFO2dCQUNWLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCO3FCQUMvQyxNQUFNLENBQUMsVUFBQSxDQUFDLElBQUksT0FBQSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssS0FBSyxTQUFTLENBQUMsS0FBSyxJQUFJLENBQUMsQ0FBQyxHQUFHLEtBQUssU0FBUyxDQUFDLEdBQUcsQ0FBQyxFQUF6RCxDQUF5RCxDQUFDLENBQUM7YUFDekU7U0FDRDtRQUNELElBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsVUFBQyxHQUFHO1lBQ25DLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLEdBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQztRQUM3QyxDQUFDLENBQUMsQ0FBQztRQUNILElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUNuQixDQUFDO0lBRUQsMkNBQVEsR0FBUixVQUFTLEdBQVE7UUFDaEIsSUFBSSxHQUFHLEdBQUcsSUFBSSxHQUFHLEVBQUUsQ0FBQztRQUNwQixNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFBLEdBQUc7WUFDM0IsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDeEIsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLEdBQUcsQ0FBQztJQUNaLENBQUM7SUFPRiwrQkFBQztBQUFELENBQUMsQUF2T0QsSUF1T0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlLCBPYmplY3REYXRhVGFibGVBZGFwdGVyIH0gZnJvbSAnQGFsZnJlc2NvL2FkZi1jb3JlJztcclxuaW1wb3J0IHsgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1lZGlhT2JzZXJ2ZXIgfSBmcm9tICdAYW5ndWxhci9mbGV4LWxheW91dCc7XHJcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlLCBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBJVEVNU19QRVJfUEFHRSB9IGZyb20gJy4uL3NoYXJlZC9jb25zdGFudHMvcGFnaW5hdGlvbi5jb25zdGFudHMnO1xyXG5pbXBvcnQgeyBBZ2dyZWdhdGUgfSBmcm9tICcuL21vZGVsL2Jhc2UtZW50aXR5JztcclxuXHJcblxyXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQmFzZUFnZ3JlZ2F0aW5nQ29tcG9uZW50PFQ+IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHRwdWJsaWMgZW50aXRpZXM6IE9iamVjdERhdGFUYWJsZUFkYXB0ZXI7XHJcblx0cHVibGljIHJhd0VudGl0aWVzOiBUW107XHJcblx0cHJvdGVjdGVkIHByZXZpb3VzUGFnZTogYW55O1xyXG5cdHByb3RlY3RlZCBwcmVkaWNhdGU6IGFueTtcclxuXHRwcm90ZWN0ZWQgcmV2ZXJzZTogYW55O1xyXG5cdHByb3RlY3RlZCBmaWx0ZXI6IGFueVtdID0gW107XHJcblx0dG90YWxJdGVtcyA9IDA7XHJcblx0cXVlcnlDb3VudDogYW55O1xyXG5cdGl0ZW1zUGVyUGFnZSA9IElURU1TX1BFUl9QQUdFO1xyXG5cdHBhZ2UgPSAxO1xyXG5cdGFnZ3JlZ2F0ZXM6IGFueTtcclxuXHRwcm90ZWN0ZWQgc2VsZWN0ZWRBZ2dyZWdhdGVzOiBhbnlbXSA9IFtdO1xyXG5cdHNvcnRPcmRlciA9ICdhc2MnO1xyXG5cdHNvcnRCeTogc3RyaW5nID0gJ2lkJztcclxuXHRjdXJyZW50U2VhcmNoOiBzdHJpbmc7XHJcblx0cm91dGVEYXRhOiBhbnk7XHJcblxyXG5cdGNvbnN0cnVjdG9yKHByb3RlY3RlZCByb3V0ZXI6IFJvdXRlcixcclxuXHQgICAgICAgICAgICBwcm90ZWN0ZWQgYWN0aXZhdGVkUm91dGU6IEFjdGl2YXRlZFJvdXRlLFxyXG5cdCAgICAgICAgICAgIHByb3RlY3RlZCBub3RpZmljYXRpb246IE5vdGlmaWNhdGlvblNlcnZpY2UsXHJcblx0ICAgICAgICAgICAgcHJvdGVjdGVkIG1lZGlhOiBNZWRpYU9ic2VydmVyKSB7XHJcblx0fVxyXG5cclxuXHRuZ09uSW5pdCgpOiB2b2lkIHtcclxuXHRcdHRoaXMucm91dGVEYXRhID0gdGhpcy5hY3RpdmF0ZWRSb3V0ZS5kYXRhLnN1YnNjcmliZSgoZGF0YSkgPT4ge1xyXG5cdFx0XHRpZiAoZGF0YVsncGFnaW5nUGFyYW1zJ10pIHtcclxuXHRcdFx0XHR0aGlzLnBhZ2UgPSBkYXRhWydwYWdpbmdQYXJhbXMnXS5wYWdlO1xyXG5cdFx0XHRcdHRoaXMucHJldmlvdXNQYWdlID0gZGF0YVsncGFnaW5nUGFyYW1zJ10ucGFnZTtcclxuXHRcdFx0XHR0aGlzLmN1cnJlbnRTZWFyY2ggPSBkYXRhWydwYWdpbmdQYXJhbXMnXS5xdWVyeTtcclxuXHRcdFx0XHR0aGlzLnJldmVyc2UgPSBkYXRhWydwYWdpbmdQYXJhbXMnXS5hc2NlbmRpbmc7XHJcblx0XHRcdFx0dGhpcy5wcmVkaWNhdGUgPSBkYXRhWydwYWdpbmdQYXJhbXMnXS5wcmVkaWNhdGU7XHJcblx0XHRcdFx0aWYgKGRhdGFbJ3BhZ2luZ1BhcmFtcyddLmZpbHRlcikge1xyXG5cdFx0XHRcdFx0dGhpcy5maWx0ZXIgPSB0eXBlb2YgZGF0YVsncGFnaW5nUGFyYW1zJ10uZmlsdGVyID09PSAnc3RyaW5nJyA/XHJcblx0XHRcdFx0XHRcdFs8c3RyaW5nPiBkYXRhWydwYWdpbmdQYXJhbXMnXS5maWx0ZXJdIDogPGFueVtdPiBkYXRhWydwYWdpbmdQYXJhbXMnXS5maWx0ZXI7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHRcdHRoaXMudHJhbnNpdGlvbigpO1xyXG5cdH1cclxuXHJcblx0c2VhcmNoRW50aXRpZXMoa2V5d29yZDogc3RyaW5nKSB7XHJcblx0XHR0aGlzLnNlbGVjdGVkQWdncmVnYXRlcyA9IFtdO1xyXG5cdFx0dGhpcy5jdXJyZW50U2VhcmNoID0ga2V5d29yZDtcclxuXHRcdHRoaXMudHJhbnNpdGlvbigpO1xyXG5cdH1cclxuXHJcblx0ZW50aXR5U2VsZWN0ZWQoZXZlbnQ6IGFueSkge1xyXG5cdFx0bGV0IHJvdyA9IGV2ZW50Lm9iajtcclxuXHRcdHRoaXMucm91dGVyLm5hdmlnYXRlKFsnLi4nLCB0aGlzLmdldFBhdGgoKSwgcm93LmlkLCAndmlldyddLCB7cmVsYXRpdmVUbzogdGhpcy5hY3RpdmF0ZWRSb3V0ZX0pO1xyXG5cdH1cclxuXHJcblx0Z2V0U29ydCgpIHtcclxuXHRcdGNvbnN0IHJlc3VsdCA9IFt0aGlzLnByZWRpY2F0ZSArICcsJyArICh0aGlzLnJldmVyc2UgPyAnYXNjJyA6ICdkZXNjJyldO1xyXG5cdFx0aWYgKHRoaXMucHJlZGljYXRlICE9PSAnaWQnKSB7XHJcblx0XHRcdHJlc3VsdC5wdXNoKCdpZCcpO1xyXG5cdFx0fVxyXG5cdFx0cmV0dXJuIHJlc3VsdDtcclxuXHR9XHJcblxyXG5cdHNvcnQoc29ydEV2ZW50OiBhbnkpOiB2b2lkIHtcclxuXHRcdHRoaXMuc29ydEJ5ID0gc29ydEV2ZW50LmtleTtcclxuXHRcdHRoaXMuc29ydE9yZGVyID0gc29ydEV2ZW50LmRpcmVjdGlvbjtcclxuXHRcdHRoaXMucHJlZGljYXRlID0gdGhpcy5zb3J0Qnk7XHJcblx0XHR0aGlzLnJldmVyc2UgPSB0aGlzLnNvcnRPcmRlciA9PT0gJ2FzYyc7XHJcblx0XHR0aGlzLnRyYW5zaXRpb24oKTtcclxuXHR9XHJcblxyXG5cdGNoYW5nZWQoKSB7XHJcblx0XHR0aGlzLnByZXZpb3VzUGFnZSA9IDE7XHJcblx0XHR0aGlzLnBhZ2UgPSAxO1xyXG5cdFx0dGhpcy50cmFuc2l0aW9uKCk7XHJcblx0fVxyXG5cclxuXHRjaGFuZ2VMaW5rcyhldmVudDogYW55KTogdm9pZCB7XHJcblx0XHR0aGlzLnBhZ2UgPSBldmVudDtcclxuXHRcdHRoaXMubG9hZFBhZ2UoZXZlbnQpO1xyXG5cdH1cclxuXHJcblx0bG9hZFBhZ2UocGFnZTogbnVtYmVyKSB7XHJcblx0XHRpZiAocGFnZSAhPT0gdGhpcy5wcmV2aW91c1BhZ2UpIHtcclxuXHRcdFx0dGhpcy5wcmV2aW91c1BhZ2UgPSBwYWdlO1xyXG5cdFx0XHR0aGlzLnRyYW5zaXRpb24oKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHRyYW5zaXRpb24oKSB7XHJcblx0XHRsZXQgcGFyYW1zOiBhbnkgPSB7fTtcclxuXHRcdGlmICgodGhpcy5wYWdlKSB8fCAodGhpcy5jdXJyZW50U2VhcmNoKSkge1xyXG5cdFx0XHRwYXJhbXMucGFnZSA9IHRoaXMucGFnZTtcclxuXHRcdH1cclxuXHRcdGlmICh0aGlzLmN1cnJlbnRTZWFyY2gpIHtcclxuXHRcdFx0cGFyYW1zLnF1ZXJ5ID0gdGhpcy5jdXJyZW50U2VhcmNoO1xyXG5cdFx0fVxyXG5cdFx0aWYgKHRoaXMucHJlZGljYXRlICYmIHRoaXMucHJlZGljYXRlICE9PSAnaWQnKSB7XHJcblx0XHRcdHBhcmFtcy5zb3J0ID0gdGhpcy5wcmVkaWNhdGUgKyAnLCcgKyAodGhpcy5yZXZlcnNlID8gJ2FzYycgOiAnZGVzYycpXHJcblx0XHR9XHJcblx0XHRpZiAodGhpcy5maWx0ZXIpIHtcclxuXHRcdFx0cGFyYW1zLmZpbHRlciA9IHRoaXMuZmlsdGVyO1xyXG5cdFx0fVxyXG5cdFx0dGhpcy5yb3V0ZXIubmF2aWdhdGUoWycuLicsIHRoaXMuZ2V0UGF0aCgpXSwge3JlbGF0aXZlVG86IHRoaXMuYWN0aXZhdGVkUm91dGUsIHF1ZXJ5UGFyYW1zOiBwYXJhbXN9KTtcclxuXHRcdHRoaXMubG9hZEFsbCgpO1xyXG5cdH1cclxuXHJcblx0bG9hZEFsbCgpIHtcclxuXHRcdGNvbnN0IGFnZ3MgPSBbXTtcclxuXHRcdGlmICh0aGlzLmZpbHRlcikge1xyXG5cdFx0XHRpZiAodGhpcy5zZWxlY3RlZEFnZ3JlZ2F0ZXMubGVuZ3RoID09IDApIHtcclxuXHRcdFx0XHR0aGlzLmZpbHRlci5mb3JFYWNoKChmaWx0ZXI6IHN0cmluZykgPT4ge1xyXG5cdFx0XHRcdFx0bGV0IHBhcnRzID0gZmlsdGVyLnNwbGl0KCc6Jyk7XHJcblx0XHRcdFx0XHR0aGlzLnNlbGVjdGVkQWdncmVnYXRlcy5wdXNoKHtmaWVsZDogcGFydHNbMF0sIGtleTogcGFydHNbMV19KTtcclxuXHRcdFx0XHR9KVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0XHRpZiAodGhpcy5zZWxlY3RlZEFnZ3JlZ2F0ZXMpIHtcclxuXHRcdFx0dGhpcy5zZWxlY3RlZEFnZ3JlZ2F0ZXMuZm9yRWFjaCgoYWdnKSA9PiB7XHJcblx0XHRcdFx0YWdncy5wdXNoKGFnZyk7XHJcblx0XHRcdH0pO1xyXG5cdFx0fVxyXG5cdFx0dGhpcy5nZXRTZXJ2aWNlKCkuc2VhcmNoKHtcclxuXHRcdFx0cXVlcnk6IHRoaXMuY3VycmVudFNlYXJjaCxcclxuXHRcdFx0cGFnZTogdGhpcy5wYWdlIC0gMSxcclxuXHRcdFx0c2l6ZTogdGhpcy5pdGVtc1BlclBhZ2UsXHJcblx0XHRcdHNvcnQ6IHRoaXMuZ2V0U29ydCgpLFxyXG5cdFx0XHRhZ2dzOiB0aGlzLnNlbGVjdGVkQWdncmVnYXRlc1xyXG5cdFx0fSkuc3Vic2NyaWJlKFxyXG5cdFx0XHQocmVzOiBhbnkpID0+IHRoaXMub25TdWNjZXNzKHJlcy5ib2R5LCByZXMuaGVhZGVycyksXHJcblx0XHRcdChyZXM6IGFueSkgPT4gdGhpcy5vbkVycm9yKHJlcy5zdGF0dXNUZXh0KVxyXG5cdFx0KTtcclxuXHR9XHJcblxyXG5cdHByb3RlY3RlZCBvblN1Y2Nlc3MoZGF0YTogYW55LCBoZWFkZXJzOiBhbnkpIHtcclxuXHRcdHRoaXMudG90YWxJdGVtcyA9IGhlYWRlcnMuZ2V0KCdYLVRvdGFsLUNvdW50Jyk7XHJcblx0XHR0aGlzLnF1ZXJ5Q291bnQgPSB0aGlzLnRvdGFsSXRlbXM7XHJcblx0XHR0aGlzLnJhd0VudGl0aWVzID0gZGF0YTtcclxuXHRcdHRoaXMuZW50aXRpZXMgPSBuZXcgT2JqZWN0RGF0YVRhYmxlQWRhcHRlcihkYXRhKTtcclxuXHRcdGxldCByZXN1bHQgPSB0aGlzLmJ1aWxkTWFwKEpTT04ucGFyc2UoaGVhZGVycy5nZXQoJ2FnZ3JlZ2F0ZXMnKSkpO1xyXG5cdFx0bGV0IG1hcCA9IG5ldyBNYXAoKTtcclxuXHRcdHJlc3VsdC5mb3JFYWNoKChlbnRyeVZhbCwgZW50cnlLZXkpID0+IHtcclxuXHRcdFx0bGV0IGFnZ3M6IGFueVtdID0gW107XHJcblx0XHRcdGlmICh0aGlzLnNlbGVjdGVkQWdncmVnYXRlcyAmJiB0aGlzLnNlbGVjdGVkQWdncmVnYXRlcy5sZW5ndGggIT09IDApIHtcclxuXHRcdFx0XHRlbnRyeVZhbC5mb3JFYWNoKChlbnRyeTogYW55KSA9PiB7XHJcblx0XHRcdFx0XHRsZXQgc2VsZWN0ZWRBZ2dyZWdhdGU6IGFueSA9IHt9O1xyXG5cdFx0XHRcdFx0dGhpcy5zZWxlY3RlZEFnZ3JlZ2F0ZXMuZmlsdGVyKChlKSA9PiB7XHJcblx0XHRcdFx0XHRcdGlmIChlLmtleSA9PSBlbnRyeS5rZXkpIHtcclxuXHRcdFx0XHRcdFx0XHRzZWxlY3RlZEFnZ3JlZ2F0ZSA9IGU7XHJcblx0XHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0aWYgKGVudHJ5LmZpZWxkID09IHNlbGVjdGVkQWdncmVnYXRlLmZpZWxkICYmIGVudHJ5LmtleSA9PSBzZWxlY3RlZEFnZ3JlZ2F0ZS5rZXkpIHtcclxuXHRcdFx0XHRcdFx0YWdncy5wdXNoKHtcclxuXHRcdFx0XHRcdFx0XHRmaWVsZDogZW50cnkuZmllbGQsXHJcblx0XHRcdFx0XHRcdFx0a2V5OiBlbnRyeS5rZXksXHJcblx0XHRcdFx0XHRcdFx0Y291bnQ6IGVudHJ5LmNvdW50LFxyXG5cdFx0XHRcdFx0XHRcdHNlbGVjdGVkOiB0cnVlXHJcblx0XHRcdFx0XHRcdH0pO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0ZWxzZSB7XHJcblx0XHRcdFx0XHRcdGFnZ3MucHVzaCh7XHJcblx0XHRcdFx0XHRcdFx0ZmllbGQ6IGVudHJ5LmZpZWxkLFxyXG5cdFx0XHRcdFx0XHRcdGtleTogZW50cnkua2V5LFxyXG5cdFx0XHRcdFx0XHRcdGNvdW50OiBlbnRyeS5jb3VudCxcclxuXHRcdFx0XHRcdFx0XHRzZWxlY3RlZDogZmFsc2VcclxuXHRcdFx0XHRcdFx0fSk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH1cclxuXHRcdFx0ZWxzZSB7XHJcblx0XHRcdFx0ZW50cnlWYWwuZm9yRWFjaCgoZW50cnk6IGFueSkgPT4ge1xyXG5cdFx0XHRcdFx0YWdncy5wdXNoKHtcclxuXHRcdFx0XHRcdFx0ZmllbGQ6IGVudHJ5LmZpZWxkLFxyXG5cdFx0XHRcdFx0XHRrZXk6IGVudHJ5LmtleSxcclxuXHRcdFx0XHRcdFx0Y291bnQ6IGVudHJ5LmNvdW50LFxyXG5cdFx0XHRcdFx0XHRzZWxlY3RlZDogZmFsc2VcclxuXHRcdFx0XHRcdH0pXHJcblx0XHRcdFx0fSk7XHJcblx0XHRcdH1cclxuXHRcdFx0bWFwLnNldChlbnRyeUtleSwgYWdncyk7XHJcblx0XHR9KTtcclxuXHRcdHRoaXMuYWdncmVnYXRlcyA9IG1hcDtcclxuXHRcdHRoaXMucXVlcnlTdWNjZXNzKCk7XHJcblx0fVxyXG5cclxuXHRwcml2YXRlIG9uRXJyb3IoZXJyb3I6IGFueSkge1xyXG5cdFx0dGhpcy5ub3RpZmljYXRpb24ub3BlblNuYWNrTWVzc2FnZShlcnJvci5tZXNzYWdlKTtcclxuXHR9XHJcblxyXG5cdGFkZEZpbHRlcihmaWVsZDogc3RyaW5nLCB2YWw6IHN0cmluZykge1xyXG5cdFx0dGhpcy5hZ2dyZWdhdGVTZWxlY3RlZChmaWVsZCwgdmFsLCB0cnVlKTtcclxuXHR9XHJcblxyXG5cdHJlbW92ZUZpbHRlcihmaWVsZDogc3RyaW5nLCB2YWw6IHN0cmluZykge1xyXG5cdFx0dGhpcy5hZ2dyZWdhdGVTZWxlY3RlZChmaWVsZCwgdmFsLCBmYWxzZSk7XHJcblx0fVxyXG5cclxuXHRhZ2dyZWdhdGVTZWxlY3RlZChmaWVsZDogc3RyaW5nLCB2YWw6IHN0cmluZywgc3RhdGU6IGJvb2xlYW4pIHtcclxuXHRcdGNvbnN0IHNlbGVjdGlvbjogQWdncmVnYXRlID0geydmaWVsZCc6IGZpZWxkLCAna2V5JzogdmFsfTtcclxuXHRcdGlmIChzdGF0ZSkge1xyXG5cdFx0XHR0aGlzLnNlbGVjdGVkQWdncmVnYXRlcy5wdXNoKHNlbGVjdGlvbik7XHJcblx0XHR9XHJcblx0XHRlbHNlIHtcclxuXHRcdFx0bGV0IGZvdW5kID0gZmFsc2U7XHJcblx0XHRcdHRoaXMuc2VsZWN0ZWRBZ2dyZWdhdGVzLmZpbHRlcihlID0+IHtcclxuXHRcdFx0XHRpZiAoZS5maWVsZCA9PT0gc2VsZWN0aW9uLmZpZWxkICYmIGUua2V5ID09PSBzZWxlY3Rpb24ua2V5KSB7XHJcblx0XHRcdFx0XHRmb3VuZCA9IHRydWU7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KTtcclxuXHRcdFx0aWYgKGZvdW5kKSB7XHJcblx0XHRcdFx0dGhpcy5zZWxlY3RlZEFnZ3JlZ2F0ZXMgPSB0aGlzLnNlbGVjdGVkQWdncmVnYXRlc1xyXG5cdFx0XHRcdFx0LmZpbHRlcihlID0+ICEoZS5maWVsZCA9PT0gc2VsZWN0aW9uLmZpZWxkICYmIGUua2V5ID09PSBzZWxlY3Rpb24ua2V5KSk7XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdHRoaXMuZmlsdGVyID0gW107XHJcblx0XHR0aGlzLnNlbGVjdGVkQWdncmVnYXRlcy5mb3JFYWNoKChhZ2cpID0+IHtcclxuXHRcdFx0dGhpcy5maWx0ZXIucHVzaChhZ2cuZmllbGQgKyAnOicgKyBhZ2cua2V5KTtcclxuXHRcdH0pO1xyXG5cdFx0dGhpcy50cmFuc2l0aW9uKCk7XHJcblx0fVxyXG5cclxuXHRidWlsZE1hcChvYmo6IGFueSkge1xyXG5cdFx0bGV0IG1hcCA9IG5ldyBNYXAoKTtcclxuXHRcdE9iamVjdC5rZXlzKG9iaikuZm9yRWFjaChrZXkgPT4ge1xyXG5cdFx0XHRtYXAuc2V0KGtleSwgb2JqW2tleV0pO1xyXG5cdFx0fSk7XHJcblx0XHRyZXR1cm4gbWFwO1xyXG5cdH1cclxuXHJcblx0YWJzdHJhY3QgZ2V0U2VydmljZSgpOiBhbnk7XHJcblxyXG5cdGFic3RyYWN0IGdldFBhdGgoKTogc3RyaW5nO1xyXG5cclxuXHRhYnN0cmFjdCBxdWVyeVN1Y2Nlc3MoKTogYW55O1xyXG59XHJcbiJdfQ==