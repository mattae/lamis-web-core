import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RelativeTimePipe } from './relative-time.pipe';
import { ExcerptPipe } from './excerpt.pipe';
import { KeysPipe } from './keys.pipe';
import { MapValuesPipe } from './map-value.pipe';
import { NairaPipe } from './naira.pipe';
var CommonPipesModule = /** @class */ (function () {
    function CommonPipesModule() {
    }
    CommonPipesModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                RelativeTimePipe,
                ExcerptPipe,
                KeysPipe,
                MapValuesPipe,
                NairaPipe
            ],
            exports: [
                RelativeTimePipe,
                ExcerptPipe,
                KeysPipe,
                MapValuesPipe,
                NairaPipe
            ]
        })
    ], CommonPipesModule);
    return CommonPipesModule;
}());
export { CommonPipesModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbW9uLXBpcGVzLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC9waXBlcy9jb21tb24vY29tbW9uLXBpcGVzLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDN0MsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUN2QyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDakQsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQWtCekM7SUFBQTtJQUNBLENBQUM7SUFEWSxpQkFBaUI7UUFoQjdCLFFBQVEsQ0FBQztZQUNOLFlBQVksRUFBRTtnQkFDVixnQkFBZ0I7Z0JBQ2hCLFdBQVc7Z0JBQ1gsUUFBUTtnQkFDUixhQUFhO2dCQUNiLFNBQVM7YUFDWjtZQUNELE9BQU8sRUFBRTtnQkFDTCxnQkFBZ0I7Z0JBQ2hCLFdBQVc7Z0JBQ1gsUUFBUTtnQkFDUixhQUFhO2dCQUNiLFNBQVM7YUFDWjtTQUNKLENBQUM7T0FDVyxpQkFBaUIsQ0FDN0I7SUFBRCx3QkFBQztDQUFBLEFBREQsSUFDQztTQURZLGlCQUFpQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJlbGF0aXZlVGltZVBpcGUgfSBmcm9tICcuL3JlbGF0aXZlLXRpbWUucGlwZSc7XHJcbmltcG9ydCB7IEV4Y2VycHRQaXBlIH0gZnJvbSAnLi9leGNlcnB0LnBpcGUnO1xyXG5pbXBvcnQgeyBLZXlzUGlwZSB9IGZyb20gJy4va2V5cy5waXBlJztcclxuaW1wb3J0IHsgTWFwVmFsdWVzUGlwZSB9IGZyb20gJy4vbWFwLXZhbHVlLnBpcGUnO1xyXG5pbXBvcnQgeyBOYWlyYVBpcGUgfSBmcm9tICcuL25haXJhLnBpcGUnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIFJlbGF0aXZlVGltZVBpcGUsXHJcbiAgICAgICAgRXhjZXJwdFBpcGUsXHJcbiAgICAgICAgS2V5c1BpcGUsXHJcbiAgICAgICAgTWFwVmFsdWVzUGlwZSxcclxuICAgICAgICBOYWlyYVBpcGVcclxuICAgIF0sXHJcbiAgICBleHBvcnRzOiBbXHJcbiAgICAgICAgUmVsYXRpdmVUaW1lUGlwZSxcclxuICAgICAgICBFeGNlcnB0UGlwZSxcclxuICAgICAgICBLZXlzUGlwZSxcclxuICAgICAgICBNYXBWYWx1ZXNQaXBlLFxyXG4gICAgICAgIE5haXJhUGlwZVxyXG4gICAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29tbW9uUGlwZXNNb2R1bGUge1xyXG59XHJcbiJdfQ==