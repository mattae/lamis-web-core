import * as tslib_1 from "tslib";
import { JhiPaginationUtil } from 'ng-jhipster';
import { Injectable, Injector } from '@angular/core';
import * as i0 from "@angular/core";
var PagingParamsResolve = /** @class */ (function () {
    function PagingParamsResolve(injector) {
        this.injector = injector;
        this.paginationUtil = this.injector.get(JhiPaginationUtil);
    }
    PagingParamsResolve.prototype.resolve = function (route, state) {
        var page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        var query = route.queryParams['query'] ? route.queryParams['query'] : '';
        var filter = route.queryParams['filter'] ? route.queryParams['filter'] : '';
        var sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: +page,
            query: query,
            filter: filter,
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
        };
    };
    PagingParamsResolve.ctorParameters = function () { return [
        { type: Injector }
    ]; };
    PagingParamsResolve.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function PagingParamsResolve_Factory() { return new PagingParamsResolve(i0.ɵɵinject(i0.INJECTOR)); }, token: PagingParamsResolve, providedIn: "root" });
    PagingParamsResolve = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        })
    ], PagingParamsResolve);
    return PagingParamsResolve;
}());
export { PagingParamsResolve };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnaW5nLXBhcmFtLXJlc29sdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvcGFnaW5nLXBhcmFtLXJlc29sdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUNoRCxPQUFPLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFLckQ7SUFHSSw2QkFBb0IsUUFBa0I7UUFBbEIsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQUNsQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFDL0QsQ0FBQztJQUVELHFDQUFPLEdBQVAsVUFBUSxLQUE2QixFQUFFLEtBQTBCO1FBQzdELElBQU0sSUFBSSxHQUFHLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztRQUN6RSxJQUFNLEtBQUssR0FBRyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDM0UsSUFBTSxNQUFNLEdBQUcsS0FBSyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1FBQzlFLElBQU0sSUFBSSxHQUFHLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQztRQUM5RSxPQUFPO1lBQ0gsSUFBSSxFQUFFLENBQUMsSUFBSTtZQUNYLEtBQUssRUFBRSxLQUFLO1lBQ1osTUFBTSxFQUFFLE1BQU07WUFDZCxTQUFTLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDO1lBQ25ELFNBQVMsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUM7U0FDdEQsQ0FBQztJQUNOLENBQUM7O2dCQWhCNkIsUUFBUTs7O0lBSDdCLG1CQUFtQjtRQUgvQixVQUFVLENBQUM7WUFDUixVQUFVLEVBQUUsTUFBTTtTQUNyQixDQUFDO09BQ1csbUJBQW1CLENBb0IvQjs4QkEzQkQ7Q0EyQkMsQUFwQkQsSUFvQkM7U0FwQlksbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgUmVzb2x2ZSwgUm91dGVyU3RhdGVTbmFwc2hvdCB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IEpoaVBhZ2luYXRpb25VdGlsIH0gZnJvbSAnbmctamhpcHN0ZXInO1xyXG5pbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3RvciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBQYWdpbmdQYXJhbXNSZXNvbHZlIGltcGxlbWVudHMgUmVzb2x2ZTxhbnk+IHtcclxuICAgIHBhZ2luYXRpb25VdGlsOiBhbnk7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBpbmplY3RvcjogSW5qZWN0b3IpIHtcclxuICAgICAgICB0aGlzLnBhZ2luYXRpb25VdGlsID0gdGhpcy5pbmplY3Rvci5nZXQoSmhpUGFnaW5hdGlvblV0aWwpO1xyXG4gICAgfVxyXG5cclxuICAgIHJlc29sdmUocm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIHN0YXRlOiBSb3V0ZXJTdGF0ZVNuYXBzaG90KSB7XHJcbiAgICAgICAgY29uc3QgcGFnZSA9IHJvdXRlLnF1ZXJ5UGFyYW1zWydwYWdlJ10gPyByb3V0ZS5xdWVyeVBhcmFtc1sncGFnZSddIDogJzEnO1xyXG4gICAgICAgIGNvbnN0IHF1ZXJ5ID0gcm91dGUucXVlcnlQYXJhbXNbJ3F1ZXJ5J10gPyByb3V0ZS5xdWVyeVBhcmFtc1sncXVlcnknXSA6ICcnO1xyXG4gICAgICAgIGNvbnN0IGZpbHRlciA9IHJvdXRlLnF1ZXJ5UGFyYW1zWydmaWx0ZXInXSA/IHJvdXRlLnF1ZXJ5UGFyYW1zWydmaWx0ZXInXSA6ICcnO1xyXG4gICAgICAgIGNvbnN0IHNvcnQgPSByb3V0ZS5xdWVyeVBhcmFtc1snc29ydCddID8gcm91dGUucXVlcnlQYXJhbXNbJ3NvcnQnXSA6ICdpZCxhc2MnO1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHBhZ2U6ICtwYWdlLFxyXG4gICAgICAgICAgICBxdWVyeTogcXVlcnksXHJcbiAgICAgICAgICAgIGZpbHRlcjogZmlsdGVyLFxyXG4gICAgICAgICAgICBwcmVkaWNhdGU6IHRoaXMucGFnaW5hdGlvblV0aWwucGFyc2VQcmVkaWNhdGUoc29ydCksXHJcbiAgICAgICAgICAgIGFzY2VuZGluZzogdGhpcy5wYWdpbmF0aW9uVXRpbC5wYXJzZUFzY2VuZGluZyhzb3J0KVxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcbn1cclxuIl19