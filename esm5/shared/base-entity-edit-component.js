import * as tslib_1 from "tslib";
import { ViewChild } from '@angular/core';
import { MatButton, MatProgressBar } from '@angular/material';
var BaseEntityEditComponent = /** @class */ (function () {
    function BaseEntityEditComponent(notification, route) {
        this.notification = notification;
        this.route = route;
        this.error = false;
    }
    BaseEntityEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.isSaving = false;
        this.route.data.subscribe(function (_a) {
            var entity = _a.entity;
            _this.entity = !!entity && entity.body ? entity.body : entity;
        });
        if (this.entity === undefined) {
            this.entity = this.createEntity();
        }
    };
    BaseEntityEditComponent.prototype.previousState = function () {
        window.history.back();
    };
    BaseEntityEditComponent.prototype.save = function (valid) {
        this.submitButton.disabled = true;
        this.progressBar.mode = 'indeterminate';
        this.isSaving = true;
        if (this.entity.id !== undefined) {
            this.subscribeToSaveResponse(this.getService().update(this.entity));
        }
        else {
            this.subscribeToSaveResponse(this.getService().create(this.entity));
        }
    };
    BaseEntityEditComponent.prototype.subscribeToSaveResponse = function (result) {
        var _this = this;
        result.subscribe(function (res) { return _this.onSaveSuccess(res.body); }, function (res) {
            _this.onSaveError();
            _this.onError(res.message);
        });
    };
    BaseEntityEditComponent.prototype.onSaveSuccess = function (result) {
        this.isSaving = false;
        this.previousState();
    };
    BaseEntityEditComponent.prototype.onSaveError = function () {
        this.isSaving = false;
        this.error = true;
        this.submitButton.disabled = true;
        this.progressBar.mode = 'determinate';
    };
    BaseEntityEditComponent.prototype.onError = function (errorMessage) {
        this.notification.openSnackMessage(errorMessage);
    };
    BaseEntityEditComponent.prototype.entityCompare = function (s1, s2) {
        return s1 && s2 ? s1.id == s2.id : s1 === s2;
    };
    tslib_1.__decorate([
        ViewChild(MatProgressBar, { static: true })
    ], BaseEntityEditComponent.prototype, "progressBar", void 0);
    tslib_1.__decorate([
        ViewChild(MatButton, { static: true })
    ], BaseEntityEditComponent.prototype, "submitButton", void 0);
    return BaseEntityEditComponent;
}());
export { BaseEntityEditComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS1lbnRpdHktZWRpdC1jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvYmFzZS1lbnRpdHktZWRpdC1jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUVBLE9BQU8sRUFBVSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbEQsT0FBTyxFQUFFLFNBQVMsRUFBRSxjQUFjLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQU05RDtJQU9JLGlDQUFzQixZQUFpQyxFQUNqQyxLQUFxQjtRQURyQixpQkFBWSxHQUFaLFlBQVksQ0FBcUI7UUFDakMsVUFBSyxHQUFMLEtBQUssQ0FBZ0I7UUFIM0MsVUFBSyxHQUFHLEtBQUssQ0FBQztJQUtkLENBQUM7SUFFRCwwQ0FBUSxHQUFSO1FBQUEsaUJBUUM7UUFQRyxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztRQUN0QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBQyxFQUFRO2dCQUFQLGtCQUFNO1lBQzlCLEtBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLE1BQU0sSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7UUFDakUsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssU0FBUyxFQUFFO1lBQzNCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1NBQ3JDO0lBQ0wsQ0FBQztJQUVELCtDQUFhLEdBQWI7UUFDSSxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzFCLENBQUM7SUFFRCxzQ0FBSSxHQUFKLFVBQUssS0FBVztRQUNaLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUNsQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksR0FBRyxlQUFlLENBQUM7UUFDeEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDckIsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsS0FBSyxTQUFTLEVBQUU7WUFDOUIsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7U0FDdkU7YUFBTTtZQUNILElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1NBQ3ZFO0lBQ0wsQ0FBQztJQUVPLHlEQUF1QixHQUEvQixVQUFnQyxNQUFxQztRQUFyRSxpQkFPQztRQU5HLE1BQU0sQ0FBQyxTQUFTLENBQ1osVUFBQyxHQUFzQixJQUFLLE9BQUEsS0FBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQTVCLENBQTRCLEVBQ3hELFVBQUMsR0FBc0I7WUFDbkIsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ25CLEtBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFBO1FBQzdCLENBQUMsQ0FBQyxDQUFDO0lBQ1gsQ0FBQztJQUVPLCtDQUFhLEdBQXJCLFVBQXNCLE1BQVc7UUFDN0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3pCLENBQUM7SUFFTyw2Q0FBVyxHQUFuQjtRQUNJLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1FBQ2xCLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUNsQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksR0FBRyxhQUFhLENBQUM7SUFDMUMsQ0FBQztJQUVTLHlDQUFPLEdBQWpCLFVBQWtCLFlBQW9CO1FBQ2xDLElBQUksQ0FBQyxZQUFZLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDckQsQ0FBQztJQUVELCtDQUFhLEdBQWIsVUFBYyxFQUFPLEVBQUUsRUFBTztRQUMxQixPQUFPLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxLQUFLLEVBQUUsQ0FBQztJQUNqRCxDQUFDO0lBL0QwQztRQUExQyxTQUFTLENBQUMsY0FBYyxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQyxDQUFDO2dFQUE2QjtJQUNqQztRQUFyQyxTQUFTLENBQUMsU0FBUyxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQyxDQUFDO2lFQUF5QjtJQW1FbEUsOEJBQUM7Q0FBQSxBQXJFRCxJQXFFQztTQXJFcUIsdUJBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJ0BhbGZyZXNjby9hZGYtY29yZSc7XHJcbmltcG9ydCB7IEh0dHBFcnJvclJlc3BvbnNlLCBIdHRwUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IE9uSW5pdCwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdEJ1dHRvbiwgTWF0UHJvZ3Jlc3NCYXIgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBJQmFzZUVudGl0eSB9IGZyb20gJy4uL3NoYXJlZC9tb2RlbC9iYXNlLWVudGl0eSc7XHJcblxyXG5cclxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEJhc2VFbnRpdHlFZGl0Q29tcG9uZW50PFQgZXh0ZW5kcyBJQmFzZUVudGl0eT4gaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgQFZpZXdDaGlsZChNYXRQcm9ncmVzc0Jhciwge3N0YXRpYzogdHJ1ZX0pIHByb2dyZXNzQmFyOiBNYXRQcm9ncmVzc0JhcjtcclxuICAgIEBWaWV3Q2hpbGQoTWF0QnV0dG9uLCB7c3RhdGljOiB0cnVlfSkgc3VibWl0QnV0dG9uOiBNYXRCdXR0b247XHJcbiAgICBlbnRpdHk6IFQ7XHJcbiAgICBpc1NhdmluZzogYm9vbGVhbjtcclxuICAgIGVycm9yID0gZmFsc2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJvdGVjdGVkIG5vdGlmaWNhdGlvbjogTm90aWZpY2F0aW9uU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByb3RlY3RlZCByb3V0ZTogQWN0aXZhdGVkUm91dGUpIHtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5pc1NhdmluZyA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMucm91dGUuZGF0YS5zdWJzY3JpYmUoKHtlbnRpdHl9KSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZW50aXR5ID0gISFlbnRpdHkgJiYgZW50aXR5LmJvZHkgPyBlbnRpdHkuYm9keSA6IGVudGl0eTtcclxuICAgICAgICB9KTtcclxuICAgICAgICBpZiAodGhpcy5lbnRpdHkgPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICB0aGlzLmVudGl0eSA9IHRoaXMuY3JlYXRlRW50aXR5KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByZXZpb3VzU3RhdGUoKSB7XHJcbiAgICAgICAgd2luZG93Lmhpc3RvcnkuYmFjaygpO1xyXG4gICAgfVxyXG5cclxuICAgIHNhdmUodmFsaWQ/OiBhbnkpIHtcclxuICAgICAgICB0aGlzLnN1Ym1pdEJ1dHRvbi5kaXNhYmxlZCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5wcm9ncmVzc0Jhci5tb2RlID0gJ2luZGV0ZXJtaW5hdGUnO1xyXG4gICAgICAgIHRoaXMuaXNTYXZpbmcgPSB0cnVlO1xyXG4gICAgICAgIGlmICh0aGlzLmVudGl0eS5pZCAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc3Vic2NyaWJlVG9TYXZlUmVzcG9uc2UodGhpcy5nZXRTZXJ2aWNlKCkudXBkYXRlKHRoaXMuZW50aXR5KSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5zdWJzY3JpYmVUb1NhdmVSZXNwb25zZSh0aGlzLmdldFNlcnZpY2UoKS5jcmVhdGUodGhpcy5lbnRpdHkpKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBzdWJzY3JpYmVUb1NhdmVSZXNwb25zZShyZXN1bHQ6IE9ic2VydmFibGU8SHR0cFJlc3BvbnNlPGFueT4+KSB7XHJcbiAgICAgICAgcmVzdWx0LnN1YnNjcmliZShcclxuICAgICAgICAgICAgKHJlczogSHR0cFJlc3BvbnNlPGFueT4pID0+IHRoaXMub25TYXZlU3VjY2VzcyhyZXMuYm9keSksXHJcbiAgICAgICAgICAgIChyZXM6IEh0dHBFcnJvclJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9uU2F2ZUVycm9yKCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9uRXJyb3IocmVzLm1lc3NhZ2UpXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25TYXZlU3VjY2VzcyhyZXN1bHQ6IGFueSkge1xyXG4gICAgICAgIHRoaXMuaXNTYXZpbmcgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLnByZXZpb3VzU3RhdGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG9uU2F2ZUVycm9yKCkge1xyXG4gICAgICAgIHRoaXMuaXNTYXZpbmcgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmVycm9yID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLnN1Ym1pdEJ1dHRvbi5kaXNhYmxlZCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5wcm9ncmVzc0Jhci5tb2RlID0gJ2RldGVybWluYXRlJztcclxuICAgIH1cclxuXHJcbiAgICBwcm90ZWN0ZWQgb25FcnJvcihlcnJvck1lc3NhZ2U6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uLm9wZW5TbmFja01lc3NhZ2UoZXJyb3JNZXNzYWdlKTtcclxuICAgIH1cclxuXHJcbiAgICBlbnRpdHlDb21wYXJlKHMxOiBhbnksIHMyOiBhbnkpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gczEgJiYgczIgPyBzMS5pZCA9PSBzMi5pZCA6IHMxID09PSBzMjtcclxuICAgIH1cclxuXHJcbiAgICBhYnN0cmFjdCBnZXRTZXJ2aWNlKCk6IGFueTtcclxuXHJcbiAgICBhYnN0cmFjdCBjcmVhdGVFbnRpdHkoKTogVDtcclxufVxyXG4iXX0=