import * as tslib_1 from "tslib";
import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule, MatDialogModule, MatIconModule, MatProgressSpinnerModule, MatTooltipModule } from '@angular/material';
import { RouterModule } from '@angular/router';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
import { SharedCommonModule } from './shared-common.module';
import { AppConfirmComponent } from './util/app-confirm/app-confirm.component';
import { AppLoaderComponent } from './util/app-loader/app-loader.component';
import { AppLoaderService } from './util/app-loader/app-loader.service';
import { ToggleFullscreenDirective } from './util/fullscreen/toggle.fullscreen.directive';
import { SpeedDialFabComponent } from './util/speed-dial-fab.component';
var LamisSharedModule = /** @class */ (function () {
    function LamisSharedModule() {
    }
    LamisSharedModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                ReactiveFormsModule,
                RouterModule,
                SharedCommonModule,
                MatIconModule,
                MatButtonModule,
                MatTooltipModule,
                MatProgressSpinnerModule,
                MatDialogModule
            ],
            declarations: [
                HasAnyAuthorityDirective,
                SpeedDialFabComponent,
                AppConfirmComponent,
                AppLoaderComponent,
                ToggleFullscreenDirective
            ],
            entryComponents: [
                AppConfirmComponent,
                AppLoaderComponent
            ],
            exports: [
                SharedCommonModule,
                HasAnyAuthorityDirective,
                SpeedDialFabComponent,
                ToggleFullscreenDirective
            ],
            providers: [
                AppLoaderService,
                AppConfirmComponent
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA]
        })
    ], LamisSharedModule);
    return LamisSharedModule;
}());
export { LamisSharedModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGFtaXMtc2hhcmVkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC9sYW1pcy1zaGFyZWQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLHNCQUFzQixFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNqRSxPQUFPLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFDbEUsT0FBTyxFQUFFLGVBQWUsRUFBRSxlQUFlLEVBQUUsYUFBYSxFQUFFLHdCQUF3QixFQUFFLGdCQUFnQixFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDaEksT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzVELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQzVFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQzFGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBc0N4RTtJQUFBO0lBQ0EsQ0FBQztJQURZLGlCQUFpQjtRQXBDN0IsUUFBUSxDQUFDO1lBQ04sT0FBTyxFQUFFO2dCQUNMLFlBQVk7Z0JBQ1osV0FBVztnQkFDWCxtQkFBbUI7Z0JBQ25CLFlBQVk7Z0JBQ1osa0JBQWtCO2dCQUNsQixhQUFhO2dCQUNiLGVBQWU7Z0JBQ2YsZ0JBQWdCO2dCQUNoQix3QkFBd0I7Z0JBQ3hCLGVBQWU7YUFDbEI7WUFDRCxZQUFZLEVBQUU7Z0JBQ1Ysd0JBQXdCO2dCQUN4QixxQkFBcUI7Z0JBQ3JCLG1CQUFtQjtnQkFDbkIsa0JBQWtCO2dCQUNsQix5QkFBeUI7YUFDNUI7WUFDRCxlQUFlLEVBQUU7Z0JBQ2IsbUJBQW1CO2dCQUNuQixrQkFBa0I7YUFDckI7WUFDRCxPQUFPLEVBQUU7Z0JBQ0wsa0JBQWtCO2dCQUNsQix3QkFBd0I7Z0JBQ3hCLHFCQUFxQjtnQkFDckIseUJBQXlCO2FBQzVCO1lBQ0QsU0FBUyxFQUFFO2dCQUNQLGdCQUFnQjtnQkFDaEIsbUJBQW1CO2FBQ3RCO1lBQ0QsT0FBTyxFQUFFLENBQUMsc0JBQXNCLENBQUM7U0FDcEMsQ0FBQztPQUNXLGlCQUFpQixDQUM3QjtJQUFELHdCQUFDO0NBQUEsQUFERCxJQUNDO1NBRFksaUJBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgQ1VTVE9NX0VMRU1FTlRTX1NDSEVNQSwgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IE1hdEJ1dHRvbk1vZHVsZSwgTWF0RGlhbG9nTW9kdWxlLCBNYXRJY29uTW9kdWxlLCBNYXRQcm9ncmVzc1NwaW5uZXJNb2R1bGUsIE1hdFRvb2x0aXBNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IFJvdXRlck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IEhhc0FueUF1dGhvcml0eURpcmVjdGl2ZSB9IGZyb20gJy4vYXV0aC9oYXMtYW55LWF1dGhvcml0eS5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBTaGFyZWRDb21tb25Nb2R1bGUgfSBmcm9tICcuL3NoYXJlZC1jb21tb24ubW9kdWxlJztcclxuaW1wb3J0IHsgQXBwQ29uZmlybUNvbXBvbmVudCB9IGZyb20gJy4vdXRpbC9hcHAtY29uZmlybS9hcHAtY29uZmlybS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBBcHBMb2FkZXJDb21wb25lbnQgfSBmcm9tICcuL3V0aWwvYXBwLWxvYWRlci9hcHAtbG9hZGVyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEFwcExvYWRlclNlcnZpY2UgfSBmcm9tICcuL3V0aWwvYXBwLWxvYWRlci9hcHAtbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUb2dnbGVGdWxsc2NyZWVuRGlyZWN0aXZlIH0gZnJvbSAnLi91dGlsL2Z1bGxzY3JlZW4vdG9nZ2xlLmZ1bGxzY3JlZW4uZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgU3BlZWREaWFsRmFiQ29tcG9uZW50IH0gZnJvbSAnLi91dGlsL3NwZWVkLWRpYWwtZmFiLmNvbXBvbmVudCc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgaW1wb3J0czogW1xyXG4gICAgICAgIENvbW1vbk1vZHVsZSxcclxuICAgICAgICBGb3Jtc01vZHVsZSxcclxuICAgICAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxyXG4gICAgICAgIFJvdXRlck1vZHVsZSxcclxuICAgICAgICBTaGFyZWRDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgTWF0SWNvbk1vZHVsZSxcclxuICAgICAgICBNYXRCdXR0b25Nb2R1bGUsXHJcbiAgICAgICAgTWF0VG9vbHRpcE1vZHVsZSxcclxuICAgICAgICBNYXRQcm9ncmVzc1NwaW5uZXJNb2R1bGUsXHJcbiAgICAgICAgTWF0RGlhbG9nTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgSGFzQW55QXV0aG9yaXR5RGlyZWN0aXZlLFxyXG4gICAgICAgIFNwZWVkRGlhbEZhYkNvbXBvbmVudCxcclxuICAgICAgICBBcHBDb25maXJtQ29tcG9uZW50LFxyXG4gICAgICAgIEFwcExvYWRlckNvbXBvbmVudCxcclxuICAgICAgICBUb2dnbGVGdWxsc2NyZWVuRGlyZWN0aXZlXHJcbiAgICBdLFxyXG4gICAgZW50cnlDb21wb25lbnRzOiBbXHJcbiAgICAgICAgQXBwQ29uZmlybUNvbXBvbmVudCxcclxuICAgICAgICBBcHBMb2FkZXJDb21wb25lbnRcclxuICAgIF0sXHJcbiAgICBleHBvcnRzOiBbXHJcbiAgICAgICAgU2hhcmVkQ29tbW9uTW9kdWxlLFxyXG4gICAgICAgIEhhc0FueUF1dGhvcml0eURpcmVjdGl2ZSxcclxuICAgICAgICBTcGVlZERpYWxGYWJDb21wb25lbnQsXHJcbiAgICAgICAgVG9nZ2xlRnVsbHNjcmVlbkRpcmVjdGl2ZVxyXG4gICAgXSxcclxuICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgIEFwcExvYWRlclNlcnZpY2UsXHJcbiAgICAgICAgQXBwQ29uZmlybUNvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIHNjaGVtYXM6IFtDVVNUT01fRUxFTUVOVFNfU0NIRU1BXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTGFtaXNTaGFyZWRNb2R1bGUge1xyXG59XHJcbiJdfQ==