import * as tslib_1 from "tslib";
import { CardViewModule } from '@alfresco/adf-core';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatFormioModule } from 'angular-material-formio';
import { Formio } from 'formiojs';
import { LocalStorageService, SessionStorageService } from 'ngx-store';
import { isObject } from 'util';
import { DetailsComponent } from './component/details.component';
import { JsonFormComponent } from './json-form.component';
var JsonFormModule = /** @class */ (function () {
    function JsonFormModule(localStorage, sessionStorage) {
        this.localStorage = localStorage;
        this.sessionStorage = sessionStorage;
        var token = this.localStorage.get('authenticationToken') || this.sessionStorage.get('authenticationToken');
        Formio.getRequestArgs = function (formio, type, url, method, data, opts) {
            method = (method || 'GET').toUpperCase();
            if (!opts || !isObject(opts)) {
                opts = {};
            }
            opts['Authorization'] = token;
            var requestArgs = {
                url: url,
                method: method,
                data: data || null,
                opts: opts
            };
            if (type) {
                requestArgs['type'] = type;
            }
            if (formio) {
                requestArgs['formio'] = formio;
            }
            return requestArgs;
        };
    }
    JsonFormModule.ctorParameters = function () { return [
        { type: LocalStorageService },
        { type: SessionStorageService }
    ]; };
    JsonFormModule = tslib_1.__decorate([
        NgModule({
            imports: [MatFormioModule, CommonModule, CardViewModule],
            declarations: [JsonFormComponent, DetailsComponent],
            exports: [JsonFormComponent, DetailsComponent, MatFormioModule],
            entryComponents: [JsonFormComponent]
        })
    ], JsonFormModule);
    return JsonFormModule;
}());
export { JsonFormModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoianNvbi1mb3JtLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC9qc29uLWZvcm0vanNvbi1mb3JtLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0sVUFBVSxDQUFDO0FBQ2xDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxxQkFBcUIsRUFBRSxNQUFNLFdBQVcsQ0FBQztBQUN2RSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2hDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQ2pFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBUTFEO0lBQ0ksd0JBQW9CLFlBQWlDLEVBQVUsY0FBcUM7UUFBaEYsaUJBQVksR0FBWixZQUFZLENBQXFCO1FBQVUsbUJBQWMsR0FBZCxjQUFjLENBQXVCO1FBQ2hHLElBQU0sS0FBSyxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUM3RyxNQUFNLENBQUMsY0FBYyxHQUFHLFVBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxJQUFJO1lBQzFELE1BQU0sR0FBRyxDQUFDLE1BQU0sSUFBSSxLQUFLLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUN6QyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUMxQixJQUFJLEdBQUcsRUFBRSxDQUFDO2FBQ2I7WUFDRCxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsS0FBSyxDQUFDO1lBQzlCLElBQU0sV0FBVyxHQUFHO2dCQUNoQixHQUFHLEtBQUE7Z0JBQ0gsTUFBTSxRQUFBO2dCQUNOLElBQUksRUFBRSxJQUFJLElBQUksSUFBSTtnQkFDbEIsSUFBSSxNQUFBO2FBQ1AsQ0FBQztZQUVGLElBQUksSUFBSSxFQUFFO2dCQUNOLFdBQVcsQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUM7YUFDOUI7WUFFRCxJQUFJLE1BQU0sRUFBRTtnQkFDUixXQUFXLENBQUMsUUFBUSxDQUFDLEdBQUcsTUFBTSxDQUFDO2FBQ2xDO1lBQ0QsT0FBTyxXQUFXLENBQUM7UUFDdkIsQ0FBQyxDQUFDO0lBQ04sQ0FBQzs7Z0JBeEJpQyxtQkFBbUI7Z0JBQTBCLHFCQUFxQjs7SUFEM0YsY0FBYztRQU4xQixRQUFRLENBQUM7WUFDTixPQUFPLEVBQUUsQ0FBQyxlQUFlLEVBQUUsWUFBWSxFQUFFLGNBQWMsQ0FBQztZQUN4RCxZQUFZLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRSxnQkFBZ0IsQ0FBQztZQUNuRCxPQUFPLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRSxnQkFBZ0IsRUFBRSxlQUFlLENBQUM7WUFDL0QsZUFBZSxFQUFFLENBQUMsaUJBQWlCLENBQUM7U0FDdkMsQ0FBQztPQUNXLGNBQWMsQ0EwQjFCO0lBQUQscUJBQUM7Q0FBQSxBQTFCRCxJQTBCQztTQTFCWSxjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ2FyZFZpZXdNb2R1bGUgfSBmcm9tICdAYWxmcmVzY28vYWRmLWNvcmUnO1xyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXRGb3JtaW9Nb2R1bGUgfSBmcm9tICdhbmd1bGFyLW1hdGVyaWFsLWZvcm1pbyc7XHJcbmltcG9ydCB7IEZvcm1pbyB9IGZyb20gJ2Zvcm1pb2pzJztcclxuaW1wb3J0IHsgTG9jYWxTdG9yYWdlU2VydmljZSwgU2Vzc2lvblN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnbmd4LXN0b3JlJztcclxuaW1wb3J0IHsgaXNPYmplY3QgfSBmcm9tICd1dGlsJztcclxuaW1wb3J0IHsgRGV0YWlsc0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50L2RldGFpbHMuY29tcG9uZW50JztcclxuaW1wb3J0IHsgSnNvbkZvcm1Db21wb25lbnQgfSBmcm9tICcuL2pzb24tZm9ybS5jb21wb25lbnQnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtNYXRGb3JtaW9Nb2R1bGUsIENvbW1vbk1vZHVsZSwgQ2FyZFZpZXdNb2R1bGVdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbSnNvbkZvcm1Db21wb25lbnQsIERldGFpbHNDb21wb25lbnRdLFxyXG4gICAgZXhwb3J0czogW0pzb25Gb3JtQ29tcG9uZW50LCBEZXRhaWxzQ29tcG9uZW50LCBNYXRGb3JtaW9Nb2R1bGVdLFxyXG4gICAgZW50cnlDb21wb25lbnRzOiBbSnNvbkZvcm1Db21wb25lbnRdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBKc29uRm9ybU1vZHVsZSB7XHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGxvY2FsU3RvcmFnZTogTG9jYWxTdG9yYWdlU2VydmljZSwgcHJpdmF0ZSBzZXNzaW9uU3RvcmFnZTogU2Vzc2lvblN0b3JhZ2VTZXJ2aWNlKSB7XHJcbiAgICAgICAgY29uc3QgdG9rZW4gPSB0aGlzLmxvY2FsU3RvcmFnZS5nZXQoJ2F1dGhlbnRpY2F0aW9uVG9rZW4nKSB8fCB0aGlzLnNlc3Npb25TdG9yYWdlLmdldCgnYXV0aGVudGljYXRpb25Ub2tlbicpO1xyXG4gICAgICAgIEZvcm1pby5nZXRSZXF1ZXN0QXJncyA9IChmb3JtaW8sIHR5cGUsIHVybCwgbWV0aG9kLCBkYXRhLCBvcHRzKSA9PiB7XHJcbiAgICAgICAgICAgIG1ldGhvZCA9IChtZXRob2QgfHwgJ0dFVCcpLnRvVXBwZXJDYXNlKCk7XHJcbiAgICAgICAgICAgIGlmICghb3B0cyB8fCAhaXNPYmplY3Qob3B0cykpIHtcclxuICAgICAgICAgICAgICAgIG9wdHMgPSB7fTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBvcHRzWydBdXRob3JpemF0aW9uJ10gPSB0b2tlbjtcclxuICAgICAgICAgICAgY29uc3QgcmVxdWVzdEFyZ3MgPSB7XHJcbiAgICAgICAgICAgICAgICB1cmwsXHJcbiAgICAgICAgICAgICAgICBtZXRob2QsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiBkYXRhIHx8IG51bGwsXHJcbiAgICAgICAgICAgICAgICBvcHRzXHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICBpZiAodHlwZSkge1xyXG4gICAgICAgICAgICAgICAgcmVxdWVzdEFyZ3NbJ3R5cGUnXSA9IHR5cGU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChmb3JtaW8pIHtcclxuICAgICAgICAgICAgICAgIHJlcXVlc3RBcmdzWydmb3JtaW8nXSA9IGZvcm1pbztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gcmVxdWVzdEFyZ3M7XHJcbiAgICAgICAgfTtcclxuICAgIH1cclxufVxyXG4iXX0=