import * as tslib_1 from "tslib";
import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { FormioComponent } from 'angular-material-formio';
import { LocalStorageService, SessionStorageService } from 'ngx-store';
import { LayoutTemplateService } from '../../services/layout.template.service';
var JsonFormComponent = /** @class */ (function () {
    function JsonFormComponent(templateService, localStorage, sessionStorage) {
        this.templateService = templateService;
        this.localStorage = localStorage;
        this.sessionStorage = sessionStorage;
        this.dataEvent = new EventEmitter(true);
        this.customEvent = new EventEmitter(true);
        this.form = {};
        this.isValid = false;
    }
    JsonFormComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.templateId) {
            this.templateService.getTemplate(this.templateId).subscribe(function (json) {
                _this.form = json.form;
            });
        }
        else {
            this.form = this.template;
        }
    };
    JsonFormComponent.prototype.reset = function () {
        this.formio.onRefresh({
            submission: this.model
        });
    };
    JsonFormComponent.prototype.onCustomEvent = function (event) {
        this.customEvent.emit(event);
    };
    JsonFormComponent.prototype.change = function (event) {
        if (event.hasOwnProperty('isValid')) {
            this.isValid = event.isValid;
            if (this.isValid) {
                this.dataEvent.emit(event.data);
            }
        }
    };
    JsonFormComponent.prototype.ngOnChanges = function (changes) {
        if (changes['model']) {
            var token = this.localStorage.get('authenticationToken') || this.sessionStorage.get('authenticationToken');
            this.model = {
                data: Object.assign({}, changes['model'].currentValue, { authorization: token })
            };
        }
    };
    JsonFormComponent.ctorParameters = function () { return [
        { type: LayoutTemplateService },
        { type: LocalStorageService },
        { type: SessionStorageService }
    ]; };
    tslib_1.__decorate([
        ViewChild(FormioComponent, { static: true })
    ], JsonFormComponent.prototype, "formio", void 0);
    tslib_1.__decorate([
        Input()
    ], JsonFormComponent.prototype, "template", void 0);
    tslib_1.__decorate([
        Input()
    ], JsonFormComponent.prototype, "templateId", void 0);
    tslib_1.__decorate([
        Input()
    ], JsonFormComponent.prototype, "model", void 0);
    tslib_1.__decorate([
        Output()
    ], JsonFormComponent.prototype, "dataEvent", void 0);
    JsonFormComponent = tslib_1.__decorate([
        Component({
            selector: 'json-form',
            template: "\n        <mat-formio [form]=\"form\"\n                    (ready)=\"reset()\"\n                    (customEvent)=\"onCustomEvent($event)\"\n                    (change)=\"change($event)\">\n        </mat-formio>\n    "
        })
    ], JsonFormComponent);
    return JsonFormComponent;
}());
export { JsonFormComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoianNvbi1mb3JtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNoYXJlZC9qc29uLWZvcm0vanNvbi1mb3JtLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFxQixNQUFNLEVBQWlCLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNwSCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFFMUQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLHFCQUFxQixFQUFFLE1BQU0sV0FBVyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBWS9FO0lBZ0JJLDJCQUFvQixlQUFzQyxFQUFVLFlBQWlDLEVBQ2pGLGNBQXFDO1FBRHJDLG9CQUFlLEdBQWYsZUFBZSxDQUF1QjtRQUFVLGlCQUFZLEdBQVosWUFBWSxDQUFxQjtRQUNqRixtQkFBYyxHQUFkLGNBQWMsQ0FBdUI7UUFOekQsY0FBUyxHQUFzQixJQUFJLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0RCxnQkFBVyxHQUFzQixJQUFJLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4RCxTQUFJLEdBQVEsRUFBRSxDQUFDO1FBQ2YsWUFBTyxHQUFHLEtBQUssQ0FBQztJQUloQixDQUFDO0lBRUQsb0NBQVEsR0FBUjtRQUFBLGlCQVFDO1FBUEcsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQyxJQUFTO2dCQUNsRSxLQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDMUIsQ0FBQyxDQUFDLENBQUM7U0FDTjthQUFNO1lBQ0gsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1NBQzdCO0lBQ0wsQ0FBQztJQUVELGlDQUFLLEdBQUw7UUFDSSxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQztZQUNsQixVQUFVLEVBQUUsSUFBSSxDQUFDLEtBQUs7U0FDekIsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELHlDQUFhLEdBQWIsVUFBYyxLQUFVO1FBQ3BCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2pDLENBQUM7SUFFRCxrQ0FBTSxHQUFOLFVBQU8sS0FBVTtRQUNiLElBQUksS0FBSyxDQUFDLGNBQWMsQ0FBQyxTQUFTLENBQUMsRUFBRTtZQUNqQyxJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUM7WUFDN0IsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUNkLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUNuQztTQUNKO0lBQ0wsQ0FBQztJQUVELHVDQUFXLEdBQVgsVUFBWSxPQUFzQjtRQUM5QixJQUFJLE9BQU8sQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUNsQixJQUFNLEtBQUssR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFDN0csSUFBSSxDQUFDLEtBQUssR0FBRztnQkFDVCxJQUFJLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFlBQVksRUFBRSxFQUFDLGFBQWEsRUFBRSxLQUFLLEVBQUMsQ0FBQzthQUNqRixDQUFDO1NBQ0w7SUFDTCxDQUFDOztnQkF4Q29DLHFCQUFxQjtnQkFBd0IsbUJBQW1CO2dCQUNqRSxxQkFBcUI7O0lBZnpEO1FBREMsU0FBUyxDQUFDLGVBQWUsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUMsQ0FBQztxREFDbkI7SUFHeEI7UUFEQyxLQUFLLEVBQUU7dURBQ1M7SUFFakI7UUFEQyxLQUFLLEVBQUU7eURBQ1c7SUFFbkI7UUFEQyxLQUFLLEVBQUU7b0RBQ0c7SUFFWDtRQURDLE1BQU0sRUFBRTt3REFDNkM7SUFYN0MsaUJBQWlCO1FBVjdCLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxXQUFXO1lBQ3JCLFFBQVEsRUFBRSw0TkFNVDtTQUNKLENBQUM7T0FDVyxpQkFBaUIsQ0F5RDdCO0lBQUQsd0JBQUM7Q0FBQSxBQXpERCxJQXlEQztTQXpEWSxpQkFBaUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE9uQ2hhbmdlcywgT25Jbml0LCBPdXRwdXQsIFNpbXBsZUNoYW5nZXMsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtaW9Db21wb25lbnQgfSBmcm9tICdhbmd1bGFyLW1hdGVyaWFsLWZvcm1pbyc7XHJcbmltcG9ydCB7IEZvcm1pbyB9IGZyb20gJ2Zvcm1pb2pzJztcclxuaW1wb3J0IHsgTG9jYWxTdG9yYWdlU2VydmljZSwgU2Vzc2lvblN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnbmd4LXN0b3JlJztcclxuaW1wb3J0IHsgTGF5b3V0VGVtcGxhdGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvbGF5b3V0LnRlbXBsYXRlLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2pzb24tZm9ybScsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxtYXQtZm9ybWlvIFtmb3JtXT1cImZvcm1cIlxyXG4gICAgICAgICAgICAgICAgICAgIChyZWFkeSk9XCJyZXNldCgpXCJcclxuICAgICAgICAgICAgICAgICAgICAoY3VzdG9tRXZlbnQpPVwib25DdXN0b21FdmVudCgkZXZlbnQpXCJcclxuICAgICAgICAgICAgICAgICAgICAoY2hhbmdlKT1cImNoYW5nZSgkZXZlbnQpXCI+XHJcbiAgICAgICAgPC9tYXQtZm9ybWlvPlxyXG4gICAgYFxyXG59KVxyXG5leHBvcnQgY2xhc3MgSnNvbkZvcm1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcyB7XHJcbiAgICBAVmlld0NoaWxkKEZvcm1pb0NvbXBvbmVudCwge3N0YXRpYzogdHJ1ZX0pXHJcbiAgICBmb3JtaW86IEZvcm1pb0NvbXBvbmVudDtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgdGVtcGxhdGU6IHN0cmluZztcclxuICAgIEBJbnB1dCgpXHJcbiAgICB0ZW1wbGF0ZUlkOiBzdHJpbmc7XHJcbiAgICBASW5wdXQoKVxyXG4gICAgbW9kZWw6IGFueTtcclxuICAgIEBPdXRwdXQoKVxyXG4gICAgZGF0YUV2ZW50OiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXIodHJ1ZSk7XHJcbiAgICBjdXN0b21FdmVudDogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyKHRydWUpO1xyXG4gICAgZm9ybTogYW55ID0ge307XHJcbiAgICBpc1ZhbGlkID0gZmFsc2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSB0ZW1wbGF0ZVNlcnZpY2U6IExheW91dFRlbXBsYXRlU2VydmljZSwgcHJpdmF0ZSBsb2NhbFN0b3JhZ2U6IExvY2FsU3RvcmFnZVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHNlc3Npb25TdG9yYWdlOiBTZXNzaW9uU3RvcmFnZVNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgICAgICBpZiAodGhpcy50ZW1wbGF0ZUlkKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGVtcGxhdGVTZXJ2aWNlLmdldFRlbXBsYXRlKHRoaXMudGVtcGxhdGVJZCkuc3Vic2NyaWJlKChqc29uOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZm9ybSA9IGpzb24uZm9ybTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5mb3JtID0gdGhpcy50ZW1wbGF0ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmVzZXQoKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtaW8ub25SZWZyZXNoKHtcclxuICAgICAgICAgICAgc3VibWlzc2lvbjogdGhpcy5tb2RlbFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG9uQ3VzdG9tRXZlbnQoZXZlbnQ6IGFueSkge1xyXG4gICAgICAgIHRoaXMuY3VzdG9tRXZlbnQuZW1pdChldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgY2hhbmdlKGV2ZW50OiBhbnkpIHtcclxuICAgICAgICBpZiAoZXZlbnQuaGFzT3duUHJvcGVydHkoJ2lzVmFsaWQnKSkge1xyXG4gICAgICAgICAgICB0aGlzLmlzVmFsaWQgPSBldmVudC5pc1ZhbGlkO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5pc1ZhbGlkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRhdGFFdmVudC5lbWl0KGV2ZW50LmRhdGEpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpOiB2b2lkIHtcclxuICAgICAgICBpZiAoY2hhbmdlc1snbW9kZWwnXSkge1xyXG4gICAgICAgICAgICBjb25zdCB0b2tlbiA9IHRoaXMubG9jYWxTdG9yYWdlLmdldCgnYXV0aGVudGljYXRpb25Ub2tlbicpIHx8IHRoaXMuc2Vzc2lvblN0b3JhZ2UuZ2V0KCdhdXRoZW50aWNhdGlvblRva2VuJyk7XHJcbiAgICAgICAgICAgIHRoaXMubW9kZWwgPSB7XHJcbiAgICAgICAgICAgICAgICBkYXRhOiBPYmplY3QuYXNzaWduKHt9LCBjaGFuZ2VzWydtb2RlbCddLmN1cnJlbnRWYWx1ZSwge2F1dGhvcml6YXRpb246IHRva2VufSlcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19