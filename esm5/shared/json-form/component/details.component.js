import * as tslib_1 from "tslib";
import { Component, Input, ViewEncapsulation } from '@angular/core';
import get from 'lodash.get';
import { LayoutTemplateService } from '../../../services/layout.template.service';
import { CardViewBoolItemModel, CardViewDateItemModel, CardViewDatetimeItemModel, CardViewFloatItemModel, CardViewIntItemModel, CardViewTextItemModel } from '@alfresco/adf-core';
export var FieldType;
(function (FieldType) {
    FieldType["date"] = "date";
    FieldType["datetime"] = "datetime";
    FieldType["text"] = "text";
    FieldType["boolean"] = "boolean";
    FieldType["int"] = "int";
    FieldType["float"] = "float";
})(FieldType || (FieldType = {}));
var DetailsComponent = /** @class */ (function () {
    function DetailsComponent(templateService) {
        this.templateService = templateService;
    }
    DetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.templateService.getTemplate(this.template).subscribe(function (json) {
            _this.details = json.template;
        });
    };
    DetailsComponent.prototype.propertiesForDetail = function (detail) {
        var e_1, _a;
        var properties = [];
        try {
            for (var _b = tslib_1.__values(detail.fields), _c = _b.next(); !_c.done; _c = _b.next()) {
                var field = _c.value;
                var dataType = field.type;
                var item = void 0;
                switch (dataType) {
                    case FieldType.boolean:
                        item = new CardViewBoolItemModel({
                            value: this.getValueForKey(field.key),
                            key: '',
                            label: field.label
                        });
                        break;
                    case FieldType.int:
                        item = new CardViewIntItemModel({
                            value: this.getValueForKey(field.key),
                            key: '',
                            label: field.label,
                        });
                        break;
                    case FieldType.float:
                        item = new CardViewFloatItemModel({
                            value: this.getValueForKey(field.key),
                            key: '',
                            label: field.label,
                        });
                        break;
                    case FieldType.date:
                        item = new CardViewDateItemModel({
                            value: this.getValueForKey(field.key),
                            key: '',
                            label: field.label,
                            format: 'dd MMM, yyyy'
                        });
                        break;
                    case FieldType.datetime:
                        item = new CardViewDatetimeItemModel({
                            value: this.getValueForKey(field.key),
                            key: '',
                            label: field.label,
                            format: 'dd MMM, yyyy HH:mm'
                        });
                        break;
                    default:
                        item = new CardViewTextItemModel({
                            value: this.getValueForKey(field.key),
                            key: '',
                            label: field.label,
                        });
                }
                properties.push(item);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return properties;
    };
    DetailsComponent.prototype.getValueForKey = function (key) {
        return get(this.model, key);
    };
    DetailsComponent.ctorParameters = function () { return [
        { type: LayoutTemplateService }
    ]; };
    tslib_1.__decorate([
        Input()
    ], DetailsComponent.prototype, "template", void 0);
    tslib_1.__decorate([
        Input()
    ], DetailsComponent.prototype, "model", void 0);
    DetailsComponent = tslib_1.__decorate([
        Component({
            selector: 'details-component',
            template: "<ng-container *ngIf=\"model && details\">\r\n    <mat-card *ngFor=\"let detail of details\" class=\"default mb-1 pb-0\">\r\n        <ng-container *ngIf=\"!!detail.header\">\r\n            <mat-card-title>{{detail.header}}</mat-card-title>\r\n            <mat-divider></mat-divider>\r\n        </ng-container>\r\n        <mat-card-content>\r\n            <adf-card-view [properties]=\"propertiesForDetail(detail)\"></adf-card-view>\r\n        </mat-card-content>\r\n    </mat-card>\r\n</ng-container>\r\n",
            encapsulation: ViewEncapsulation.None,
            styles: [""]
        })
    ], DetailsComponent);
    return DetailsComponent;
}());
export { DetailsComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGV0YWlscy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvanNvbi1mb3JtL2NvbXBvbmVudC9kZXRhaWxzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQVUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDNUUsT0FBTyxHQUFHLE1BQU0sWUFBWSxDQUFDO0FBQzdCLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBQ2xGLE9BQU8sRUFDSCxxQkFBcUIsRUFDckIscUJBQXFCLEVBQ3JCLHlCQUF5QixFQUN6QixzQkFBc0IsRUFDdEIsb0JBQW9CLEVBRXBCLHFCQUFxQixFQUN4QixNQUFNLG9CQUFvQixDQUFDO0FBYzVCLE1BQU0sQ0FBTixJQUFZLFNBT1g7QUFQRCxXQUFZLFNBQVM7SUFDakIsMEJBQWEsQ0FBQTtJQUNiLGtDQUFxQixDQUFBO0lBQ3JCLDBCQUFhLENBQUE7SUFDYixnQ0FBbUIsQ0FBQTtJQUNuQix3QkFBVyxDQUFBO0lBQ1gsNEJBQWUsQ0FBQTtBQUNuQixDQUFDLEVBUFcsU0FBUyxLQUFULFNBQVMsUUFPcEI7QUFRRDtJQVNJLDBCQUFvQixlQUFzQztRQUF0QyxvQkFBZSxHQUFmLGVBQWUsQ0FBdUI7SUFDMUQsQ0FBQztJQUVELG1DQUFRLEdBQVI7UUFBQSxpQkFJQztRQUhHLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQyxJQUFTO1lBQ2hFLEtBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUNqQyxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTSw4Q0FBbUIsR0FBMUIsVUFBMkIsTUFBYzs7UUFDckMsSUFBTSxVQUFVLEdBQUcsRUFBRSxDQUFDOztZQUN0QixLQUFvQixJQUFBLEtBQUEsaUJBQUEsTUFBTSxDQUFDLE1BQU0sQ0FBQSxnQkFBQSw0QkFBRTtnQkFBOUIsSUFBTSxLQUFLLFdBQUE7Z0JBQ1osSUFBTSxRQUFRLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQztnQkFDNUIsSUFBSSxJQUFJLFNBQWMsQ0FBQztnQkFDdkIsUUFBUSxRQUFRLEVBQUU7b0JBQ2QsS0FBSyxTQUFTLENBQUMsT0FBTzt3QkFDbEIsSUFBSSxHQUFHLElBQUkscUJBQXFCLENBQUM7NEJBQzdCLEtBQUssRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7NEJBQ3JDLEdBQUcsRUFBRSxFQUFFOzRCQUNQLEtBQUssRUFBRSxLQUFLLENBQUMsS0FBSzt5QkFDckIsQ0FBQyxDQUFDO3dCQUNILE1BQU07b0JBQ1YsS0FBSyxTQUFTLENBQUMsR0FBRzt3QkFDZCxJQUFJLEdBQUcsSUFBSSxvQkFBb0IsQ0FBQzs0QkFDNUIsS0FBSyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQzs0QkFDckMsR0FBRyxFQUFFLEVBQUU7NEJBQ1AsS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLO3lCQUNyQixDQUFDLENBQUM7d0JBQ0gsTUFBTTtvQkFDVixLQUFLLFNBQVMsQ0FBQyxLQUFLO3dCQUNoQixJQUFJLEdBQUcsSUFBSSxzQkFBc0IsQ0FBQzs0QkFDOUIsS0FBSyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQzs0QkFDckMsR0FBRyxFQUFFLEVBQUU7NEJBQ1AsS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLO3lCQUNyQixDQUFDLENBQUM7d0JBQ0gsTUFBTTtvQkFDVixLQUFLLFNBQVMsQ0FBQyxJQUFJO3dCQUNmLElBQUksR0FBRyxJQUFJLHFCQUFxQixDQUFDOzRCQUM3QixLQUFLLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDOzRCQUNyQyxHQUFHLEVBQUUsRUFBRTs0QkFDUCxLQUFLLEVBQUUsS0FBSyxDQUFDLEtBQUs7NEJBQ2xCLE1BQU0sRUFBRSxjQUFjO3lCQUN6QixDQUFDLENBQUM7d0JBQ0gsTUFBTTtvQkFDVixLQUFLLFNBQVMsQ0FBQyxRQUFRO3dCQUNuQixJQUFJLEdBQUcsSUFBSSx5QkFBeUIsQ0FBQzs0QkFDakMsS0FBSyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQzs0QkFDckMsR0FBRyxFQUFFLEVBQUU7NEJBQ1AsS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLOzRCQUNsQixNQUFNLEVBQUUsb0JBQW9CO3lCQUMvQixDQUFDLENBQUM7d0JBQ0gsTUFBTTtvQkFDVjt3QkFDSSxJQUFJLEdBQUcsSUFBSSxxQkFBcUIsQ0FBQzs0QkFDN0IsS0FBSyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQzs0QkFDckMsR0FBRyxFQUFFLEVBQUU7NEJBQ1AsS0FBSyxFQUFFLEtBQUssQ0FBQyxLQUFLO3lCQUNyQixDQUFDLENBQUM7aUJBQ1Y7Z0JBQ0QsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUN6Qjs7Ozs7Ozs7O1FBQ0QsT0FBTyxVQUFVLENBQUM7SUFDdEIsQ0FBQztJQUVPLHlDQUFjLEdBQXRCLFVBQXVCLEdBQVc7UUFDOUIsT0FBTyxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNoQyxDQUFDOztnQkFsRW9DLHFCQUFxQjs7SUFQMUQ7UUFEQyxLQUFLLEVBQUU7c0RBQ1M7SUFHakI7UUFEQyxLQUFLLEVBQUU7bURBQ0c7SUFMRixnQkFBZ0I7UUFONUIsU0FBUyxDQUFDO1lBQ1AsUUFBUSxFQUFFLG1CQUFtQjtZQUM3QixtZ0JBQXVDO1lBRXZDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOztTQUN4QyxDQUFDO09BQ1csZ0JBQWdCLENBNEU1QjtJQUFELHVCQUFDO0NBQUEsQUE1RUQsSUE0RUM7U0E1RVksZ0JBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgZ2V0IGZyb20gJ2xvZGFzaC5nZXQnO1xyXG5pbXBvcnQgeyBMYXlvdXRUZW1wbGF0ZVNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy9sYXlvdXQudGVtcGxhdGUuc2VydmljZSc7XHJcbmltcG9ydCB7XHJcbiAgICBDYXJkVmlld0Jvb2xJdGVtTW9kZWwsXHJcbiAgICBDYXJkVmlld0RhdGVJdGVtTW9kZWwsXHJcbiAgICBDYXJkVmlld0RhdGV0aW1lSXRlbU1vZGVsLFxyXG4gICAgQ2FyZFZpZXdGbG9hdEl0ZW1Nb2RlbCxcclxuICAgIENhcmRWaWV3SW50SXRlbU1vZGVsLFxyXG4gICAgQ2FyZFZpZXdJdGVtLFxyXG4gICAgQ2FyZFZpZXdUZXh0SXRlbU1vZGVsXHJcbn0gZnJvbSAnQGFsZnJlc2NvL2FkZi1jb3JlJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgRGV0YWlsIHtcclxuICAgIGhlYWRlcj86IHN0cmluZztcclxuICAgIGhlYWRlckNsYXNzPzogc3RyaW5nO1xyXG4gICAgZmllbGRzOiBGaWVsZFtdO1xyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEZpZWxkIHtcclxuICAgIHR5cGU6IEZpZWxkVHlwZTtcclxuICAgIGtleTogc3RyaW5nO1xyXG4gICAgbGFiZWw6IHN0cmluZztcclxufVxyXG5cclxuZXhwb3J0IGVudW0gRmllbGRUeXBlIHtcclxuICAgIGRhdGUgPSAnZGF0ZScsXHJcbiAgICBkYXRldGltZSA9ICdkYXRldGltZScsXHJcbiAgICB0ZXh0ID0gJ3RleHQnLFxyXG4gICAgYm9vbGVhbiA9ICdib29sZWFuJyxcclxuICAgIGludCA9ICdpbnQnLFxyXG4gICAgZmxvYXQgPSAnZmxvYXQnXHJcbn1cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdkZXRhaWxzLWNvbXBvbmVudCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vZGV0YWlscy5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9kZXRhaWxzLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBEZXRhaWxzQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIEBJbnB1dCgpXHJcbiAgICB0ZW1wbGF0ZTogc3RyaW5nO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBtb2RlbDogYW55O1xyXG5cclxuICAgIGRldGFpbHM6IERldGFpbFtdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgdGVtcGxhdGVTZXJ2aWNlOiBMYXlvdXRUZW1wbGF0ZVNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnRlbXBsYXRlU2VydmljZS5nZXRUZW1wbGF0ZSh0aGlzLnRlbXBsYXRlKS5zdWJzY3JpYmUoKGpzb246IGFueSkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmRldGFpbHMgPSBqc29uLnRlbXBsYXRlO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBwcm9wZXJ0aWVzRm9yRGV0YWlsKGRldGFpbDogRGV0YWlsKTogQXJyYXk8Q2FyZFZpZXdJdGVtPiB7XHJcbiAgICAgICAgY29uc3QgcHJvcGVydGllcyA9IFtdO1xyXG4gICAgICAgIGZvciAoY29uc3QgZmllbGQgb2YgZGV0YWlsLmZpZWxkcykge1xyXG4gICAgICAgICAgICBjb25zdCBkYXRhVHlwZSA9IGZpZWxkLnR5cGU7XHJcbiAgICAgICAgICAgIGxldCBpdGVtOiBDYXJkVmlld0l0ZW07XHJcbiAgICAgICAgICAgIHN3aXRjaCAoZGF0YVR5cGUpIHtcclxuICAgICAgICAgICAgICAgIGNhc2UgRmllbGRUeXBlLmJvb2xlYW46XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbSA9IG5ldyBDYXJkVmlld0Jvb2xJdGVtTW9kZWwoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogdGhpcy5nZXRWYWx1ZUZvcktleShmaWVsZC5rZXkpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBrZXk6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYWJlbDogZmllbGQubGFiZWxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgRmllbGRUeXBlLmludDpcclxuICAgICAgICAgICAgICAgICAgICBpdGVtID0gbmV3IENhcmRWaWV3SW50SXRlbU1vZGVsKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZ2V0VmFsdWVGb3JLZXkoZmllbGQua2V5KSxcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5OiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw6IGZpZWxkLmxhYmVsLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSBGaWVsZFR5cGUuZmxvYXQ6XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbSA9IG5ldyBDYXJkVmlld0Zsb2F0SXRlbU1vZGVsKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZ2V0VmFsdWVGb3JLZXkoZmllbGQua2V5KSxcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5OiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw6IGZpZWxkLmxhYmVsLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSBGaWVsZFR5cGUuZGF0ZTpcclxuICAgICAgICAgICAgICAgICAgICBpdGVtID0gbmV3IENhcmRWaWV3RGF0ZUl0ZW1Nb2RlbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiB0aGlzLmdldFZhbHVlRm9yS2V5KGZpZWxkLmtleSksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleTogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxhYmVsOiBmaWVsZC5sYWJlbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9ybWF0OiAnZGQgTU1NLCB5eXl5J1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSBGaWVsZFR5cGUuZGF0ZXRpbWU6XHJcbiAgICAgICAgICAgICAgICAgICAgaXRlbSA9IG5ldyBDYXJkVmlld0RhdGV0aW1lSXRlbU1vZGVsKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZ2V0VmFsdWVGb3JLZXkoZmllbGQua2V5KSxcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5OiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw6IGZpZWxkLmxhYmVsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBmb3JtYXQ6ICdkZCBNTU0sIHl5eXkgSEg6bW0nXHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgICAgIGl0ZW0gPSBuZXcgQ2FyZFZpZXdUZXh0SXRlbU1vZGVsKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IHRoaXMuZ2V0VmFsdWVGb3JLZXkoZmllbGQua2V5KSxcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5OiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGFiZWw6IGZpZWxkLmxhYmVsLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHByb3BlcnRpZXMucHVzaChpdGVtKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHByb3BlcnRpZXM7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXRWYWx1ZUZvcktleShrZXk6IHN0cmluZyk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIGdldCh0aGlzLm1vZGVsLCBrZXkpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==