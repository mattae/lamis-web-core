import * as tslib_1 from "tslib";
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgJhipsterModule } from 'ng-jhipster';
import { AlertErrorComponent } from './alert/alert-error.component';
import { AlertComponent } from './alert/alert.component';
import { PhoneNumberValidator } from './phone.number.validator';
import { CommonPipesModule } from './pipes/common/common-pipes.module';
var SharedCommonModule = /** @class */ (function () {
    function SharedCommonModule() {
    }
    SharedCommonModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                NgJhipsterModule,
                CommonPipesModule,
            ],
            declarations: [
                AlertComponent,
                AlertErrorComponent,
                PhoneNumberValidator
            ],
            exports: [
                AlertComponent,
                AlertErrorComponent,
                CommonPipesModule,
                PhoneNumberValidator
            ]
        })
    ], SharedCommonModule);
    return SharedCommonModule;
}());
export { SharedCommonModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmVkLWNvbW1vbi5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AbGFtaXMvd2ViLWNvcmUvIiwic291cmNlcyI6WyJzaGFyZWQvc2hhcmVkLWNvbW1vbi5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUMvQyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUNwRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDekQsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDaEUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFvQnZFO0lBQUE7SUFDQSxDQUFDO0lBRFksa0JBQWtCO1FBbEI5QixRQUFRLENBQUM7WUFDTixPQUFPLEVBQUU7Z0JBQ0wsWUFBWTtnQkFDWixnQkFBZ0I7Z0JBQ2hCLGlCQUFpQjthQUNwQjtZQUNELFlBQVksRUFBRTtnQkFDVixjQUFjO2dCQUNkLG1CQUFtQjtnQkFDbkIsb0JBQW9CO2FBQ3ZCO1lBQ0QsT0FBTyxFQUFFO2dCQUNMLGNBQWM7Z0JBQ2QsbUJBQW1CO2dCQUNuQixpQkFBaUI7Z0JBQ2pCLG9CQUFvQjthQUN2QjtTQUNKLENBQUM7T0FDVyxrQkFBa0IsQ0FDOUI7SUFBRCx5QkFBQztDQUFBLEFBREQsSUFDQztTQURZLGtCQUFrQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5nSmhpcHN0ZXJNb2R1bGUgfSBmcm9tICduZy1qaGlwc3Rlcic7XHJcbmltcG9ydCB7IEFsZXJ0RXJyb3JDb21wb25lbnQgfSBmcm9tICcuL2FsZXJ0L2FsZXJ0LWVycm9yLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEFsZXJ0Q29tcG9uZW50IH0gZnJvbSAnLi9hbGVydC9hbGVydC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBQaG9uZU51bWJlclZhbGlkYXRvciB9IGZyb20gJy4vcGhvbmUubnVtYmVyLnZhbGlkYXRvcic7XHJcbmltcG9ydCB7IENvbW1vblBpcGVzTW9kdWxlIH0gZnJvbSAnLi9waXBlcy9jb21tb24vY29tbW9uLXBpcGVzLm1vZHVsZSc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgaW1wb3J0czogW1xyXG4gICAgICAgIENvbW1vbk1vZHVsZSxcclxuICAgICAgICBOZ0poaXBzdGVyTW9kdWxlLFxyXG4gICAgICAgIENvbW1vblBpcGVzTW9kdWxlLFxyXG4gICAgXSxcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIEFsZXJ0Q29tcG9uZW50LFxyXG4gICAgICAgIEFsZXJ0RXJyb3JDb21wb25lbnQsXHJcbiAgICAgICAgUGhvbmVOdW1iZXJWYWxpZGF0b3JcclxuICAgIF0sXHJcbiAgICBleHBvcnRzOiBbXHJcbiAgICAgICAgQWxlcnRDb21wb25lbnQsXHJcbiAgICAgICAgQWxlcnRFcnJvckNvbXBvbmVudCxcclxuICAgICAgICBDb21tb25QaXBlc01vZHVsZSxcclxuICAgICAgICBQaG9uZU51bWJlclZhbGlkYXRvclxyXG4gICAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgU2hhcmVkQ29tbW9uTW9kdWxlIHtcclxufVxyXG4iXX0=