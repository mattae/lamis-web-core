import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { JhiEventManager } from 'ng-jhipster';
import { NotificationService } from '@alfresco/adf-core';
var AlertErrorComponent = /** @class */ (function () {
    function AlertErrorComponent(notification, eventManager) {
        var _this = this;
        this.notification = notification;
        this.eventManager = eventManager;
        /* tslint:enable */
        this.cleanHttpErrorListener = eventManager.subscribe('app.httpError', function (response) {
            var i;
            var httpErrorResponse = response.content;
            switch (httpErrorResponse.status) {
                // connection refused, server not reachable
                case 0:
                    _this.addErrorAlert('Server not reachable', 'error.server.not.reachable');
                    break;
                case 400:
                    var arr = httpErrorResponse.headers.keys();
                    var errorHeader_1 = null;
                    var entityKey_1 = null;
                    arr.forEach(function (entry) {
                        if (entry.endsWith('app-error')) {
                            errorHeader_1 = httpErrorResponse.headers.get(entry);
                        }
                        else if (entry.endsWith('app-params')) {
                            entityKey_1 = httpErrorResponse.headers.get(entry);
                        }
                    });
                    if (errorHeader_1) {
                        _this.addErrorAlert(errorHeader_1, errorHeader_1, { entityName: entityKey_1 });
                    }
                    else if (httpErrorResponse.error !== '' && httpErrorResponse.error.fieldErrors) {
                        var fieldErrors = httpErrorResponse.error.fieldErrors;
                        for (i = 0; i < fieldErrors.length; i++) {
                            var fieldError = fieldErrors[i];
                            // convert 'something[14].other[4].id' to 'something[].other[].id' so translations can be written to it
                            var convertedField = fieldError.field.replace(/\[\d*\]/g, '[]');
                            var fieldName = convertedField.charAt(0).toUpperCase() + convertedField.slice(1);
                            _this.addErrorAlert('Error on field "' + fieldName + '"', 'error.' + fieldError.message, { fieldName: fieldName });
                        }
                    }
                    else if (httpErrorResponse.error !== '' && httpErrorResponse.error.message) {
                        _this.addErrorAlert(httpErrorResponse.error.message, httpErrorResponse.error.message, httpErrorResponse.error.params);
                    }
                    else {
                        _this.addErrorAlert(httpErrorResponse.error);
                    }
                    break;
                case 404:
                    _this.addErrorAlert('Not found', 'error.url.not.found');
                    break;
                default:
                    if (httpErrorResponse.error !== '' && httpErrorResponse.error.message) {
                        _this.addErrorAlert(httpErrorResponse.error.message);
                    }
                    else {
                        _this.addErrorAlert(httpErrorResponse.error);
                    }
            }
        });
    }
    AlertErrorComponent.prototype.ngOnDestroy = function () {
        if (this.cleanHttpErrorListener !== undefined && this.cleanHttpErrorListener !== null) {
            this.eventManager.destroy(this.cleanHttpErrorListener);
        }
    };
    AlertErrorComponent.prototype.addErrorAlert = function (message, key, data) {
        this.notification.showError(message);
    };
    AlertErrorComponent.ctorParameters = function () { return [
        { type: NotificationService },
        { type: JhiEventManager }
    ]; };
    AlertErrorComponent = tslib_1.__decorate([
        Component({
            selector: 'alert-error',
            template: ""
        })
    ], AlertErrorComponent);
    return AlertErrorComponent;
}());
export { AlertErrorComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxlcnQtZXJyb3IuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL2FsZXJ0L2FsZXJ0LWVycm9yLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUNyRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBRTlDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBTXpEO0lBR0ksNkJBQW9CLFlBQWlDLEVBQVUsWUFBNkI7UUFBNUYsaUJBd0RDO1FBeERtQixpQkFBWSxHQUFaLFlBQVksQ0FBcUI7UUFBVSxpQkFBWSxHQUFaLFlBQVksQ0FBaUI7UUFDeEYsbUJBQW1CO1FBQ25CLElBQUksQ0FBQyxzQkFBc0IsR0FBRyxZQUFZLENBQUMsU0FBUyxDQUFDLGVBQWUsRUFBRSxVQUFBLFFBQVE7WUFDMUUsSUFBSSxDQUFDLENBQUM7WUFDTixJQUFNLGlCQUFpQixHQUFHLFFBQVEsQ0FBQyxPQUFPLENBQUM7WUFDM0MsUUFBUSxpQkFBaUIsQ0FBQyxNQUFNLEVBQUU7Z0JBQzlCLDJDQUEyQztnQkFDM0MsS0FBSyxDQUFDO29CQUNGLEtBQUksQ0FBQyxhQUFhLENBQUMsc0JBQXNCLEVBQUUsNEJBQTRCLENBQUMsQ0FBQztvQkFDekUsTUFBTTtnQkFFVixLQUFLLEdBQUc7b0JBQ0osSUFBTSxHQUFHLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO29CQUM3QyxJQUFJLGFBQVcsR0FBRyxJQUFJLENBQUM7b0JBQ3ZCLElBQUksV0FBUyxHQUFHLElBQUksQ0FBQztvQkFDckIsR0FBRyxDQUFDLE9BQU8sQ0FBQyxVQUFBLEtBQUs7d0JBQ2IsSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxFQUFFOzRCQUM3QixhQUFXLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDdEQ7NkJBQU0sSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxFQUFFOzRCQUNyQyxXQUFTLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQzt5QkFDcEQ7b0JBQ0wsQ0FBQyxDQUFDLENBQUM7b0JBQ0gsSUFBSSxhQUFXLEVBQUU7d0JBQ2IsS0FBSSxDQUFDLGFBQWEsQ0FBQyxhQUFXLEVBQUUsYUFBVyxFQUFFLEVBQUMsVUFBVSxFQUFFLFdBQVMsRUFBQyxDQUFDLENBQUM7cUJBQ3pFO3lCQUFNLElBQUksaUJBQWlCLENBQUMsS0FBSyxLQUFLLEVBQUUsSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsV0FBVyxFQUFFO3dCQUM5RSxJQUFNLFdBQVcsR0FBRyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDO3dCQUN4RCxLQUFLLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7NEJBQ3JDLElBQU0sVUFBVSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDbEMsdUdBQXVHOzRCQUN2RyxJQUFNLGNBQWMsR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLENBQUM7NEJBQ2xFLElBQU0sU0FBUyxHQUFHLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxFQUFFLEdBQUcsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDbkYsS0FBSSxDQUFDLGFBQWEsQ0FBQyxrQkFBa0IsR0FBRyxTQUFTLEdBQUcsR0FBRyxFQUFFLFFBQVEsR0FBRyxVQUFVLENBQUMsT0FBTyxFQUFFLEVBQUMsU0FBUyxXQUFBLEVBQUMsQ0FBQyxDQUFDO3lCQUN4RztxQkFDSjt5QkFBTSxJQUFJLGlCQUFpQixDQUFDLEtBQUssS0FBSyxFQUFFLElBQUksaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRTt3QkFDMUUsS0FBSSxDQUFDLGFBQWEsQ0FDZCxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUMvQixpQkFBaUIsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUMvQixpQkFBaUIsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUNqQyxDQUFDO3FCQUNMO3lCQUFNO3dCQUNILEtBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLENBQUM7cUJBQy9DO29CQUNELE1BQU07Z0JBRVYsS0FBSyxHQUFHO29CQUNKLEtBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLHFCQUFxQixDQUFDLENBQUM7b0JBQ3ZELE1BQU07Z0JBRVY7b0JBQ0ksSUFBSSxpQkFBaUIsQ0FBQyxLQUFLLEtBQUssRUFBRSxJQUFJLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUU7d0JBQ25FLEtBQUksQ0FBQyxhQUFhLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO3FCQUN2RDt5QkFBTTt3QkFDSCxLQUFJLENBQUMsYUFBYSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUMvQzthQUNSO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQseUNBQVcsR0FBWDtRQUNJLElBQUksSUFBSSxDQUFDLHNCQUFzQixLQUFLLFNBQVMsSUFBSSxJQUFJLENBQUMsc0JBQXNCLEtBQUssSUFBSSxFQUFFO1lBQ25GLElBQUksQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1NBQzFEO0lBQ0wsQ0FBQztJQUVELDJDQUFhLEdBQWIsVUFBYyxPQUFPLEVBQUUsR0FBSSxFQUFFLElBQUs7UUFDOUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDekMsQ0FBQzs7Z0JBbEVpQyxtQkFBbUI7Z0JBQXdCLGVBQWU7O0lBSG5GLG1CQUFtQjtRQUovQixTQUFTLENBQUM7WUFDUCxRQUFRLEVBQUUsYUFBYTtZQUN2QixRQUFRLEVBQUUsRUFBRTtTQUNmLENBQUM7T0FDVyxtQkFBbUIsQ0FzRS9CO0lBQUQsMEJBQUM7Q0FBQSxBQXRFRCxJQXNFQztTQXRFWSxtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBKaGlFdmVudE1hbmFnZXIgfSBmcm9tICduZy1qaGlwc3Rlcic7XHJcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnQGFsZnJlc2NvL2FkZi1jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhbGVydC1lcnJvcicsXHJcbiAgICB0ZW1wbGF0ZTogYGBcclxufSlcclxuZXhwb3J0IGNsYXNzIEFsZXJ0RXJyb3JDb21wb25lbnQgaW1wbGVtZW50cyBPbkRlc3Ryb3kge1xyXG4gICAgY2xlYW5IdHRwRXJyb3JMaXN0ZW5lcjogU3Vic2NyaXB0aW9uO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgbm90aWZpY2F0aW9uOiBOb3RpZmljYXRpb25TZXJ2aWNlLCBwcml2YXRlIGV2ZW50TWFuYWdlcjogSmhpRXZlbnRNYW5hZ2VyKSB7XHJcbiAgICAgICAgLyogdHNsaW50OmVuYWJsZSAqL1xyXG4gICAgICAgIHRoaXMuY2xlYW5IdHRwRXJyb3JMaXN0ZW5lciA9IGV2ZW50TWFuYWdlci5zdWJzY3JpYmUoJ2FwcC5odHRwRXJyb3InLCByZXNwb25zZSA9PiB7XHJcbiAgICAgICAgICAgIGxldCBpO1xyXG4gICAgICAgICAgICBjb25zdCBodHRwRXJyb3JSZXNwb25zZSA9IHJlc3BvbnNlLmNvbnRlbnQ7XHJcbiAgICAgICAgICAgIHN3aXRjaCAoaHR0cEVycm9yUmVzcG9uc2Uuc3RhdHVzKSB7XHJcbiAgICAgICAgICAgICAgICAvLyBjb25uZWN0aW9uIHJlZnVzZWQsIHNlcnZlciBub3QgcmVhY2hhYmxlXHJcbiAgICAgICAgICAgICAgICBjYXNlIDA6XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hZGRFcnJvckFsZXJ0KCdTZXJ2ZXIgbm90IHJlYWNoYWJsZScsICdlcnJvci5zZXJ2ZXIubm90LnJlYWNoYWJsZScpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgICAgIGNhc2UgNDAwOlxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGFyciA9IGh0dHBFcnJvclJlc3BvbnNlLmhlYWRlcnMua2V5cygpO1xyXG4gICAgICAgICAgICAgICAgICAgIGxldCBlcnJvckhlYWRlciA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IGVudGl0eUtleSA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgYXJyLmZvckVhY2goZW50cnkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZW50cnkuZW5kc1dpdGgoJ2FwcC1lcnJvcicpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlcnJvckhlYWRlciA9IGh0dHBFcnJvclJlc3BvbnNlLmhlYWRlcnMuZ2V0KGVudHJ5KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChlbnRyeS5lbmRzV2l0aCgnYXBwLXBhcmFtcycpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbnRpdHlLZXkgPSBodHRwRXJyb3JSZXNwb25zZS5oZWFkZXJzLmdldChlbnRyeSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZXJyb3JIZWFkZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hZGRFcnJvckFsZXJ0KGVycm9ySGVhZGVyLCBlcnJvckhlYWRlciwge2VudGl0eU5hbWU6IGVudGl0eUtleX0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoaHR0cEVycm9yUmVzcG9uc2UuZXJyb3IgIT09ICcnICYmIGh0dHBFcnJvclJlc3BvbnNlLmVycm9yLmZpZWxkRXJyb3JzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGZpZWxkRXJyb3JzID0gaHR0cEVycm9yUmVzcG9uc2UuZXJyb3IuZmllbGRFcnJvcnM7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvciAoaSA9IDA7IGkgPCBmaWVsZEVycm9ycy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZmllbGRFcnJvciA9IGZpZWxkRXJyb3JzW2ldO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy8gY29udmVydCAnc29tZXRoaW5nWzE0XS5vdGhlcls0XS5pZCcgdG8gJ3NvbWV0aGluZ1tdLm90aGVyW10uaWQnIHNvIHRyYW5zbGF0aW9ucyBjYW4gYmUgd3JpdHRlbiB0byBpdFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgY29udmVydGVkRmllbGQgPSBmaWVsZEVycm9yLmZpZWxkLnJlcGxhY2UoL1xcW1xcZCpcXF0vZywgJ1tdJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBmaWVsZE5hbWUgPSBjb252ZXJ0ZWRGaWVsZC5jaGFyQXQoMCkudG9VcHBlckNhc2UoKSArIGNvbnZlcnRlZEZpZWxkLnNsaWNlKDEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hZGRFcnJvckFsZXJ0KCdFcnJvciBvbiBmaWVsZCBcIicgKyBmaWVsZE5hbWUgKyAnXCInLCAnZXJyb3IuJyArIGZpZWxkRXJyb3IubWVzc2FnZSwge2ZpZWxkTmFtZX0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChodHRwRXJyb3JSZXNwb25zZS5lcnJvciAhPT0gJycgJiYgaHR0cEVycm9yUmVzcG9uc2UuZXJyb3IubWVzc2FnZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZEVycm9yQWxlcnQoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBodHRwRXJyb3JSZXNwb25zZS5lcnJvci5tZXNzYWdlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaHR0cEVycm9yUmVzcG9uc2UuZXJyb3IubWVzc2FnZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGh0dHBFcnJvclJlc3BvbnNlLmVycm9yLnBhcmFtc1xyXG4gICAgICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYWRkRXJyb3JBbGVydChodHRwRXJyb3JSZXNwb25zZS5lcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG5cclxuICAgICAgICAgICAgICAgIGNhc2UgNDA0OlxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYWRkRXJyb3JBbGVydCgnTm90IGZvdW5kJywgJ2Vycm9yLnVybC5ub3QuZm91bmQnKTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuXHJcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChodHRwRXJyb3JSZXNwb25zZS5lcnJvciAhPT0gJycgJiYgaHR0cEVycm9yUmVzcG9uc2UuZXJyb3IubWVzc2FnZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmFkZEVycm9yQWxlcnQoaHR0cEVycm9yUmVzcG9uc2UuZXJyb3IubWVzc2FnZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5hZGRFcnJvckFsZXJ0KGh0dHBFcnJvclJlc3BvbnNlLmVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpIHtcclxuICAgICAgICBpZiAodGhpcy5jbGVhbkh0dHBFcnJvckxpc3RlbmVyICE9PSB1bmRlZmluZWQgJiYgdGhpcy5jbGVhbkh0dHBFcnJvckxpc3RlbmVyICE9PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZXZlbnRNYW5hZ2VyLmRlc3Ryb3kodGhpcy5jbGVhbkh0dHBFcnJvckxpc3RlbmVyKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgYWRkRXJyb3JBbGVydChtZXNzYWdlLCBrZXk/LCBkYXRhPykge1xyXG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uLnNob3dFcnJvcihtZXNzYWdlKTtcclxuICAgIH1cclxufVxyXG4iXX0=