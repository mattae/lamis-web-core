import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { JhiAlertService } from 'ng-jhipster';
import { NotificationService } from '@alfresco/adf-core';
var AlertComponent = /** @class */ (function () {
    function AlertComponent(notification, alertService) {
        this.notification = notification;
        this.alertService = alertService;
    }
    AlertComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.alerts = this.alertService.get();
        this.alerts.forEach(function (alert) { return _this.notification.openSnackMessage(alert.msg, 5000); });
    };
    AlertComponent.prototype.ngOnDestroy = function () {
        this.alerts = [];
    };
    AlertComponent.ctorParameters = function () { return [
        { type: NotificationService },
        { type: JhiAlertService }
    ]; };
    AlertComponent = tslib_1.__decorate([
        Component({
            selector: 'alert',
            template: ""
        })
    ], AlertComponent);
    return AlertComponent;
}());
export { AlertComponent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxlcnQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsic2hhcmVkL2FsZXJ0L2FsZXJ0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsT0FBTyxFQUFFLFNBQVMsRUFBcUIsTUFBTSxlQUFlLENBQUM7QUFDN0QsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUM5QyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQU16RDtJQUdJLHdCQUFvQixZQUFpQyxFQUFVLFlBQTZCO1FBQXhFLGlCQUFZLEdBQVosWUFBWSxDQUFxQjtRQUFVLGlCQUFZLEdBQVosWUFBWSxDQUFpQjtJQUFHLENBQUM7SUFFaEcsaUNBQVEsR0FBUjtRQUFBLGlCQUdDO1FBRkcsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLEdBQUcsRUFBRSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLFVBQUEsS0FBSyxJQUFJLE9BQUEsS0FBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxFQUFuRCxDQUFtRCxDQUFDLENBQUM7SUFDdEYsQ0FBQztJQUVELG9DQUFXLEdBQVg7UUFDSSxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztJQUNyQixDQUFDOztnQkFUaUMsbUJBQW1CO2dCQUF3QixlQUFlOztJQUhuRixjQUFjO1FBSjFCLFNBQVMsQ0FBQztZQUNQLFFBQVEsRUFBRSxPQUFPO1lBQ2pCLFFBQVEsRUFBRSxFQUFFO1NBQ2YsQ0FBQztPQUNXLGNBQWMsQ0FhMUI7SUFBRCxxQkFBQztDQUFBLEFBYkQsSUFhQztTQWJZLGNBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uRGVzdHJveSwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEpoaUFsZXJ0U2VydmljZSB9IGZyb20gJ25nLWpoaXBzdGVyJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uU2VydmljZSB9IGZyb20gJ0BhbGZyZXNjby9hZGYtY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWxlcnQnLFxyXG4gICAgdGVtcGxhdGU6IGBgXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBbGVydENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuICAgIGFsZXJ0czogYW55W107XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBub3RpZmljYXRpb246IE5vdGlmaWNhdGlvblNlcnZpY2UsIHByaXZhdGUgYWxlcnRTZXJ2aWNlOiBKaGlBbGVydFNlcnZpY2UpIHt9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5hbGVydHMgPSB0aGlzLmFsZXJ0U2VydmljZS5nZXQoKTtcclxuICAgICAgICB0aGlzLmFsZXJ0cy5mb3JFYWNoKGFsZXJ0ID0+IHRoaXMubm90aWZpY2F0aW9uLm9wZW5TbmFja01lc3NhZ2UoYWxlcnQubXNnLCA1MDAwKSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICAgICAgdGhpcy5hbGVydHMgPSBbXTtcclxuICAgIH1cclxufVxyXG4iXX0=