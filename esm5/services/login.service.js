import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { flatMap } from 'rxjs/operators';
import { AccountService } from '../core/auth/account.service';
import { AuthServerProvider } from '../core/auth/auth-jwt.service';
import { Router } from '@angular/router';
import * as i0 from "@angular/core";
import * as i1 from "../core/auth/account.service";
import * as i2 from "../core/auth/auth-jwt.service";
import * as i3 from "@angular/router";
var LoginService = /** @class */ (function () {
    function LoginService(accountService, authServerProvider, router) {
        this.accountService = accountService;
        this.authServerProvider = authServerProvider;
        this.router = router;
    }
    LoginService.prototype.login = function (credentials) {
        var _this = this;
        return this.authServerProvider.login(credentials).pipe(flatMap(function () { return _this.accountService.identity(true); }));
    };
    LoginService.prototype.logout = function () {
        this.authServerProvider.logout().subscribe();
        this.accountService.authenticate(null);
        this.router.navigateByUrl('/sessions/login');
    };
    LoginService.ctorParameters = function () { return [
        { type: AccountService },
        { type: AuthServerProvider },
        { type: Router }
    ]; };
    LoginService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function LoginService_Factory() { return new LoginService(i0.ɵɵinject(i1.AccountService), i0.ɵɵinject(i2.AuthServerProvider), i0.ɵɵinject(i3.Router)); }, token: LoginService, providedIn: "root" });
    LoginService = tslib_1.__decorate([
        Injectable({ providedIn: 'root' })
    ], LoginService);
    return LoginService;
}());
export { LoginService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL2xvZ2luLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3pDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUNuRSxPQUFPLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7Ozs7O0FBSXpDO0lBQ0Msc0JBQW9CLGNBQThCLEVBQVUsa0JBQXNDLEVBQ3ZGLE1BQWM7UUFETCxtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFBVSx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3ZGLFdBQU0sR0FBTixNQUFNLENBQVE7SUFDekIsQ0FBQztJQUVELDRCQUFLLEdBQUwsVUFBTSxXQUFXO1FBQWpCLGlCQUVDO1FBREEsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFsQyxDQUFrQyxDQUFDLENBQUMsQ0FBQztJQUMzRyxDQUFDO0lBRUQsNkJBQU0sR0FBTjtRQUNDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUM3QyxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN2QyxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFBO0lBQzdDLENBQUM7O2dCQVptQyxjQUFjO2dCQUE4QixrQkFBa0I7Z0JBQy9FLE1BQU07OztJQUZiLFlBQVk7UUFEeEIsVUFBVSxDQUFDLEVBQUMsVUFBVSxFQUFFLE1BQU0sRUFBQyxDQUFDO09BQ3BCLFlBQVksQ0FjeEI7dUJBdkJEO0NBdUJDLEFBZEQsSUFjQztTQWRZLFlBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgZmxhdE1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgQWNjb3VudFNlcnZpY2UgfSBmcm9tICcuLi9jb3JlL2F1dGgvYWNjb3VudC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXV0aFNlcnZlclByb3ZpZGVyIH0gZnJvbSAnLi4vY29yZS9hdXRoL2F1dGgtand0LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5cclxuXHJcbkBJbmplY3RhYmxlKHtwcm92aWRlZEluOiAncm9vdCd9KVxyXG5leHBvcnQgY2xhc3MgTG9naW5TZXJ2aWNlIHtcclxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIGFjY291bnRTZXJ2aWNlOiBBY2NvdW50U2VydmljZSwgcHJpdmF0ZSBhdXRoU2VydmVyUHJvdmlkZXI6IEF1dGhTZXJ2ZXJQcm92aWRlcixcclxuXHRcdFx0XHRwcml2YXRlIHJvdXRlcjogUm91dGVyKSB7XHJcblx0fVxyXG5cclxuXHRsb2dpbihjcmVkZW50aWFscyk6IE9ic2VydmFibGU8QWNjb3VudCB8IG51bGw+IHtcclxuXHRcdHJldHVybiB0aGlzLmF1dGhTZXJ2ZXJQcm92aWRlci5sb2dpbihjcmVkZW50aWFscykucGlwZShmbGF0TWFwKCgpID0+IHRoaXMuYWNjb3VudFNlcnZpY2UuaWRlbnRpdHkodHJ1ZSkpKTtcclxuXHR9XHJcblxyXG5cdGxvZ291dCgpIHtcclxuXHRcdHRoaXMuYXV0aFNlcnZlclByb3ZpZGVyLmxvZ291dCgpLnN1YnNjcmliZSgpO1xyXG5cdFx0dGhpcy5hY2NvdW50U2VydmljZS5hdXRoZW50aWNhdGUobnVsbCk7XHJcblx0XHR0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKCcvc2Vzc2lvbnMvbG9naW4nKVxyXG5cdH1cclxufVxyXG4iXX0=