import * as tslib_1 from "tslib";
import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { SERVER_API_URL_CONFIG } from '../app.constants';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../app.constants";
var StateService = /** @class */ (function () {
    function StateService(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
        this.resourceUrl = '';
        this.resourceUrl = serverUrl.SERVER_API_URL + '/api/states';
    }
    StateService.prototype.find = function (id) {
        return this.http.get(this.resourceUrl + "/" + id, { observe: 'response' });
    };
    StateService.prototype.getStates = function () {
        return this.http.get("" + this.resourceUrl, { observe: 'response' });
    };
    StateService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
    ]; };
    StateService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function StateService_Factory() { return new StateService(i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i2.SERVER_API_URL_CONFIG)); }, token: StateService, providedIn: "root" });
    StateService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__param(1, Inject(SERVER_API_URL_CONFIG))
    ], StateService);
    return StateService;
}());
export { StateService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGUuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL3N0YXRlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFHaEUsT0FBTyxFQUFFLHFCQUFxQixFQUFzQixNQUFNLGtCQUFrQixDQUFDOzs7O0FBSzdFO0lBRUMsc0JBQW9CLElBQWdCLEVBQXlDLFNBQTZCO1FBQXRGLFNBQUksR0FBSixJQUFJLENBQVk7UUFBeUMsY0FBUyxHQUFULFNBQVMsQ0FBb0I7UUFEMUcsZ0JBQVcsR0FBRyxFQUFFLENBQUM7UUFFaEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxTQUFTLENBQUMsY0FBYyxHQUFHLGFBQWEsQ0FBQztJQUM3RCxDQUFDO0lBRUQsMkJBQUksR0FBSixVQUFLLEVBQVU7UUFDZCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFJLElBQUksQ0FBQyxXQUFXLFNBQUksRUFBSSxFQUFFLEVBQUMsT0FBTyxFQUFFLFVBQVUsRUFBQyxDQUFDLENBQUM7SUFDMUUsQ0FBQztJQUVELGdDQUFTLEdBQVQ7UUFDQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFXLEtBQUcsSUFBSSxDQUFDLFdBQWEsRUFBRSxFQUFDLE9BQU8sRUFBRSxVQUFVLEVBQUMsQ0FBQyxDQUFDO0lBQzlFLENBQUM7O2dCQVZ5QixVQUFVO2dEQUFHLE1BQU0sU0FBQyxxQkFBcUI7OztJQUZ2RCxZQUFZO1FBSHhCLFVBQVUsQ0FBQztZQUNYLFVBQVUsRUFBRSxNQUFNO1NBQ2xCLENBQUM7UUFHc0MsbUJBQUEsTUFBTSxDQUFDLHFCQUFxQixDQUFDLENBQUE7T0FGeEQsWUFBWSxDQWF4Qjt1QkF0QkQ7Q0FzQkMsQUFiRCxJQWFDO1NBYlksWUFBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgSVN0YXRlIH0gZnJvbSAnLi4vc2hhcmVkL21vZGVsL3N0YXRlLm1vZGVsJztcclxuaW1wb3J0IHsgU0VSVkVSX0FQSV9VUkxfQ09ORklHLCBTZXJ2ZXJBcGlVcmxDb25maWcgfSBmcm9tICcuLi9hcHAuY29uc3RhbnRzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuXHRwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFN0YXRlU2VydmljZSB7XHJcblx0cmVzb3VyY2VVcmwgPSAnJztcclxuXHRjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQsIEBJbmplY3QoU0VSVkVSX0FQSV9VUkxfQ09ORklHKSBwcml2YXRlIHNlcnZlclVybDogU2VydmVyQXBpVXJsQ29uZmlnKSB7XHJcblx0XHR0aGlzLnJlc291cmNlVXJsID0gc2VydmVyVXJsLlNFUlZFUl9BUElfVVJMICsgJy9hcGkvc3RhdGVzJztcclxuXHR9XHJcblxyXG5cdGZpbmQoaWQ6IG51bWJlcik6IE9ic2VydmFibGU8SHR0cFJlc3BvbnNlPElTdGF0ZT4+IHtcclxuXHRcdHJldHVybiB0aGlzLmh0dHAuZ2V0KGAke3RoaXMucmVzb3VyY2VVcmx9LyR7aWR9YCwge29ic2VydmU6ICdyZXNwb25zZSd9KTtcclxuXHR9XHJcblxyXG5cdGdldFN0YXRlcygpOiBPYnNlcnZhYmxlPEh0dHBSZXNwb25zZTxJU3RhdGVbXT4+IHtcclxuXHRcdHJldHVybiB0aGlzLmh0dHAuZ2V0PElTdGF0ZVtdPihgJHt0aGlzLnJlc291cmNlVXJsfWAsIHtvYnNlcnZlOiAncmVzcG9uc2UnfSk7XHJcblx0fVxyXG59XHJcbiJdfQ==