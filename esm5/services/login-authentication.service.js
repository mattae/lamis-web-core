import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { JhiEventManager } from 'ng-jhipster';
import { LocalStorageService, SessionStorageService } from 'ngx-store';
import { map } from 'rxjs/operators';
import { StateStorageService } from '../core/auth/state-storage.service';
import { LoginService } from './login.service';
var LoginAuthenticationService = /** @class */ (function () {
    function LoginAuthenticationService(router, loginService, $localStorage, $sessionStorage, eventManager, stateStorageService) {
        this.router = router;
        this.loginService = loginService;
        this.$localStorage = $localStorage;
        this.$sessionStorage = $sessionStorage;
        this.eventManager = eventManager;
        this.stateStorageService = stateStorageService;
    }
    LoginAuthenticationService.prototype.setRedirect = function (value) {
    };
    LoginAuthenticationService.prototype.isEcmLoggedIn = function () {
        return false;
    };
    LoginAuthenticationService.prototype.isBpmLoggedIn = function () {
        return false;
    };
    LoginAuthenticationService.prototype.isOauth = function () {
        return false;
    };
    LoginAuthenticationService.prototype.getRedirect = function () {
        return null;
    };
    LoginAuthenticationService.prototype.login = function (username, password, rememberMe) {
        var _this = this;
        if (rememberMe === void 0) { rememberMe = false; }
        return this.loginService.login({
            username: username,
            password: password,
            rememberMe: rememberMe
        }).pipe(map(function () {
            if (_this.router.url === '/account/register' || _this.router.url.startsWith('/account/activate') ||
                _this.router.url.startsWith('/account/reset/')) {
                _this.router.navigate(['']);
            }
            _this.eventManager.broadcast({
                name: 'authenticationSuccess',
                content: 'Sending Authentication Success'
            });
            // previousState was set in the authExpiredInterceptor before being redirected to login modal.
            // since login is successful, go to stored previousState and clear previousState
            var redirect = _this.stateStorageService.getUrl();
            if (redirect) {
                _this.stateStorageService.storeUrl('');
                _this.router.navigate([redirect]);
            }
            else {
                _this.router.navigate(['/dashboard']);
            }
        }));
    };
    LoginAuthenticationService.ctorParameters = function () { return [
        { type: Router },
        { type: LoginService },
        { type: LocalStorageService },
        { type: SessionStorageService },
        { type: JhiEventManager },
        { type: StateStorageService }
    ]; };
    LoginAuthenticationService = tslib_1.__decorate([
        Injectable()
    ], LoginAuthenticationService);
    return LoginAuthenticationService;
}());
export { LoginAuthenticationService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tYXV0aGVudGljYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL2xvZ2luLWF1dGhlbnRpY2F0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLE1BQU0sRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQ3pDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDOUMsT0FBTyxFQUFFLG1CQUFtQixFQUFFLHFCQUFxQixFQUFFLE1BQU0sV0FBVyxDQUFDO0FBRXZFLE9BQU8sRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNyQyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUN6RSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFHL0M7SUFFSSxvQ0FBb0IsTUFBYyxFQUNkLFlBQTBCLEVBQzFCLGFBQWtDLEVBQ2xDLGVBQXNDLEVBQ3RDLFlBQTZCLEVBQzdCLG1CQUF3QztRQUx4QyxXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsaUJBQVksR0FBWixZQUFZLENBQWM7UUFDMUIsa0JBQWEsR0FBYixhQUFhLENBQXFCO1FBQ2xDLG9CQUFlLEdBQWYsZUFBZSxDQUF1QjtRQUN0QyxpQkFBWSxHQUFaLFlBQVksQ0FBaUI7UUFDN0Isd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtJQUM1RCxDQUFDO0lBRUQsZ0RBQVcsR0FBWCxVQUFZLEtBQVU7SUFFdEIsQ0FBQztJQUVELGtEQUFhLEdBQWI7UUFDSSxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBRUQsa0RBQWEsR0FBYjtRQUNJLE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFRCw0Q0FBTyxHQUFQO1FBQ0ksT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUVELGdEQUFXLEdBQVg7UUFDSSxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQsMENBQUssR0FBTCxVQUFNLFFBQWdCLEVBQUUsUUFBZ0IsRUFBRSxVQUEyQjtRQUFyRSxpQkE4QkM7UUE5QnlDLDJCQUFBLEVBQUEsa0JBQTJCO1FBQ2pFLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUM7WUFDM0IsUUFBUSxFQUFFLFFBQVE7WUFDbEIsUUFBUSxFQUFFLFFBQVE7WUFDbEIsVUFBVSxFQUFFLFVBQVU7U0FDekIsQ0FBQyxDQUFDLElBQUksQ0FDSCxHQUFHLENBQ0M7WUFDSSxJQUFJLEtBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxLQUFLLG1CQUFtQixJQUFJLEtBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQztnQkFDMUYsS0FBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLEVBQUU7Z0JBQy9DLEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQzthQUM5QjtZQUVELEtBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDO2dCQUN4QixJQUFJLEVBQUUsdUJBQXVCO2dCQUM3QixPQUFPLEVBQUUsZ0NBQWdDO2FBQzVDLENBQUMsQ0FBQztZQUVILDhGQUE4RjtZQUM5RixnRkFBZ0Y7WUFDaEYsSUFBTSxRQUFRLEdBQUcsS0FBSSxDQUFDLG1CQUFtQixDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ25ELElBQUksUUFBUSxFQUFFO2dCQUNWLEtBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7Z0JBQ3RDLEtBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzthQUNwQztpQkFBTTtnQkFDSCxLQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUM7YUFDeEM7UUFDTCxDQUFDLENBQ0osQ0FDSixDQUFDO0lBQ04sQ0FBQzs7Z0JBMUQyQixNQUFNO2dCQUNBLFlBQVk7Z0JBQ1gsbUJBQW1CO2dCQUNqQixxQkFBcUI7Z0JBQ3hCLGVBQWU7Z0JBQ1IsbUJBQW1COztJQVBuRCwwQkFBMEI7UUFEdEMsVUFBVSxFQUFFO09BQ0EsMEJBQTBCLENBNkR0QztJQUFELGlDQUFDO0NBQUEsQUE3REQsSUE2REM7U0E3RFksMEJBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBKaGlFdmVudE1hbmFnZXIgfSBmcm9tICduZy1qaGlwc3Rlcic7XHJcbmltcG9ydCB7IExvY2FsU3RvcmFnZVNlcnZpY2UsIFNlc3Npb25TdG9yYWdlU2VydmljZSB9IGZyb20gJ25neC1zdG9yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBTdGF0ZVN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnLi4vY29yZS9hdXRoL3N0YXRlLXN0b3JhZ2Uuc2VydmljZSc7XHJcbmltcG9ydCB7IExvZ2luU2VydmljZSB9IGZyb20gJy4vbG9naW4uc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBMb2dpbkF1dGhlbnRpY2F0aW9uU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgbG9naW5TZXJ2aWNlOiBMb2dpblNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlICRsb2NhbFN0b3JhZ2U6IExvY2FsU3RvcmFnZVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlICRzZXNzaW9uU3RvcmFnZTogU2Vzc2lvblN0b3JhZ2VTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBldmVudE1hbmFnZXI6IEpoaUV2ZW50TWFuYWdlcixcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgc3RhdGVTdG9yYWdlU2VydmljZTogU3RhdGVTdG9yYWdlU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIHNldFJlZGlyZWN0KHZhbHVlOiBhbnkpIHtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgaXNFY21Mb2dnZWRJbigpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaXNCcG1Mb2dnZWRJbigpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaXNPYXV0aCgpIHtcclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UmVkaXJlY3QoKSB7XHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgbG9naW4odXNlcm5hbWU6IHN0cmluZywgcGFzc3dvcmQ6IHN0cmluZywgcmVtZW1iZXJNZTogYm9vbGVhbiA9IGZhbHNlKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5sb2dpblNlcnZpY2UubG9naW4oe1xyXG4gICAgICAgICAgICB1c2VybmFtZTogdXNlcm5hbWUsXHJcbiAgICAgICAgICAgIHBhc3N3b3JkOiBwYXNzd29yZCxcclxuICAgICAgICAgICAgcmVtZW1iZXJNZTogcmVtZW1iZXJNZVxyXG4gICAgICAgIH0pLnBpcGUoXHJcbiAgICAgICAgICAgIG1hcChcclxuICAgICAgICAgICAgICAgICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5yb3V0ZXIudXJsID09PSAnL2FjY291bnQvcmVnaXN0ZXInIHx8IHRoaXMucm91dGVyLnVybC5zdGFydHNXaXRoKCcvYWNjb3VudC9hY3RpdmF0ZScpIHx8XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLnVybC5zdGFydHNXaXRoKCcvYWNjb3VudC9yZXNldC8nKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJyddKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZXZlbnRNYW5hZ2VyLmJyb2FkY2FzdCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICdhdXRoZW50aWNhdGlvblN1Y2Nlc3MnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb250ZW50OiAnU2VuZGluZyBBdXRoZW50aWNhdGlvbiBTdWNjZXNzJ1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyBwcmV2aW91c1N0YXRlIHdhcyBzZXQgaW4gdGhlIGF1dGhFeHBpcmVkSW50ZXJjZXB0b3IgYmVmb3JlIGJlaW5nIHJlZGlyZWN0ZWQgdG8gbG9naW4gbW9kYWwuXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gc2luY2UgbG9naW4gaXMgc3VjY2Vzc2Z1bCwgZ28gdG8gc3RvcmVkIHByZXZpb3VzU3RhdGUgYW5kIGNsZWFyIHByZXZpb3VzU3RhdGVcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCByZWRpcmVjdCA9IHRoaXMuc3RhdGVTdG9yYWdlU2VydmljZS5nZXRVcmwoKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVkaXJlY3QpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zdGF0ZVN0b3JhZ2VTZXJ2aWNlLnN0b3JlVXJsKCcnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW3JlZGlyZWN0XSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoWycvZGFzaGJvYXJkJ10pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19