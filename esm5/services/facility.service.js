import * as tslib_1 from "tslib";
import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { SERVER_API_URL_CONFIG } from '../app.constants';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
import * as i2 from "../app.constants";
var FacilityService = /** @class */ (function () {
    function FacilityService(http, serverUrl) {
        this.http = http;
        this.serverUrl = serverUrl;
        this.resourceUrl = '';
        this.resourceUrl = serverUrl.SERVER_API_URL + '/api/facilities';
    }
    FacilityService.prototype.create = function (facility) {
        return this.http
            .post(this.resourceUrl, facility, { observe: 'response' });
    };
    FacilityService.prototype.update = function (facility) {
        return this.http
            .put(this.resourceUrl, facility, { observe: 'response' });
    };
    FacilityService.prototype.delete = function (id) {
        return this.http.delete(this.resourceUrl + "/" + id, { observe: 'response' });
    };
    FacilityService.prototype.find = function (id) {
        return this.http.get(this.resourceUrl + "/" + id, { observe: 'response' });
    };
    FacilityService.prototype.findByLga = function (id) {
        return this.http.get(this.resourceUrl + "/lga/" + id, { observe: 'response' });
    };
    FacilityService.ctorParameters = function () { return [
        { type: HttpClient },
        { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
    ]; };
    FacilityService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function FacilityService_Factory() { return new FacilityService(i0.ɵɵinject(i1.HttpClient), i0.ɵɵinject(i2.SERVER_API_URL_CONFIG)); }, token: FacilityService, providedIn: "root" });
    FacilityService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__param(1, Inject(SERVER_API_URL_CONFIG))
    ], FacilityService);
    return FacilityService;
}());
export { FacilityService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmFjaWxpdHkuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL2ZhY2lsaXR5LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ25ELE9BQU8sRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDaEUsT0FBTyxFQUFFLHFCQUFxQixFQUFzQixNQUFNLGtCQUFrQixDQUFDOzs7O0FBVTdFO0lBR0kseUJBQW9CLElBQWdCLEVBQXlDLFNBQTZCO1FBQXRGLFNBQUksR0FBSixJQUFJLENBQVk7UUFBeUMsY0FBUyxHQUFULFNBQVMsQ0FBb0I7UUFGMUcsZ0JBQVcsR0FBRyxFQUFFLENBQUM7UUFHYixJQUFJLENBQUMsV0FBVyxHQUFHLFNBQVMsQ0FBQyxjQUFjLEdBQUcsaUJBQWlCLENBQUM7SUFDcEUsQ0FBQztJQUVELGdDQUFNLEdBQU4sVUFBTyxRQUFrQjtRQUNyQixPQUFPLElBQUksQ0FBQyxJQUFJO2FBQ1gsSUFBSSxDQUFXLElBQUksQ0FBQyxXQUFXLEVBQUUsUUFBUSxFQUFFLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxDQUFDLENBQUM7SUFDN0UsQ0FBQztJQUVELGdDQUFNLEdBQU4sVUFBTyxRQUFrQjtRQUNyQixPQUFPLElBQUksQ0FBQyxJQUFJO2FBQ1gsR0FBRyxDQUFXLElBQUksQ0FBQyxXQUFXLEVBQUUsUUFBUSxFQUFFLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxDQUFDLENBQUM7SUFDNUUsQ0FBQztJQUVELGdDQUFNLEdBQU4sVUFBTyxFQUFVO1FBQ2IsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBUyxJQUFJLENBQUMsV0FBVyxTQUFJLEVBQUksRUFBRSxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZGLENBQUM7SUFFRCw4QkFBSSxHQUFKLFVBQUssRUFBVTtRQUNYLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUksSUFBSSxDQUFDLFdBQVcsU0FBSSxFQUFJLEVBQUUsRUFBQyxPQUFPLEVBQUUsVUFBVSxFQUFDLENBQUMsQ0FBQztJQUM3RSxDQUFDO0lBRUQsbUNBQVMsR0FBVCxVQUFVLEVBQVU7UUFDaEIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBZ0IsSUFBSSxDQUFDLFdBQVcsYUFBUSxFQUFJLEVBQUUsRUFBQyxPQUFPLEVBQUUsVUFBVSxFQUFDLENBQUMsQ0FBQztJQUM3RixDQUFDOztnQkF4QnlCLFVBQVU7Z0RBQUcsTUFBTSxTQUFDLHFCQUFxQjs7O0lBSDFELGVBQWU7UUFIM0IsVUFBVSxDQUFDO1lBQ1IsVUFBVSxFQUFFLE1BQU07U0FDckIsQ0FBQztRQUl5QyxtQkFBQSxNQUFNLENBQUMscUJBQXFCLENBQUMsQ0FBQTtPQUgzRCxlQUFlLENBNEIzQjswQkF4Q0Q7Q0F3Q0MsQUE1QkQsSUE0QkM7U0E1QlksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50LCBIdHRwUmVzcG9uc2UgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IFNFUlZFUl9BUElfVVJMX0NPTkZJRywgU2VydmVyQXBpVXJsQ29uZmlnIH0gZnJvbSAnLi4vYXBwLmNvbnN0YW50cyc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgRmFjaWxpdHkgfSBmcm9tICcuLi9zaGFyZWQvbW9kZWwvZmFjaWxpdHkubW9kZWwnO1xyXG5cclxudHlwZSBFbnRpdHlSZXNwb25zZVR5cGUgPSBIdHRwUmVzcG9uc2U8RmFjaWxpdHk+O1xyXG50eXBlIEVudGl0eUFycmF5UmVzcG9uc2VUeXBlID0gSHR0cFJlc3BvbnNlPEZhY2lsaXR5W10+O1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGYWNpbGl0eVNlcnZpY2Uge1xyXG4gICAgcmVzb3VyY2VVcmwgPSAnJztcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQsIEBJbmplY3QoU0VSVkVSX0FQSV9VUkxfQ09ORklHKSBwcml2YXRlIHNlcnZlclVybDogU2VydmVyQXBpVXJsQ29uZmlnKSB7XHJcbiAgICAgICAgdGhpcy5yZXNvdXJjZVVybCA9IHNlcnZlclVybC5TRVJWRVJfQVBJX1VSTCArICcvYXBpL2ZhY2lsaXRpZXMnO1xyXG4gICAgfVxyXG5cclxuICAgIGNyZWF0ZShmYWNpbGl0eTogRmFjaWxpdHkpOiBPYnNlcnZhYmxlPEVudGl0eVJlc3BvbnNlVHlwZT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHBcclxuICAgICAgICAgICAgLnBvc3Q8RmFjaWxpdHk+KHRoaXMucmVzb3VyY2VVcmwsIGZhY2lsaXR5LCB7IG9ic2VydmU6ICdyZXNwb25zZScgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlKGZhY2lsaXR5OiBGYWNpbGl0eSk6IE9ic2VydmFibGU8RW50aXR5UmVzcG9uc2VUeXBlPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cFxyXG4gICAgICAgICAgICAucHV0PEZhY2lsaXR5Pih0aGlzLnJlc291cmNlVXJsLCBmYWNpbGl0eSwgeyBvYnNlcnZlOiAncmVzcG9uc2UnIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGRlbGV0ZShpZDogbnVtYmVyKTogT2JzZXJ2YWJsZTxIdHRwUmVzcG9uc2U8YW55Pj4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAuZGVsZXRlPGFueT4oYCR7dGhpcy5yZXNvdXJjZVVybH0vJHtpZH1gLCB7IG9ic2VydmU6ICdyZXNwb25zZScgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZmluZChpZDogbnVtYmVyKTogT2JzZXJ2YWJsZTxIdHRwUmVzcG9uc2U8RmFjaWxpdHk+PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQoYCR7dGhpcy5yZXNvdXJjZVVybH0vJHtpZH1gLCB7b2JzZXJ2ZTogJ3Jlc3BvbnNlJ30pO1xyXG4gICAgfVxyXG5cclxuICAgIGZpbmRCeUxnYShpZDogbnVtYmVyKTogT2JzZXJ2YWJsZTxFbnRpdHlBcnJheVJlc3BvbnNlVHlwZT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0PEZhY2lsaXR5W10+KGAke3RoaXMucmVzb3VyY2VVcmx9L2xnYS8ke2lkfWAsIHtvYnNlcnZlOiAncmVzcG9uc2UnfSk7XHJcbiAgICB9XHJcbn1cclxuIl19