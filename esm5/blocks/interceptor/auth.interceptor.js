import * as tslib_1 from "tslib";
import { Inject, Injectable } from '@angular/core';
import { LocalStorageService, SessionStorageService } from 'ngx-store';
import { SERVER_API_URL_CONFIG } from '../../app.constants';
var AuthInterceptor = /** @class */ (function () {
    function AuthInterceptor(localStorage, sessionStorage, serverUrl) {
        this.localStorage = localStorage;
        this.sessionStorage = sessionStorage;
        this.serverUrl = serverUrl;
    }
    AuthInterceptor.prototype.intercept = function (request, next) {
        // tslint:disable-next-line:max-line-length
        if (!request || !request.url || (/^http/.test(request.url) && !(this.serverUrl.SERVER_API_URL && request.url.startsWith(this.serverUrl.SERVER_API_URL)))) {
            return next.handle(request);
        }
        var token = this.localStorage.get('authenticationToken') || this.sessionStorage.get('authenticationToken');
        if (!!token) {
            request = request.clone({
                setHeaders: {
                    Authorization: 'Bearer ' + token
                }
            });
        }
        return next.handle(request);
    };
    AuthInterceptor.ctorParameters = function () { return [
        { type: LocalStorageService },
        { type: SessionStorageService },
        { type: undefined, decorators: [{ type: Inject, args: [SERVER_API_URL_CONFIG,] }] }
    ]; };
    AuthInterceptor = tslib_1.__decorate([
        Injectable(),
        tslib_1.__param(2, Inject(SERVER_API_URL_CONFIG))
    ], AuthInterceptor);
    return AuthInterceptor;
}());
export { AuthInterceptor };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC5pbnRlcmNlcHRvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BsYW1pcy93ZWItY29yZS8iLCJzb3VyY2VzIjpbImJsb2Nrcy9pbnRlcmNlcHRvci9hdXRoLmludGVyY2VwdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFDQSxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNuRCxPQUFPLEVBQUUsbUJBQW1CLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxXQUFXLENBQUM7QUFFdkUsT0FBTyxFQUFFLHFCQUFxQixFQUFzQixNQUFNLHFCQUFxQixDQUFDO0FBSWhGO0lBRUkseUJBQW9CLFlBQWlDLEVBQVUsY0FBcUMsRUFDakQsU0FBNkI7UUFENUQsaUJBQVksR0FBWixZQUFZLENBQXFCO1FBQVUsbUJBQWMsR0FBZCxjQUFjLENBQXVCO1FBQ2pELGNBQVMsR0FBVCxTQUFTLENBQW9CO0lBQ2hGLENBQUM7SUFFRCxtQ0FBUyxHQUFULFVBQVUsT0FBeUIsRUFBRSxJQUFpQjtRQUNsRCwyQ0FBMkM7UUFDM0MsSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLEVBQUU7WUFDdEosT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQy9CO1FBQ0QsSUFBTSxLQUFLLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1FBQzdHLElBQUksQ0FBQyxDQUFDLEtBQUssRUFBRTtZQUNULE9BQU8sR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDO2dCQUNwQixVQUFVLEVBQUU7b0JBQ1IsYUFBYSxFQUFFLFNBQVMsR0FBRyxLQUFLO2lCQUNuQzthQUNKLENBQUMsQ0FBQztTQUNOO1FBQ0QsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ2hDLENBQUM7O2dCQWxCaUMsbUJBQW1CO2dCQUEwQixxQkFBcUI7Z0RBQ3ZGLE1BQU0sU0FBQyxxQkFBcUI7O0lBSGhDLGVBQWU7UUFEM0IsVUFBVSxFQUFFO1FBSUksbUJBQUEsTUFBTSxDQUFDLHFCQUFxQixDQUFDLENBQUE7T0FIakMsZUFBZSxDQXFCM0I7SUFBRCxzQkFBQztDQUFBLEFBckJELElBcUJDO1NBckJZLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBIdHRwRXZlbnQsIEh0dHBIYW5kbGVyLCBIdHRwSW50ZXJjZXB0b3IsIEh0dHBSZXF1ZXN0IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBJbmplY3QsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTG9jYWxTdG9yYWdlU2VydmljZSwgU2Vzc2lvblN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnbmd4LXN0b3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBTRVJWRVJfQVBJX1VSTF9DT05GSUcsIFNlcnZlckFwaVVybENvbmZpZyB9IGZyb20gJy4uLy4uL2FwcC5jb25zdGFudHMnO1xyXG5cclxuXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIEF1dGhJbnRlcmNlcHRvciBpbXBsZW1lbnRzIEh0dHBJbnRlcmNlcHRvciB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBsb2NhbFN0b3JhZ2U6IExvY2FsU3RvcmFnZVNlcnZpY2UsIHByaXZhdGUgc2Vzc2lvblN0b3JhZ2U6IFNlc3Npb25TdG9yYWdlU2VydmljZSxcclxuICAgICAgICAgICAgICAgIEBJbmplY3QoU0VSVkVSX0FQSV9VUkxfQ09ORklHKSBwcml2YXRlIHNlcnZlclVybDogU2VydmVyQXBpVXJsQ29uZmlnKSB7XHJcbiAgICB9XHJcblxyXG4gICAgaW50ZXJjZXB0KHJlcXVlc3Q6IEh0dHBSZXF1ZXN0PGFueT4sIG5leHQ6IEh0dHBIYW5kbGVyKTogT2JzZXJ2YWJsZTxIdHRwRXZlbnQ8YW55Pj4ge1xyXG4gICAgICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTptYXgtbGluZS1sZW5ndGhcclxuICAgICAgICBpZiAoIXJlcXVlc3QgfHwgIXJlcXVlc3QudXJsIHx8ICgvXmh0dHAvLnRlc3QocmVxdWVzdC51cmwpICYmICEodGhpcy5zZXJ2ZXJVcmwuU0VSVkVSX0FQSV9VUkwgJiYgcmVxdWVzdC51cmwuc3RhcnRzV2l0aCh0aGlzLnNlcnZlclVybC5TRVJWRVJfQVBJX1VSTCkpKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gbmV4dC5oYW5kbGUocmVxdWVzdCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IHRva2VuID0gdGhpcy5sb2NhbFN0b3JhZ2UuZ2V0KCdhdXRoZW50aWNhdGlvblRva2VuJykgfHwgdGhpcy5zZXNzaW9uU3RvcmFnZS5nZXQoJ2F1dGhlbnRpY2F0aW9uVG9rZW4nKTtcclxuICAgICAgICBpZiAoISF0b2tlbikge1xyXG4gICAgICAgICAgICByZXF1ZXN0ID0gcmVxdWVzdC5jbG9uZSh7XHJcbiAgICAgICAgICAgICAgICBzZXRIZWFkZXJzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgQXV0aG9yaXphdGlvbjogJ0JlYXJlciAnICsgdG9rZW5cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBuZXh0LmhhbmRsZShyZXF1ZXN0KTtcclxuICAgIH1cclxufVxyXG4iXX0=