var MomentDateFormat = /** @class */ (function () {
    function MomentDateFormat() {
    }
    MomentDateFormat.DATE_FORMAT = 'DD-MM-YYYY';
    MomentDateFormat.DATE_TIME_FORMAT = 'DD-MM-YYYY ';
    return MomentDateFormat;
}());
export { MomentDateFormat };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9tZW50LWRhdGUtZm9ybWF0LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGxhbWlzL3dlYi1jb3JlLyIsInNvdXJjZXMiOlsibW9tZW50LWRhdGUtZm9ybWF0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQUE7SUFHQSxDQUFDO0lBRlUsNEJBQVcsR0FBVyxZQUFZLENBQUM7SUFDbkMsaUNBQWdCLEdBQVcsYUFBYSxDQUFDO0lBQ3BELHVCQUFDO0NBQUEsQUFIRCxJQUdDO1NBSFksZ0JBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiZXhwb3J0IGNsYXNzIE1vbWVudERhdGVGb3JtYXQge1xyXG4gICAgc3RhdGljIERBVEVfRk9STUFUOiBzdHJpbmcgPSAnREQtTU0tWVlZWSc7XHJcbiAgICBzdGF0aWMgREFURV9USU1FX0ZPUk1BVDogc3RyaW5nID0gJ0RELU1NLVlZWVkgJztcclxufVxyXG4iXX0=