import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ServerApiUrlConfig } from '../../app.constants';
import { Account } from '../user/account.model';
export declare class AccountService {
    private http;
    private apiUrlConfig;
    private userIdentity;
    private authenticated;
    private authenticationState;
    constructor(http: HttpClient, apiUrlConfig: ServerApiUrlConfig);
    fetch(): Observable<HttpResponse<Account>>;
    save(account: any): Observable<HttpResponse<any>>;
    authenticate(identity: any): void;
    hasAnyAuthority(authorities: string[]): boolean;
    hasAuthority(authority: string): Promise<boolean>;
    identity(force?: boolean): Promise<any>;
    isAuthenticated(): boolean;
    isIdentityResolved(): boolean;
    getAuthenticationState(): Observable<any>;
    getImageUrl(): string;
}
