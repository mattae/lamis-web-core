import { Injector } from '@angular/core';
export declare class StateStorageService {
    private injector;
    private $sessionStorage;
    constructor(injector: Injector);
    getPreviousState(): any;
    resetPreviousState(): void;
    storePreviousState(previousStateName: any, previousStateParams: any): void;
    getDestinationState(): any;
    storeUrl(url: string): void;
    getUrl(): any;
    storeDestinationState(destinationState: any, destinationStateParams: any, fromState: any): void;
}
