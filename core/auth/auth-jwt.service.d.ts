import { Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ServerApiUrlConfig } from '../../app.constants';
export declare class AuthServerProvider {
    private http;
    private injector;
    private serverUrl;
    private $localStorage;
    private $sessionStorage;
    constructor(http: HttpClient, injector: Injector, serverUrl: ServerApiUrlConfig);
    getToken(): any;
    getAuthorizationToken(): string;
    login(credentials: any): Observable<any>;
    logout(): Observable<any>;
    private authenticateSuccess;
}
