import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { LocalStorageService, SessionStorageService } from 'ngx-store';
import { Observable } from 'rxjs';
import { ServerApiUrlConfig } from '../../app.constants';
export declare class AuthInterceptor implements HttpInterceptor {
    private localStorage;
    private sessionStorage;
    private serverUrl;
    constructor(localStorage: LocalStorageService, sessionStorage: SessionStorageService, serverUrl: ServerApiUrlConfig);
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
}
