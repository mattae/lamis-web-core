import { JhiEventManager } from 'ng-jhipster';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
export declare class ErrorHandlerInterceptor implements HttpInterceptor {
    private eventManager;
    constructor(eventManager: JhiEventManager);
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
}
