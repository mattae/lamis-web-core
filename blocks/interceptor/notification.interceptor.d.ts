import { JhiAlertService } from 'ng-jhipster';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
export declare class NotificationInterceptor implements HttpInterceptor {
    private alertService;
    constructor(alertService: JhiAlertService);
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>;
}
